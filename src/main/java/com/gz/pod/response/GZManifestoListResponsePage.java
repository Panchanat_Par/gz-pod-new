package com.gz.pod.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class GZManifestoListResponsePage {
    private String title;
    private HttpStatus code;
    private String message;
    private String developerMessage;
    private List<?> data;
    private Integer totalItems;
    private Integer pageSize;
    private Integer currentPage;
    private Integer totalPage;
    private Integer countPodNumber;
    private Integer countSo;
    private Integer countLog;
    private Integer countInvoice;
    private Integer countApprovalSo;
    private Integer countApprovalLog;
    private Integer countNotApprovalSo;
    private Integer countNotApprovalLog;
    private Boolean checkerConfirmStatus = false;
    private Boolean checkHiddenAlert = null;
    private Object podStartList;
    private Object podNumberStart;
    private Boolean podNumberStatusStart;

    public GZManifestoListResponsePage(String title, HttpStatus code, String message, String developerMessage) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
    }

    public GZManifestoListResponsePage(String title, HttpStatus code, String message, String developerMessage, Boolean checkHiddenAlert) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
        this.checkHiddenAlert = checkHiddenAlert;
    }

    public GZManifestoListResponsePage() {
    }
}
