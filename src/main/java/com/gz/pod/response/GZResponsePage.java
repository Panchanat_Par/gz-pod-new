package com.gz.pod.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GZResponsePage {
    private String title;
    private HttpStatus code;
    private String message;
    private String developerMessage;
    private List<?> data;
    private Integer totalItems;
    private Integer totalApproval;
    private Integer totalWaitingApproval;

    private Integer pageSize;
    private Integer currentPage;
    private Integer totalPage;

    public GZResponsePage(String title, HttpStatus code, String message, String developerMessage) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
    }

    public GZResponsePage() {
    }
}
