package com.gz.pod.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GZResponse<T> {
    @Setter private String title;
    private HttpStatus code;
    private String message;
    private String developerMessage;
    private T data;

    public GZResponse(String title, HttpStatus code, String message, String developerMessage) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
    }

    public GZResponse() {
    }
}
