package com.gz.pod.response;

import lombok.Data;

import java.util.List;

@Data
public class ErrorResponse {
    private List<Errors> errors;

    public ErrorResponse(List<Errors> errors) {
        this.errors = errors;
    }
}

