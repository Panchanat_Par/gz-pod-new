package com.gz.pod.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class GZErrorResponse {
    @ApiModelProperty(value = "400")
    private String title;
    private HttpStatus code;
    @ApiModelProperty(value = "error")
    private String message;
    @ApiModelProperty(value = "developer message")
    private String developerMessage;

    public GZErrorResponse(String title, HttpStatus code, String message, String developerMessage) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
    }
}
