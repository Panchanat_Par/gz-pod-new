package com.gz.pod.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gz.pod.entities.Config;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigResponse {

    private Config config;

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
