//package com.gz.pod.response;
//
//import com.gz.pod.categories.shippedlist.driverconfirm.responses.UploadFilesResponse;
//import lombok.Data;
//import org.springframework.http.HttpStatus;
//
//@Data
//public class UploadFileResponse {
//    private String title;
//    private HttpStatus code;
//    private String message;
//    private String developerMessage;
//    private UploadFilesResponse data;
//
//    public UploadFileResponse(String title, HttpStatus code, String message, UploadFilesResponse data,
//                              String developerMessage) {
//        this.title = title;
//        this.code = code;
//        this.message = message;
//        this.data = data;
//        this.developerMessage = developerMessage;
//    }
//
//    public UploadFileResponse() {
//
//    }
//}
