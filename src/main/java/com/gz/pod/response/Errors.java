package com.gz.pod.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class Errors {
    private HttpStatus code;
    private String fieldName;
    private String message;

    public Errors(HttpStatus code,String fieldName, String message) {
        this.code = code;
        this.fieldName = fieldName;
        this.message = message;
    }
}
