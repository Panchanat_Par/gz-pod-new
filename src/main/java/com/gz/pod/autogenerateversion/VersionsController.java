package com.gz.pod.autogenerateversion;

import com.gz.pod.exceptions.GZException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("versions.json")
public class VersionsController {

    @Autowired
    VersionsService versionsService;

    @GetMapping
    public ResponseEntity getConfig() throws GZException {
        return ResponseEntity.ok(versionsService.getVersions());
    }

}
