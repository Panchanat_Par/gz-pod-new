package com.gz.pod.autogenerateversion;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.constants.Constant;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class VersionsService {

    public static final String VERSIONS_LOCATION =
            "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\" +
                    "webapps\\api-soms-0.0.1-SNAPSHOT\\WEB-INF\\classes\\";
    public Object getVersions() throws GZException {
        Object config;

        ObjectMapper mapper = new ObjectMapper();
        try {
            config = mapper.readValue(new File(VERSIONS_LOCATION + "versions.json"), Object.class);
        } catch (Exception e) {
            throw new GZException(Constant.CONFIG, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
            );
        }
        return config;
    }
}
