package com.gz.pod.constants;

import java.io.File;

public class Constant {

    public static final String CONFIG = "CONFIG";
    public static final String CONFIG_LOCATION = System.getProperty("user.home")
            + File.separator + "Config" + File.separator;

    private Constant() {
        throw new UnsupportedOperationException();
    }
}
