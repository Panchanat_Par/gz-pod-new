package com.gz.pod.constants;

public enum LogConstants {
    no_assign_to_driver(1),
    request_confirm_SO(2),
    request_cancel_SO(3),
    checker_confirm(4),
    checker_cancel(5),
    start_delivery(6),
    cancel_delivery(7),
    arrive_delivery(8),
    customer_delivered(9),
    customer_partial_delivered(10),
    customer_reject(11),
    finop_receive(12),
    request_confirm_item(13),
    request_cancel_item(14),
    request_confirm_partial_item(15);
    private final int value;

    LogConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
