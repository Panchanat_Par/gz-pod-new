package com.gz.pod.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class GZException extends Exception {
    private final String title;
    private final HttpStatus code;
    private final String message;
    private final String developerMessage;

    public GZException(String title, HttpStatus code, String message, String developerMessage) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
    }
}
