package com.gz.pod.advices;

import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.ErrorResponse;
import com.gz.pod.response.Errors;
import com.gz.pod.response.GZErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.stream.Collectors;


@ControllerAdvice
public class GZExceptionHandler {

    @ExceptionHandler(BindException.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> bindExceptionHandler(BindException ex) {
        ErrorResponse res = new ErrorResponse(
                ex.getBindingResult().getFieldErrors()
                        .stream()
                        .map(err -> new Errors(HttpStatus.BAD_REQUEST,
                                err.getField(),
                                err.getDefaultMessage()))
                        .collect(Collectors.toList())
        );

        return ResponseEntity.badRequest().body(res);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex) {
        ErrorResponse res = new ErrorResponse(
                ex.getBindingResult().getFieldErrors()
                        .stream()
                        .map(err -> new Errors(HttpStatus.BAD_REQUEST,
                                err.getField(),
                                err.getDefaultMessage()))
                        .collect(Collectors.toList())
        );

        return ResponseEntity.badRequest().body(res);
    }

    @ExceptionHandler(GZException.class)
    @ResponseBody
    public ResponseEntity<GZErrorResponse> gzExceptionHandler(GZException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GZErrorResponse(ex.getTitle()
                , ex.getCode()
                , ex.getMessage()
                , ex.getDeveloperMessage()));
    }
}
