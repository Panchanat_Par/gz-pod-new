package com.gz.pod.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {

        final ArrayList<ResponseMessage> responseMessageList = new ArrayList<>();


        ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();
        responseMessageBuilder.code(400);
        responseMessageBuilder.message("400");
        responseMessageBuilder.responseModel(new ModelRef("GZErrorResponse"));
        ResponseMessage error400 = responseMessageBuilder.build();

        responseMessageList.add(error400);

        responseMessageBuilder = new ResponseMessageBuilder();
        responseMessageBuilder.code(500);
        responseMessageBuilder.message("500");
        responseMessageBuilder.responseModel(new ModelRef("Errors"));
        ResponseMessage error500 = responseMessageBuilder.build();

        responseMessageList.add(error500);


        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.gz.pod.categories"))
                .paths(PathSelectors.any())
                .build()
                .globalResponseMessage(RequestMethod.GET ,responseMessageList)
                .globalResponseMessage(RequestMethod.POST ,responseMessageList)
                .apiInfo(metaData())
                .useDefaultResponseMessages(true);
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder() 
                .title("POD-API")
                .description("\"Spring Boot REST API for POD service\"")
                .version("1.0.0")
                .contact(new Contact("LYNX CTA", "", "panchanat.par@pantavanij.com"))
                .build();
    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("gz-pod/swagger-ui.html**")
//                .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
//        registry.addResourceHandler("/gz-pod/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }
}
