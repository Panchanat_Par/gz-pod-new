package com.gz.pod.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.constants.Constant;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class CreateConfig {
    private static final Logger LOGGER = Logger.getLogger(CreateConfig.class.getName());

    public void createConfig() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            File dirConfig = new File(Constant.CONFIG_LOCATION);
            dirConfig.mkdir();
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("openQrCode", "true");
            mapper.writeValue(new File(Constant.CONFIG_LOCATION + "config.json"), jsonMap);
        } catch (IOException e) {
            LOGGER.warning("context" + e);
        }
    }
}
