package com.gz.pod.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

@Configuration
@ComponentScan("com.starter.api")
@EnableRetry
public class TokenConfig {

}
