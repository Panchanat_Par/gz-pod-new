package com.gz.pod.configs;

import org.springframework.context.annotation.Configuration;

import java.io.*;
@Configuration
public class SettingConfig {

    public static  String getValueProperties(String key) throws IOException {
        final int lhs = 0;
        final int rhs = 1;
        File file = new File(".");
        String Result = "";
        BufferedReader bfr = new BufferedReader(new FileReader(new File("D:\\PodSetting\\setting.txt")));

        String line;
        try {
            while ((line = bfr.readLine()) != null) {
                if (!line.startsWith("#") && !line.isEmpty()) {
                    String[] pair = line.trim().split("=>");
                    if (pair[lhs].trim().equals(key)) {
                        Result = pair[rhs].trim();
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new FileNotFoundException("Exception file '" + e.getMessage());
        }
        finally {
            bfr.close();
        }

        return Result;
    }
}