package com.gz.pod.models

data class GitInfo(var commitId:String, var gitBranch:String, var gitMessage:String)