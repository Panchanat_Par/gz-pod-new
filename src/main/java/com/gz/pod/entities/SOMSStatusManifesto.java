//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_Status_Log")
//public class SOMSStatusManifesto implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "statusId")
//    private int id;
//    @Column(name = "statusName")
//    private String statusName;
//    @Column(name = "typeUser")
//    private String typeUser;
//
//    public SOMSStatusManifesto() {}
//
//    public SOMSStatusManifesto(String statusName, String typeUser) {
//        this.statusName = statusName;
//        this.typeUser = typeUser;
//    }
//}
