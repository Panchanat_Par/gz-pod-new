package com.gz.pod.entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "pod.jobStatus")
public class JobStatus implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "jobId")
    private int jobId;
    @Column(name = "statusId")
    private int statusId;
    @Column(name = "remark")
    private String remark;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdDateUTC")
    private Timestamp createdDateUTC;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "statusId", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private MasterStatus masterStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdBy", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private UserTypeMaster userTypeMaster;

    public JobStatus(int jobId, int statusId, String remark, Integer createdBy, Timestamp createdDateUTC) {
        this.jobId = jobId;
        this.statusId = statusId;
        this.remark = remark;
        this.createdBy = createdBy;
        this.createdDateUTC = createdDateUTC;
    }

    public JobStatus() {
    }


}
