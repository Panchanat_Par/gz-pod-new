package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "GenericAttribute")
public class GenericAttribute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "EntityId")
    private int entityId;
    @Column(name = "KeyGroup")
    private String keyGroup;
    @Column(name = "[Key]")
    private String key;
    @Column(name = "Value")
    private String value;
    @Column(name = "StoreId")
    private int storeId;

    public GenericAttribute(int entityId, String keyGroup, String key, String value, int storeId) {
        this.entityId = entityId;
        this.keyGroup = keyGroup;
        this.key = key;
        this.value = value;
        this.storeId = storeId;
    }

    public GenericAttribute() {

    }
}