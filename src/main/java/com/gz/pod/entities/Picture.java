//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.util.ArrayList;
//import java.util.List;
//
//@Data
//@Entity
//@Table(name = "Product_Picture_Mapping")
//public class Picture {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "Id")
//    private int pictureId;
//    @Column(name = "PictureBinary")
//    private String pictureBinary;
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "PictureId")
//    private List<ProductPictureMapping> productPictureMappings = new ArrayList<>();
//
//    public Picture() {
//    }
//
//    public Picture(String pictureBinary, List<ProductPictureMapping> productPictureMappings) {
//        this.pictureBinary = pictureBinary;
//        this.productPictureMappings = productPictureMappings;
//    }
//
//}
