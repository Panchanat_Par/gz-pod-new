package com.gz.pod.entities;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pod.masterReasonRejectManifesto")
public class Reject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "typeReject")
    private int typeReject;

    public Reject(int id, String description, int typeReject) {
        this.id = id;
        this.description = description;
        this.typeReject = typeReject;
    }

    public Reject() {

    }
}
