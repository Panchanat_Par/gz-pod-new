//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//@Data
//@Entity
//@Table(name = "Product_Picture_Mapping")
//public class ProductPictureMapping implements Serializable {
//
//    private static final long serialVersionUID = 1905122041950251207L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "Id")
//    private int idMapPicture;
//    @Column(name = "ProductId")
//    private String productId;
//    @Column(name = "pictureId")
//    private String pictureId;
//    @Column(name = "DisplayOrder")
//    private int displayOrder;
//
//    public ProductPictureMapping() {
//    }
//
//    public ProductPictureMapping(String productId, String pictureId, int displayOrder) {
//        this.productId = productId;
//        this.pictureId = pictureId;
//        this.displayOrder = displayOrder;
//    }
//
//}
