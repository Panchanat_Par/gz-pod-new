//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_ProductPrice")
//public class SOMSProductPrice {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "SAPCustCode")
//    private String sapCustCode;
//    @Column(name = "TierType")
//    private int tierType;
//    @Column(name = "ProductCode")
//    private String productCode;
//    @Column(name = "SellingPrice")
//    private BigDecimal sellingPrice;
//    @Column(name = "StartDate")
//    private Timestamp startDate;
//    @Column(name = "EndDate")
//    private Timestamp endDate;
//    @Column(name = "Actived")
//    private int actived;
//    @Column(name = "CustomerId")
//    private int customerId;
//    @Column(name = "LastRequestDate")
//    private Timestamp lastRequestDate;
//    @Column(name = "EmpBookingId")
//    private int empBookingId;
//    @Column(name = "materialCode")
//    private String materialCode;
//
//    @SuppressWarnings("squid:S00107")
//    public SOMSProductPrice(String sapCustCode, int tierType, String productCode, BigDecimal sellingPrice,
//                            Timestamp startDate, Timestamp endDate, int actived, int customerId,
//                            Timestamp lastRequestDate, int empBookingId, String materialCode) {
//        this.sapCustCode = sapCustCode;
//        this.tierType = tierType;
//        this.productCode = productCode;
//        this.sellingPrice = sellingPrice;
//        this.startDate = startDate;
//        this.endDate = endDate;
//        this.actived = actived;
//        this.customerId = customerId;
//        this.lastRequestDate = lastRequestDate;
//        this.empBookingId = empBookingId;
//        this.materialCode = materialCode;
//    }
//
//    public SOMSProductPrice() {
//    }
//}
