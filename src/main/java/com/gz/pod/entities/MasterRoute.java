//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//@Data
//@Entity
//@Table(name = "Tbl_Master_Route")
//public class MasterRoute implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "RouteMaster")
//    private String routeMaster;
//    @Column(name = "RouteName")
//    private String routeName;
//    @Column(name = "AcceptBilling")
//    private int acceptBilling;
//
//    public MasterRoute(String routeMaster, String routeName, int acceptBilling) {
//        this.routeMaster = routeMaster;
//        this.routeName = routeName;
//        this.acceptBilling = acceptBilling;
//    }
//
//    public MasterRoute(){
//
//    }
//}
