package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pod.userTypeMaster")
public class UserTypeMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "userTypeId")
    private int userTypeId;
    @Column(name = "loginId")
    private int loginId;
}
