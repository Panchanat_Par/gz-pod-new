//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.sql.Timestamp;
//import java.util.Objects;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_Confirm_Shipping_List")
//public class ConfirmShippingList {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "ManifestoId")
//    private int manifestoId;
//    @Column(name = "IsConfirm")
//    private int isConfirm;
//    @Column(name = "Remark")
//    private String remark;
//    @Column(name = "ReasonId")
//    private int reasonId;
//    @Column(name = "ActionBy")
//    private int actionBy;
//    @Column(name = "ConfirmDate")
//    private String confirmDate;
//    @Column(name = "IsConfirmByManager")
//    private Integer isConfirmByManager;
//    @Column(name = "RemarkByManager")
//    private String remarkByManager;
//    @Column(name = "ReasonIdByManager")
//    private Integer reasonIdByManager;
//    @Column(name = "UpdateDateByManager")
//    private Timestamp updateDateByManager;
//
//    @OneToOne
//    @JoinColumn(name = "manifestoId", referencedColumnName = "id", insertable = false, updatable = false)
//    private Manifesto manifesto;
//
//    public ConfirmShippingList(int manifestoId, int isConfirm, String remark,
//                               int reasonId, int actionBy, String confirmDate,
//                               Integer isConfirmByManager, String remarkByManager,
//                               Integer reasonIdByManager, Timestamp updateDateByManager) {
//        this.manifestoId = manifestoId;
//        this.isConfirm = isConfirm;
//        this.remark = remark;
//        this.reasonId = reasonId;
//        this.actionBy = actionBy;
//        this.confirmDate = confirmDate;
//        this.isConfirmByManager = isConfirmByManager;
//        this.remarkByManager = remarkByManager;
//        this.reasonIdByManager = reasonIdByManager;
//        this.updateDateByManager = updateDateByManager;
//    }
//
//    public ConfirmShippingList() {
//    }
//
//    public void setManifestoId(int manifestoId) {
//        this.manifestoId = manifestoId;
//    }
//
//    public void setIsConfirm(int isConfirm) {
//        this.isConfirm = isConfirm;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = Objects.toString(remark, "");
//    }
//
//    public void setReasonId(int reasonId) {
//        this.reasonId = reasonId;
//    }
//
//    public void setActionBy(int actionBy) {
//        this.actionBy = actionBy;
//    }
//
//    public void setConfirmDate(String confirmDate) {
//        this.confirmDate = Objects.toString(confirmDate, "");
//    }
//
//    public void setIsConfirmByManager(Integer isConfirmByManager) {
//        this.isConfirmByManager = isConfirmByManager;
//    }
//
//    public void setRemarkByManager(String remarkByManager) {
//        this.remarkByManager = Objects.toString(remarkByManager, "");
//    }
//
//    public void setReasonIdByManager(Integer reasonIdByManager) {
//        this.reasonIdByManager = reasonIdByManager;
//    }
//
//    public void setUpdateDateByManager(Timestamp updateDateByManager) {
//        this.updateDateByManager = updateDateByManager;
//    }
//
//    public void setManifesto(Manifesto manifesto) {
//        this.manifesto = manifesto;
//    }
//}
