//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//
//@Data
//@Entity
//@Table(name="Tbl_Soms_LineNotify")
//public class LineNotify {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int id;
//    private String name;
//    private String token;
//    private String types;
//
//    public LineNotify() {
//    }
//
//    public LineNotify(String name, String token, String types) {
//        this.name = name;
//        this.token = token;
//        this.types = types;
//    }
//}
