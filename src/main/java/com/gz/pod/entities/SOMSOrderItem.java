package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "Tbl_SOMS_OrderItem")
public class SOMSOrderItem implements Serializable {

    private static final long serialVersionUID = 1905122041950251207L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "OrderItemGuid", columnDefinition = "uniqueidentifier")
    private String orderItemGuid;
    @Column(name = "OrderId")
    private int orderId;
    @Column(name = "ProductCode")
    private String productCode;
    @Column(name = "Quantity")
    private int quantity;
    @Column(name = "UnitPriceInclTax")
    private BigDecimal unitPriceInclTax;
    @Column(name = "UnitPriceExclTax")
    private BigDecimal unitPriceExclTax;
    @Column(name = "PriceInclTax")
    private BigDecimal priceInclTax;
    @Column(name = "PriceExclTax")
    private BigDecimal priceExclTax;
    @Column(name = "DiscountAmountInclTax")
    private BigDecimal discountAmountInclTax;
    @Column(name = "DiscountAmountExclTax")
    private BigDecimal discountAmountExclTax;
    @Column(name = "OriginalProductCost")
    private BigDecimal originalProductCost;
    @Column(name = "AttributeDescription")
    private String attributeDescription;
    @Column(name = "AttributesXml")
    private String attributesXml;
    @Column(name = "DownloadCount")
    private int downloadCount;
    @Column(name = "IsDownloadActivated")
    private int isDownloadActivated;
    @Column(name = "LicenseDownloadId")
    private int licenseDownloadId;
    @Column(name = "ItemWeight")
    private BigDecimal itemWeight;
    @Column(name = "RentalStartDateUtc")
    private Timestamp rentalStartDateUtc;
    @Column(name = "RentalEndDateUtc")
    private Timestamp rentalEndDateUtc;
    @Column(name = "ProductID")
    private int productID;
    @Column(name = "DeliveryDate")
    private Timestamp deliveryDate;
    @Column(name = "IsFreeItem")
    private int isFreeItem;
    @Column(name = "Sortable")
    private int sortable;
    @Column(name = "FMSSKU")
    private String fmssku;
    @Column(name = "ShipToCode")
    private String shipToCode;
    @Column(name = "IsEdit")
    private int isEdit;
    @Column(name = "DiscountId")
    private int discountId;
    @Column(name = "CanSaleTypeX")
    private int canSaleTypeX;
    @Column(name = "IsApproveSurge")
    private int isApproveSurge;

}
