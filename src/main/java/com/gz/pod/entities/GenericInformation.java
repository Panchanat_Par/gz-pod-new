package com.gz.pod.entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "pod.genericInformation")
public class GenericInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "refModule")
    private String refModule;
    @Column(name = "refAction")
    private String refAction;
    @Column(name = "refId")
    private int refId;
    @Column(name = "value")
    private String value;
    @Column(name = "createdBy")
    private int createdBy;
    @Column(name = "createdDate")
    private Timestamp createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdBy", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private UserTypeMaster userTypeMaster;

    public GenericInformation(String refModule, String refAction, int refId, String value, int createdBy, Timestamp createdDate) {
        this.refModule = refModule;
        this.refAction = refAction;
        this.refId = refId;
        this.value = value;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public GenericInformation() {
    }
}
