package com.gz.pod.entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "pod.manifestoFile")
public class ManifestoFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;

    @Column(name = "manifestoId")
    private int manifestoId;

    @Column(name = "fileByte")
    private byte[] fileByte;

    @Column(name = "createBy")
    private int createBy;

    @Column(name = "createDate")
    private Timestamp createDate;


    public ManifestoFile() {
    }

    public ManifestoFile(int id, int manifestoId, int createBy, Timestamp createDate) {
        this.id = id;
        this.manifestoId = manifestoId;
        this.createBy = createBy;
        this.createDate = createDate;
    }

    public ManifestoFile(int manifestoId, byte[] fileByte, int createBy, Timestamp createDate) {
        this.manifestoId = manifestoId;
        this.fileByte = fileByte;
        this.createBy = createBy;
        this.createDate = createDate;
    }
}
