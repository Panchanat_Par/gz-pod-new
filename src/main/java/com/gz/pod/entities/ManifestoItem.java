package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "pod.manifestoItem")
public class ManifestoItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;

    @Column(name = "manifestoId")
    private Integer manifestoId;

    @Column(name = "soNumber")
    private String soNumber;

    @Column(name = "soItem")
    private String soItem;

    @Column(name = "matDoc")
    private String matDoc;

    @Column(name = "docDate")
    private String docDate;

    @Column(name = "postDate")
    private String postDate;

    @Column(name = "productCode")
    private String productCode;

    @Column(name = "materialCode")
    private String materialCode;

    @Column(name = "productName")
    private String productName;

    @Column(name = "uom")
    private String uom;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "newQuantity")
    private Integer newQuantity;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "remark")
    private String remark;

    @Column(name = "roundGet")
    private Integer roundGet;

    public ManifestoItem() {
    }
    public ManifestoItem(String soNumber,String soItem,String matDoc,String docDate,String postDate,String productCode,String materialCode,
                         String productName,String uom,Integer quantity,Integer newQuantity,BigDecimal amount,String remark, int roundGet) {
        this.soNumber = soNumber;
        this.soItem = soItem;
        this.matDoc = matDoc;
        this.docDate = docDate;
        this.postDate = postDate;
        this.productCode = productCode;
        this.materialCode = materialCode;
        this.productName = productName;
        this.uom = uom;
        this.quantity = quantity;
        this.newQuantity = newQuantity;
        this.amount = amount;
        this.remark = remark;
        this.roundGet = roundGet;
    }


}
