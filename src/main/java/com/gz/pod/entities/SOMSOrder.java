package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "Tbl_SOMS_Order")
public class SOMSOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "OrderGuid", columnDefinition = "uniqueidentifier")
    private String orderGuid;
    @Column(name = "StoreId")
    private int storeId;
    @Column(name = "CustomerId")
    private int customerId;
    @Column(name = "CustomerNumber")
    private String customerNumber;
    @Column(name = "BillingAddressId")
    private int billingAddressId;
    @Column(name = "ShippingAddressId")
    private int shippingAddressId;
    @Column(name = "PickUpInStore")
    private int pickUpInStore;
    @Column(name = "OrderStatusId")
    private int orderStatusId;
    @Column(name = "ShippingStatusId")
    private int shippingStatusId;
    @Column(name = "PaymentStatusId")
    private int paymentStatusId;
    @Column(name = "PaymentMethodSystemName")
    private String paymentMethodSystemName;
    @Column(name = "CustomerCurrencyCode")
    private String customerCurrencyCode;
    @Column(name = "CurrencyRate")
    private BigDecimal currencyRate;
    @Column(name = "CustomerTaxDisplayTypeId")
    private int customerTaxDisplayTypeId;
    @Column(name = "VatNumber")
    private String vatNumber;
    @Column(name = "OrderSubtotalInclTax")
    private BigDecimal orderSubtotalInclTax;
    @Column(name = "OrderSubtotalExclTax")
    private BigDecimal orderSubtotalExclTax;
    @Column(name = "OrderSubTotalDiscountInclTax")
    private BigDecimal orderSubTotalDiscountInclTax;
    @Column(name = "OrderSubTotalDiscountExclTax")
    private BigDecimal orderSubTotalDiscountExclTax;
    @Column(name = "OrderShippingInclTax")
    private BigDecimal orderShippingInclTax;
    @Column(name = "OrderShippingExclTax")
    private BigDecimal orderShippingExclTax;
    @Column(name = "PaymentMethodAdditionalFeeInclTax")
    private BigDecimal paymentMethodAdditionalFeeInclTax;
    @Column(name = "PaymentMethodAdditionalFeeExclTax")
    private BigDecimal paymentMethodAdditionalFeeExclTax;
    @Column(name = "TaxRates")
    private String taxRates;
    @Column(name = "OrderTax")
    private BigDecimal orderTax;
    @Column(name = "OrderDiscount")
    private BigDecimal orderDiscount;
    @Column(name = "OrderTotal")
    private BigDecimal orderTotal;
    @Column(name = "RefundedAmount")
    private BigDecimal refundedAmount;
    @Column(name = "RewardPointsWereAdded")
    private int rewardPointsWereAdded;
    @Column(name = "CheckoutAttributeDescription")
    private String checkoutAttributeDescription;
    @Column(name = "CheckoutAttributesXml")
    private String checkoutAttributesXml;
    @Column(name = "CustomerLanguageId")
    private int customerLanguageId;
    @Column(name = "AffiliateId")
    private int affiliateId;
    @Column(name = "CustomerIp")
    private String customerIp;
    @Column(name = "AllowStoringCreditCardNumber")
    private int allowStoringCreditCardNumber;
    @Column(name = "CardType")
    private String cardType;
    @Column(name = "CardName")
    private String cardName;
    @Column(name = "CardNumber")
    private String cardNumber;
    @Column(name = "MaskedCreditCardNumber")
    private String maskedCreditCardNumber;
    @Column(name = "CardCvv2")
    private String cardCvv2;
    @Column(name = "CardExpirationMonth")
    private String cardExpirationMonth;
    @Column(name = "CardExpirationYear")
    private String cardExpirationYear;
    @Column(name = "AuthorizationTransactionId")
    private String authorizationTransactionId;
    @Column(name = "AuthorizationTransactionCode")
    private String authorizationTransactionCode;
    @Column(name = "AuthorizationTransactionResult")
    private String authorizationTransactionResult;
    @Column(name = "CaptureTransactionId")
    private String captureTransactionId;
    @Column(name = "CaptureTransactionResult")
    private String captureTransactionResult;
    @Column(name = "SubscriptionTransactionId")
    private String subscriptionTransactionId;
    @Column(name = "PaidDateUtc")
    private Timestamp paidDateUtc;
    @Column(name = "ShippingMethod")
    private String shippingMethod;
    @Column(name = "ShippingRateComputationMethodSystemName")
    private String shippingRateComputationMethodSystemName;
    @Column(name = "CustomValuesXml")
    private String customValuesXml;
    @Column(name = "Deleted")
    private int deleted;
    @Column(name = "CreatedBy")
    private int createdBy;
    @Column(name = "CreatedOnUtc")
    private Timestamp createdOnUtc;
    @Column(name = "UpdatedBy")
    private int updatedBy;
    @Column(name = "UpdatedOnUtc")
    private Timestamp updatedOnUtc;
    @Column(name = "OrderNumber")
    private String orderNumber;
    @Column(name = "BillingNumber")
    private String billingNumber;
    @Column(name = "PickupDate")
    private Timestamp pickupDate;
    @Column(name = "PickupTime", updatable = false, insertable = false)
    private String pickupTime;
    @Column(name = "ReturnCredit", updatable = false, insertable = false)
    private int returnCredit;
    @Column(name = "ExportStatus")
    private String exportStatus;
    @Column(name = "QuotationNumber")
    private String quotationNumber;
    @Column(name = "VoucherCode")
    private String voucherCode;
    @Column(name = "CancelRemark")
    private String cancelRemark;
    @Column(name = "TransferBankStatus")
    private String transferBankStatus;
    @Column(name = "TransferBankPictureId")
    private int transferBankPictureId;
    @Column(name = "TransferBankRemark")
    private String transferBankRemark;
    @Column(name = "PurchaseOrderNumber")
    private String purchaseOrderNumber;
    @Column(name = "GeneratedDocNumber")
    private String generatedDocNumber;
    @Column(name = "HasBillingAddress")
    private int hasBillingAddress;
    @Column(name = "PlaceOrderId")
    private int placeOrderId;
    @Column(name = "SpecialInstruction")
    private String specialInstruction;
    @Column(name = "CustomerPONumber")
    private String customerPONumber;
    @Column(name = "SODocNumber")
    private String sODocNumber;
    @Column(name = "CustomerPODate")
    private Timestamp customerPODate;
    @Column(name = "ShipToContact")
    private String shipToContact;
    @Column(name = "EmpBookingId")
    private int empBookingId;
    @Column(name = "CustomerCCEmail")
    private String customerCCEmail;
    @Column(name = "IsReceiveCCEmail")
    private int isReceiveCCEmail;
    @Column(name = "WebOrderId")
    private int webOrderId;
    @Column(name = "FIDocNumber")
    private String fIDocNumber;
    @Column(name = "IsShippingFee")
    private int isShippingFee;
    @Column(name = "EsolutionDepartmentId")
    private int esolutionDepartmentId;
    @Column(name = "EsolutionCostCenterId")
    private int esolutionCostCenterId;
    @Column(name = "HubSpotDealId")
    private int hubSpotDealId;
    @Column(name = "ReadyForFMS")
    private int readyForFMS;
    @Column(name = "ReworkRemark")
    private int reworkRemark;
    @Column(name = "UniqueCustomerPONumber")
    private String uniqueCustomerPONumber;
    @Column(name = "CustomerPOIsFullSO")
    private int customerPOIsFullSO;
    @Column(name = "IsMonthlyInvoice")
    private int isMonthlyInvoice;
    @Column(name = "OrderRef")
    private int orderRef;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "orderId")
    private List<SOMSOrderItem> orderItems = new ArrayList<>();

}
