//package com.gz.pod.entities;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//import java.util.Date;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_Confirm_Manifesto")
//public class ConfirmManifesto implements Serializable {
//
//    private static final long serialVersionUID = 2405172041950251807L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "ManifestoId")
//    private int manifestoId;
//    @Column(name = "IsConfirm")
//    private int isConfirm;
//    @Column(name = "Remark")
//    private String remark;
//    @Column(name = "ReasonId")
//    private int reasonId;
//    @Column(name = "ActionDate")
//    @JsonFormat(pattern = "YYYY-MM-dd")
//    private String actionDate;
//    @Column(name = "ActionBy")
//    private int actionBy;
//    @Column(name = "UpdateDate")
//    @JsonFormat(pattern = "YYYY-MM-dd")
//    private Date updateDate;
//    @Column(name = "UpdateBy")
//    private int updateBy;
//    @Column(name = "Longitude")
//    private BigDecimal longitude;
//    @Column(name = "Latitude")
//    private BigDecimal latitude;
//    @Column(name = "IsConfirmByManager")
//    private Integer isConfirmByManager;
//    @Column(name = "RemarkByManager")
//    private String remarkByManager;
//    @Column(name = "ReasonIdByManager")
//    private Integer reasonIdByManager;
//    @Column(name = "UpdateDateByManager")
//    private Timestamp updateDateByManager;
//
//    @OneToOne
//    @JoinColumn(name = "manifestoId", referencedColumnName = "id", insertable = false, updatable = false)
//    private Manifesto manifesto;
//
//    public ConfirmManifesto() {
//    }
//
//    public ConfirmManifesto(int manifestoId, int isConfirm, String remark, int reasonId, String actionDate,
//                            int actionBy, Date updateDate, int updateBy, BigDecimal longitude, BigDecimal latitude,
//                            Manifesto manifesto) {
//        this.manifestoId = manifestoId;
//        this.isConfirm = isConfirm;
//        this.remark = remark;
//        this.reasonId = reasonId;
//        this.actionDate = actionDate;
//        this.actionBy = actionBy;
//        this.updateDate = updateDate;
//        this.updateBy = updateBy;
//        this.longitude = longitude;
//        this.latitude = latitude;
//        this.manifesto = manifesto;
//    }
//
//    public ConfirmManifesto(int manifestoId, int isConfirmByManager, String remarkByManager,
//                            Integer reasonIdByManager, Timestamp updateDateByManager) {
//        this.manifestoId = manifestoId;
//        this.isConfirmByManager = isConfirmByManager;
//        this.remarkByManager = remarkByManager;
//        this.reasonIdByManager = reasonIdByManager;
//        this.updateDateByManager = updateDateByManager;
//    }
//}
