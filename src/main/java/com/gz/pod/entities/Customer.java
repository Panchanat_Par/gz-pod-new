//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.List;
//
//@Data
//@Entity
//@Table(name = "Customer")
//public class Customer {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "CustomerGuid", columnDefinition = "uniqueidentifier")
//    private String customerGuid;
//    @Column(name = "username")
//    private String username;
//    @Column(name = "Email")
//    private String email;
//    @Column(name = "Password")
//    private String password;
//    @Column(name = "PasswordFormatId")
//    private int passwordFormatId;
//    @Column(name = "PasswordSalt")
//    private String passwordSalt;
//    @Column(name = "adminComment")
//    private String adminComment;
//    @Column(name = "isTaxExempt")
//    private int isTaxExempt;
//    @Column(name = "AffiliateId")
//    private int affiliateId;
//    @Column(name = "VendorId")
//    private int vendorId;
//    @Column(name = "HasShoppingCartItems")
//    private int hasShoppingCartItems;
//    @Column(name = "Active")
//    private int active;
//    @Column(name = "Deleted")
//    private int deleted;
//    @Column(name = "IsSystemAccount")
//    private int isSystemAccount;
//    @Column(name = "SystemName")
//    private String systemName;
//    @Column(name = "LastIpAddress")
//    private String lastIpAddress;
//    @Column(name = "CreatedOnUtc")
//    private Timestamp createdOnUtc;
//    @Column(name = "LastLoginDateUtc")
//    private Timestamp lastLoginDateUtc;
//    @Column(name = "LastActivityDateUtc")
//    private Timestamp lastActivityDateUtc;
//    @Column(name = "BillingAddress_Id")
//    private int billingAddressId;
//    @Column(name = "ShippingAddress_Id")
//    private int shippingAddressId;
//    @Column(name = "CustomerNumber")
//    private String customerNumber;
//    @Column(name = "CustomerTypeId")
//    private int customerTypeId;
//    @Column(name = "RefQuotationNumber")
//    private String refQuotationNumber;
//    @Column(name = "CreditLimit")
//    private BigDecimal creditLimit;
//    @Column(name = "creditBalance")
//    private BigDecimal creditBalance;
//    @Column(name = "TaxNo")
//    private String taxNo;
//    @Column(name = "SOMSBillingTypeId")
//    private int somsBillingTypeId;
//    @Column(name = "IsCheckedCreditTerm")
//    private int isCheckedCreditTerm;
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
//    @JoinColumn(name = "entityId")
//    private List<GenericAttribute> attributeList = new ArrayList<>();
//
//    public Customer() {
//    }
//
//    @SuppressWarnings("squid:S00107")
//    public Customer(String customerGuid, String username, String email, String password, int passwordFormatId,
//                    String passwordSalt, String adminComment, int isTaxExempt, int affiliateId, int vendorId,
//                    int hasShoppingCartItems, int active, int deleted, int isSystemAccount, String systemName,
//                    String lastIpAddress, Timestamp createdOnUtc, Timestamp lastLoginDateUtc,
//                    Timestamp lastActivityDateUtc, int billingAddressId, int shippingAddressId, String customerNumber,
//                    int customerTypeId, String refQuotationNumber, BigDecimal creditLimit, BigDecimal creditBalance,
//                    String taxNo, int somsBillingTypeId, int isCheckedCreditTerm, List<GenericAttribute> attributeList) {
//        this.customerGuid = customerGuid;
//        this.username = username;
//        this.email = email;
//        this.password = password;
//        this.passwordFormatId = passwordFormatId;
//        this.passwordSalt = passwordSalt;
//        this.adminComment = adminComment;
//        this.isTaxExempt = isTaxExempt;
//        this.affiliateId = affiliateId;
//        this.vendorId = vendorId;
//        this.hasShoppingCartItems = hasShoppingCartItems;
//        this.active = active;
//        this.deleted = deleted;
//        this.isSystemAccount = isSystemAccount;
//        this.systemName = systemName;
//        this.lastIpAddress = lastIpAddress;
//        this.createdOnUtc = createdOnUtc;
//        this.lastLoginDateUtc = lastLoginDateUtc;
//        this.lastActivityDateUtc = lastActivityDateUtc;
//        this.billingAddressId = billingAddressId;
//        this.shippingAddressId = shippingAddressId;
//        this.customerNumber = customerNumber;
//        this.customerTypeId = customerTypeId;
//        this.refQuotationNumber = refQuotationNumber;
//        this.creditLimit = creditLimit;
//        this.creditBalance = creditBalance;
//        this.taxNo = taxNo;
//        this.somsBillingTypeId = somsBillingTypeId;
//        this.isCheckedCreditTerm = isCheckedCreditTerm;
//        this.attributeList = attributeList;
//    }
//}
