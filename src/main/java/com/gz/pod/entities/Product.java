//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//@Data
//@Entity
//@Table(name = "Product")
//public class Product implements Serializable {
//
//    private static final long serialVersionUID = 1905122041950251207L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "Id")
//    private int pId;
//    @Column(name = "Name")
//    private String name;
//    @Column(name = "ManufacturerPartNumber")
//    private String manufacturerPartNumber;
//    @Column(name = "ProductTypeId")
//    private int productTypeId;
//    @Column(name = "ParentGroupedProductId")
//    private int parentGroupedProductId;
//    @Column(name = "VisibleIndividually")
//    private int visibleIndividually;
//    @Column(name = "ShortDescription")
//    private String shortDescription;
//    @Column(name = "FullDescription")
//    private String fullDescription;
//    @Column(name = "AdminComment")
//    private String adminComment;
//    @Column(name = "ProductTemplateId")
//    private int productTemplateId;
//    @Column(name = "VendorId")
//    private int vendorId;
//    @Column(name = "ShowOnHomePage")
//    private int showOnHomePage;
//    @Column(name = "MetaKeywords")
//    private String metaKeywords;
//    @Column(name = "MetaDescription")
//    private String metaDescription;
//    @Column(name = "MetaTitle")
//    private String metaTitle;
//    @Column(name = "AllowCustomerReviews")
//    private int allowCustomerReviews;
//    @Column(name = "ApprovedRatingSum")
//    private int approvedRatingSum;
//    @Column(name = "NotApprovedRatingSum")
//    private int notApprovedRatingSum;
//    @Column(name = "ApprovedTotalReviews")
//    private int approvedTotalReviews;
//    @Column(name = "NotApprovedTotalReviews")
//    private int notApprovedTotalReviews;
//    @Column(name = "SubjectToAcl")
//    private int subjectToAcl;
//    @Column(name = "LimitedToStores")
//    private int limitedToStores;
//    @Column(name = "Sku")
//    private String sku;
//    @Column(name = "Gtin")
//    private String gtin;
//    @Column(name = "IsGiftCard")
//    private int isGiftCard;
//    @Column(name = "GiftCardTypeId")
//    private int giftCardTypeId;
//    @Column(name = "OverriddenGiftCardAmount")
//    private BigDecimal overriddenGiftCardAmount;
//    @Column(name = "RequireOtherProducts")
//    private int requireOtherProducts;
//    @Column(name = "RequiredProductIds")
//    private String requiredProductIds;
//    @Column(name = "AutomaticallyAddRequiredProducts")
//    private int automaticallyAddRequiredProducts;
//    @Column(name = "IsDownload")
//    private int isDownload;
//    @Column(name = "DownloadId")
//    private int downloadId;
//    @Column(name = "UnlimitedDownloads")
//    private int unlimitedDownloads;
//    @Column(name = "MaxNumberOfDownloads")
//    private int maxNumberOfDownloads;
//    @Column(name = "DownloadExpirationDays")
//    private Integer downloadExpirationDays;
//    @Column(name = "DownloadActivationTypeId")
//    private int downloadActivationTypeId;
//    @Column(name = "HasSampleDownload")
//    private int hasSampleDownload;
//    @Column(name = "SampleDownloadId")
//    private int sampleDownloadId;
//    @Column(name = "HasUserAgreement")
//    private int hasUserAgreement;
//    @Column(name = "UserAgreementText")
//    private String userAgreementText;
//    @Column(name = "IsRecurring")
//    private int isRecurring;
//    @Column(name = "RecurringCycleLength")
//    private int recurringCycleLength;
//    @Column(name = "RecurringCyclePeriodId")
//    private int recurringCyclePeriodId;
//    @Column(name = "RecurringTotalCycles")
//    private int recurringTotalCycles;
//    @Column(name = "IsRental")
//    private int isRental;
//    @Column(name = "RentalPriceLength")
//    private int rentalPriceLength;
//    @Column(name = "RentalPricePeriodId")
//    private int rentalPricePeriodId;
//    @Column(name = "IsShipEnabled")
//    private int isShipEnabled;
//    @Column(name = "IsFreeShipping")
//    private int isFreeShipping;
//    @Column(name = "ShipSeparately")
//    private int shipSeparately;
//    @Column(name = "AdditionalShippingCharge")
//    private BigDecimal additionalShippingCharge;
//    @Column(name = "DeliveryDateId")
//    private int deliveryDateId;
//    @Column(name = "IsTaxExempt")
//    private int isTaxExempt;
//    @Column(name = "TaxCategoryId")
//    private int taxCategoryId;
//    @Column(name = "IsTelecommunicationsOrBroadcastingOrElectronicServices")
//    private int isTelecommunicationsOrBroadcastingOrElectronicServices;
//    @Column(name = "ManageInventoryMethodId")
//    private int manageInventoryMethodId;
//    @Column(name = "UseMultipleWarehouses")
//    private int useMultipleWarehouses;
//    @Column(name = "WarehouseId")
//    private int warehouseId;
//    @Column(name = "StockQuantity")
//    private int stockQuantity;
//    @Column(name = "DisplayStockAvailability")
//    private int displayStockAvailability;
//    @Column(name = "DisplayStockQuantity")
//    private int displayStockQuantity;
//    @Column(name = "MinStockQuantity")
//    private int minStockQuantity;
//    @Column(name = "LowStockActivityId")
//    private int lowStockActivityId;
//    @Column(name = "NotifyAdminForQuantityBelow")
//    private int notifyAdminForQuantityBelow;
//    @Column(name = "BackorderModeId")
//    private int backorderModeId;
//    @Column(name = "AllowBackInStockSubscriptions")
//    private int allowBackInStockSubscriptions;
//    @Column(name = "OrderMinimumQuantity")
//    private int orderMinimumQuantity;
//    @Column(name = "OrderMaximumQuantity")
//    private int orderMaximumQuantity;
//    @Column(name = "AllowedQuantities")
//    private String allowedQuantities;
//    @Column(name = "AllowAddingOnlyExistingAttributeCombinations")
//    private int allowAddingOnlyExistingAttributeCombinations;
//    @Column(name = "NotReturnable")
//    private int notReturnable;
//    @Column(name = "DisableBuyButton")
//    private int disableBuyButton;
//    @Column(name = "DisableWishlistButton")
//    private int disableWishlistButton;
//    @Column(name = "AvailableForPreOrder")
//    private int availableForPreOrder;
//    @Column(name = "PreOrderAvailabilityStartDateTimeUtc")
//    private Date preOrderAvailabilityStartDateTimeUtc;
//    @Column(name = "CallForPrice")
//    private int callForPrice;
//    @Column(name = "Price")
//    private BigDecimal price;
//    @Column(name = "OldPrice")
//    private BigDecimal oldPrice;
//    @Column(name = "ProductCost")
//    private BigDecimal productCost;
//    @Column(name = "SpecialPrice")
//    private BigDecimal specialPrice;
//    @Column(name = "SpecialPriceStartDateTimeUtc")
//    private Timestamp specialPriceStartDateTimeUtc;
//    @Column(name = "SpecialPriceEndDateTimeUtc")
//    private Timestamp specialPriceEndDateTimeUtc;
//    @Column(name = "CustomerEntersPrice")
//    private int customerEntersPrice;
//    @Column(name = "MinimumCustomerEnteredPrice")
//    private BigDecimal minimumCustomerEnteredPrice;
//    @Column(name = "MaximumCustomerEnteredPrice")
//    private BigDecimal maximumCustomerEnteredPrice;
//    @Column(name = "BasepriceEnabled")
//    private int basepriceEnabled;
//    @Column(name = "BasepriceAmount")
//    private BigDecimal basepriceAmount;
//    @Column(name = "BasepriceUnitId")
//    private int basepriceUnitId;
//    @Column(name = "BasepriceBaseAmount")
//    private BigDecimal basepriceBaseAmount;
//    @Column(name = "BasepriceBaseUnitId")
//    private int basepriceBaseUnitId;
//    @Column(name = "MarkAsNew")
//    private int markAsNew;
//    @Column(name = "MarkAsNewStartDateTimeUtc")
//    private Timestamp markAsNewStartDateTimeUtc;
//    @Column(name = "MarkAsNewEndDateTimeUtc")
//    private Timestamp markAsNewEndDateTimeUtc;
//    @Column(name = "HasTierPrices")
//    private int hasTierPrices;
//    @Column(name = "HasDiscountsApplied")
//    private int hasDiscountsApplied;
//    @Column(name = "Weight")
//    private BigDecimal weight;
//    @Column(name = "Length")
//    private BigDecimal length;
//    @Column(name = "Width")
//    private BigDecimal width;
//    @Column(name = "Height")
//    private BigDecimal height;
//    @Column(name = "AvailableStartDateTimeUtc")
//    private Timestamp availableStartDateTimeUtc;
//    @Column(name = "AvailableEndDateTimeUtc")
//    private Timestamp availableEndDateTimeUtc;
//    @Column(name = "DisplayOrder")
//    private int displayOrder;
//    @Column(name = "Published")
//    private int published;
//    @Column(name = "Deleted")
//    private int deleted;
//    @Column(name = "CreatedOnUtc")
//    private Timestamp createdOnUtc;
//    @Column(name = "UpdatedOnUtc")
//    private Timestamp updatedOnUtc;
//
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "ProductId")
//    private List<ProductPictureMapping> productPictureMappings = new ArrayList<>();
//
//    public Product() {
//    }
//
//    public Product(String name, String manufacturerPartNumber, List<ProductPictureMapping> productPictureMappings) {
//        this.name = name;
//        this.manufacturerPartNumber = manufacturerPartNumber;
//        this.productPictureMappings = productPictureMappings;
//    }
//
//}
