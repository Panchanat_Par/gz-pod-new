package com.gz.pod.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "pod.manifesto")
public class ManifestolistWithDriver implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "DocumentNumber")
    private String documentNumber;
    @Column(name = "DeliveryDate")
    private String deliveryDate;
    @Column(name = "DONumber")
    private String dONumber;
    @Column(name = "DriverId")
    private int driverId;
    @Column(name = "ManifestoType")
    private String manifestoType;
    @Column(name = "ShipmentNo")
    private String shipmentNo;
    @Column(name = "JobId")
    private String jobId;
    @Column(name = "RouteMasterId")
    private int routeMasterId;
    @Column(name = "RouteName")
    private String routeName;
    @Column(name = "CustomerName")
    private String customerName;
    @Column(name = "ShipToCode")
    private String shipToCode;
    @Column(name = "ShipToName")
    private String shipToName;
    @Column(name = "ShipToAddress")
    private String shipToAddress;
    @Column(name = "BoxQty")
    private Integer boxQty;
    @Column(name = "ItemQty")
    private Integer itemQty;
    @Column(name = "SKUQty")
    private Integer sKUQty;
    @Column(name = "Remark")
    private String remark;
    @Column(name = "CreatedBy")
    private Integer createdBy;
    @Column(name = "CreatedDate")
    private Timestamp createdDate;
    @Column(name = "UpdatedBy")
    private Integer updatedBy;
    @Column(name = "UpdatedDate")
    private Timestamp updatedDate;
    @Column(name = "AddressType")
    private String addressType;
    @Column(name = "OrderTotalInVat")
    private BigDecimal orderTotalInVat;
    @Column(name = "PODNumber")
    private String podNumber;
    @Column(name = "Note")
    private String note;
    @Column(name = "isGetItem")
    private Integer isGetItem;
    @Column(name = "newBoxQty")
    private Integer newBoxQty;
    @Column(name = "sapCustCode")
    private String sapCustCode;
    @Column(name = "invoiceNo")
    private String invoiceNo;
    @Column(name = "logType")
    private String logType;
    @Column(name = "logRemark")
    private String logRemark;
    @Column(name = "FulfillId")
    private Integer fulfillId;
    @Column(name = "podFlag")
    private String podFlag;
    @Column(name = "ContactPerson")
    private String ContactPerson;
    @Column(name = "DelayDate")
    private  String delayDate;

    @Transient
    private String originDeliveryDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driverId", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    UserTypeMaster userTypeMaster;
}
