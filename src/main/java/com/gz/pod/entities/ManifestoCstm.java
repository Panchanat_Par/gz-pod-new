//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_Manifesto_Cstm")
//public class ManifestoCstm implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "JobId")
//    private String jobId;
//    @Column(name = "SONo")
//    private String sONo;
//    @Column(name = "SODate")
//    private String sODate;
//    @Column(name = "BillNo")
//    private String billNo;
//    @Column(name = "BillDate")
//    private String billDate;
//    @Column(name = "TypePayment")
//    private String typePayment;
//    @Column(name = "SalesArea")
//    private String salesArea;
//
//    public ManifestoCstm() {
//
//    }
//
//    public ManifestoCstm(String jobId, String sONo, String sODate, String billNo, String billDate, String typePayment,
//                         String salesArea) {
//        this.jobId = jobId;
//        this.sONo = sONo;
//        this.sODate = sODate;
//        this.billNo = billNo;
//        this.billDate = billDate;
//        this.typePayment = typePayment;
//        this.salesArea = salesArea;
//    }
//}
