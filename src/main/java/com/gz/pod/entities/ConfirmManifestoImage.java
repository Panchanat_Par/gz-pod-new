//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_Confirm_Manifesto_Image")
//public class ConfirmManifestoImage {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "ManifestoId")
//    private int manifestoId;
//    @Column(name = "ConfirmManifestoId")
//    private int confirmManifestoId;
//    @Column(name = "UpdateDate")
//    private String updateDate;
//    @Column(name = "UpdateBy")
//    private int updateBy;
//    @Column(name = "ImageFile")
//    private String imageFile;
//
//
//    public ConfirmManifestoImage(int id,
//                                 int manifestoId,
//                                 int confirmManifestoId,
//                                 String updateDate,
//                                 int updateBy,
//                                 String imageFile) {
//        this.id = id;
//        this.manifestoId = manifestoId;
//        this.confirmManifestoId = confirmManifestoId;
//        this.updateDate = updateDate;
//        this.updateBy = updateBy;
//        this.imageFile = imageFile;
//    }
//
//    public ConfirmManifestoImage() {
//
//    }
//
//
//}
