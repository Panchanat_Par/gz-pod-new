//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//
//@Data
//@Entity
//@Table(name = "PISGoodChoiz_ProductDetail")
//public class PISGoodChoizProductDetail implements Serializable {
//
//    private static final long serialVersionUID = 1905122041950251207L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "ProductID")
//    private int productID;
//    @Column(name = "PTVNSKU")
//    private String materialCode;
//    @Column(name = "ProductCode")
//    private String productCode;
//    @Column(name = "ProductName")
//    private String productName;
//    @Column(name = "SAPshortDesc")
//    private String shortDes;
//    @Column(name = "SAPlongDesc")
//    private String fullDes;
//    @Column(name = "StockTypeId")
//    private int stockTypeId;
//    @Column(name = "Cost")
//    private BigDecimal cost;
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "DeliveryPeriodID")
//    private PISGoodChoizDeliveryPeriodInfo pisGoodChoizDeliveryPeriodInfo;
//    @Column(name = "SellingPrice")
//    private BigDecimal sellingPrice;
//
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
//    @JoinColumn(name = "manufacturerPartNumber", referencedColumnName = "PTVNSKU")
//    private List<Product> products = new ArrayList<>();
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinColumn(name = "CatID")
//    private Category category;
//
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "productID")
//    private List<SOMSOrderItem> orderItems = new ArrayList<>();
//
//    public PISGoodChoizProductDetail() {
//    }
//
//    @SuppressWarnings("squid:S00107")
//    public PISGoodChoizProductDetail(String materialCode, String productCode, String productName, String shortDes,
//                                     String fullDes, int stockTypeId, BigDecimal cost,
//                                     PISGoodChoizDeliveryPeriodInfo pisGoodChoizDeliveryPeriodInfo,
//                                     BigDecimal sellingPrice, List<Product> products, Category category) {
//        this.materialCode = materialCode;
//        this.productCode = productCode;
//        this.productName = productName;
//        this.shortDes = shortDes;
//        this.fullDes = fullDes;
//        this.stockTypeId = stockTypeId;
//        this.cost = cost;
//        this.pisGoodChoizDeliveryPeriodInfo = pisGoodChoizDeliveryPeriodInfo;
//        this.sellingPrice = sellingPrice;
//        this.products = products;
//        this.category = category;
//    }
//}