//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.List;
//
//@Data
//@Entity
//@Table(name ="Tbl_Master_Reason_RejectManifesto")
//public class ReasonRejectManifesto implements Serializable {
//
//    private static final long serialVersionUID = 1905122041950251207L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "Description")
//    private String description;
//    @Column(name = "TypeReject")
//    private  int typeReject;
//
//    @OneToMany
//    @JoinColumn(name = "reasonId")
//    private List<ConfirmManifesto> reasonDriver;
//
//    @OneToMany
//    @JoinColumn(name = "reasonIdByManager")
//    private List<ConfirmManifesto> reasonManager;
//
//    public ReasonRejectManifesto(){}
//
//    public ReasonRejectManifesto(String description, int typeReject) {
//        this.description = description;
//        this.typeReject = typeReject;
//    }
//}
