package com.gz.pod.entities;

import lombok.Data;

@Data
public class Config {

    private boolean openQrCode;

}
