//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_ProductPriceMaster")
//public class SOMSProductPriceMaster {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "TierType")
//    private int tierType;
//    @Column(name = "ProductCode")
//    private String productCode;
//    @Column(name = "SellingPrice")
//    private BigDecimal sellingPrice;
//    @Column(name = "BottomPrice")
//    private BigDecimal bottomPrice;
//    @Column(name = "MaterialCode")
//    private String materialCode;
//    @Column(name = "Cost")
//    private BigDecimal cost;
//    @Column(name = "ASMBottomPrice")
//    private BigDecimal asmBottomPrice;
//    @Column(name = "LowerLimit")
//    private BigDecimal lowerLimit;
//
//    @SuppressWarnings("squid:S00107")
//    public SOMSProductPriceMaster(int tierType, String productCode, BigDecimal sellingPrice, BigDecimal bottomPrice,
//                                  String materialCode, BigDecimal cost, BigDecimal asmBottomPrice, BigDecimal lowerLimit) {
//        this.tierType = tierType;
//        this.productCode = productCode;
//        this.sellingPrice = sellingPrice;
//        this.bottomPrice = bottomPrice;
//        this.materialCode = materialCode;
//        this.cost = cost;
//        this.asmBottomPrice = asmBottomPrice;
//        this.lowerLimit = lowerLimit;
//    }
//
//    public SOMSProductPriceMaster() {
//    }
//}
