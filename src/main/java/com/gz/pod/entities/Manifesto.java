//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//@Data
//@Entity
//@Table(name = "Tbl_SOMS_Manifesto")
//public class Manifesto implements Serializable {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "DocumentNumber")
//    private String documentNumber;
//    @Column(name = "DeliveryDate")
//    private String deliveryDate;
//    @Column(name = "DONumber")
//    private String dONumber;
//    @Column(name = "DriverId")
//    private int driverId;
//    @Column(name = "ManifestoType")
//    private String manifestoType;
//    @Column(name = "ShipmentNo")
//    private String shipmentNo;
//    @Column(name = "JobId")
//    private String jobId;
//    @Column(name = "RouteMasterId")
//    private int routeMasterId;
//    @Column(name = "CustomerName")
//    private String customerName;
//    @Column(name = "ShipToCode")
//    private String shipToCode;
//    @Column(name = "ShipToName")
//    private String shipToName;
//    @Column(name = "ShipToAddress")
//    private String shipToAddress;
//    @Column(name = "BoxQty")
//    private Integer boxQty;
//    @Column(name = "ItemQty")
//    private Integer itemQty;
//    @Column(name = "SKUQty")
//    private Integer sKUQty;
//    @Column(name = "Remark")
//    private String remark;
//    @Column(name = "CreatedBy")
//    private String createdBy;
//    @Column(name = "AddressType")
//    private String addressType;
//    @Column(name = "OrderTotalInVat")
//    private BigDecimal orderTotalInVat;
//    @Column(name = "ShippingStatus")
//    private int shippingStatus;
////    @Column(name = "FulfillId")
////    private int fulfillId;
////    @Column(name = "PODNumber")
////    private int podNumber;
////    @Column(name = "IsDeleted")
////    private boolean isDeleted;
//
//    @OneToOne
//    @JoinColumn(name = "routeMasterId")
//    private MasterRoute masterRoute;
//
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "jobId", referencedColumnName = "jobId")
//    private ManifestoCstm manifestoCstm;
//
//    public Manifesto(String documentNumber, String deliveryDate, String dONumber, int driverId, String manifestoType,
//                     String shipmentNo, String jobId, int routeMasterId, String customerName, String shipToCode,
//                     String shipToName, String shipToAddress, Integer boxQty, Integer itemQty, Integer sKUQty,
//                     String remark, String createdBy, String addressType, BigDecimal orderTotalInVat,
//                     int shippingStatus, MasterRoute masterRoute, ManifestoCstm manifestoCstm) {
//        this.documentNumber = documentNumber;
//        this.deliveryDate = deliveryDate;
//        this.dONumber = dONumber;
//        this.driverId = driverId;
//        this.manifestoType = manifestoType;
//        this.shipmentNo = shipmentNo;
//        this.jobId = jobId;
//        this.routeMasterId = routeMasterId;
//        this.customerName = customerName;
//        this.shipToCode = shipToCode;
//        this.shipToName = shipToName;
//        this.shipToAddress = shipToAddress;
//        this.boxQty = boxQty;
//        this.itemQty = itemQty;
//        this.sKUQty = sKUQty;
//        this.remark = remark;
//        this.createdBy = createdBy;
//        this.addressType = addressType;
//        this.orderTotalInVat = orderTotalInVat;
//        this.shippingStatus = shippingStatus;
//        this.masterRoute = masterRoute;
//        this.manifestoCstm = manifestoCstm;
//    }
//
//    public Manifesto(int id , String documentNumber, String deliveryDate, String dONumber, int driverId, String manifestoType,
//                     String shipmentNo, String jobId, int routeMasterId, String customerName, String shipToCode,
//                     String shipToName, String shipToAddress, Integer boxQty, Integer itemQty, Integer sKUQty,
//                     String remark, String createdBy, String addressType, BigDecimal orderTotalInVat,
//                     int shippingStatus, MasterRoute masterRoute, ManifestoCstm manifestoCstm) {
//        this.id = id;
//        this.documentNumber = documentNumber;
//        this.deliveryDate = deliveryDate;
//        this.dONumber = dONumber;
//        this.driverId = driverId;
//        this.manifestoType = manifestoType;
//        this.shipmentNo = shipmentNo;
//        this.jobId = jobId;
//        this.routeMasterId = routeMasterId;
//        this.customerName = customerName;
//        this.shipToCode = shipToCode;
//        this.shipToName = shipToName;
//        this.shipToAddress = shipToAddress;
//        this.boxQty = boxQty;
//        this.itemQty = itemQty;
//        this.sKUQty = sKUQty;
//        this.remark = remark;
//        this.createdBy = createdBy;
//        this.addressType = addressType;
//        this.orderTotalInVat = orderTotalInVat;
//        this.shippingStatus = shippingStatus;
//        this.masterRoute = masterRoute;
//        this.manifestoCstm = manifestoCstm;
//    }
//
//    public Manifesto() {
//    }
//}
//
//
