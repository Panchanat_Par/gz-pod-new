//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//
//@Data
//@Entity
//@Table(name = "PISGoodChoiz_DeliveryPeriodInfo")
//public class PISGoodChoizDeliveryPeriodInfo implements Serializable {
//
//    private static final long serialVersionUID = 1905122041950251207L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "DeliveryPeriodID")
//    private int id;
//    @Column(name = "DeliveryPeriodName")
//    private String deliveryPeriodName;
//
//    public PISGoodChoizDeliveryPeriodInfo(String deliveryPeriodName) {
//        this.deliveryPeriodName = deliveryPeriodName;
//    }
//
//    public PISGoodChoizDeliveryPeriodInfo() {
//    }
//}
