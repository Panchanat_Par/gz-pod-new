//package com.gz.pod.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//
//@Data
//@Entity
//@Table(name = "Tbl_CustomerpriceByShiptocode")
//public class CustomerPriceByShipToCode {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "Id")
//    private int id;
//    @Column(name = "ProductCode")
//    private String productCode;
//    @Column(name = "ShipToCode")
//    private String shipToCode;
//    @Column(name = "Price")
//    private BigDecimal price;
//
//    public CustomerPriceByShipToCode(String productCode, String shipToCode, BigDecimal price) {
//        this.productCode = productCode;
//        this.shipToCode = shipToCode;
//        this.price = price;
//    }
//
//    public CustomerPriceByShipToCode() {
//    }
//}
