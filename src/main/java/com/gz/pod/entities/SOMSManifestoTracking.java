package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "Tbl_SOMS_Manifesto_Tracking")
public class SOMSManifestoTracking implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "docType")
    private String docType;
    @Column(name = "docRef")
    private String docRef;
    @Column(name = "createBy")
    private int createBy;
    @Column(name = "createDate")
    private String createDate;
    @Column(name = "updateBy")
    private int updateBy;
    @Column(name = "updateDate")
    private String updateDate;
    @Column(name = "statusId")
    private int statusId;
    @Column(name = "saleOrderItem")
    private int saleOrderItem;


    public SOMSManifestoTracking() {}

    public SOMSManifestoTracking(String docType, String docRef, int createBy, String createDate,
                                 int updateBy, String updateDate, int statusId) {
        this.docType = docType;
        this.docRef = docRef;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.statusId = statusId;
    }

    public SOMSManifestoTracking(String docType, String docRef, int createBy,
                                 String createDate, int statusId, int saleOrderItem) {
        this.docType = docType;
        this.docRef = docRef;
        this.createBy = createBy;
        this.createDate = createDate;
        this.statusId = statusId;
        this.saleOrderItem = saleOrderItem;
    }
}