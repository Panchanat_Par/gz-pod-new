package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pod.userType")
public class UserType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;

    public UserType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserType() {
    }
}
