package com.gz.pod.entities;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pod.masterReasonRejectManifesto")
public class MasterReasonRejectManifesto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "typeReject")
    private  int typeReject;
}
