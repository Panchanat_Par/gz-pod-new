package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
@Data
@Entity
@Table(name = "pod.manifesto")
public class ManifestoOwner implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;

    @Column(name = "DriverId")
    private int driverId;

    @Column(name = "ManifestoType")
    private String manifestoType;

    @Column(name = "FulfillId")
    private Integer fulfillId;

    @Column(name = "JobId")
    private String jobId;
}
