package com.gz.pod.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "pod.masterStatus")
public class MasterStatus implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "moduleType")
    private String moduleType;
    @Column(name = "name")
    private String name;
    @Column(name = "userTypeId")
    private Integer userTypeId;
    @Column(name = "isUpdateSOMS")
    private boolean isUpdateSOMS;
    @Column(name = "isSendMail")
    private boolean isSendMail;

    public MasterStatus() {

    }

    public MasterStatus(int id, String moduleType, String name) {
        this.id = id;
        this.moduleType = moduleType;
        this.name = name;
    }

    public MasterStatus(int id, String moduleType, String name, Integer userTypeId, boolean isUpdateSOMS, boolean isSendMail) {
        this.id = id;
        this.moduleType = moduleType;
        this.name = name;
        this.userTypeId = userTypeId;
        this.isUpdateSOMS = isUpdateSOMS;
        this.isSendMail = isSendMail;
    }
}
