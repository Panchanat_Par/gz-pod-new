package com.gz.pod.entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "pod.receiveDocument")
public class ReceiveDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "manifestoId")
    private int manifestoId;
    @Column(name = "statusId")
    private int statusId;
    @Column(name = "createdBy")
    private int createdBy;
    @Column(name = "createdDate")
    private Timestamp createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdBy", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private UserTypeMaster userTypeMaster;

    public ReceiveDocument(int id, int manifestoId, int statusId, int createdBy, Timestamp createdDate) {
        this.id = id;
        this.manifestoId = manifestoId;
        this.statusId = statusId;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public ReceiveDocument()
    {

    }
}
