package com.gz.pod.entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "pod.manifesto")
public class Manifestolist implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "DocumentNumber")
    private String documentNumber;
    @Column(name = "DeliveryDate")
    private String deliveryDate;
    @Column(name = "DONumber")
    private String dONumber;
    @Column(name = "DriverId")
    private int driverId;
    @Column(name = "ManifestoType")
    private String manifestoType;
    @Column(name = "ShipmentNo")
    private String shipmentNo;
    @Column(name = "JobId")
    private String jobId;
    @Column(name = "RouteMasterId")
    private int routeMasterId;
    @Column(name = "RouteName")
    private String routeName;
    @Column(name = "CustomerName")
    private String customerName;
    @Column(name = "ShipToCode")
    private String shipToCode;
    @Column(name = "ShipToName")
    private String shipToName;
    @Column(name = "ShipToAddress")
    private String shipToAddress;
    @Column(name = "BoxQty")
    private Integer boxQty;
    @Column(name = "ItemQty")
    private Integer itemQty;
    @Column(name = "SKUQty")
    private Integer sKUQty;
    @Column(name = "Remark")
    private String remark;
    @Column(name = "CreatedBy")
    private Integer createdBy;
    @Column(name = "CreatedDate")
    private Timestamp createdDate;
    @Column(name = "UpdatedBy")
    private Integer updatedBy;
    @Column(name = "UpdatedDate")
    private Timestamp updatedDate;
    @Column(name = "AddressType")
    private String addressType;
    @Column(name = "OrderTotalInVat")
    private BigDecimal orderTotalInVat;
    @Column(name = "PODNumber")
    private String podNumber;
    @Column(name = "Note")
    private String note;
    @Column(name = "isGetItem")
    private Integer isGetItem;
    @Column(name = "newBoxQty")
    private Integer newBoxQty;
    @Column(name = "sapCustCode")
    private String sapCustCode;
    @Column(name = "invoiceNo")
    private String invoiceNo;
    @Column(name = "logType")
    private String logType;
    @Column(name = "logRemark")
    private String logRemark;
    @Column(name = "FulfillId")
    private Integer fulfillId;
    @Column(name = "podFlag")
    private String podFlag;
    @Column(name = "ContactPerson")
    private String ContactPerson;
    @Column(name = "DelayDate")
    private  String delayDate;

    public Manifestolist(String documentNumber, String deliveryDate, String dONumber, int driverId, String manifestoType,
                         String shipmentNo, String jobId, int routeMasterId, String routeName, String customerName, String shipToCode,
                         String shipToName, String shipToAddress, Integer boxQty, Integer itemQty, Integer sKUQty,
                         String remark, Integer createdBy, String addressType, BigDecimal orderTotalInVat,
                         String podNumber,String Note,Integer newBoxQty,Integer isGetItem,
                         String sapCustCode,String invoiceNo, String logType ,String logRemark ,Integer fulfillId,
                         String podFlag, String contactPerson, String delayDate) {

        this.documentNumber = documentNumber;
        this.deliveryDate = deliveryDate;
        this.dONumber = dONumber;
        this.driverId = driverId;
        this.manifestoType = manifestoType;
        this.shipmentNo = shipmentNo;
        this.jobId = jobId;
        this.routeMasterId = routeMasterId;
        this.routeName = routeName;
        this.customerName = customerName;
        this.shipToCode = shipToCode;
        this.shipToName = shipToName;
        this.shipToAddress = shipToAddress;
        this.boxQty = boxQty;
        this.itemQty = itemQty;
        this.sKUQty = sKUQty;
        this.remark = remark;
        this.createdBy = createdBy;
        this.addressType = addressType;
        this.orderTotalInVat = orderTotalInVat;
        this.podNumber = podNumber;
        this.note = Note;
        this.newBoxQty = newBoxQty;
        this.isGetItem = isGetItem;
        this.sapCustCode = sapCustCode;
        this.invoiceNo = invoiceNo;
        this.logType = logType;
        this.logRemark = logRemark;
        this.fulfillId = fulfillId;
        this.podFlag = podFlag;
        this.ContactPerson = contactPerson;
        this.delayDate = delayDate;

    }

    public Manifestolist() {
    }
}