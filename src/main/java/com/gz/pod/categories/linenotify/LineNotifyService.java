//package com.gz.pod.categories.linenotify;
//
//import com.gz.pod.entities.LineNotify;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.categories.linenotify.request.IdRequest;
//import com.gz.pod.categories.linenotify.request.LineNotifyRequest;
//import com.gz.pod.categories.linenotify.request.TypeRequest;
//import com.gz.pod.categories.linenotify.response.LineNotifyResponse;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.io.PrintWriter;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.logging.Logger;
//import java.util.regex.Pattern;
//
//@Service
//public class LineNotifyService {
//
//    @Autowired
//    LineNotifyRepository lineRepository;
//
//    private static final Logger LOGGER = Logger.getLogger(LineNotifyService.class.getName());
//
//    public boolean sendMessageNotify(String token, String message) {
//        boolean result = false;
//        try {
//            message = replaceProcess(message);
//            message = URLEncoder.encode(message, "UTF-8");
//            URL url = new URL( LineNotifyConstant.URLAPI );
//            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//            connection.addRequestProperty("Authorization",  "Bearer " + token);
//            connection.setRequestMethod( "POST" );
//            connection.addRequestProperty( "Content-Type", "application/x-www-form-urlencoded" );
//            connection.setDoOutput( true );
//            String parameterString = "message=" + message;
//            PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
//            printWriter.print(parameterString);
//            printWriter.close();
//            connection.connect();
//
//            int statusCode = connection.getResponseCode();
//            if ( statusCode == 200 ) {
//                result = true;
//            } else {
//                throw new Exception( "Error:(StatusCode)" + statusCode + ", " + connection.getResponseMessage() );
//            }
//            connection.disconnect();
//        } catch (Exception e) {
//            LOGGER.warning("context" + e);
//        }
//
//        return result;
//    }
//
//    private String replaceProcess(String txt){
//        txt = replaceAllRegex(txt, "\\\\", "￥");		// \
//        return txt;
//    }
//    private String replaceAllRegex(String value, String regex, String replacement) {
//        if ( value == null || value.length() == 0 || regex == null || regex.length() == 0 || replacement == null )
//            return "";
//        return Pattern.compile(regex).matcher(value).replaceAll(replacement);
//    }
//
//    public GZResponse addLineNotifyToken(LineNotifyRequest lineNotifyRequest) throws GZException {
//        GZResponse gzResponse = new GZResponse();
//        try{
//            LineNotify lineNotify = new LineNotify(lineNotifyRequest.getName(), lineNotifyRequest.getToken(), lineNotifyRequest.getTypes());
//            gzResponse.setTitle(LineNotifyConstant.ADDLINENOTIFYTOKEN);
//            gzResponse.setCode(HttpStatus.OK);
//            gzResponse.setMessage(HttpStatus.OK.toString());
//            gzResponse.setDeveloperMessage(LineNotifyConstant.ADDLINENOTIFYTOKENSUCCESS);
//            lineRepository.save(lineNotify);
//        }catch(Exception e){
//            throw new GZException(LineNotifyConstant.ADDLINENOTIFYTOKEN, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
//            );
//        }
//        return gzResponse;
//    }
//
//
//
//    public GZResponse deleteLineNotifyToken(IdRequest idRequest) throws GZException {
//        GZResponse gzResponse = new GZResponse();
//        try{
//            gzResponse.setTitle(LineNotifyConstant.DELETELINENOTIFYTOKEN);
//            gzResponse.setCode(HttpStatus.OK);
//            gzResponse.setMessage(HttpStatus.OK.toString());
//            gzResponse.setDeveloperMessage(LineNotifyConstant.DELETELINENOTIFYTOKENSUCCESS);
//            lineRepository.delete(idRequest.getId());
//        }catch(Exception e){
//            throw new GZException(LineNotifyConstant.DELETELINENOTIFYTOKEN, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
//            );
//        }
//        return gzResponse;
//    }
//
//    public LineNotifyResponse listTokenByType(TypeRequest typeRequest) throws GZException {
//        LineNotifyResponse lineNotifyResponse = new LineNotifyResponse();
//        try {
//            ArrayList<LineNotify> lineNotify = lineRepository.listTokenByType(typeRequest.getTypes());
//            lineNotifyResponse.setTitle(LineNotifyConstant.LISTNOTIFY);
//            lineNotifyResponse.setCode(HttpStatus.OK);
//            lineNotifyResponse.setMessage(HttpStatus.OK.toString());
//            lineNotifyResponse.setDeveloperMessage(LineNotifyConstant.LISTNOTIFYSUCCESS);
//            lineNotifyResponse.setData(lineNotify);
//        } catch (Exception e) {
//            throw new GZException(LineNotifyConstant.LISTNOTIFY, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
//            );
//        }
//        return lineNotifyResponse;
//    }
//}
