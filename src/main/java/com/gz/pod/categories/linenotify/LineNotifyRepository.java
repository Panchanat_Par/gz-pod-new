//package com.gz.pod.categories.linenotify;
//
//import com.gz.pod.entities.LineNotify;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import java.util.ArrayList;
//
//@Repository
//public interface LineNotifyRepository extends CrudRepository<LineNotify, Integer> {
//
//    @Query("select l from LineNotify l where l.types = :types")
//    ArrayList<LineNotify> listTokenByType(@Param("types") String types);
//}
