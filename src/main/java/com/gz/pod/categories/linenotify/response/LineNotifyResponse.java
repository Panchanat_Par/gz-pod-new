//package com.gz.pod.categories.linenotify.response;
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.gz.pod.entities.LineNotify;
//import lombok.Data;
//import org.springframework.http.HttpStatus;
//
//import java.util.ArrayList;
//
//@Data
//@JsonInclude(JsonInclude.Include.NON_NULL)
//public class LineNotifyResponse {
//    private String title;
//    private HttpStatus code;
//    private String message;
//    private String developerMessage;
//    private ArrayList<LineNotify> data;
//}
