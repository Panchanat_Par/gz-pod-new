//package com.gz.pod.categories.linenotify.request;
//
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Pattern;
//
//@Data
//public class LineNotifyRequest {
//    @Pattern(regexp = "^[A-Za-z0-9/_]{1,30} *$",
//            message = "Name must be numbers or characters A to z and" +
//                    "Name must be more than 1 but less than 30 characters")
//    @ApiModelProperty(example = "Group_LYNX")
//    @NotNull(message = "Name can not be null")
//    private String name=null;
//    @ApiModelProperty(example = "WNc736jQmAcHaOOeeA0muXIpqAn8pKFKWgOMNCt0U19")
//    @NotNull(message = "Token can not be null")
//    private String token;
//    @ApiModelProperty(example = "driver")
//    @NotNull(message = "Types can not be null")
//    private String types;
//}
