//package com.gz.pod.categories.linenotify;
//
//public class LineNotifyConstant {
//    public static final String ADDLINENOTIFYTOKEN = "Add Linenotify Token";
//    public static final String DELETELINENOTIFYTOKEN = "Delete Linenotify Token";
//    public static final String ADDLINENOTIFYTOKENSUCCESS = "Add Linenotify Token Success";
//    public static final String DELETELINENOTIFYTOKENSUCCESS = "Delete Linenotify Token Success";
//    public static final String LISTNOTIFY = "ListNotify";
//    public static final String LISTNOTIFYSUCCESS = "ListNotify Success";
//    public static final String URLAPI = "https://notify-api.line.me/api/notify";
//
//    private LineNotifyConstant() {
//        throw new UnsupportedOperationException();
//    }
//}
