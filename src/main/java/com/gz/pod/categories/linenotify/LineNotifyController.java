//package com.gz.pod.categories.linenotify;
//
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.categories.linenotify.request.IdRequest;
//import com.gz.pod.categories.linenotify.request.LineNotifyRequest;
//import com.gz.pod.categories.linenotify.request.TypeRequest;
//import com.gz.pod.categories.linenotify.response.LineNotifyResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@ConfigurationProperties("application.properties")
//@RestController
//@RequestMapping("api/${api.version}/linenotify")
//@Api(value = "LineNotify", description = "LineNotify management", produces = "application/json", tags = {"LineNotify"})
//public class LineNotifyController {
//
//    @Autowired
//    LineNotifyService lineNotifyService;
//
//    @PostMapping("addToken")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
//    public ResponseEntity<GZResponse> addLineNotifyToken(@RequestBody LineNotifyRequest lineNotifyRequest) throws Exception {
//        return ResponseEntity.ok(lineNotifyService.addLineNotifyToken(lineNotifyRequest));
//    }
//
//    @GetMapping("listTokenByType")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
//    public ResponseEntity<LineNotifyResponse> deleteLineNotifyToken(@Valid TypeRequest typeRequest) throws Exception {
//        return ResponseEntity.ok(lineNotifyService.listTokenByType(typeRequest));
//    }
//
//    @DeleteMapping("deleteToken")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
//    public ResponseEntity<GZResponse> deleteLineNotifyToken(@RequestBody @Valid IdRequest idRequest) throws Exception {
//        return ResponseEntity.ok(lineNotifyService.deleteLineNotifyToken(idRequest));
//    }
//
//}
