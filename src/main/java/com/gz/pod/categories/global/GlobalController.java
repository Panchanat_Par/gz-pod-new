package com.gz.pod.categories.global;

import com.gz.pod.response.Errors;
import com.gz.pod.response.GZErrorResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/global")
@Api(value = "Global Response", description = "Global management", produces = "application/json", tags = {"Global Response"})
public class GlobalController {

    @PostMapping("/gzErrorResponse")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Error", response = GZErrorResponse.class)})
    public ResponseEntity<GZErrorResponse> gzErrorResponse() {
        return ResponseEntity.ok(new GZErrorResponse("", HttpStatus.BAD_REQUEST, "", ""));
    }

    @PostMapping("/errorsResponse")
    @ApiResponses(value = {@ApiResponse(code = 500, message = "Error", response = Errors.class)})
    public ResponseEntity<Errors> errorsResponse() {
        return ResponseEntity.ok(new Errors(HttpStatus.BAD_REQUEST, "", ""));
    }

}
