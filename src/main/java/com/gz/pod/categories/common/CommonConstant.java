package com.gz.pod.categories.common;

import org.springframework.beans.factory.annotation.Value;

public class CommonConstant {
    public static final String DRIVER = "Driver";
    public static final String CHECKER = "Checker";
    public static final String FINOPS = "FinOps";

    public static final String THERE_ARE_SOME_MANIFESTO_NOT_YOURS = "there.are.some.manifesto.not.yours";
}
