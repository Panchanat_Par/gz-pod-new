package com.gz.pod.categories.common;

import com.gz.pod.entities.UserTypeMaster;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserTypeMasterRepository extends CrudRepository<UserTypeMaster, Integer> {

    UserTypeMaster findById(int id);

    UserTypeMaster findByloginId(int loginId);

    List<UserTypeMaster> findBynameContainingAndUserTypeId(String name, int userTypeId);

    @Query("select u.name from UserTypeMaster u where id = :id")
    String findDriverNamebyDriverId(@Param("id") int id);

}
