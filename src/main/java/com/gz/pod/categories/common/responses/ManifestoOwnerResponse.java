package com.gz.pod.categories.common.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ManifestoOwnerResponse {
    @JsonIgnore
    private Integer manifestoId;

    private boolean isOwner;

    @JsonIgnore
    private boolean owner;

    private Integer fulfillId;
    private String jobId;
    private String manifestoType;
    private String refId;

    public ManifestoOwnerResponse(Integer manifestoId, boolean isOwner, boolean owner, String jobId, String manifestoType) {
        this.manifestoId = manifestoId;
        this.isOwner = isOwner;
        this.owner = owner;
        this.jobId = jobId;
        this.manifestoType = manifestoType;
    }

    public ManifestoOwnerResponse() {
    }
}