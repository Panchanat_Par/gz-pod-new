package com.gz.pod.categories.common;

import com.gz.pod.entities.MasterStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterStatusRepository extends CrudRepository<MasterStatus, Integer> {

    MasterStatus findByIdAndUserTypeId(int id, int userTypeId);

    MasterStatus findById(int id);
}
