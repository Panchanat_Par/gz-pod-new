package com.gz.pod.categories.common;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.common.responses.ManifestoOwnerResponse;
import com.gz.pod.categories.datafromsoms.DataFromSOMSConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.manifestolist.responses.ManifestoResponse;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentConstant;
import com.gz.pod.entities.*;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.*;

@Service
public class CommonService {
    @Autowired
    UserTypeMasterRepository userTypeMasterRepository;

    @Autowired
    MasterStatusRepository masterStatusRepository;

    @Autowired
    UserTypeRepository userTypeRepository;

    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    RestTemplate restTemplate;

    @Value("${PrivateKey}")
    public String PrivateKey;

    private ObjectMapper mapper = new ObjectMapper();

    public UserType getUserType(int userTypeId)
    {
        UserType userType = userTypeRepository.findById(userTypeId);
        if(userType != null)
            return userType;
        else
            return null;
    }

    public UserTypeMaster getUserTypeMaster(String title, int loginId) throws Exception {
        try {
            UserTypeMaster userTypeMaster = userTypeMasterRepository.findByloginId(loginId);
            if(userTypeMaster != null)
                return userTypeMaster;
            else
                throw new GZException(title, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND  );
        }
        catch (Exception e) {
            throw new GZException(title, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
            );
        }
    }
    public UserTypeMaster getUserTypeMasterById(String title, int id) throws Exception {
        try {
            UserTypeMaster userTypeMaster = userTypeMasterRepository.findById(id);
            if(userTypeMaster != null)
                return userTypeMaster;
            else
                return null;
        }
        catch (Exception e) {
            throw new GZException(title, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
            );
        }
    }
    public UserTypeMaster getUserTypeMasterForReturn(int loginId)
    {
        UserTypeMaster userTypeMaster = userTypeMasterRepository.findByloginId(loginId);
        if(userTypeMaster != null)
            return userTypeMaster;
        else
            return null;
    }

    public boolean isCheckMasterStatus(String title, Integer statusId, int userTypeId)  throws Exception {
        try {
            MasterStatus masterStatus =  masterStatusRepository.findByIdAndUserTypeId(statusId, userTypeId);
            if(masterStatus != null)
                return true;
            else
                throw new GZException(title, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString()
                        , PODNumberStatusConstant.STATUS_AND_USERTYPE_NOT_FOUND  );

        } catch (Exception e) {
            throw new GZException(title, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString()
                    , PODNumberStatusConstant.STATUS_AND_USERTYPE_NOT_FOUND  );
        }
    }

    public static boolean isInteger(String str) {
        boolean amIValid = false;
        try {
            Integer.parseInt(str);
            // s is a valid integer!
            amIValid = true;
        } catch (NumberFormatException e) {
            amIValid = false;
        }
        return amIValid;
    }

    public List<ManifestoOwnerResponse> validateOwnerByManifestoId(List<Integer> manifestoIdList, Integer driverId)
    {
        String deliveryDate = java.time.LocalDate.now().toString();
        List<ManifestoOwner> manifestoOwnerList = manifestolistRepository.findDriveIdByListId(manifestoIdList, deliveryDate);

        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        for (ManifestoOwner manifesto : manifestoOwnerList)
        {
            ManifestoOwnerResponse manifestoOwnerResponse = new ManifestoOwnerResponse();
            manifestoOwnerResponse.setFulfillId(manifesto.getFulfillId());
            manifestoOwnerResponse.setJobId(manifesto.getJobId());
            manifestoOwnerResponse.setManifestoId(manifesto.getId());
            manifestoOwnerResponse.setOwner(manifesto.getDriverId() == driverId);
            manifestoOwnerResponse.setManifestoType(manifesto.getManifestoType().equals("Log") ? "Log" : "SO");
            manifestoOwnerResponse.setRefId(manifesto.getManifestoType().equals("Log") ? Integer.toString(manifesto.getFulfillId()) : manifesto.getJobId());

            manifestoOwnerResponseList.add(manifestoOwnerResponse);
        }

        return manifestoOwnerResponseList;
    }

    public GZResponse getRefDocByQRCodeFromSOMS(String qrNumber)
    {
        GZResponse response = new GZResponse();
        try
        {
            String[] payloads = new String[]{};
            final String secretKey = generateSignature(payloads);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type","application/json");
            //
            headers.set("secretKey", secretKey);
            Map body = new HashMap();
            HttpEntity<?> entity = new HttpEntity<>(body,headers);
            ResponseEntity<String> res = restTemplate.exchange(DataFromSOMSConstant.URL_SOMS_API + "API/GetQRByQRNumber?QRNumber=" + qrNumber
                    , HttpMethod.GET
                    , entity
                    , String.class);
            if(res.getStatusCode() == HttpStatus.OK)
            {
                JsonNode tree = mapper.readTree(res.getBody());
                JsonNode docRef = tree.path("DocRef");
                JsonNode docName = tree.path("DocName");

                if(!docRef.isMissingNode() && !docName.isMissingNode())
                {
                    String docRefText = docRef.asText();
                    String docNameText = docName.asText();

                    Map dataMap = new HashMap();
                    dataMap.put("docRef", docRefText);
                    dataMap.put("docName", docNameText);

                    response.setDeveloperMessage(ReceiveDocumentConstant.QRNUMBER_FOUND);
                    response.setData(dataMap);
                }
                else
                {
                    String message = tree.get("Message").asText();
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());

                    if (message.equals(ReceiveDocumentConstant.QRNUMBER_NOTFOUND))
                    {
                        response.setDeveloperMessage(ReceiveDocumentConstant.QRNUMBER_NOTFOUND);
                    }
                    else
                    {
                        response.setDeveloperMessage(message);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public String generateSignature( String[] payloads) throws Exception {
        return calculateSignature(randomString(), PrivateKey, payloads);
    }

    public String calculateSignature(String randomString, String secretKey, String[] payloads) throws Exception {
        final String message = String.join(";", payloads) + ";" + randomString;
        Mac sha256HMAC = Mac.getInstance("HmacSHA256");
        sha256HMAC.init(new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256"));
        String hash = Base64.encodeBase64String(sha256HMAC.doFinal(message.getBytes()));
        return randomString + "." + hash;
    }

    public String randomString() {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 3) { // length of the random string.
            int index = (int) (rnd.nextFloat() * chars.length());
            salt.append(chars.charAt(index));
        }
        return salt.toString();
    }


}
