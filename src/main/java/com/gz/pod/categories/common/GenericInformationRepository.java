package com.gz.pod.categories.common;

import com.gz.pod.entities.GenericInformation;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface GenericInformationRepository extends CrudRepository<GenericInformation, Integer> {

    GenericInformation findByRefModuleAndRefActionAndRefId(String refModule, String refAction, int refId);

//    @Query("SELECT g.value FROM GenericInformation g " +
//            "WHERE refId =: id")
//    Integer findRefIdStatusByListJobId(@Param("id") Integer id);
}
