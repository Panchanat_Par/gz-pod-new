//package com.gz.pod.categories.product;
//
//import com.gz.pod.entities.CustomerPriceByShipToCode;
//import com.gz.pod.entities.PISGoodChoizProductDetail;
//import com.gz.pod.entities.SOMSProductPrice;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponsePage;
//import com.gz.pod.categories.product.requests.ProductCategaryRequest;
//import com.gz.pod.categories.product.requests.ProductRequest;
//import com.gz.pod.categories.product.requests.ProductSearchRequest;
//import com.gz.pod.categories.product.responses.ProductResponseListAll;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.support.PagedListHolder;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@Service
//public class ProductService {
//
//    @Autowired
//    private ProductRepository productRepository;
//    @Autowired
//    private ProductRepositoryShipToCodePrice productRepositoryShipToCodePrice;
//    @Autowired
//    private ProductRepositorySOMSPrice productRepositorySOMSPrice;
//    private ModelMapper modelMapper = new ModelMapper();
//
//    public GZResponsePage getAllProductDetail(ProductRequest request) throws GZException {
//        GZResponsePage response;
//        try {
//            List<PISGoodChoizProductDetail> data = productRepository.getAllProduct();
//            response = setDataToPage(data, request);
//        } catch (Exception ex) {
//            throw new GZException(ProductConstant.PRODUCT_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
//            );
//        }
//        return response;
//    }
//
//    public GZResponsePage getProductBySearch(ProductSearchRequest request) throws GZException {
//        GZResponsePage response;
//        modelMapper.getConfiguration().setAmbiguityIgnored(true);
//        try {
//            List<PISGoodChoizProductDetail> data = productRepository.findByProductNameContaining(request.getProductName());
//            response = setDataToPage(data, modelMapper.map(request, ProductRequest.class));
//        } catch (Exception ex) {
//            throw new GZException(ProductConstant.PRODUCT_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
//            );
//        }
//        return response;
//    }
//
//    public GZResponsePage getProductByCategory(ProductCategaryRequest request) throws GZException {
//        GZResponsePage response;
//        modelMapper.getConfiguration().setAmbiguityIgnored(true);
//        try {
//            List<PISGoodChoizProductDetail> data = productRepository.getProductByCategory(request.getCategory());
//            response = setDataToPage(data, modelMapper.map(request, ProductRequest.class));
//        } catch (Exception ex) {
//            throw new GZException(ProductConstant.PRODUCT_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
//            );
//        }
//        return response;
//    }
//
//    public List<PISGoodChoizProductDetail> getListByCheckPrice(List<PISGoodChoizProductDetail> list,
//                                                               String sapCode, String shipToCode) {
//        Set<PISGoodChoizProductDetail> data = new HashSet<>();
//        List<String> pdcs = list.stream()
//                .map(PISGoodChoizProductDetail::getProductCode)
//                .collect(Collectors.toList());
//
//        List<CustomerPriceByShipToCode> stcpLists = productRepositoryShipToCodePrice.queryIn(pdcs, shipToCode);
//        List<SOMSProductPrice> scpLists = productRepositorySOMSPrice.queryIn(pdcs, sapCode);
//
//        for (PISGoodChoizProductDetail pis : list) {
//            PISGoodChoizProductDetail piss = pis;
//            for (CustomerPriceByShipToCode ship : stcpLists) {
//                if (pis.getProductCode().equals(ship.getProductCode())) {
//                    piss.setSellingPrice(ship.getPrice());
//                    data.add(piss);
//                } else {
//                    for (SOMSProductPrice sap : scpLists) {
//                        if (pis.getProductCode().equals(sap.getProductCode())) {
//                            piss.setSellingPrice(sap.getSellingPrice());
//                            data.add(piss);
//                        } else {
//                            data.add(piss);
//                        }
//                    }
//                }
//            }
//        }
//        return new ArrayList<>(data);
//    }
//
//    public GZResponsePage setDataToPage(List<PISGoodChoizProductDetail> data, ProductRequest request) throws GZException {
//        GZResponsePage response = new GZResponsePage();
//        List<PISGoodChoizProductDetail> dataComplete = getListByCheckPrice(data, request.getSapCode(), request.getShipToCode());
//        List<ProductResponseListAll> products = dataComplete.stream()
//                .map(p -> new ProductResponseListAll(p.getMaterialCode(), p.getProductCode(), p.getProductName(),
//                        p.getShortDes(), p.getFullDes(), p.getCost(),
//                        p.getPisGoodChoizDeliveryPeriodInfo().getDeliveryPeriodName(),
//                        p.getSellingPrice())).collect(Collectors.toList());
//        PagedListHolder<ProductResponseListAll> holder = new PagedListHolder<>(products);
//        if (holder.getNrOfElements() == 0) {
//            throw new GZException(ProductConstant.PRODUCT_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ProductConstant.GET_LIST_ERROR
//            );
//        } else {
//            holder.setPage(request.getPage() - 1);
//            holder.setPageSize(request.getPerPage());
//            response.setTitle(ProductConstant.PRODUCT_LIST);
//            response.setCode(HttpStatus.OK);
//            response.setMessage(HttpStatus.OK.toString());
//            response.setDeveloperMessage(ProductConstant.GET_LIST_SUCCESS);
//            response.setData(holder.getPageList());
//            response.setCurrentPage(request.getPage());
//            response.setPageSize(holder.getPageSize());
//            response.setTotalItems(holder.getNrOfElements());
//            response.setTotalPage(holder.getPageCount());
//        }
//        return response;
//    }
//
//}
