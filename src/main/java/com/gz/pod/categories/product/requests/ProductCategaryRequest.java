//package com.gz.pod.categories.product.requests;
//
//import io.swagger.annotations.ApiModelProperty;
//import io.swagger.annotations.ApiParam;
//import lombok.Data;
//
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Pattern;
//
//@Data
//public class ProductCategaryRequest {
//    @ApiParam(value = "หมายเลขหน้าที่ต้องการดูสินค้า", defaultValue = "1")
//    @Min(value = 1, message = "page must be more than 0")
//    private int page = 1;
//    @ApiParam(value = "จำนวนสินค้าที่จะโชว์ใน 1 หน้า", defaultValue = "9")
//    @Min(value = 1, message = "perPage must be more than 0")
//    private int perPage = 9;
//    @ApiParam(value = "ประเภทสินค้าที่ต้องการค้นหา", defaultValue = "เครื่องเขียน")
//    private String category;
//    @ApiParam(value = "หมายเลขลูกค้าที่มีใน SAP", defaultValue = "F411")
//    @ApiModelProperty(required = true)
//    @NotNull(message = "SAPCode can not be null")
//    private String sapCode;
//    @ApiParam(value = "หมายเลขที่อยู่ในการจัดส่ง", defaultValue = "500004")
//    @ApiModelProperty(required = true)
//    @NotNull(message = "shipToCode can not be null")
//    @Pattern(regexp = "^[0-9]*$", message = "shipToCode must be type string format number")
//    private String shipToCode;
//}
