//package com.gz.pod.categories.product;
//
//import com.gz.pod.categories.product.requests.ProductCategaryRequest;
//import com.gz.pod.categories.product.requests.ProductRequest;
//import com.gz.pod.categories.product.requests.ProductSearchRequest;
//import com.gz.pod.categories.product.responses.ProductResponseListAll;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.Valid;
//@ConfigurationProperties("application.properties")
//@RestController
//@RequestMapping("api/${api.version}/product")
//@Api(value = "Product", description = "Product management", produces = "application/json", tags = {"Product"})
//public class ProductController {
//    private ProductService productService;
//
//    @Autowired
//    public ProductController(ProductService productDetailService) {
//        this.productService = productDetailService;
//    }
//
//    @GetMapping
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ProductResponseListAll.class)})
//    public ResponseEntity getAllProductDetail(@Valid ProductRequest productRequest) throws Exception {
//        return ResponseEntity.ok(productService.getAllProductDetail(productRequest));
//    }
//
//    @GetMapping("/search")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ProductResponseListAll.class)})
//    public ResponseEntity getAllProductDetailBySearch(@Valid ProductSearchRequest productRequest) throws Exception {
//        return ResponseEntity.ok(productService.getProductBySearch(productRequest));
//    }
//
//    @GetMapping("/category")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ProductResponseListAll.class)})
//    public ResponseEntity getAllProductDetailByCategory(@Valid ProductCategaryRequest productRequest) throws Exception {
//        return ResponseEntity.ok(productService.getProductByCategory(productRequest));
//    }
//}
