//package com.gz.pod.categories.product;
//
//import com.gz.pod.entities.SOMSProductPrice;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ProductRepositorySOMSPrice extends CrudRepository<SOMSProductPrice, Integer> {
//
//    @Query("select s from SOMSProductPrice s where s.productCode in :productCodes or s.sapCustCode = :sapCode")
//    List<SOMSProductPrice> queryIn(@Param("productCodes") List<String> productCode, @Param("sapCode") String sapCode);
//
//}
