//package com.gz.pod.categories.product;
//
//import com.gz.pod.entities.PISGoodChoizProductDetail;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ProductRepository extends CrudRepository<PISGoodChoizProductDetail, Integer> {
//
//    @Query("select p from PISGoodChoizProductDetail p inner join p.products pd " +
//            "where p.productCode != '' and pd is not null")
//    List<PISGoodChoizProductDetail> getAllProduct();
//
//    @Query("select p from PISGoodChoizProductDetail p inner join p.products pd " +
//            "where p.productCode != '' and pd is not null and p.productName like %:search%")
//    List<PISGoodChoizProductDetail> findByProductNameContaining(@Param("search") String search);
//
//    @Query("select p from PISGoodChoizProductDetail p where p.productCode != '' and p.category.name = :category")
//    List<PISGoodChoizProductDetail> getProductByCategory(@Param("category") String category);
//}
