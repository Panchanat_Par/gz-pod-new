//package com.gz.pod.categories.product;
//
//import com.gz.pod.entities.CustomerPriceByShipToCode;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ProductRepositoryShipToCodePrice extends CrudRepository<CustomerPriceByShipToCode, Integer> {
//
//    @Query("select c from CustomerPriceByShipToCode c where c.productCode in :productCodes or c.shipToCode = :shipToCode")
//    List<CustomerPriceByShipToCode> queryIn(@Param("productCodes") List<String> productCode, @Param("shipToCode") String shipToCode);
//
//}
