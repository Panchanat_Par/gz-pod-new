//package com.gz.pod.categories.product.responses;
//
//import lombok.Data;
//
//import java.math.BigDecimal;
//
//@Data
//public class ProductResponseListAll {
//    private String materialCode;
//    private String productCode;
//    private String productName;
//    private String shortDes;
//    private String fullDes;
//    private BigDecimal cost;
//    private String deliveryDate;
//    private BigDecimal price;
//
//    @SuppressWarnings("squid:S00107")
//    public ProductResponseListAll(String materialCode, String productCode, String productName, String shortDes,
//                                  String fullDes, BigDecimal cost, String deliveryDate, BigDecimal price) {
//        this.materialCode = materialCode;
//        this.productCode = productCode;
//        this.productName = productName;
//        this.shortDes = shortDes;
//        this.fullDes = fullDes;
//        this.cost = cost;
//        this.deliveryDate = deliveryDate;
//        this.price = price;
//    }
//}
