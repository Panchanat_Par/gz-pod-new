//package com.gz.pod.categories.jobItemStatus;
//
//import com.gz.pod.entities.JobItemStatus;
//import com.gz.pod.entities.JobStatus;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface JobItemStatusRepository extends CrudRepository<JobItemStatus, Integer> {
//
//    List<JobItemStatus> findByitemIdOrderByIdDesc(int itemId);
//}
