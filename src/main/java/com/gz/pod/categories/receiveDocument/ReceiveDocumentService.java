package com.gz.pod.categories.receiveDocument;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.common.*;
import com.gz.pod.categories.datafromsoms.DataFromSOMSConstant;
import com.gz.pod.categories.genericInformation.GenericInformationConstant;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.receiveDocument.requests.UpdateReceiveDocument;
import com.gz.pod.categories.receiveDocument.requests.ValidateReceiveDocument;
import com.gz.pod.categories.receiveDocument.respones.ReceiveDocumentRespones;
import com.gz.pod.entities.*;
import com.gz.pod.response.GZResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ReceiveDocumentService {
    @Autowired
    ReceiveDocumentRepository receiveDocumentRepository;

    @Autowired
    CommonService commonService;

    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    JobStatusRepository jobStatusRepository;

    @Autowired
    GenericInformationRepository genericInformationRepository;

    @Autowired
    MasterStatusRepository masterStatusRepository;

    @Autowired
    UserTypeMasterRepository userTypeMasterRepository;

    private ObjectMapper mapper = new ObjectMapper();
    private int userIdTMP;
    private int manifestoIdTMP;
    private String jobIdTMP;
    private String manifestoTypeTMP;
    private String userTypeTMP;
    private int driverIdTMP;

    public GZResponse validateReceiveDocumentMain(ValidateReceiveDocument request)
    {
        GZResponse response = new GZResponse();
        try
        {
            response = validateReceiveDocument(request);
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        response.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        return response;
    }

    public GZResponse updateReceiveDocumentMain(UpdateReceiveDocument request)
    {
        GZResponse response = new GZResponse();
        try
        {
            ///Map Request
            ValidateReceiveDocument validate = new ValidateReceiveDocument();
            validate.setStatusId(request.getStatusId());
            validate.setLoginId(request.getLoginId());
            validate.setRefId(request.getRefId());
            validate.setRefModuleType(request.getRefModuleType());

            response = validateReceiveDocument(validate);

            if(response.getDeveloperMessage().equals(ReceiveDocumentConstant.CAN_RECEIVE))
            {
                java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

                ReceiveDocument receiveDocument = new ReceiveDocument();
                receiveDocument.setManifestoId(manifestoIdTMP);
                receiveDocument.setStatusId(request.getStatusId());
                receiveDocument.setCreatedBy(userIdTMP);
                receiveDocument.setCreatedDate(date);
                ReceiveDocument receiveDocumentSave = receiveDocumentRepository.save(receiveDocument);

                if(request.getRemark() != null && !request.getRemark().isEmpty())
                {
                    GenericInformation genericInformation = new GenericInformation();
                    genericInformation.setRefModule(GenericInformationConstant.RECEIVEDOCUMENT_DB);
                    genericInformation.setRefAction(GenericInformationConstant.RECEIVEDOCUMENT_REFACTION);
                    genericInformation.setRefId(receiveDocumentSave.getId());
                    genericInformation.setValue(request.getRemark());
                    genericInformation.setCreatedBy(userIdTMP);
                    genericInformation.setCreatedDate(date);
                    genericInformationRepository.save(genericInformation);
                }

                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ReceiveDocumentConstant.RECEIVE_SUCCESS);

                ReceiveDocumentRespones respones = new ReceiveDocumentRespones();
                respones.setJobId(jobIdTMP);
                respones.setManifestoType(manifestoTypeTMP);
                respones.setManifestoId(manifestoIdTMP);

                response.setData(respones);
            }
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        response.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        return response;
    }

    public GZResponse validateReceiveDocument(ValidateReceiveDocument request)
    {
        GZResponse response = new GZResponse();
        try
        {
            ///Validate user can receive document
            response = validateUserCanReceiveDocument(request.getLoginId());

            if(response.getDeveloperMessage().equals(ReceiveDocumentConstant.USER_HAVE_PERMISSION))
            {
                response = validateMappingStatusAndUser(request.getStatusId());

                if(response.getDeveloperMessage().equals(ReceiveDocumentConstant.STATUS_AND_USER_MATCH))
                {
                    if(request.getRefModuleType().equals("qrCode"))
                    {
                        if(request.getRefId().length() <= 50)
                        {
                            ///Logic for receive document by QR Code
                            response = validateQRNumberNotFoundReceiveDocument(request.getRefId());
                        }
                        else
                        {
                            response.setCode(HttpStatus.BAD_REQUEST);
                            response.setMessage(HttpStatus.BAD_REQUEST.toString());
                            response.setDeveloperMessage(ReceiveDocumentConstant.REFID_LENGTH_LESS_THAN_51);

                            return response;
                        }
                    }
                    else if(request.getRefModuleType().equals("manifestoId")) {
                        ///Logic for receive document by manifestoId
                        response = validateManifestoNotFoundReceiveDocument(Integer.parseInt(request.getRefId()));
                    }
                    else {
                        response.setCode(HttpStatus.BAD_REQUEST);
                        response.setMessage(HttpStatus.BAD_REQUEST.toString());
                        response.setDeveloperMessage(ReceiveDocumentConstant.REFMODULE_NOTFOUND);

                        return response;
                    }

                    ///When after validate manifesto is found
                    if(response.getDeveloperMessage().equals(ReceiveDocumentConstant.MANIFESTO_FOUND))
                    {
                        ///Check ever receive before
                        ReceiveDocument receiveDocument = receiveDocumentRepository.findByDeliveryDateGroupByDriver(manifestoIdTMP, request.getStatusId());

                        if(receiveDocument != null)
                        {
                            response.setCode(HttpStatus.BAD_REQUEST);
                            response.setMessage(HttpStatus.BAD_REQUEST.toString());
                            response.setDeveloperMessage(ReceiveDocumentConstant.EVER_RECEIVE);

                            ReceiveDocumentRespones respones = new ReceiveDocumentRespones();
                            respones.setJobId(jobIdTMP);
                            respones.setManifestoType(manifestoTypeTMP);
                            respones.setManifestoId(manifestoIdTMP);

                            response.setData(respones);
                        }
                        else
                        {
                            ///if FinOps receive checker must be receive before
                            if(userTypeTMP.equals(CommonConstant.FINOPS))
                            {
                                ReceiveDocument receiveDocumentChecker = receiveDocumentRepository.findByDeliveryDateGroupByDriver(manifestoIdTMP, PODNumberStatusConstant.RECEIVE_CHECKER_CONFIRM);
                                if(receiveDocumentChecker == null)
                                {
                                    response.setCode(HttpStatus.BAD_REQUEST);
                                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                                    response.setDeveloperMessage(ReceiveDocumentConstant.STATUS_CANNOT_RECEIVE);

                                    return response;
                                }
                            }

                            ///Check Roll Driver UPC Cannot Check Status
                            boolean check = false;

                            UserTypeMaster user = userTypeMasterRepository.findById(driverIdTMP);
                            if(user != null)
                            {
                                if(user.getUserTypeId() != PODNumberStatusConstant.DRIVERUPC)
                                {
                                    ///Check status can receive document
                                    JobStatus jobStatus = jobStatusRepository.findLastByjobId(manifestoIdTMP);
                                    if(jobStatus != null)
                                    {
                                        int currentStatus = jobStatus.getStatusId();
                                        switch (currentStatus)
                                        {
                                            case PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY :
                                                check = true;
                                                break;
                                            case PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY :
                                                check = true;
                                                break;
                                            case PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY :
                                                check = true;
                                                break;
                                            case PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM :
                                                check = true;
                                                break;
                                            case PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT :
                                                check = true;
                                                break;
                                            default:
                                                check = false;
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    check = true;
                                }

                                ///Cast status can receive
                                if(check)
                                {
                                    response.setCode(HttpStatus.OK);
                                    response.setMessage(HttpStatus.OK.toString());
                                    response.setDeveloperMessage(ReceiveDocumentConstant.CAN_RECEIVE);

                                    ReceiveDocumentRespones respones = new ReceiveDocumentRespones();
                                    respones.setJobId(jobIdTMP);
                                    respones.setManifestoType(manifestoTypeTMP);
                                    respones.setManifestoId(manifestoIdTMP);

                                    response.setData(respones);

                                    return response;
                                }
                                else
                                {
                                    response.setCode(HttpStatus.BAD_REQUEST);
                                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                                    response.setDeveloperMessage(ReceiveDocumentConstant.STATUS_CANNOT_RECEIVE);
                                }
                            }
                            else
                            {
                                response.setCode(HttpStatus.BAD_REQUEST);
                                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                                response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public GZResponse validateUserCanReceiveDocument(int logInId)
    {
        GZResponse response = new GZResponse();
        try
        {
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(logInId);
            if(userTypeMaster != null)
            {
                UserType userType = commonService.getUserType(userTypeMaster.getUserTypeId());
                if(userType != null)
                {
                    userTypeTMP = userType.getName();
                    if(userTypeTMP.equals(CommonConstant.CHECKER) || userTypeTMP.equals(CommonConstant.FINOPS))
                    {
                        response.setDeveloperMessage(ReceiveDocumentConstant.USER_HAVE_PERMISSION);
                        userIdTMP = userTypeMaster.getId();
                    }
                    else
                    {
                        response.setCode(HttpStatus.BAD_REQUEST);
                        response.setMessage(HttpStatus.BAD_REQUEST.toString());
                        response.setDeveloperMessage(ReceiveDocumentConstant.USER_NOTHAVE_PERMISSION);
                    }

                    return response;
                }
            }

            ///Validate user null
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public GZResponse validateMappingStatusAndUser(int statusId)
    {
        GZResponse response = new GZResponse();
        try
        {
            MasterStatus masterStatus = masterStatusRepository.findById(statusId);
            if(masterStatus != null)
            {
                UserTypeMaster userTypeMaster = userTypeMasterRepository.findById(userIdTMP);
                boolean check = false;
                if(userTypeMaster.getUserTypeId() == PODNumberStatusConstant.CHECKER)
                {
                    switch (statusId)
                    {
                        case PODNumberStatusConstant.RECEIVE_CHECKER_CONFIRM :
                            check = true;
                            break;
                        default:
                            check = false;
                            break;
                    }
                }
                else if(userTypeMaster.getUserTypeId() == PODNumberStatusConstant.FINOPS)
                {
                    switch (statusId)
                    {
                        case PODNumberStatusConstant.RECEIVE_FINOP_CONFIRM :
                            check = true;
                            break;
                        default:
                            check = false;
                            break;
                    }
                }

                if(check)
                {
                    response.setDeveloperMessage(ReceiveDocumentConstant.STATUS_AND_USER_MATCH);
                }
                else
                {
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(ReceiveDocumentConstant.STATUS_AND_USER_NOT_MATCH);
                }
            }
            else
            {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ReceiveDocumentConstant.STATUS_REQUEST_NOTFOUND);
            }
        }
        catch (Exception ex) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public GZResponse findManifestoIdByJobId(String jobId, String manifestoType)
    {
        GZResponse response = new GZResponse();
        try
        {
            Manifestolist manifesto ;

            if(manifestoType.equalsIgnoreCase("log"))
            {
                manifesto = manifestolistRepository.findByFulfillId(Integer.parseInt(jobId));
            }
            else
            {
                manifesto = manifestolistRepository.findManifestoBySoNumberNotLog(jobId);
            }

            if(manifesto != null)
            {
                response = validateManifestoNotFoundReceiveDocument(manifesto.getId());
            }
            else
            {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ReceiveDocumentConstant.JOBID_FROM_SOMS_MANIFESTO_NOTFOUND);
            }
        }
        catch (Exception ex) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public GZResponse validateManifestoNotFoundReceiveDocument(int manifestoId)
    {
        GZResponse response = new GZResponse();
        try
        {
            Manifestolist manifestolist = manifestolistRepository.findAllManifestolistById(manifestoId);

            if(manifestolist != null)
            {
                response.setDeveloperMessage(ReceiveDocumentConstant.MANIFESTO_FOUND);
                manifestoIdTMP = manifestolist.getId();
                manifestoTypeTMP = manifestolist.getManifestoType();
                driverIdTMP = manifestolist.getDriverId();

                if(manifestoTypeTMP.equals("Log"))
                {
                    if(manifestolist.getFulfillId() == null)
                    {
                        jobIdTMP = "";
                    }
                    else
                    {
                        jobIdTMP = manifestolist.getFulfillId().toString();
                    }
                }
                else
                {
                    jobIdTMP = manifestolist.getJobId();
                }
            }
            else
            {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ReceiveDocumentConstant.MANIFESTO_NOTFOUND);
            }
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public GZResponse validateQRNumberNotFoundReceiveDocument(String qrNumber)
    {
        GZResponse response = new GZResponse();
        try
        {
            String[] payloads = new String[]{};
            final String secretKey = commonService.generateSignature(payloads);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type","application/json");
            headers.set("secretKey", secretKey);
            Map body = new HashMap();
            HttpEntity<?> entity = new HttpEntity<>(body,headers);
            ResponseEntity<String> res = restTemplate.exchange(DataFromSOMSConstant.URL_SOMS_API + "API/GetQRByQRNumber?QRNumber=" + qrNumber
                    , HttpMethod.GET
                    , entity
                    , String.class);
            if(res.getStatusCode() == HttpStatus.OK)
            {
                JsonNode tree = mapper.readTree(res.getBody());
                JsonNode docRef = tree.path("DocRef");
                JsonNode docName = tree.path("DocName");
                if(!docRef.isMissingNode() || !docName.isMissingNode())
                {
                    String docRefText = docRef.asText();
                    String docNameText = docName.asText();
                    response = findManifestoIdByJobId(docRefText, docNameText);
                }
                else
                {
                    String message = tree.get("Message").asText();
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());

                    if (message.equals(ReceiveDocumentConstant.QRNUMBER_NOTFOUND))
                    {
                        response.setDeveloperMessage(ReceiveDocumentConstant.QRNUMBER_NOTFOUND);
                    }
                    else
                    {
                        response.setDeveloperMessage(message);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }
}
