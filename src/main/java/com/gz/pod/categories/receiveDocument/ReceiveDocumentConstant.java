package com.gz.pod.categories.receiveDocument;

public class ReceiveDocumentConstant {
    ///Response
    public static final String RECEIVE_DOCUMENT = "Receive Document";

    public static final String REFMODULE_NOTFOUND = "receive.document.refModule.not.found";

    public static final String USER_HAVE_PERMISSION = "receive.document.user.have.permission";
    public static final String USER_NOTHAVE_PERMISSION = "receive.document.user.not.have.permission";

    public static final String MANIFESTO_FOUND = "receive.document.manifesto.found";
    public static final String MANIFESTO_NOTFOUND = "receive.document.manifesto.not.found";
    public static final String JOBID_FROM_SOMS_MANIFESTO_NOTFOUND = "receive.document.jobid.from.soms.manifesto.not.found";

    public static final String QRNUMBER_NOTFOUND = "receive.document.qrnumber.not.found";
    public static final String QRNUMBER_FOUND = "receive.document.qrnumber.found";

    public static final String EVER_RECEIVE = "receive.document.ever.receive";

    public static final String STATUS_CANNOT_RECEIVE = "receive.document.status.can.not.receive";
    public static final String CAN_RECEIVE = "receive.document.can.receive";

    public static final String STATUS_REQUEST_NOTFOUND = "receive.document.status.request.not.found";
    public static final String STATUS_AND_USER_MATCH = "receive.document.status.and.user.match";
    public static final String STATUS_AND_USER_NOT_MATCH = "receive.document.status.and.user.not.match";

    public static final String RECEIVE_SUCCESS = "receive.document.receive.success";

    public static final String REFID_LENGTH_LESS_THAN_51 = "receive.document.refId.must.less.than.51";
}
