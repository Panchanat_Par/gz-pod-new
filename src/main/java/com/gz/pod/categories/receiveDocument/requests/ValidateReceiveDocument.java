package com.gz.pod.categories.receiveDocument.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ValidateReceiveDocument {
    @ApiModelProperty(required = true)
    @NotNull(message = "refId can't be null")
    @ApiParam(value = "หมายเลขอ้างอิง", defaultValue = "GZQ-1-20")
    private String refId;

    @ApiModelProperty(required = true)
    @NotNull(message = "refModuleType can't be null")
    @ApiParam(value = "ชนิดข้อมูลอ้างอิง", defaultValue = "qr")
    private String refModuleType;

    @Min(value = 1, message = "loginId must be greater than 0 or can't be null")
    @ApiModelProperty(required = true)
    @NotNull(message = "loginId can't be null")
    @ApiParam(value = "รหัสผู้ใช้", defaultValue = "77")
    private int loginId;

    @Min(value = 1, message = "statusId must be greater than 0 or can't be null")
    @ApiModelProperty(required = true)
    @NotNull(message = "statusId can't be null")
    @ApiParam(value = "รหัสสถานะ", defaultValue = "413")
    private int statusId;
}
