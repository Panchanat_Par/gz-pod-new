package com.gz.pod.categories.receiveDocument;

import com.gz.pod.entities.ReceiveDocument;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReceiveDocumentRepository extends CrudRepository<ReceiveDocument, Integer>
{
    @Query("SELECT r " +
            " FROM ReceiveDocument r " +
            " WHERE manifestoId = ?1 AND statusId = ?2 ")
    ReceiveDocument findByDeliveryDateGroupByDriver(
            @Param("manifestoId") int manifestoId
            , @Param("statusId") int statusId);

    ReceiveDocument findByManifestoIdAndStatusId(int manifestoId, int statusId);
}
