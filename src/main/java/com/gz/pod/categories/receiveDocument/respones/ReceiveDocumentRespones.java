package com.gz.pod.categories.receiveDocument.respones;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ReceiveDocumentRespones {
    private String jobId;
    private String manifestoType;
    private int manifestoId;
}
