package com.gz.pod.categories.receiveDocument;

import com.gz.pod.categories.receiveDocument.requests.UpdateReceiveDocument;
import com.gz.pod.categories.receiveDocument.requests.ValidateReceiveDocument;
import com.gz.pod.response.GZResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("receiveDocument")
@Api(value = "ReceiveDocument", description = "ReceiveDocument", produces = "application/json", tags = {"ReceiveDocument"})
public class ReceiveDocumentController {
    @Autowired
    ReceiveDocumentService receiveDocumentService;

    @GetMapping("/validateReceiveDocument")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity validateReceiveDocument(@Valid ValidateReceiveDocument validateReceiveDocument) {
        return ResponseEntity.ok(receiveDocumentService.validateReceiveDocumentMain(validateReceiveDocument));
    }

    @PutMapping("/updateReceiveDocument")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity updateReceiveDocument(@RequestBody @Valid UpdateReceiveDocument updateReceiveDocument) {
        return ResponseEntity.ok(receiveDocumentService.updateReceiveDocumentMain(updateReceiveDocument));
    }
}
