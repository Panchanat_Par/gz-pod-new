package com.gz.pod.categories.podNumberStatus.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveShippingResponse {
    @Setter
    private String title;
    private HttpStatus code;
    private String message;
    private String developerMessage;
    private List<Integer> dataIsSelected;
    private List<Integer> dataLogNotIsSelected;
    private List<String> dataSoNotIsSelected;
    private int countSoNotOwner;
    private int countLogNotOwner;

    public SaveShippingResponse(String title, HttpStatus code, String message, String developerMessage) {
        this.title = title;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
    }
}
