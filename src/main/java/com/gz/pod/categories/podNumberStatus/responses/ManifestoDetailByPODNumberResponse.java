package com.gz.pod.categories.podNumberStatus.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ManifestoDetailByPODNumberResponse {
    private int manifestoId;
    private String manifestoType;
    private Integer newBoxQty;
    private String boxRemark;
    private Integer itemQty;
    private Integer skuQty;
    private String nameUserUpdateRemark;
    private String remark;
    private String statusRemark;
    private String rejectRemark;
    private String jobId;
    private String status;
    private String note;
    private String contactPerson;
    private String shipToAddress;
    private String sapCustCode;
    private String invoiceNo;
    private String issueType;
    private Integer fulfillId;
    private boolean receive;
    private String receiveRemark;
    private String nameUserReceive;
//    private boolean delay;

    public ManifestoDetailByPODNumberResponse(int manifestoId, String manifestoType,String nameUserUpdateRemark, String remark,String statusRemark, String rejectRemark,String jobId,String status, Integer newBoxQty, Integer itemQty, Integer skuQty, boolean receive,String receiveRemark, String nameUserReceive) {
        this.manifestoId = manifestoId;
        this.manifestoType = manifestoType;
        this.nameUserUpdateRemark = nameUserUpdateRemark;
        this.remark = remark;
        this.statusRemark =  statusRemark;
        this.rejectRemark = rejectRemark;
        this.jobId = jobId;
        this.status = status;
        this.newBoxQty = newBoxQty;
        this.itemQty = itemQty;
        this.skuQty = skuQty;
        this.receive = receive;
        this.receiveRemark = receiveRemark;
        this.nameUserReceive = nameUserReceive;
//        this.delay = delay;

    }

    public ManifestoDetailByPODNumberResponse(int manifestoId, String manifestoType, String nameUserUpdateRemark,String remark,String statusRemark, String rejectRemark, String jobId, String status, String contactPerson, String invoiceNo, String issueType, Integer fulfillId, boolean receive,String receiveRemark, String nameUserReceive) {
        this.manifestoId = manifestoId;
        this.manifestoType = manifestoType;
        this.nameUserUpdateRemark = nameUserUpdateRemark;
        this.remark = remark;
        this.statusRemark =  statusRemark;
        this.rejectRemark = rejectRemark;
        this.jobId = jobId;
        this.status = status;
        this.contactPerson = contactPerson;
        this.invoiceNo = invoiceNo;
        this.issueType = issueType;
        this.fulfillId = fulfillId;
        this.receive = receive;
        this.receiveRemark = receiveRemark;
        this.nameUserReceive = nameUserReceive;
//        this.delay = delay;

    }
}
