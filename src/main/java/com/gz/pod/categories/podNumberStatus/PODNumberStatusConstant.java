package com.gz.pod.categories.podNumberStatus;

public class PODNumberStatusConstant {
    public static final String MANIFESTO_NOT_FOUND = "Manifesto Not Found";
    public static final String PODNUMBER_NOT_FOUND = "PODNumber Not Found";
    public static final String STATUS_AND_USERTYPE_NOT_FOUND = "Status Or UserType Not Found";
    public static final String STATUS_NOT_END = "Status Not End";
    public static final String DRIVER_CAN_NOT_START = "Can not Start other PODNumber is starting";
    public static final String STATUS_NOT_MAP = "Status Or Field Not Mapping";
    public static final String MANIFESTO_NOT_MAP = "Manifesto list Not Mapping";
    public static final String USER_NOT_FOUND = "User Not Found";
    public static final String USER_NOT_ALLOW = "User not allow";
    public static final String FIELDTYPE_NOT_FOUND = "fieldType Not Found";
    public static final String REFID_NOT_FOUND = "refId Not Found";
    public static final String TYPE_DRIVER_ONLY = "FieldType must be driverId";
    public static final String REMARK_IS_REQUEST = "Remark is Request";
    public static final String REMARK_NOT_FOUND = "Remark Not Found";
    public static final String REMARK_REASON_ID_NOT_FOUND = "RemarkReason Id Not Found";
    public static final String MASTER_STATUS_DRIVER_WAIT = "POD.Driver.Status.Wait";
    public static final String PODNUMBER_NOT_UPDATE_STATUS_FOR_MOVE = "Can not update status Because there is a PODNumber move";
    public static final String NOT_IN_CONFIRM = "Not in the status Confirm";
    public static final String WRONG_TAB = "Input wrong tab";

    public static final String CHECKER_APPROVING = "Checker Approving";
    public static final String DRIVER_APPROVING = "Driver Approving";
    public static final String ERROR = "ข้อมูลมีปัญหา";

    //contant driver start
    public static final String UPDATE_PODNUMBERSTATUS = "Update PODNumberStatus";
    public static final String UPDATE_JOBSTATUS = "Update Job Status";
    public static final String UPDATE_PODNUMBERSTATUS_SUCCESS = "Update PODNumberStatus Success";
    public static final String UPDATE_DELAY_SUCCESS = "Update Delay Success";
    public static final String CHECKER_NOT_APPROVE = "Checker Not Approve";
    public static final String DRIVER_NOT_START_DELIVER = "Driver Not Start Deliver";
    public static final String DRIVER_NOT_END_DELIVER = "Driver Not End Deliver";
    public static final String DRIVER_START_PODNUMBER_ACTION = "podNumberStartDeliver";
    public static final String DRIVER_START_JOB_ACTION = "jobStartDeliver";
    public static final String DRIVER_END_PODNUMBER_ACTION = "podNumberEndDeliver";
    public static final String DRIVER_END_JOB_ACTION = "jobEndDeliver";
    public static final String START_IS_REQUEST = "startDate is request";
    public static final String END_IS_REQUEST = "endDate is request";

    public static final String GET_PODNUMBERSTATUSDETAIL = "Get PODNumberStatus Detail";
    public static final String GET_PODNUMBERSTATUSDETAIL_SUCCESS = "Get PODNumberStatus Detail Success";

    public static final int DRIVER = 1;
    public static final String DRIVER_ID_STRING = "driverId";
    public static final String PODNUMBER_STRING = "podNumber";
    public static final int CHECKER = 2;
    public static final int FINOPS = 3;
    public static final int DRIVERUPC = 4;


    public static final String MANIFESTO_DB = "manifesto";
    public static final String PODNUMBERSTATUS_DB = "podNumberStatus";
    public static final String JOBSTATUS_DB = "jobStatus";

    public static final String VALIDATE_UPDATE_STATUS= "Validate Update Status";
    public static final String JOB_STATUS_VALIDATE_SUCCESS = "validate.success";
    public static final String  JOB_STATUS_VALIDATE_FAILED = "validate.failed";
    public static final String  MANIFESTOIDS_IS_REQUEST = "ManifestoIds is request";

    public static final String CHECKER_WAIT_STRING = "POD.Checker.Status.Wait";

    public static final String UPDATE_PODNUMBERSTATUS_BY_SHEDULED = "Update PODNumberStatus and Job by Scheduled";
    public static final String UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS = "Update PODNumberStatus and Job by Scheduled Success";



    // new status pod
    public static final int CHECKSTAMPSTATUS = 12;
    public static final int CANCEL_REQUEST = 22;
    public static final int JOB_DRIVER_SO_CHECKCONFIRM = 21;

    public static final int RECEIVE_CHECKER_CONFIRM = 421;
    public static final int RECEIVE_FINOP_CONFIRM = 431;

    public static final int PODNUMBERSTATUS_DRIVER_CONFIRMALLDELIVERY = 1111;
    public static final int PODNUMBERSTATUS_DRIVER_REJECTALLDELIVERY = 1112;
    public static final int PODNUMBERSTATUS_DRIVER_PARTIALDELIVERY = 1113;
    public static final int PODNUMBERSTATUS_DRIVER_INCOMPLETED = 1114;

    public static final int JOB_DRIVER_SO_WAIT = 210;
    public static final int JOB_DRIVER_CONFIRM = 2111;
    public static final int JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER = 220;
    public static final int JOB_CHECKER_APPROVE = 221;
    public static final int JOB_CHECKER_NOT_APPROVE = 222;
    public static final int JOB_CHECKER_CONFIRM = 2113;
    public static final int JOB_CHECKER_NOT_CONFIRM = 2112;
    public static final int JOB_DRIVER_REJECT = 2114;
    public static final int JOB_DRIVER_START = 2115;
    public static final int JOB_DRIVER_END = 2116;
    public static final int JOB_DRIVER_REJECT_START = 2117;
    public static final int JOB_DRIVER_DELAY = 2118;

    public static final int JOB_DRIVER_SO_CONFIRMALLDELIVERY = 211;
    public static final int JOB_DRIVER_SO_REJECTALLDELIVERY = 212;
    public static final int JOB_DRIVER_SO_PARTIALDELIVERY = 213;
    public static final int JOB_DRIVER_LOG_CONFIRM = 216;
    public static final int JOB_DRIVER_LOG_REJECT = 217;
    public static final int JOB_DRIVER_INCOMPLETED = 219;
    // ------------------------------------

    public static final int DRIVER_WAIT = 110;
    public static final int DRIVER_CONFIRM = 111;
    public static final int DRIVER_REJECT = 112;
    public static final int DRIVER_AWAITING_APPROVAL = 114;
    public static final int DRIVER_START = 115;
    public static final int DRIVER_END = 116;
    public static final int DRIVER_REJECT_DELIVER = 117;

    public static final int CHECKER_WAIT = 120;
    public static final int CHECKER_CONFIRM = 121;
    public static final int CHECKER_REJECT = 122;
    public static final int CHECKER_AWAITING_APPROVAL = 124;
    public static final int CHECKER_APPROVAL = 123;
    public static final int CHECKER_MOVE_SO = 125;

    public static final int DRIVER_DELETE_JOB = 214;
    public static final int JOB_DRIVER_MOVE_SO = 218;
    public static final int JOB_CHECKER_MOVE_SO = 228;

}
