package com.gz.pod.categories.podNumberStatus;

import com.gz.pod.categories.datafromsoms.DataFromSOMSConstant;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberDetailRequest;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberStatusRequest;
import com.gz.pod.categories.podNumberStatus.requests.ValidateUpdateShippingRequest;
import com.gz.pod.categories.podNumberStatus.responses.PODNumberStatusDetailResponse;
import com.gz.pod.response.GZResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("podNumberStatus")
@Api(value = "podNumberStatus", description = "PodNumberStatus management", produces = "application/json", tags = {"podNumberStatus"})
public class PODNumberStatusController {

    @Autowired
    PODNumberStatusService podNumberStatusService;
    //Validate show Detail
    @GetMapping("/detail")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = PODNumberStatusDetailResponse.class)})
    public ResponseEntity getPODNumberDetail(@Valid PODNumberDetailRequest podNumberDetailRequest) throws Exception {
        return ResponseEntity.ok(podNumberStatusService.getPODNumberDetail(podNumberDetailRequest));
    }

    @PutMapping
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity updatePODNumberStatus(@RequestBody @Valid PODNumberStatusRequest podNumberDetailRequest) throws Exception {
        return ResponseEntity.ok(podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest));
    }

    @PostMapping("/changeDeliveryDateForDelay")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity changeDeliveryDateForDelay(@RequestBody @Valid DataFromSOMSRequest dataFromSOMSRequest) throws Exception {
        return ResponseEntity.ok(podNumberStatusService.changeDeliveryDateForDelay(dataFromSOMSRequest));
    }

    @Scheduled(cron = DataFromSOMSConstant.CHANGE_DELIVERY_DATE)
    public void changeDeliveryDateForDelay() throws Exception
    {
        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        podNumberStatusService.changeDeliveryDateForDelay(dataFromSOMSRequest);
    }

    @PostMapping("/stampDelay")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity stampDelay(@RequestBody @Valid DataFromSOMSRequest dataFromSOMSRequest) throws Exception
    {
        return ResponseEntity.ok(podNumberStatusService.stampDelay(dataFromSOMSRequest));
    }

    @Scheduled(cron = DataFromSOMSConstant.STAMP_DELAY_DATE)
    public void stampDelay() throws Exception {
        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        podNumberStatusService.stampDelay(dataFromSOMSRequest);
    }

    @PostMapping("/validateUpdate")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity validateUpdateShipping(@RequestBody @Valid ValidateUpdateShippingRequest validateUpdateShippingRequest) throws Exception {
        return ResponseEntity.ok(podNumberStatusService.validateOwner(validateUpdateShippingRequest));
    }

}
