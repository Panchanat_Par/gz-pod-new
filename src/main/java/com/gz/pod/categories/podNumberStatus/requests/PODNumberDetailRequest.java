package com.gz.pod.categories.podNumberStatus.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class PODNumberDetailRequest {
    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่วันจัดส่ง", defaultValue = "092018-0001")
    @NotNull(message = "podNumber is Request")
    private String podNumber;

    @ApiModelProperty(required = true)
    @NotNull(message = "login Id can't be null")
    @ApiParam(value = "login Id", defaultValue = "1")
    private Integer loginId;

    @ApiModelProperty(required = true)
    @Pattern(regexp = "(shipping|shipped|delivered|approved|wait|checker|finops)", message = "Please check your tab again.")//shipping , shipped or delivered Only
    @NotNull(message = "tab can't be null")
    private String tab;



}
