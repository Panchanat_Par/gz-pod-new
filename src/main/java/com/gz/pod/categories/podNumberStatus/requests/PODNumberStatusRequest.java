package com.gz.pod.categories.podNumberStatus.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
public class PODNumberStatusRequest {

    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "manifestoIds is request")
    private List<Integer> manifestoIds;

    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "refId is request")
    private String refId;

    @ApiParam(value = "ประเภท", defaultValue = "podNumber")
    @NotNull(message = "field not null Ex: driverId , podNumber , job or log.")
    @Pattern(regexp = "(driverId|podNumber|job|log)", message = "driverId , podNumber , job or log Only")
    private String fieldType;

    @ApiModelProperty(required = true)
    @ApiParam(value = "ประเภทผู้เข้าใช้", defaultValue = "1")
    @NotNull(message = "userTypeId is request")
    private Integer userTypeId;

    @ApiModelProperty(required = true)
    @ApiParam(value = "สถานะ")
    @NotNull(message = "statusId is request")
    private Integer statusId;

    @ApiParam(value = "ข้อสังเกต")
    @Length(max = 1000, message = "Remark must be less than 1000 characters")
    private String remark = "";

    @ApiModelProperty(required = true)
    @ApiParam(value = "update By")
    @NotNull(message = "updateBy not null")
    private Integer updateBy;

    @ApiModelProperty(required = true)
    @ApiParam(value = "วันเวลาเริ่มเดินทาง", defaultValue = "2018-10-10 15:00:05")
    private String startDate;

    @ApiModelProperty(required = true)
    @ApiParam(value = "ถึงจุดหมาย", defaultValue = "2018-10-10 15:00:05")
    private String endDate;

    @ApiParam(value = "delayDate")
    private boolean delay = false;

}
