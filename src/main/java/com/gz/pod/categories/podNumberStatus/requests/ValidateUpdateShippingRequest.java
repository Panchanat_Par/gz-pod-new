package com.gz.pod.categories.podNumberStatus.requests;

import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ValidateUpdateShippingRequest {
    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "manifestoIds is request")
    private List<Integer> manifestoIds;

    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "loginId is request")
    private Integer loginId;

    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "loginId is request")
    private Integer statusId = PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER;

}
