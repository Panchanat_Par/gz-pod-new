package com.gz.pod.categories.podNumberStatus.responses;

import lombok.Data;

import java.util.List;

@Data
public class PODNumberStatusDetailResponse {
    private String podNumber;
    private String shipToAddress;
    private String driverName;
    private String routeName;
    private String customerName;
    private String status;
    private String remarkStatus;
    private String userRemark;
    private boolean flgDelay;


    List<ManifestoDetailByPODNumberResponse> manifestoDetailResponses;

//    public PODNumberStatusDetailResponse(String podNumber, String shipToAddress, int routeMasterId, int driverId, String routeName, String customerName, String podNumberStatus, String podStatusChecker, List<Object> manifestoDetailResponses) {
//        this.podNumber = podNumber;
//        this.shipToAddress = shipToAddress;
//        this.routeMasterId = routeMasterId;
//        this.driverId = driverId;
//        this.routeName = routeName;
//        this.customerName = customerName;
//        this.podNumberStatus = podNumberStatus;
//        this.podStatusChecker = podStatusChecker;
//        this.manifestoDetailResponses = manifestoDetailResponses;
//    }

    public PODNumberStatusDetailResponse() {
    }
}
