package com.gz.pod.categories.podNumberStatus;

import com.gz.pod.categories.MasterReasonRejectManifesto.MasterReasonRejectManifestoRepository;
import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.GenericInformationRepository;
import com.gz.pod.categories.common.MasterStatusRepository;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.common.responses.ManifestoOwnerResponse;
import com.gz.pod.categories.datafromsoms.DataFromSOMSService;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.JobStatusToSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.PicIdRequest;
import com.gz.pod.categories.genericInformation.GenericInformationConstant;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestoFile.ManifestoFileRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.manifestolist.ManifestolistService;
import com.gz.pod.categories.manifestolist.responses.ManifestoResponse;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberDetailRequest;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberStatusRequest;
import com.gz.pod.categories.podNumberStatus.requests.ValidateUpdateShippingRequest;
import com.gz.pod.categories.podNumberStatus.responses.ManifestoDetailByPODNumberResponse;
import com.gz.pod.categories.podNumberStatus.responses.PODNumberStatusDetailResponse;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentRepository;
import com.gz.pod.entities.*;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.text.DateFormat;

@Service
public class PODNumberStatusService {

    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    JobStatusRepository jobStatusRepository;

    @Autowired
    CommonService commonService;

    @Autowired
    GenericInformationRepository genericInformationRepository;

    @Autowired
    ManifestoItemRepository manifestoItemRepository;

    @Autowired
    MasterReasonRejectManifestoRepository masterReasonRejectManifestoRepository;

    @Autowired
    ManifestolistService manifestolistService;

    @Autowired
    DataFromSOMSService dataFromSOMSService;

    @Autowired
    ManifestoFileRepository manifestoFileRepository;

    @Autowired
    MasterStatusRepository masterStatusRepository;

    @Autowired
    ReceiveDocumentRepository receiveDocumentRepository;

    @Autowired
     UserTypeMasterRepository userTypeMasterRepository;


    public GZResponse getPODNumberDetail(PODNumberDetailRequest podNumberDetailRequest) throws Exception {
        GZResponse gzResponse;
        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();

        try {
            UserTypeMaster userLogin = commonService.getUserTypeMasterForReturn(podNumberDetailRequest.getLoginId());
            List<Manifestolist> listManifestoList = manifestolistRepository.findAllBypodNumberOrderByIdAsc(podNumberDetailRequest.getPodNumber());

            if(listManifestoList.isEmpty()) {
                return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_FOUND
                );
            }

            if (userLogin == null){
                return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
                );
            }

            if(userLogin.getUserTypeId() == ManifestolistConstant.DRIVER_ID && listManifestoList.get(0).getDriverId() != userLogin.getId() ) {
                return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_MAP
                );
            }

            // set Data and get last status
            List<ManifestoResponse> shippingByTab = listManifestoList.stream().map(
                    shipping -> new ManifestoResponse(
                            shipping.getDriverId(),
                            shipping.getId(),
                            shipping.getManifestoType(),
                            shipping.getJobId(),
                            shipping.getCustomerName(),
                            shipping.getShipToCode(),
                            shipping.getShipToName(),
                            shipping.getShipToAddress(),
                            shipping.getAddressType(),
                            shipping.getBoxQty(),
                            shipping.getNewBoxQty(),
                            shipping.getItemQty(),
                            shipping.getSKUQty(),
                            shipping.getRemark(),
                            shipping.getRouteName(),
                            shipping.getPodNumber(),
                            shipping.getLogType(),
                            shipping.getFulfillId(),
                            shipping.getDeliveryDate(),
                            shipping.getContactPerson(),
                            shipping.getInvoiceNo(),
                            shipping.getNote(),
                            getLastJobStatus(shipping.getId(), shipping.getDeliveryDate()),
                            shipping.getDelayDate()
                    )
            ).collect(Collectors.toList());

            if(shippingByTab.isEmpty()) {
                return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.PODNUMBER_NOT_FOUND );
            }

            shippingByTab = shippingByTab.stream().filter(s -> s.getJobStatus() != null).collect(Collectors.toList());

            podNumberStatusDetailResponse.setPodNumber(podNumberDetailRequest.getPodNumber());
            podNumberStatusDetailResponse.setCustomerName(shippingByTab.get(0).getCustomerName());
            podNumberStatusDetailResponse.setRouteName(shippingByTab.get(0).getRouteName());
            podNumberStatusDetailResponse.setShipToAddress(shippingByTab.get(0).getShipToAddress());

            if (userLogin.getUserTypeId() == PODNumberStatusConstant.CHECKER || userLogin.getUserTypeId() == PODNumberStatusConstant.FINOPS) {
                int checkerDriverId = shippingByTab.get(0).getDriverId();
                String name = userTypeMasterRepository.findDriverNamebyDriverId(checkerDriverId);
                podNumberStatusDetailResponse.setDriverName(name);
            }
            else{
                podNumberStatusDetailResponse.setDriverName(userLogin.getName());
            }

            if (userLogin.getUserTypeId() == PODNumberStatusConstant.DRIVER){
                if (podNumberDetailRequest.getTab().equalsIgnoreCase(ManifestolistConstant.SHIPPING)) {
                    List<Integer> shippingList = new ArrayList<>();
                    shippingList.add(PODNumberStatusConstant.JOB_DRIVER_CONFIRM);
                    shippingList.add(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
                    shippingList.add(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
                    shippingList.add(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
                    shippingList.add(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);
                    shippingByTab = shippingByTab.stream().filter(s -> shippingList.indexOf(s.getJobStatus().getStatusId()) != -1 && s.getDeliveryDate().equals(java.time.LocalDate.now().toString())).collect(Collectors.toList());

                }
                else if (podNumberDetailRequest.getTab().equalsIgnoreCase(ManifestolistConstant.SHIPPED)) {
                    List<Integer> shippedList = new ArrayList<>();
                    shippedList.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                    shippedList.add(PODNumberStatusConstant.JOB_DRIVER_REJECT);
                    shippedList.add(PODNumberStatusConstant.JOB_DRIVER_START);
                    shippedList.add(PODNumberStatusConstant.JOB_DRIVER_END);
                    shippingByTab = shippingByTab.stream().filter(s -> shippedList.indexOf(s.getJobStatus().getStatusId()) != -1 && s.getDeliveryDate().equals(java.time.LocalDate.now().toString())).collect(Collectors.toList());

                }
                else if(podNumberDetailRequest.getTab().equalsIgnoreCase(ManifestolistConstant.DELIVERED)){
                    List<Integer> deliveredList = new ArrayList<>();
                    deliveredList.add(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);
                    deliveredList.add(PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY);
                    deliveredList.add(PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY);
                    deliveredList.add(PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM);
                    deliveredList.add(PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT);
                    shippingByTab = shippingByTab.stream().filter(s -> deliveredList.indexOf(s.getJobStatus().getStatusId()) != -1 && s.getDeliveryDate().equals(java.time.LocalDate.now().toString())).collect(Collectors.toList());
                }
                else {
                    return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.WRONG_TAB );
                }
            }
            else if (userLogin.getUserTypeId() == PODNumberStatusConstant.CHECKER) {
                if (podNumberDetailRequest.getTab().equalsIgnoreCase("approved")){
                    List<Integer> approvedList = new ArrayList<>();
                    approvedList.add(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
                    approvedList.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_REJECT);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_START);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_END);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM);
                    approvedList.add(PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT);
                    shippingByTab = shippingByTab.stream().filter(s -> approvedList.indexOf(s.getJobStatus().getStatusId()) != -1 && s.getDeliveryDate().equals(java.time.LocalDate.now().toString())).collect(Collectors.toList());
                }
                else if (podNumberDetailRequest.getTab().equalsIgnoreCase("wait")) {
                    List<Integer> waitingApprovalList = new ArrayList<>();
                    waitingApprovalList.add(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
                    waitingApprovalList.add(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
                    waitingApprovalList.add(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);
                    shippingByTab = shippingByTab.stream().filter(s -> waitingApprovalList.indexOf(s.getJobStatus().getStatusId()) != -1 && s.getDeliveryDate().equals(java.time.LocalDate.now().toString())).collect(Collectors.toList());
                }
                else if (podNumberDetailRequest.getTab().equalsIgnoreCase("checker")){
                    shippingByTab = shippingByTab.stream().filter(s -> s.getJobStatus() != null).collect(Collectors.toList());
                }
                else {
                    return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.WRONG_TAB );
                }
            }

            else if (userLogin.getUserTypeId() == PODNumberStatusConstant.FINOPS){
                if (podNumberDetailRequest.getTab().equalsIgnoreCase("finops")) {
                    shippingByTab = shippingByTab.stream().filter(s -> s.getJobStatus() != null).collect(Collectors.toList());
                }
                else {
                    return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.WRONG_TAB );
                }
            }

            if(shippingByTab.isEmpty()) {
                return new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.PODNUMBER_NOT_FOUND );
            }

            boolean checkDelay = manifestolistService.findStatusDelayDate(shippingByTab, podNumberDetailRequest.getTab());

            Integer masterId =  manifestolistService.checkShowStatusPODNumber(shippingByTab, userLogin.getUserTypeId(), podNumberDetailRequest.getTab());

            if(masterId != null) {
                MasterStatus masterStatus = masterStatusRepository.findOne(masterId);
                if (shippingByTab.get(0).getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM
                        || shippingByTab.get(0).getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT)
                {
                    podNumberStatusDetailResponse.setRemarkStatus(shippingByTab.get(0).getJobStatus().getRemark());
                }
                else {
                    podNumberStatusDetailResponse.setRemarkStatus("");
                }
                podNumberStatusDetailResponse.setUserRemark(shippingByTab.get(0).getJobStatus().getUserTypeMaster().getName());
                podNumberStatusDetailResponse.setFlgDelay(checkDelay);

                if(masterStatus != null) {
                    podNumberStatusDetailResponse.setStatus(masterStatus.getName());
                }
            } else {
                podNumberStatusDetailResponse.setStatus("");
                podNumberStatusDetailResponse.setRemarkStatus("");
                podNumberStatusDetailResponse.setUserRemark("");
                podNumberStatusDetailResponse.setFlgDelay(checkDelay);
            }

            podNumberStatusDetailResponse.setManifestoDetailResponses(PrepareJobStatus(shippingByTab,podNumberDetailRequest.getTab()));

            gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.OK, HttpStatus.OK.toString(),PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL_SUCCESS );
            gzResponse.setData(podNumberStatusDetailResponse);

        } catch (GZException e) {
            throw new GZException(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getDeveloperMessage()
            );
        }
        return gzResponse;
    }

    public JobStatus getLastJobStatus(int manifestoId, String deliveryDate) {
        return jobStatusRepository.findLastByjobIdAndDelivery(manifestoId, deliveryDate);
    }

    public List<ManifestoDetailByPODNumberResponse> PrepareJobStatus(List<ManifestoResponse> listManifestoList , String tab) throws Exception {
        List<ManifestoDetailByPODNumberResponse> manifestoDetailByPODNumberResponses = new ArrayList<>();

        List<ManifestoResponse> sortedJobList = new ArrayList<>();
        List<ManifestoResponse> logList = listManifestoList.stream()
                .filter(b -> b.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
                .sorted(Comparator.comparing(b -> b.getFulfillId()))
                .collect(Collectors.toList());
        List<ManifestoResponse> soList = listManifestoList.stream()
                .filter(b -> !b.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
//                    .sorted(Comparator.comparing(b -> Long.parseLong(b.getJobId())))
                .sorted(Comparator.comparing(b -> b.getJobId()))
                .collect(Collectors.toList());

        sortedJobList.addAll(soList);
        sortedJobList.addAll(logList);

        String jobRemarkReject = "";

        for (ManifestoResponse item : sortedJobList) {
            ManifestoDetailByPODNumberResponse data;

            boolean receive = false;
            String receiveRemark = "";
            String nameUserReceive = "";
            int checkTab = tab.equalsIgnoreCase(ManifestolistConstant.CHECKER ) || tab.equalsIgnoreCase(ManifestolistConstant.CHECKER_TAB_APPROVE) ? PODNumberStatusConstant.RECEIVE_CHECKER_CONFIRM : PODNumberStatusConstant.RECEIVE_FINOP_CONFIRM;
            ReceiveDocument receiveDocument = receiveDocumentRepository.findByManifestoIdAndStatusId(item.getManifestoId(), checkTab);
            receive = receiveDocument != null;
            if (receiveDocument != null) {
                receiveRemark = manifestolistService.PrepareGeneric(receiveDocument.getId(), GenericInformationConstant.RECEIVEDOCUMENT_DB, GenericInformationConstant.RECEIVEDOCUMENT_REFACTION);
                if (receiveDocument.getUserTypeMaster() != null)
                    nameUserReceive = receiveDocument.getUserTypeMaster().getName();
            }

            if (item.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY || item.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT){
                if (item.getJobStatus() != null) {
                    GenericInformation genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN, item.getJobStatus().getId());
                    if (genericInformation != null) {
                        jobRemarkReject = genericInformation.getValue();
                        if (jobRemarkReject != null && !CommonService.isInteger(jobRemarkReject))
                            jobRemarkReject = PODNumberStatusConstant.REMARK_REASON_ID_NOT_FOUND;

                        MasterReasonRejectManifesto masterReasonRejectManifesto = masterReasonRejectManifestoRepository.findOne(Integer.parseInt(jobRemarkReject));
                        if (masterReasonRejectManifesto != null)
                            jobRemarkReject = masterReasonRejectManifesto.getDescription();
                        else
                            jobRemarkReject = PODNumberStatusConstant.REMARK_REASON_ID_NOT_FOUND;
                    }
                }
            }

            if(item.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG)) {
                data = new ManifestoDetailByPODNumberResponse(
                        item.getManifestoId(),
                        item.getManifestoType(),
                        item.getJobStatus().getUserTypeMaster().getName(),
                        (item.getRemark() == null) ? "" : item.getRemark(),
                        (item.getJobStatus().getRemark() == null) ? "" : item.getJobStatus().getRemark(),
                        jobRemarkReject,
                        item.getJobId(),
                        (item.getJobStatus().getMasterStatus() == null) ? "" : item.getJobStatus().getMasterStatus().getName(),
                        (item.getContactPerson() == null) ? "" : item.getContactPerson(),
                        (item.getInvoiceNo() == null) ? "" : item.getInvoiceNo(),
                        (item.getIssueType() == null) ? "" : item.getIssueType(),
                        item.getFulfillId(),
                        receive,
                        receiveRemark,
                        nameUserReceive
                );
            }
            else {
                data = new ManifestoDetailByPODNumberResponse(
                        item.getManifestoId(),
                        item.getManifestoType(),
                        item.getJobStatus().getUserTypeMaster().getName(),
                        (item.getRemark() == null) ? "" : item.getRemark(),
                        (item.getJobStatus().getRemark() == null) ? "" : item.getJobStatus().getRemark(),
                        jobRemarkReject,
                        item.getJobId(),
                        (item.getJobStatus().getMasterStatus() == null) ? "" : item.getJobStatus().getMasterStatus().getName(),
                        (item.getNewBoxQty() == null) ? 0 : item.getNewBoxQty(),
                        (item.getItemQty() == null) ? 0 : item.getItemQty(),
                        (item.getSkuQty() == null) ? 0 : item.getSkuQty(),
                        receive,
                        receiveRemark,
                        nameUserReceive
                );
            }
            manifestoDetailByPODNumberResponses.add(data);
            jobRemarkReject = "";
        }

        return manifestoDetailByPODNumberResponses;
    }

    public GZResponse validateOwner(ValidateUpdateShippingRequest validateUpdateShippingRequest) throws Exception {
        try {
            UserTypeMaster driver = commonService.getUserTypeMasterForReturn(validateUpdateShippingRequest.getLoginId());
            if(driver == null) {
                return new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);
            }

            List<ManifestoOwnerResponse> manifestoOwnerResponseList = commonService.validateOwnerByManifestoId(validateUpdateShippingRequest.getManifestoIds(), driver.getId());
            if(!manifestoOwnerResponseList.isEmpty()) {
                if(validateUpdateShippingRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER) {
                    if(manifestoOwnerResponseList.size() == validateUpdateShippingRequest.getManifestoIds().size() &&
                            manifestoOwnerResponseList.stream().filter(m -> !m.isOwner()).collect(Collectors.toList()).isEmpty() ) // เช็คว่าเป็นของเค้าและต้องเท่ากัน
                    {
                        return new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.OK,
                                HttpStatus.OK.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_SUCCESS);
                    }
                }
                else if(manifestoOwnerResponseList.stream().filter(m -> !m.isOwner()).collect(Collectors.toList()).isEmpty() ) // เช็คว่าเป็นของเค้ารึเปล่า
                {
                    return new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.OK,
                            HttpStatus.OK.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_SUCCESS);
                }
            }

        } catch (Exception e) {
            throw new GZException(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }
        return new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_FAILED);
    }

    public GZResponse updatePODNumberStatus(PODNumberStatusRequest podNumberDetailRequest) throws Exception {
        GZResponse response = new GZResponse();

        if(podNumberDetailRequest.getUserTypeId() == PODNumberStatusConstant.DRIVER || podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER) {
            ValidateUpdateShippingRequest validateUpdateShippingRequest = new ValidateUpdateShippingRequest();
            validateUpdateShippingRequest.setManifestoIds(podNumberDetailRequest.getManifestoIds());
            validateUpdateShippingRequest.setLoginId(podNumberDetailRequest.getUpdateBy());
            validateUpdateShippingRequest.setStatusId(podNumberDetailRequest.getStatusId());
            GZResponse validateDataInUser = validateOwner(validateUpdateShippingRequest);

            if(validateDataInUser.getCode() != HttpStatus.OK) {
                return new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_FAILED);
            }
        }

        UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(podNumberDetailRequest.getUpdateBy());
        if(userTypeMaster == null) {
            return new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);
        }

        try {
            String deliveryDate;
            Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            deliveryDate = formatter.format(new Date());

            if(PODNumberStatusConstant.CHECKSTAMPSTATUS == podNumberDetailRequest.getStatusId()) {
                response = checkStatusUpdateByCheckerConfirmAll(podNumberDetailRequest, deliveryDate, currentDate, userTypeMaster.getId());
            } else if (PODNumberStatusConstant.CANCEL_REQUEST == podNumberDetailRequest.getStatusId()) {
                response = cancelRequest(podNumberDetailRequest, deliveryDate, currentDate, userTypeMaster.getId());
            } else {
                if(podNumberDetailRequest.getStatusId() != PODNumberStatusConstant.JOB_DRIVER_SO_CHECKCONFIRM)
                    commonService.isCheckMasterStatus(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, podNumberDetailRequest.getStatusId(), podNumberDetailRequest.getUserTypeId());

                switch (podNumberDetailRequest.getFieldType())
                {
                    case "driverId":
                        response = updateJobStatusNew(podNumberDetailRequest, deliveryDate, currentDate, userTypeMaster.getId());
                        break;

                    case "podNumber":
                        if (podNumberDetailRequest.isDelay() && podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM)
                            response = updateDelayDate(podNumberDetailRequest, deliveryDate, currentDate, userTypeMaster.getId());
                        else if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_START || podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_END)
                            response = driverStartAndEndShipped(podNumberDetailRequest, deliveryDate, userTypeMaster.getId());
                        else
                            response = updateJobStatusNew(podNumberDetailRequest, deliveryDate, currentDate, userTypeMaster.getId());
                        break;

                    case "job":
                        if ((podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_CHECKCONFIRM ||
                                podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY) &&
                                podNumberDetailRequest.getUserTypeId() == PODNumberStatusConstant.DRIVER) {
                            response = updateJobStatus(podNumberDetailRequest, deliveryDate);
                        } else {
                            response.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS);
                            response.setCode(HttpStatus.BAD_REQUEST);
                            response.setMessage(HttpStatus.BAD_REQUEST.toString());
                            response.setDeveloperMessage(PODNumberStatusConstant.STATUS_NOT_MAP);
                        }
                        break;

                    case "log":
                        if ((podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM ||
                                podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT) &&
                                podNumberDetailRequest.getUserTypeId() == PODNumberStatusConstant.DRIVER) {
                            response = updateJobStatus(podNumberDetailRequest, deliveryDate);
                        } else {
                            response.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS);
                            response.setCode(HttpStatus.BAD_REQUEST);
                            response.setMessage(HttpStatus.BAD_REQUEST.toString());
                            response.setDeveloperMessage(PODNumberStatusConstant.STATUS_NOT_MAP);
                        }
                        break;
                    default:
                        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);
                }
            }
        } catch (GZException e) {
            throw new GZException(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getDeveloperMessage()  );
        }
        return response;
    }

    public GZResponse updateJobStatusNew(PODNumberStatusRequest podNumberDetailRequest, String deliveryDate, Timestamp currentDate, int driverId) throws Exception {
        int status = podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT_START ? PODNumberStatusConstant.JOB_DRIVER_REJECT_START : 0;
        List<Integer> listStatus = new ArrayList<>();
        if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER) {
            listStatus.add(PODNumberStatusConstant.JOB_DRIVER_CONFIRM);
            listStatus.add(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
        } else if(podNumberDetailRequest.getUserTypeId() == PODNumberStatusConstant.CHECKER) {
            listStatus.add(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
            listStatus.add(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);
            listStatus.add(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        } else if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT) {
            listStatus.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        } else if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT_START) {
            listStatus.add(PODNumberStatusConstant.JOB_DRIVER_START);
            podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        }

        GZResponse gzResponse = validateUpdateStatusWithMatch(podNumberDetailRequest, listStatus, deliveryDate);
        if(gzResponse.getCode() == HttpStatus.OK) {
            for (Integer item : podNumberDetailRequest.getManifestoIds()) {
                jobStatusRepository.save(
                        new JobStatus(item, podNumberDetailRequest.getStatusId(), podNumberDetailRequest.getRemark().equalsIgnoreCase("") ? null : podNumberDetailRequest.getRemark(), driverId, currentDate)
                );

                // stamp status to soms
                if(status != PODNumberStatusConstant.JOB_DRIVER_REJECT_START)
                    status = podNumberDetailRequest.getStatusId();

                stampStatusToSOMS(item, status);
            }
        } else {
            return gzResponse;
        }

        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);
    }

    public void stampStatusToSOMS(Integer manifestoId, Integer statusId) throws Exception {
        Manifestolist manifestolist = manifestolistRepository.findOne(manifestoId);
        if(manifestolist != null)
        {
            JobStatus jobStatus = jobStatusRepository.findLastByjobIdAndDelivery(manifestolist.getId(), manifestolist.getDeliveryDate());
            if (jobStatus != null )
            {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String createDate = formatter.format(jobStatus.getCreatedDateUTC());
                String manifestoType = manifestolist.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG) ? ManifestolistConstant.LOG : ManifestolistConstant.ORDER;

                String remark = "";
                String jobId;
                MasterStatus masterStatus = masterStatusRepository.findById(statusId);
                if(masterStatus.isUpdateSOMS()) {

                    if(jobStatus.getRemark() != null && !jobStatus.getRemark().equalsIgnoreCase(""))
                        remark = jobStatus.getRemark();

                    GenericInformation genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN, jobStatus.getId());
                    if(genericInformation != null) {
                        MasterReasonRejectManifesto masterReasonRejectManifesto = masterReasonRejectManifestoRepository.findOne(Integer.parseInt(genericInformation.getValue()));
                        remark += masterReasonRejectManifesto != null ? "|" + masterReasonRejectManifesto.getDescription() : "";
                    }

                    if(manifestoType.equals("log")) {
                        jobId = String.valueOf(manifestolist.getFulfillId());

                        GenericInformation genericInformation2 = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS, jobStatus.getId());
                        remark += genericInformation2 != null ? "|" + genericInformation2.getValue() : "";
                    } else {
                        jobId = manifestolist.getJobId();
                    }

                    JobStatusToSOMSRequest jobStatusToSOMSRequest;
                    if(masterStatus.isSendMail()) {
                        List<PicIdRequest> listPicId = manifestoFileRepository.findBymanifestoIdOrderByIdDesc(manifestolist.getId()).stream().map(m -> new PicIdRequest( m.getId() ) ).collect(Collectors.toList());
                        jobStatusToSOMSRequest = new JobStatusToSOMSRequest(jobId, manifestoType, remark, statusId, createDate, manifestolist.getDriverId(), listPicId);
                    } else {
                        jobStatusToSOMSRequest = new JobStatusToSOMSRequest(jobId, manifestoType, remark, statusId, createDate, jobStatus.getCreatedBy());
                    }

                    try {
                        dataFromSOMSService.callAPIPostStampStatusOrder(jobStatusToSOMSRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public GZResponse updateDelayDate(PODNumberStatusRequest podNumberDetailRequest, String deliveryDate, Timestamp currentDate, int driverId) throws Exception {
        if(!podNumberDetailRequest.getFieldType().equalsIgnoreCase("podNumber")) {
            return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.STATUS_NOT_MAP);
        }

        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        listStatus.add(PODNumberStatusConstant.JOB_DRIVER_REJECT);

        GZResponse gzResponse = validateUpdateStatusWithMatch(podNumberDetailRequest, listStatus, deliveryDate);

        if(gzResponse.getCode() == HttpStatus.OK) {
            for (Integer item : podNumberDetailRequest.getManifestoIds()) {
                JobStatus lastJobStatus = manifestolistService.getLastJobStatus(item, deliveryDate);
                if(lastJobStatus != null && lastJobStatus.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT) {
                    jobStatusRepository.save(
                            new JobStatus(item, PODNumberStatusConstant.JOB_CHECKER_CONFIRM, podNumberDetailRequest.getRemark().equalsIgnoreCase("") ? null : podNumberDetailRequest.getRemark(), driverId, currentDate)
                    );
                }
                Manifestolist manifestoUpdate = manifestolistRepository.findOne(item);
                manifestoUpdate.setDelayDate(manifestoUpdate.getDeliveryDate());
                manifestolistRepository.save(manifestoUpdate);

                //stamp delay status to soms
                stampStatusToSOMS(item, PODNumberStatusConstant.JOB_DRIVER_DELAY);
            }
        } else {
            return gzResponse;
        }

        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_DELAY_SUCCESS);
    }

    public GZResponse updateJobStatus(PODNumberStatusRequest podNumberDetailRequest, String deliveryDate) throws Exception {
        GZResponse response = new GZResponse();
        int statusId = podNumberDetailRequest.getStatusId();

        List<Integer> listDeliveredCondition = new ArrayList<>();
        listDeliveredCondition.add(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);
        listDeliveredCondition.add(PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY);
        listDeliveredCondition.add(PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY);
        listDeliveredCondition.add(PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM);
        listDeliveredCondition.add(PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT);

        if(!CommonService.isInteger(podNumberDetailRequest.getRefId()))
        {
            return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REFID_NOT_FOUND);
        }

        Manifestolist manifestolist = manifestolistRepository.findOne(Integer.parseInt(podNumberDetailRequest.getRefId()));
        if (manifestolist == null) {
            return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_FOUND);
        }

        JobStatus checkJobStatus = jobStatusRepository.findLastByjobIdAndDelivery(manifestolist.getId(), deliveryDate);
        if(checkJobStatus == null || checkJobStatus.getStatusId() != PODNumberStatusConstant.JOB_DRIVER_END) {
            return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.STATUS_NOT_END);
        }

        if(podNumberDetailRequest.getStatusId() == ManifestolistConstant.JOB_DRIVER_REJECTALLDELIVERY && (podNumberDetailRequest.getRemark() == null || podNumberDetailRequest.getRemark().equals(""))) {
            response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ManifestolistConstant.UPDATE_UNDELETE_JOB_REMARK_NULL);
            return response;
        }


        if(podNumberDetailRequest.getFieldType().equalsIgnoreCase("job")) {
            List<ManifestoItem> manifestoItemList = manifestoItemRepository.findBymanifestoId(Integer.parseInt(podNumberDetailRequest.getRefId()));
            if(manifestoItemList == null || manifestoItemList.isEmpty())
            {
                return new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.MANIFESTO_ITEM_NOT_FOUND);
            }
            boolean checkQuantityAllZero = manifestoItemList.stream().filter(m -> m.getNewQuantity() == 0).collect(Collectors.toList()).size() == manifestoItemList.size();
            if (podNumberDetailRequest.getStatusId() != ManifestolistConstant.JOB_DRIVER_REJECTALLDELIVERY && !checkQuantityAllZero ) {
                for (ManifestoItem manifestoItem:manifestoItemList) {
                    if(manifestoItem.getQuantity() > manifestoItem.getNewQuantity()) {
                        statusId = ManifestolistConstant.JOB_DRIVER_PARTIALDELIVERY;
                        break;
                    }
                    if(manifestoItem.getQuantity().intValue() == manifestoItem.getNewQuantity().intValue()) {
                        statusId = ManifestolistConstant.JOB_DRIVER_CONFIRMALLDELIVERY;
                    }
                }
            } else {
                podNumberDetailRequest.setStatusId(ManifestolistConstant.JOB_DRIVER_REJECTALLDELIVERY);
                statusId = ManifestolistConstant.JOB_DRIVER_REJECTALLDELIVERY;
                for (ManifestoItem manifestoItem : manifestoItemList) {
                    manifestoItem.setNewQuantity(0);
                    manifestoItemRepository.save(manifestoItem);
                }
            }
        }

        String remarkDropdown = "";
        String remarkLog = "";
        String remarkText = (podNumberDetailRequest.getRemark() == null) ? "" : podNumberDetailRequest.getRemark();

        if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY ||
                podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM ||
                podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT) {
            String[] remarks = podNumberDetailRequest.getRemark().split("\\|");
            remarkText = remarks[0];
            if(remarks.length == 3) {
                remarkDropdown = remarks[1];
                remarkLog = remarks[2];
            }
            else if(remarks.length == 2){
                remarkDropdown = remarks[1];
            }
            if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM) {
                remarkLog = remarkDropdown;
                remarkDropdown = "";
            }
        }

        //check remark
        if(podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY ||
                podNumberDetailRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT) {
            if(remarkDropdown.equals(""))
                return new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REMARK_IS_REQUEST);

            if(!CommonService.isInteger(remarkDropdown))
                return new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REMARK_NOT_FOUND);

            MasterReasonRejectManifesto masterReasonRejectManifesto = masterReasonRejectManifestoRepository.findOne(Integer.parseInt(remarkDropdown));
            if(masterReasonRejectManifesto == null)
                return new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REMARK_NOT_FOUND);
        }

        // update Job status
        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

        int createdBy = commonService.getUserTypeMaster(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, podNumberDetailRequest.getUpdateBy()).getId();
        JobStatus jobStatus = new JobStatus(Integer.parseInt(podNumberDetailRequest.getRefId()), statusId, remarkText,
                createdBy, currentDate);

        JobStatus saved = jobStatusRepository.save(jobStatus);

        if (listDeliveredCondition.contains(saved.getStatusId()))
        {
            Manifestolist manifestoSetDelay = manifestolistRepository.findAllManifestolistById(Integer.parseInt(podNumberDetailRequest.getRefId()));

            if (manifestoSetDelay != null && manifestoSetDelay.getDelayDate() != null)
            {
                if (manifestoSetDelay.getDelayDate().equalsIgnoreCase(manifestoSetDelay.getDeliveryDate()))
                {
                    manifestoSetDelay.setDelayDate(null);
                    manifestolistRepository.save(manifestoSetDelay);
                }
            }
        }

        // add remark jobStatus after create jobStatus
        if(!remarkDropdown.equals(""))
        {
            GenericInformation genericInformation = new GenericInformation(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN, saved.getId(), remarkDropdown, createdBy, currentDate);
            genericInformationRepository.save(genericInformation);
        }
        if(!remarkLog.equals(""))
        {
            GenericInformation genericInformation = new GenericInformation(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS, saved.getId(), remarkLog, createdBy, currentDate);
            genericInformationRepository.save(genericInformation);
        }


        //stamp status to soms
        stampStatusToSOMS(Integer.parseInt(podNumberDetailRequest.getRefId()), statusId);

        response.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS_SUCCESS);

        return response;
    }

    public GZResponse cancelRequest(PODNumberStatusRequest podNumberDetailRequest, String deliveryDate, Timestamp currentDate, int driverId) throws Exception {
        if(!podNumberDetailRequest.getFieldType().equalsIgnoreCase(PODNumberStatusConstant.DRIVER_ID_STRING)) {
            return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);
        }
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        listStatus.add(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
        listStatus.add(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);

        GZResponse gzResponse = validateUpdateStatusWithMatch(podNumberDetailRequest, listStatus, deliveryDate);

        if(gzResponse.getCode() == HttpStatus.OK) {
            for (Integer item : podNumberDetailRequest.getManifestoIds()) {
                jobStatusRepository.save(
                        new JobStatus(item, PODNumberStatusConstant.JOB_DRIVER_CONFIRM, podNumberDetailRequest.getRemark().equalsIgnoreCase("") ? null : podNumberDetailRequest.getRemark(), driverId, currentDate)
                );
            }
        } else {
            return gzResponse;
        }

        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);
    }

    public GZResponse checkStatusUpdateByCheckerConfirmAll(PODNumberStatusRequest podNumberDetailRequest, String deliveryDate, Timestamp currentDate, int driverId) throws Exception {
        if(!podNumberDetailRequest.getFieldType().equalsIgnoreCase(PODNumberStatusConstant.DRIVER_ID_STRING)) {
            return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);
        }

        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
        listStatus.add(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);

        GZResponse gzResponse = validateUpdateStatusWithMatch(podNumberDetailRequest, listStatus, deliveryDate);
        if(gzResponse.getCode() == HttpStatus.OK) {
            for (Integer item : podNumberDetailRequest.getManifestoIds()) {
                JobStatus jobStatus = manifestolistService.getLastJobStatus(item, deliveryDate);
                if (jobStatus.getStatusId() == PODNumberStatusConstant.JOB_CHECKER_APPROVE) {
                    JobStatus saved = jobStatusRepository.save(
                            new JobStatus(item, PODNumberStatusConstant.JOB_CHECKER_CONFIRM, jobStatus.getRemark(), driverId, currentDate)
                    );

                    //stamp status to soms
                    stampStatusToSOMS(item, PODNumberStatusConstant.JOB_CHECKER_CONFIRM);

                    boolean checkPR = checkStatusPostponeAndReject(saved, deliveryDate, currentDate);
                    if(!checkPR) {
                        checkOldPodNumberInStatus(saved, deliveryDate, currentDate);
                    }
                } else if (jobStatus.getStatusId() == PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE) {
                    jobStatusRepository.save(
                            new JobStatus(item, PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM, jobStatus.getRemark(), driverId, currentDate)
                    );

                    //stamp status to soms
                    stampStatusToSOMS(item, PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
                }
            }
        } else {
            return gzResponse;
        }

        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);
    }

    public boolean checkStatusPostponeAndReject(JobStatus saved, String deliveryDate, Timestamp currentDate) throws Exception {
        boolean response = false;
        Manifestolist manifestolist = manifestolistRepository.findOne(saved.getJobId());
        List<Manifestolist> listManifestoByPodNumber = manifestolistRepository.findAllBypodNumberAndDeliveryDateOrderByIdAsc(manifestolist.getPodNumber(), deliveryDate);
        for (Manifestolist item: listManifestoByPodNumber) {
            JobStatus lastJobStatus = jobStatusRepository.findLastByjobIdAndDelivery(item.getId(), deliveryDate);
            if(lastJobStatus != null) {
                if(lastJobStatus.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT)
                {
                    jobStatusRepository.save(new JobStatus(item.getId(), PODNumberStatusConstant.JOB_CHECKER_CONFIRM, null, saved.getCreatedBy(), currentDate));
                    response = true;

                }
                if(item.getDelayDate() != null && item.getDelayDate().equalsIgnoreCase(item.getDeliveryDate()))
                {
                    item.setDelayDate(null);
                    manifestolistRepository.save(item);
                    response = true;
                }

                if(response) {
                    // stamp status to soms
                    stampStatusToSOMS(item.getId(), PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                }

            }
        }

        return response;
    }

    public void checkOldPodNumberInStatus(JobStatus saved, String deliveryDate, Timestamp currentDate) throws Exception {
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_DRIVER_END);
        Manifestolist manifestolist = manifestolistRepository.findOne(saved.getJobId());
        List<Integer> listManifestoByPodNumber = manifestolistRepository.findByDeliveryDateAndPodNumberWithManifestoId(deliveryDate, manifestolist.getPodNumber());
        List<Integer> jobStatusEnd = jobStatusRepository.findLastJobStatusByListJobId(listManifestoByPodNumber, deliveryDate, listStatus);
        // check pod end
        if(!jobStatusEnd.isEmpty()) {
            List<Integer> jobStatusStart = jobStatusRepository.findLastSecondJobStatusByJobIdReturnId(jobStatusEnd.get(0), deliveryDate);
            // stamp start
            JobStatus oldJobStatusStart = jobStatusRepository.findOne(jobStatusStart.get(0));
            if(oldJobStatusStart != null)
            {
                JobStatus newSavedStart = jobStatusRepository.save(new JobStatus(saved.getJobId(), PODNumberStatusConstant.JOB_DRIVER_START, oldJobStatusStart.getRemark(), saved.getCreatedBy(), currentDate));

                GenericInformation oldGenericInformationStart = genericInformationRepository.findByRefModuleAndRefActionAndRefId(PODNumberStatusConstant.JOBSTATUS_DB, PODNumberStatusConstant.DRIVER_START_JOB_ACTION, oldJobStatusStart.getId());
                if(oldGenericInformationStart != null)
                    genericInformationRepository.save(new GenericInformation(PODNumberStatusConstant.JOBSTATUS_DB, PODNumberStatusConstant.DRIVER_START_JOB_ACTION, newSavedStart.getId(), oldGenericInformationStart.getValue(), saved.getCreatedBy(), currentDate));

                // stamp status to soms
                stampStatusToSOMS(saved.getJobId(), PODNumberStatusConstant.JOB_DRIVER_START);

                // stamp end
                JobStatus oldJobStatusEnd = jobStatusRepository.findLastByjobId(jobStatusEnd.get(0));
                if(oldJobStatusEnd != null) {
                    JobStatus newSavedEnd = jobStatusRepository.save(new JobStatus(saved.getJobId(), PODNumberStatusConstant.JOB_DRIVER_END, oldJobStatusEnd.getRemark(), saved.getCreatedBy(), currentDate));

                    GenericInformation oldGenericInformationEnd = genericInformationRepository.findByRefModuleAndRefActionAndRefId(PODNumberStatusConstant.JOBSTATUS_DB, PODNumberStatusConstant.DRIVER_END_JOB_ACTION, oldJobStatusEnd.getId());
                    if (oldGenericInformationEnd != null)
                        genericInformationRepository.save(new GenericInformation(PODNumberStatusConstant.JOBSTATUS_DB, PODNumberStatusConstant.DRIVER_END_JOB_ACTION, newSavedEnd.getId(), oldGenericInformationEnd.getValue(), saved.getCreatedBy(), currentDate));
                }

                // stamp status to soms
                stampStatusToSOMS(saved.getJobId(), PODNumberStatusConstant.JOB_DRIVER_END);
            }
        }
        else
        {
            // check pod starting
            listStatus = new ArrayList<>();
            listStatus.add(PODNumberStatusConstant.JOB_DRIVER_START);
            List<Integer> jobStatusStart = jobStatusRepository.findLastJobStatusByListJobId(listManifestoByPodNumber, deliveryDate, listStatus);
            if(!jobStatusStart.isEmpty())
            {
                JobStatus oldJobStatusStart = jobStatusRepository.findLastByjobId(jobStatusStart.get(0));
                if(oldJobStatusStart != null)
                {
                    JobStatus newSavedStart = jobStatusRepository.save(new JobStatus(saved.getJobId(), PODNumberStatusConstant.JOB_DRIVER_START, oldJobStatusStart.getRemark(), saved.getCreatedBy(), currentDate));

                    GenericInformation oldGenericInformationStart = genericInformationRepository.findByRefModuleAndRefActionAndRefId(PODNumberStatusConstant.JOBSTATUS_DB, PODNumberStatusConstant.DRIVER_START_JOB_ACTION, oldJobStatusStart.getId());
                    if (oldGenericInformationStart != null)
                        genericInformationRepository.save(new GenericInformation(PODNumberStatusConstant.JOBSTATUS_DB, PODNumberStatusConstant.DRIVER_START_JOB_ACTION, newSavedStart.getId(), oldGenericInformationStart.getValue(), saved.getCreatedBy(), currentDate));

                    // stamp status to soms
                    stampStatusToSOMS(saved.getJobId(), PODNumberStatusConstant.JOB_DRIVER_START);
                }
            }
        }
    }

    public GZResponse validateUpdateStatusWithMatch(PODNumberStatusRequest podNumberDetailRequest, List<Integer> listStatus, String deliveryDate) throws Exception {
        List<Integer> manifestolists;
        try {
            switch (podNumberDetailRequest.getFieldType()) {
                case "driverId":
                    if(!CommonService.isInteger(podNumberDetailRequest.getRefId())) {
                        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REFID_NOT_FOUND);
                    }
                    UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(Integer.parseInt(podNumberDetailRequest.getRefId()));
                    if(userTypeMaster == null) {
                        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);
                    }
                    manifestolists = manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(deliveryDate, userTypeMaster.getId());
                    break;

                case "podNumber":
                    manifestolists = manifestolistRepository.findByDeliveryDateAndPodNumberWithManifestoId(deliveryDate, podNumberDetailRequest.getRefId());
                    break;

                default:
                    return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                            HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);
            }

            List<Integer> listManifestoTodayTemp = jobStatusRepository.findLastJobStatusByListJobId(manifestolists, deliveryDate, listStatus); // list page shipping now no list at approved
            List<Integer> listManifestoToday = jobStatusRepository.findLastJobStatusByListJobId(podNumberDetailRequest.getManifestoIds(), deliveryDate, listStatus); // list page shipping now no list at approved

            if(listManifestoToday.isEmpty() || listManifestoTodayTemp.isEmpty() ) {
                return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.NOT_IN_CONFIRM);
            } else if(listManifestoTodayTemp.size() != podNumberDetailRequest.getManifestoIds().size() ||
                    listManifestoToday.size() != podNumberDetailRequest.getManifestoIds().size()
            ) {
                return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_MAP);
            }
        } catch (Exception e) {
            throw new GZException(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }

        return new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), "");
    }

    public GZResponse driverStartAndEndShipped(PODNumberStatusRequest podNumberStatusRequest, String deliveryDate, int driverId) throws Exception {
        GZResponse response = new GZResponse();
        try {

            if (podNumberStatusRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_START){
                // Start
                if(podNumberStatusRequest.getStartDate() == null || podNumberStatusRequest.getStartDate().equals("")) {
                    response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(PODNumberStatusConstant.START_IS_REQUEST);
                }else{
                        //delcare job status which you want to find status
                    List<Integer> listStatusCondition = new ArrayList<>();
                    listStatusCondition.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                    listStatusCondition.add(PODNumberStatusConstant.JOB_DRIVER_REJECT);
                    GZResponse checkMaiching = validateUpdateStatusWithMatch(podNumberStatusRequest, listStatusCondition, deliveryDate);
                    if(checkMaiching.getCode() == HttpStatus.OK) {
                               listStatusCondition.clear();
                               List<Integer> manifestolists = manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(deliveryDate, driverId);
                               listStatusCondition.add(PODNumberStatusConstant.JOB_DRIVER_START);
                               listStatusCondition.add(PODNumberStatusConstant.JOB_DRIVER_END);
                               List<Integer> listJobStart = jobStatusRepository.findLastJobStatusByListJobId(manifestolists, deliveryDate, listStatusCondition);
                               if (listJobStart.isEmpty()){
                                   response = saveDriverConfirmAndRejectShipping(podNumberStatusRequest, PODNumberStatusConstant.DRIVER_START_JOB_ACTION, driverId);
                               }else{
                                   response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
                                   response.setCode(HttpStatus.BAD_REQUEST);
                                   response.setMessage(HttpStatus.BAD_REQUEST.toString());
                                   response.setDeveloperMessage(PODNumberStatusConstant.DRIVER_CAN_NOT_START);
                               }
                           }else{
                               response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
                               response.setCode(HttpStatus.BAD_REQUEST);
                               response.setMessage(HttpStatus.BAD_REQUEST.toString());
                               response.setDeveloperMessage(PODNumberStatusConstant.MANIFESTO_NOT_MAP);
                           }
                }
            }else{
                // END
                if(podNumberStatusRequest.getEndDate() == null || podNumberStatusRequest.getEndDate().equals("")) {
                    response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(PODNumberStatusConstant.END_IS_REQUEST);
                } else {
                    //delcare job status which you want to find status
                    List<Integer> listStatusCondition = new ArrayList<>();
                    listStatusCondition.add(PODNumberStatusConstant.JOB_DRIVER_START);
                    listStatusCondition.add(PODNumberStatusConstant.JOB_DRIVER_END);
                    GZResponse checkMaiching = validateUpdateStatusWithMatch(podNumberStatusRequest, listStatusCondition, deliveryDate);
                    if(checkMaiching.getCode() == HttpStatus.OK) {
                        listStatusCondition.clear();
                        List<Integer> manifestolists = manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(deliveryDate, driverId);
                        listStatusCondition.add(PODNumberStatusConstant.JOB_DRIVER_START);
                        List<Integer> listJobStart = jobStatusRepository.findLastJobStatusByListJobId(manifestolists, deliveryDate, listStatusCondition);
                        if (!listJobStart.isEmpty()){
                            response = saveDriverConfirmAndRejectShipping(podNumberStatusRequest, PODNumberStatusConstant.DRIVER_END_JOB_ACTION, driverId);
                        }else{
                            response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
                            response.setCode(HttpStatus.BAD_REQUEST);
                            response.setMessage(HttpStatus.BAD_REQUEST.toString());
                            response.setDeveloperMessage(PODNumberStatusConstant.DRIVER_NOT_START_DELIVER);
                        }
                    }else{
                        response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
                        response.setCode(HttpStatus.BAD_REQUEST);
                        response.setMessage(HttpStatus.BAD_REQUEST.toString());
                        response.setDeveloperMessage(PODNumberStatusConstant.MANIFESTO_NOT_MAP);
                    }
                }
            }

        } catch (Exception e) {
            throw new GZException(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }
        return response;
    }

    public GZResponse saveDriverConfirmAndRejectShipping(PODNumberStatusRequest podNumberStatusRequest, String refAction, int driverId) throws Exception {
        GZResponse response = new GZResponse();
        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());
        String date;
        Integer jobStatus;
        if(podNumberStatusRequest.getStatusId() == PODNumberStatusConstant.JOB_DRIVER_START) {
            date = podNumberStatusRequest.getStartDate();
            jobStatus = PODNumberStatusConstant.JOB_DRIVER_START;
        } else {
            date = podNumberStatusRequest.getEndDate();
            jobStatus = PODNumberStatusConstant.JOB_DRIVER_END;
        }

        for (Integer manifestoId: podNumberStatusRequest.getManifestoIds()) {

            JobStatus saved = jobStatusRepository.save(new JobStatus(manifestoId, jobStatus, null, driverId, currentDate));
            GenericInformation genericInformation = new GenericInformation(PODNumberStatusConstant.JOBSTATUS_DB, refAction, saved.getId(), date, driverId, currentDate);
            genericInformationRepository.save(genericInformation);

            //stamp status to soms
            stampStatusToSOMS(manifestoId, jobStatus);
        }

        response.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        return response;
    }

    public GZResponse changeDeliveryDateForDelay(DataFromSOMSRequest request) throws Exception
    {
        try
        {
            /// call job stampDelay again
            stampDelay(request);

            if (request.getDeliveryDate() == null || request.getDeliveryDate().equals(""))
                request.setDeliveryDate(java.time.LocalDate.now().toString());

            List<Integer> manifestoIdList = manifestolistRepository.findIdByDeliveryDateAndDelayDateIsNotNull(request.getDeliveryDate());

            if(!manifestoIdList.isEmpty())
            {
                List<Integer> statusIdList = new ArrayList<>();
                statusIdList.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                statusIdList.add(PODNumberStatusConstant.JOB_DRIVER_START);
                statusIdList.add(PODNumberStatusConstant.JOB_DRIVER_END);

                List<JobStatus> jobStatusList = jobStatusRepository.findJobIdByStatusIdListAndDelivery(manifestoIdList, statusIdList, request.getDeliveryDate());

                ///Update delayDate
                if(!jobStatusList.isEmpty())
                {
                    ////// Cal Next Day //////
                    Date nextDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getDeliveryDate());

                    Calendar c = Calendar.getInstance();
                    c.setTime(nextDate);
                    c.add(Calendar.DATE, 1);

                    Date currentDatePlusOne = c.getTime();

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String strNextDate = dateFormat.format(currentDatePlusOne);
                    /////////////////////////

                    manifestolistRepository.updateDeliveryDateById(jobStatusList.stream().map(m -> m.getJobId()).collect(Collectors.toList()), strNextDate);

                    ///Re generate PODNumber
                    ///dataFromSOMSService.UpdatePODNumber(strNextDate, jobStatusList.stream().map(m -> m.getJobId()).collect(Collectors.toList()));

                    Timestamp nextDateTS = new Timestamp(currentDatePlusOne.getTime());
                    for (JobStatus jobStatus : jobStatusList)
                    {
                        //Save JobStatus
                        JobStatus jobStatusSave = new JobStatus();
                        jobStatusSave.setJobId(jobStatus.getJobId());
                        jobStatusSave.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                        jobStatusSave.setCreatedBy(jobStatus.getCreatedBy());
                        jobStatusSave.setCreatedDateUTC(nextDateTS);

                        jobStatusRepository.save(jobStatusSave);

                        // stamp delay date to soms
                        stampStatusToSOMS(jobStatus.getJobId(), PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new GZException(
                    PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED
                    , HttpStatus.BAD_REQUEST
                    , HttpStatus.BAD_REQUEST.toString()
                    , e.toString()
            );
        }

        return new GZResponse(
                PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED
                , HttpStatus.OK
                , HttpStatus.OK.toString()
                , PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS
        );
    }

    public GZResponse stampDelay(DataFromSOMSRequest request) throws Exception
    {
        try
        {
            if (request.getDeliveryDate() == null || request.getDeliveryDate().equals(""))
                request.setDeliveryDate(java.time.LocalDate.now().toString());

            List<Integer> manifestoIdList = manifestolistRepository.findIdByDeliveryDateAndDelayDateIsNull(request.getDeliveryDate());

            if(!manifestoIdList.isEmpty())
            {
                List<Integer> statusIdList = new ArrayList<>();
                statusIdList.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
                statusIdList.add(PODNumberStatusConstant.JOB_DRIVER_START);
                statusIdList.add(PODNumberStatusConstant.JOB_DRIVER_END);

                List<JobStatus> jobStatusList = jobStatusRepository.findJobIdByStatusIdListAndDelivery(manifestoIdList, statusIdList, request.getDeliveryDate());

                ///Update delayDate
                if(!jobStatusList.isEmpty())
                {
                    manifestolistRepository.updateDelayDateById(jobStatusList.stream().map(m -> m.getJobId()).collect(Collectors.toList()), request.getDeliveryDate());

                    for (int manifestoId : jobStatusList.stream().map(m -> m.getJobId()).collect(Collectors.toList())) {

                        stampStatusToSOMS(manifestoId, PODNumberStatusConstant.JOB_DRIVER_DELAY);
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new GZException(
                    PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED
                    , HttpStatus.BAD_REQUEST
                    , HttpStatus.BAD_REQUEST.toString()
                    , e.toString()
            );
        }

        return new GZResponse(
                PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED
                , HttpStatus.OK
                , HttpStatus.OK.toString()
                , PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS
        );
    }
}
