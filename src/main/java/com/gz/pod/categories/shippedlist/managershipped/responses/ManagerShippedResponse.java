//package com.gz.pod.categories.shippedlist.managershipped.responses;
//
//import lombok.Data;
//
//import java.math.BigDecimal;
//
//@Data
//public class ManagerShippedResponse {
//
//    private int manifestoId;
//    private int isConfirm;
//    private String remark;
//    private String reason;
//    private String actionDate;
//    private int actionBy;
//    private String updateDate;
//    private int updateBy;
//    private BigDecimal longitude;
//    private BigDecimal latitude;
//    private String customerName;
//    private String shipToAddress;
//    private String soNo;
//    private String dONo;
//    private String routeMaster;
//    private String routeName;
//    private String billingNo;
//    private String manifestoType;
//
//    ManagerShippedResponse() {
//    }
//
//    public ManagerShippedResponse(int manifestoId, int isConfirm, String remark, String reason, String actionDate,
//                                  int actionBy, String updateDate, int updateBy, BigDecimal longitude,
//                                  BigDecimal latitude, String customerName, String shipToAddress, String soNo,
//                                  String dONo, String routeMaster, String routeName, String billingNo,
//                                  String manifestoType) {
//        this.manifestoId = manifestoId;
//        this.isConfirm = isConfirm;
//        this.remark = remark;
//        this.reason = reason;
//        this.actionDate = actionDate;
//        this.actionBy = actionBy;
//        this.updateDate = updateDate;
//        this.updateBy = updateBy;
//        this.longitude = longitude;
//        this.latitude = latitude;
//        this.customerName = customerName;
//        this.shipToAddress = shipToAddress;
//        this.soNo = soNo;
//        this.dONo = dONo;
//        this.routeMaster = routeMaster;
//        this.routeName = routeName;
//        this.billingNo = billingNo;
//        this.manifestoType = manifestoType;
//    }
//}
