//package com.gz.pod.categories.shippedlist.managershipped;
//
//import com.gz.pod.entities.ConfirmManifesto;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.response.GZResponsePage;
//import com.gz.pod.categories.shippedlist.managershipped.requests.ManagerConfirmRequest;
//import com.gz.pod.categories.shippedlist.managershipped.requests.ManagerShippedRequest;
//import com.gz.pod.categories.shippedlist.managershipped.responses.ManagerConfirmResponse;
//import com.gz.pod.categories.shippedlist.managershipped.responses.ManagerShippedResponse;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.support.PagedListHolder;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Service
//public class ManagerShippedService {
//
//    @Autowired
//    ManagerShippedRepository managerShippedRepository;
//    @Autowired
//    ManagerReasonRepository managerReasonRepository;
//
//    public ManagerShippedService(ManagerShippedRepository managerShippedRepository) {
//        this.managerShippedRepository = managerShippedRepository;
//    }
//
//    private String remark;
//    private int reason;
//    private int hasRemarkWhenReject = 0;
//
//    public GZResponsePage getShippedList(ManagerShippedRequest request) throws GZException {
//        GZResponsePage response = new GZResponsePage();
//        if (request.getDay() == null) {
//            request.setDay(java.time.LocalDate.now().toString());
//        }
//        try {
//            List<ConfirmManifesto> list = managerShippedRepository
//                    .getShippedlistByDriverID(
//                            request.getDriverId(),
//                            request.getDay() + "T00:00:00.000",
//                            request.getDay() + "T23:59:59.999"
//                    );
//            List<ManagerShippedResponse> res = list.stream().map(
//                    shipped -> new ManagerShippedResponse(
//                            shipped.getManifestoId(),
//                            shipped.getIsConfirm(),
//                            shipped.getRemark(),
//                            managerReasonRepository.findOne(shipped.getReasonId()).getDescription(),
//                            shipped.getActionDate(),
//                            shipped.getActionBy(),
//                            shipped.getUpdateDate().toString(),
//                            shipped.getUpdateBy(),
//                            shipped.getLongitude(),
//                            shipped.getLatitude(),
//                            shipped.getManifesto().getCustomerName(),
//                            shipped.getManifesto().getShipToAddress(),
//                            shipped.getManifesto().getManifestoCstm().getSONo(),
//                            shipped.getManifesto().getDONumber(),
//                            shipped.getManifesto().getMasterRoute().getRouteMaster(),
//                            shipped.getManifesto().getMasterRoute().getRouteName(),
//                            shipped.getManifesto().getManifestoCstm().getBillNo(),
//                            shipped.getManifesto().getManifestoType()
//                    )
//            ).collect(Collectors.toList());
//            if (list.isEmpty()) {
//                setResponsePage(request, res);
//            } else {
//                throw new GZException(ManagerShippedContant.SHIPPED_LIST, HttpStatus.BAD_REQUEST,
//                        HttpStatus.BAD_REQUEST.toString(), ManagerShippedContant.GET_LIST_ERROR
//                );
//            }
//        } catch (Exception ex) {
//            throw new GZException(ManagerShippedContant.SHIPPED_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ManagerShippedContant.GET_LIST_ERROR
//            );
//        }
//        return response;
//    }
//
//    public GZResponse confirmShippedByManager(ManagerConfirmRequest managerConfirmRequest) throws GZException {
//        GZResponse gzResponse = new GZResponse();
//        ManagerConfirmResponse response = new ManagerConfirmResponse();
//        try {
//            if (managerConfirmRequest.getIsConfirmByManager() == 0) {
//                if (managerConfirmRequest.getRemarkByManager() != null) {
//                    remark = managerConfirmRequest.getRemarkByManager();
//                    reason = managerConfirmRequest.getReasonIdByManager();
//                } else {
//                    hasRemarkWhenReject = 1;
//                    throw new GZException(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER, HttpStatus.BAD_REQUEST,
//                            HttpStatus.BAD_REQUEST.toString(), ManagerShippedContant.REMARK_IS_EMPTY
//                    );
//                }
//            } else if (managerConfirmRequest.getIsConfirmByManager() == 1) {
//                remark = null;
//                reason = 0;
//            }
//            managerShippedRepository.confirmShippedByManager(managerConfirmRequest.getManifestoId(),
//                    managerConfirmRequest.getIsConfirmByManager(), remark, reason,
//                    new java.sql.Timestamp(new java.util.Date().getTime()));
//            gzResponse.setTitle(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER);
//            ConfirmManifesto cm = managerShippedRepository
//                    .getConfirmManifestoById(managerConfirmRequest.getManifestoId());
//            response.setManifestoId(cm.getManifestoId());
//            response.setIsConfirmByManager(cm.getIsConfirmByManager());
//            response.setReasonIdByManager(cm.getReasonIdByManager());
//            response.setRemarkByManager(cm.getRemarkByManager());
//            gzResponse.setData(response);
//            gzResponse.setCode(HttpStatus.OK);
//            gzResponse.setMessage(HttpStatus.OK.toString());
//            gzResponse.setDeveloperMessage(ManagerShippedContant.CONFIRM_SHIPPED_SUCCESS);
//        } catch (Exception ex) {
//            if (hasRemarkWhenReject == 1) {
//                throw new GZException(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER, HttpStatus.BAD_REQUEST,
//                        HttpStatus.BAD_REQUEST.toString(), ManagerShippedContant.REMARK_IS_EMPTY
//                );
//            } else {
//                throw new GZException(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER, HttpStatus.BAD_REQUEST,
//                        HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
//                );
//            }
//        }
//        return gzResponse;
//    }
//
//    public GZResponsePage setResponsePage(ManagerShippedRequest request, List res) {
//        PagedListHolder<ManagerShippedResponse> setPage = new PagedListHolder(res);
//        GZResponsePage response = new GZResponsePage();
//        setPage.setPage(request.getPage() - 1);
//        setPage.setPageSize(request.getPerPage());
//        response.setPageSize(request.getPerPage());
//        response.setTitle(ManagerShippedContant.SHIPPED_LIST);
//        response.setCode(HttpStatus.OK);
//        response.setMessage(HttpStatus.OK.toString());
//        response.setDeveloperMessage(ManagerShippedContant.GET_LIST_SUCCESS);
//        response.setCurrentPage(request.getPage());
//        response.setTotalPage(setPage.getPageCount());
//        response.setPageSize(setPage.getPageSize());
//        response.setTotalItems(setPage.getNrOfElements());
//        response.setData(setPage.getPageList());
//        return response;
//    }
//}
