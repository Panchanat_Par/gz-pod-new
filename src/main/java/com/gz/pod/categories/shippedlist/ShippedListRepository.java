//package com.gz.pod.categories.shippedlist;
//
//import com.gz.pod.entities.ConfirmManifesto;
//import com.gz.pod.entities.ConfirmShippingList;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ShippedListRepository extends CrudRepository<ConfirmShippingList, Integer> {
//
//    @Query(value = "select c from ConfirmManifesto c " +
//            "where c.actionBy = ?1 and c.manifestoId = ?2")
//    List<ConfirmManifesto> getShipped(int driverId, Integer manifestoId);
//
//}
