//package com.gz.pod.categories.shippedlist.managershipped;
//
//import com.gz.pod.categories.shippedlist.managershipped.requests.ManagerConfirmRequest;
//import com.gz.pod.categories.shippedlist.managershipped.requests.ManagerShippedRequest;
//import com.gz.pod.categories.shippedlist.managershipped.responses.ManagerConfirmResponse;
//import com.gz.pod.categories.shippedlist.managershipped.responses.ManagerShippedResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/shippedlist/manager")
//@Api(value = "Manager Confirm shipped", description = "Manager Confirm shipped management",
//        produces = "application/json", tags = {"Confirm Shipped/Manager"})
//public class ManagerShippedController {
//
//    @Autowired
//    ManagerShippedService managerShippedService;
//
//    @GetMapping("/shippedlistOfEachDriver")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ManagerShippedResponse.class)})
//    public ResponseEntity getShippedlist(@Valid ManagerShippedRequest request) throws Exception {
//        return ResponseEntity.ok(managerShippedService.getShippedList(request));
//    }
//
//    @PutMapping("/confirmShippedByManager")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ManagerConfirmResponse.class)})
//    public ResponseEntity confirmShippedByManager(@Valid @RequestBody ManagerConfirmRequest request) throws Exception {
//        return ResponseEntity.ok(managerShippedService.confirmShippedByManager(request));
//    }
//
//}
