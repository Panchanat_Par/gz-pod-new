//package com.gz.pod.categories.shippedlist.driverconfirm.responses;
//
//import com.gz.pod.entities.ConfirmManifesto;
//import lombok.Data;
//
//import java.util.List;
//
//@Data
//public class MapConfirmManifestoResponse {
//    private List<ConfirmManifesto> confirmManifesto;
//    private List<UploadFilesResponse> fileUpload;
//
//    public MapConfirmManifestoResponse() {
//
//    }
//
//    public MapConfirmManifestoResponse(List<ConfirmManifesto> confirmManifesto, List<UploadFilesResponse> fileUpload) {
//        this.confirmManifesto = confirmManifesto;
//        this.fileUpload = fileUpload;
//    }
//}
