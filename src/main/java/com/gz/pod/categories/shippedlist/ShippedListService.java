//package com.gz.pod.categories.shippedlist;
//
//import com.gz.pod.categories.reject.RejectRepository;
//import com.gz.pod.categories.shippedlist.response.ShipToCodeResponse;
//import com.gz.pod.categories.shippinglist.managershipping.ManagerShippingRepository;
//import com.gz.pod.categories.shippinglist.managershipping.responses.ManagerShippingResponse;
//import com.gz.pod.entities.ConfirmManifesto;
//import com.gz.pod.entities.ConfirmShippingList;
//import com.gz.pod.entities.Manifesto;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponsePage;
//import com.gz.pod.categories.shippedlist.requests.ShippedRequest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.support.PagedListHolder;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.List;
//
//
//@Service
//public class ShippedListService {
//
////    @Autowired
////    ShippedListRepository shippedListRepository;
////
////    @Autowired
////    RejectRepository rejectRepository;
////
////    @Autowired
////    ManagerShippingRepository managerShippingRepository;
////
////    public GZResponsePage getShippedList(ShippedRequest request) throws GZException {
////        GZResponsePage response = new GZResponsePage();
////        if (request.getDay() == null) {
////            request.setDay(java.time.LocalDate.now().toString());
////        }
////        try {
////            List<Manifesto> list = manifestoRepository
////                    .getShippinglistByDriverIdDateShpippingStatus(
////                            request.getDriverId(),
////                            request.getDay() + "T00:00:00.000"
////                    );
////            List<ManagerShippingResponse> mapConfirmArray = new ArrayList<>();
////            for (int i = 0; i < list.size(); i++) {
////                List<ConfirmShippingList> confirmList = managerShippingRepository
////                        .getConfirmListByDriverID(
////                                request.getDriverId(),
////                                list.get(i).getId()
////                        );
////                List<ConfirmManifesto> confirmManifetoList = shippedListRepository
////                        .getShipped(
////                                request.getDriverId(),
////                                list.get(i).getId()
////                        );
////                if (list.get(i).getShippingStatus() > 1 && confirmList.get(0).getIsConfirmByManager() != 0) {
////                    ManagerShippingResponse mapConfirmToManifesto = new ManagerShippingResponse();
////                    mapConfirmToManifesto.setDriverId(list.get(i).getDriverId());
////                    mapConfirmToManifesto.setManifestoId(list.get(i).getId());
////                    mapConfirmToManifesto.setDONumber(list.get(i).getDONumber());
////                    mapConfirmToManifesto.setManifestoType(list.get(i).getManifestoType());
////                    mapConfirmToManifesto.setCustomerName(list.get(i).getCustomerName());
////                    mapConfirmToManifesto.setAddressType(list.get(i).getAddressType());
////                    mapConfirmToManifesto.setBoxQty(list.get(i).getBoxQty());
////                    mapConfirmToManifesto.setItemQty(list.get(i).getItemQty());
////                    mapConfirmToManifesto.setSKUQty(list.get(i).getSKUQty());
////                    double vat = Double.parseDouble(String.valueOf(list.get(i).getOrderTotalInVat()));
////                    DecimalFormat df = new DecimalFormat("#,###.00");
////                    mapConfirmToManifesto.setOrderTotalInVat(df.format(vat));
////                    mapConfirmToManifesto.setRemark(list.get(i).getRemark());
////                    mapConfirmToManifesto.setRouteMaster(list.get(i).getMasterRoute().getRouteMaster());
////                    mapConfirmToManifesto.setRouteName(list.get(i).getMasterRoute().getRouteName());
////                    mapConfirmToManifesto.setBillNo(list.get(i).getManifestoCstm().getBillNo());
////                    mapConfirmToManifesto.setSONo(list.get(i).getManifestoCstm().getSONo());
////                    mapConfirmToManifesto.setDocumentNumber(list.get(i).getDocumentNumber());
////                    mapConfirmToManifesto.setShippingStatus(list.get(i).getShippingStatus());
////                    mapConfirmToManifesto.setShipToCode(list.get(i).getShipToCode());
////                    if (confirmManifetoList.isEmpty()) {
////                        setNull(mapConfirmToManifesto);
////                    } else {
////                        setConfirmManifesto(mapConfirmToManifesto, confirmManifetoList);
////                    }
////                    mapConfirmArray.add(mapConfirmToManifesto);
////                }
////
////            }
////
////            List<ShipToCodeResponse> shipToCodeResponseList = convertListGroupByShipTOCode(mapConfirmArray);
////
////            PagedListHolder<ShipToCodeResponse> setPage = new PagedListHolder<>(shipToCodeResponseList);
////            if (!mapConfirmArray.isEmpty()) {
////                setPage.setPage(request.getPage() - 1);
////                setPage.setPageSize(request.getPerPage());
////                response.setPageSize(request.getPerPage());
////                response.setTitle(ShippedListConstant.SHIPPED_LIST);
////                response.setCode(HttpStatus.OK);
////                response.setMessage(HttpStatus.OK.toString());
////                response.setDeveloperMessage(ShippedListConstant.GET_LIST_SUCCESS);
////                response.setCurrentPage(request.getPage());
////                response.setTotalPage(setPage.getPageCount());
////                response.setPageSize(setPage.getPageSize());
////                response.setTotalItems(setPage.getNrOfElements());
////                response.setData(setPage.getPageList());
////            } else {
////                throw new GZException(ShippedListConstant.SHIPPED_LIST, HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(), ShippedListConstant.GET_LIST_ERROR
////                );
////            }
////        } catch (Exception ex) {
////            throw new GZException(ShippedListConstant.SHIPPED_LIST, HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
////            );
////        }
////        return response;
////    }
////
////    public void setNull(ManagerShippingResponse mapConfirmToManifesto) {
////        mapConfirmToManifesto.setIsConfirm(0);
////        mapConfirmToManifesto.setRemarkReject("");
////        mapConfirmToManifesto.setReasonId(0);
////        mapConfirmToManifesto.setReasonString("");
////        mapConfirmToManifesto.setActionBy(0);
////        mapConfirmToManifesto.setIsConfirmByManager(0);
////        mapConfirmToManifesto.setRemarkByManager("");
////        mapConfirmToManifesto.setReasonIdByManager(0);
////    }
////
////    public void setConfirmManifesto(ManagerShippingResponse mapConfirmToManifesto, List<ConfirmManifesto> confirmManifetoList) {
////        mapConfirmToManifesto.setIsConfirm(confirmManifetoList.get(0).getIsConfirm());
////        mapConfirmToManifesto.setRemarkReject(confirmManifetoList.get(0).getRemark());
////        mapConfirmToManifesto.setReasonId(confirmManifetoList.get(0).getReasonId());
////        if (confirmManifetoList.get(0).getReasonId() != 0) {
////            String reasonString = rejectRepository.getRejectById(confirmManifetoList.get(0).getReasonId())
////                    .getDescription();
////            mapConfirmToManifesto.setReasonString(reasonString);
////        } else {
////            mapConfirmToManifesto.setReasonString("");
////        }
////        mapConfirmToManifesto.setActionBy(confirmManifetoList.get(0).getActionBy());
////        mapConfirmToManifesto.setIsConfirmByManager(confirmManifetoList.get(0).getIsConfirmByManager());
////        mapConfirmToManifesto.setRemarkByManager(confirmManifetoList.get(0).getRemarkByManager());
////        mapConfirmToManifesto.setReasonIdByManager(confirmManifetoList.get(0).getReasonIdByManager());
////    }
////
////    public List<ShipToCodeResponse> convertListGroupByShipTOCode(List<ManagerShippingResponse> list) {
////        List<ShipToCodeResponse> snList = new ArrayList<>();
////        List<ManagerShippingResponse> spList = new ArrayList<>();
////        String shipToCode = list.get(0).getShipToCode();
////
////        // We can use this logic when list order by shipToCode.
////        for (int i = 0; i < list.size(); i++) {
////            if (list.get(i).getShipToCode().equals(shipToCode)) {
////                spList.add(list.get(i));
////                if (i == list.size() - 1) {
////                    ShipToCodeResponse sn = new ShipToCodeResponse();
////                    sn.setShipToCode(list.get(i).getShipToCode());
////                    sn.setStatus(list.get(i).getShippingStatus());
////                    sn.setData(spList);
////                    snList.add(sn);
////                }
////            } else { // If shipToCode not same old value.
////                shipToCode = list.get(i).getShipToCode();
////                i--; // Then we can get old value that duplicate by position - 1.
////                ShipToCodeResponse sn = new ShipToCodeResponse();
////                sn.setShipToCode(list.get(i).getShipToCode());
////                sn.setStatus(list.get(i).getShippingStatus());
////                sn.setData(spList);
////                snList.add(sn);
////                spList = new ArrayList<>(); // Clear shippingList to next shipToCode.
////            }
////        }
////        return snList;
////    }
//}
