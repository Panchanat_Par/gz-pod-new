//package com.gz.pod.categories.shippedlist.response;
//
//import lombok.Data;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//
//@Data
//public class ShippedResponse {
//
//    private Integer driverId;
//    private int manifestoId;
//    private String dONumber;
//    private String manifestoType;
//    private String customerName;
//    private String addressType;
//    private Integer boxQty;
//    private Integer itemQty;
//    private Integer sKUQty;
//    private String orderTotalInVat;
//    private String remark;
//    private String routeMaster;
//    private String routeName;
//    private String billNo;
//    private String sONo;
//    private String documentNumber;
//    private  int shippingStatus;
//    private Integer isConfirm;
//    private String remarkReject;
//    private Integer reasonId;
//    private String reasonString;
//    private Integer actionBy;
//    private Integer isConfirmByManager;
//    private String remarkByManager;
//    private Integer reasonIdByManager;
//    private String shipToCode;
//
//    @SuppressWarnings("squid:S00107")
//    public ShippedResponse(Integer driverId, int manifestoId, String dONumber, String manifestoType,
//                           String customerName, String addressType, Integer boxQty, Integer itemQty,
//                           Integer sKUQty, BigDecimal orderTotalInVat, String remark, String routeMaster,
//                           String routeName, String billNo, String sONo, String documentNumber,
//                           int shippingStatus, Integer isConfirm, String remarkReject, Integer reasonId,
//                           String reasonString, Integer actionBy, Integer isConfirmByManager,
//                           String remarkByManager, Integer reasonIdByManager, String shipToCode) {
//        this.driverId = driverId;
//        this.manifestoId = manifestoId;
//        this.dONumber = dONumber;
//        this.manifestoType = manifestoType;
//        this.customerName = customerName;
//        this.addressType = addressType;
//        this.boxQty = boxQty;
//        this.itemQty = itemQty;
//        this.sKUQty = sKUQty;
//
//        double vat = Double.parseDouble(String.valueOf(orderTotalInVat));
//        DecimalFormat df = new DecimalFormat("#,###.00");
//        this.orderTotalInVat = df.format(vat);
//
//        this.remark = remark;
//        this.routeMaster = routeMaster;
//        this.routeName = routeName;
//        this.billNo = billNo;
//        this.sONo = sONo;
//        this.documentNumber = documentNumber;
//        this.shippingStatus = shippingStatus;
//        this.isConfirm = isConfirm;
//        this.remarkReject = remarkReject;
//        this.reasonId = reasonId;
//        this.reasonString = reasonString;
//        this.actionBy = actionBy;
//        this.isConfirmByManager = isConfirmByManager;
//        this.remarkByManager = remarkByManager;
//        this.reasonIdByManager = reasonIdByManager;
//        this.shipToCode = shipToCode;
//    }
//
//    public ShippedResponse() {
//
//    }
//}
