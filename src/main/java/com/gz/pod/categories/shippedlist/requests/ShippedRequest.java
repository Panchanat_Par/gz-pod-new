//package com.gz.pod.categories.shippedlist.requests;
//
//import io.swagger.annotations.ApiModelProperty;
//import io.swagger.annotations.ApiParam;
//import lombok.Data;
//
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//
//@Data
//public class ShippedRequest {
//
//    @ApiModelProperty(example = "1", required = true)
//    @ApiParam(value = "รหัสพนักงานขับรถ", defaultValue = "3")
//    @NotNull(message = "Driver id can't not be null")
//    private Integer driverId;
//    @ApiParam(value = "วันที่ต้องการดู", defaultValue = "2018-07-02")
//    private String day;
//    @ApiParam(value = "สถานะสินค้า", defaultValue = "1")
//    private Integer shippingStatus;
//    @ApiModelProperty(example = "1")
//    @ApiParam(value = "หมายเลขหน้าที่ต้องการดู", defaultValue = "1")
//    @Min(value = 1, message = "page must be greater than 1")
//    private Integer page = 1;
//    @ApiModelProperty(example = "10")
//    @ApiParam(value = "จำนวนข้อมูลที่ต้องการดูต่อหน้า", defaultValue = "10")
//    @Min(value = 1, message = "perPage must be greater than 1")
//    private Integer perPage = 10;
//}
