//package com.gz.pod.categories.shippedlist.driverconfirm.requests;
//
//import io.swagger.annotations.ApiModelProperty;
//import io.swagger.annotations.ApiParam;
//import lombok.Data;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.validation.constraints.NotNull;
//import java.math.BigDecimal;
//
//@Data
//public class DriverConfirmRequests {
//
//    @NotNull(message = "Status confirm is require")
//    @ApiModelProperty(example = "1", allowableValues = "1,0", required = true)
//    @ApiParam(value = "ยืนยันการส่งของ", defaultValue = "1")
//    private Integer isConfirm;
//    @ApiParam(value = "หมายเลข manifesto ไอดี", defaultValue = "1")
//    @ApiModelProperty(example = "1", required = true)
//    @NotNull(message = "ManifestoId is require")
//    private Integer manifestoId;
//    @ApiParam(value = "สถานะการส่งของ", defaultValue = "1")
//    @ApiModelProperty(example = "1", required = true)
//    @NotNull(message = "Shipping status is require")
//    private Integer shippingStatus;
//    @ApiParam(value = "เหตุผลในการ reject", defaultValue = "ของชำรุด")
//    private String remark;
//    @ApiParam(value = "หมายเลขหัวข้อเหตุผลที่ reject", defaultValue = "1")
//    private Integer reasonId;
//    @ApiParam(value = "ยืนยันโดยพนักงาน", defaultValue = "1")
//    private Integer actionBy;
//    @ApiModelProperty(example = "1", required = true)
//    @ApiParam(value = "แก้ไขโดยพนักงาน", defaultValue = "1")
//    @NotNull(message = "Update by is require")
//    private Integer updateBy;
//    @ApiParam(value = "ละติจูดที่ส่งของ", defaultValue = "222.211211")
//    @ApiModelProperty(example = "222.211211", required = true)
//    @NotNull(message = "Longitude is require")
//    private BigDecimal longitude;
//    @ApiParam(value = "ลองติจูดที่ส่งของ", defaultValue = "000.888888")
//    @ApiModelProperty(example = "000.888888", required = true)
//    @NotNull(message = "Latitude is require")
//    private BigDecimal latitude;
//    private MultipartFile[] files;
//
//}
