//package com.gz.pod.categories.shippedlist.driverconfirm;
//
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.response.UploadFileResponse;
//import com.gz.pod.categories.shippedlist.driverconfirm.requests.DriverConfirmRequests;
//import com.gz.pod.categories.shippedlist.driverconfirm.responses.MapConfirmManifestoResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/shipped")
//@Api(value = "Driver Confirm shipped", description = "Driver Confirm shipped management", produces = "application/json"
//        , tags = {"Confirm Shipped/Driver"})
//public class DriverConfirmController {
//
////    @Autowired
////    DriverConfirmService driverConfirmService;
////
////    @Autowired
////    public DriverConfirmController(DriverConfirmService driverConfirmService){
////        this.driverConfirmService = driverConfirmService;
////    }
////
////    //  Add data confirm/reject shipping
////    @PostMapping(value = "add/confirmReject")
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = MapConfirmManifestoResponse.class)})
////    public ResponseEntity<GZResponse> addConfirmShipping(
////            @Valid @ModelAttribute DriverConfirmRequests requests,
////            @RequestParam("files") MultipartFile[] files) throws Exception {
////        return ResponseEntity.ok(driverConfirmService.addConfirmReject(requests, files));
////    }
////
////    //  Update data confirm/reject shipping
////    @PutMapping(value = "update/confirmReject/{confirmManifestoId}")
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = MapConfirmManifestoResponse.class)})
////    public ResponseEntity<GZResponse> updateConfirmReject(
////            @ApiParam(value = "manifesto ไอดี", defaultValue = "9") @PathVariable int confirmManifestoId,
////            @Valid @RequestBody DriverConfirmRequests requests) throws Exception {
////        return ResponseEntity.ok(driverConfirmService.updateConfirmRejectManifesto(requests, confirmManifestoId));
////    }
////
////    //  Update file image shipping by id
////    @PutMapping(value = "update/uploadFile/{confirmManifestoImageId}")
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = UploadFileResponse.class)})
////    public ResponseEntity<UploadFileResponse> updateImageFile(
////            @ApiParam(value = "ไอดีรูปภาพ", defaultValue = "9") @PathVariable int confirmManifestoImageId,
////            @Valid @RequestParam("files") MultipartFile fileData) throws Exception {
////        return ResponseEntity.ok(driverConfirmService.updateUploadFile(fileData, confirmManifestoImageId));
////    }
////
////    //  delete file image shipping by id
////    @DeleteMapping(value = "delete/ImageFile/{confirmManifestoImageId}")
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = UploadFileResponse.class)})
////    public ResponseEntity<UploadFileResponse> deleteImageFile(
////            @ApiParam(value = "ไอดีรูปภาพ", defaultValue = "9")
////            @PathVariable int confirmManifestoImageId) throws Exception {
////        return ResponseEntity.ok(driverConfirmService.deleteImageFile(confirmManifestoImageId));
////    }
//
//}
