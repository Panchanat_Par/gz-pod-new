//package com.gz.pod.categories.shippedlist.driverconfirm;
//
//import com.gz.pod.categories.logs.LogService;
//import com.gz.pod.categories.logs.requests.LogRequest;
//import com.gz.pod.constants.LogConstants;
//import com.gz.pod.entities.ConfirmManifesto;
//import com.gz.pod.entities.ConfirmManifestoImage;
//import com.gz.pod.entities.Manifesto;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.response.UploadFileResponse;
//import com.gz.pod.categories.shippedlist.driverconfirm.requests.DriverConfirmRequests;
//import com.gz.pod.categories.shippedlist.driverconfirm.responses.MapConfirmManifestoResponse;
//import com.gz.pod.categories.shippedlist.driverconfirm.responses.UploadFilesResponse;
//import com.gz.pod.categories.shippedlist.driverconfirmimage.DriverConfirmImageRepository;
//import org.apache.tomcat.util.codec.binary.Base64;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//import sun.rmi.runtime.Log;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class DriverConfirmService {
//
////    @Autowired
////    DriverConfirmRepository driverConfirmRepository;
////
////    @Autowired
////    DriverConfirmImageRepository driverConfirmImageRepository;
////
////    @Autowired
////    ImageValidator imageValidator;
////
////    @Autowired
////    LogService logService;
////
////    //  Add Confirm/Reject ConfirmManifesto table
////    public GZResponse addConfirmReject(DriverConfirmRequests requests, MultipartFile[] files)
////            throws GZException {
////        List<ConfirmManifesto> confirmManifestoList = new ArrayList<>();
////        List<MapConfirmManifestoResponse> mapConfirmManifestoResponse = new ArrayList<>();
////        GZResponse response = new GZResponse();
////        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
////        List uploadFileResponse;
////        Manifesto manifestoData = manifestoRepository.getManifestoById(requests.getManifestoId());
////        LogRequest log = new LogRequest();
////        log.setDocType(manifestoData.getManifestoType());
////        log.setDocRef(manifestoData.getJobId());
////        log.setCreateBy(manifestoData.getDriverId());
////
////        ConfirmManifesto confirmManifesto = new ConfirmManifesto();
////
////        confirmManifesto.setManifestoId(requests.getManifestoId());
////        confirmManifesto.setIsConfirm(requests.getIsConfirm());
////        confirmManifesto.setLongitude(requests.getLongitude());
////        confirmManifesto.setLatitude(requests.getLatitude());
////        confirmManifesto.setActionDate(date.toString());
////        confirmManifesto.setActionBy(requests.getActionBy());
////        confirmManifesto.setUpdateDate(date);
////        confirmManifesto.setUpdateBy(requests.getUpdateBy());
////
////        if (requests.getIsConfirm() == 0) {
////            if (!requests.getRemark().isEmpty()) {
////                confirmManifesto.setRemark(requests.getRemark());
////                confirmManifesto.setReasonId(requests.getReasonId());
////                confirmManifestoList.add(driverConfirmRepository.save(confirmManifesto));
////                manifestoRepository.updateShippingStatus(requests.getManifestoId(), requests.getShippingStatus());
////                response.setCode(HttpStatus.OK);
////                response.setData(confirmManifestoList);
////            } else {
////                throw new GZException(
////                        DriverConfirmConstant.CONFIRMMANIFESTO_ADD,
////                        HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(),
////                        DriverConfirmConstant.ADD_REJECT_CONFIRMMANIFESTO_ERROR
////                );
////            }
////        log.setStatusId(LogConstants.customer_reject.getValue());
////        } else if (requests.getIsConfirm() == 1) {
////
////            if (files.length == 0) {
////                throw new GZException(UploadFileConstant.FILE_LIST, HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(), UploadFileConstant.PUT_FILE_ERROR
////                );
////            }
////
////            if (!imageValidator.mutipleImageValidators(files)) {
////                throw new GZException(
////                        DriverConfirmConstant.CONFIRMMANIFESTO_ADD,
////                        HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(),
////                        DriverConfirmConstant.ADD_CONFIRMMANIFESTO_FILE_ERROR
////                );
////            }
////
////            try {
////                confirmManifestoList.add(driverConfirmRepository.save(confirmManifesto));
////                manifestoRepository.updateShippingStatus(requests.getManifestoId(), requests.getShippingStatus());
////                uploadFileResponse = uploadFile(
////                        files,
////                        requests,
////                        confirmManifestoList.get(0).getId()
////                );
////                response.setCode(HttpStatus.OK);
////                mapConfirmManifestoResponse.add(
////                        new MapConfirmManifestoResponse(
////                                confirmManifestoList,
////                                uploadFileResponse)
////                );
////                response.setData(mapConfirmManifestoResponse);
////            } catch (IOException e) {
////                throw new GZException(
////                        DriverConfirmConstant.CONFIRMMANIFESTO_ADD,
////                        HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(),
////                        DriverConfirmConstant.ADD_CONFIRMMANIFESTO_ERROR
////                );
////            }
////            log.setStatusId(LogConstants.customer_delivered.getValue());
////        } else {
////            throw new GZException(
////                    DriverConfirmConstant.CONFIRMMANIFESTO_ADD,
////                    HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(),
////                    DriverConfirmConstant.ADD_CONFIRMMANIFESTO_ERROR
////            );
////        }
////
////        logService.setLog(log);
////        response.setTitle(DriverConfirmConstant.CONFIRMMANIFESTO_ADD);
////        response.setMessage(HttpStatus.OK.toString());
////        response.setDeveloperMessage(DriverConfirmConstant.ADD_CONFIRMMANIFESTO_SUCCESS);
////
////        return response;
////    }
////
////    //  Update Confirm/Reject ConfirmManifesto table
////    public GZResponse updateConfirmRejectManifesto(
////            DriverConfirmRequests requests,
////            int confirmManifestoId)
////            throws GZException {
////        List<ConfirmManifesto> confirmManifestoList = new ArrayList<>();
////        GZResponse response = new GZResponse();
////        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
////        try {
////
////            ConfirmManifesto confirmManifesto = getConfirmManifesto(confirmManifestoId);
////
////            if (requests.getIsConfirm() == 0) {
////                if (!requests.getRemark().isEmpty()) {
////                    confirmManifesto.setRemark(requests.getRemark());
////                    confirmManifesto.setReasonId(requests.getReasonId());
////                } else {
////                    throw new GZException(
////                            DriverConfirmConstant.CONFIRMMANIFESTO_UPDATE,
////                            HttpStatus.BAD_REQUEST,
////                            HttpStatus.BAD_REQUEST.toString(),
////                            DriverConfirmConstant.UPDATE_REJECT_CONFIRMMANIFESTO_ERROR
////                    );
////                }
////
////            } else if (requests.getIsConfirm() == 1) {
////                confirmManifesto.setRemark("");
////                confirmManifesto.setReasonId(0);
////            }
////            confirmManifesto.setManifestoId(requests.getManifestoId());
////            confirmManifesto.setIsConfirm(requests.getIsConfirm());
////            confirmManifesto.setLongitude(requests.getLongitude());
////            confirmManifesto.setLatitude(requests.getLatitude());
////            confirmManifesto.setActionDate(date.toString());
////            confirmManifesto.setActionBy(requests.getActionBy());
////            confirmManifesto.setUpdateDate(date);
////            confirmManifesto.setUpdateBy(requests.getUpdateBy());
////            confirmManifestoList.add(driverConfirmRepository.save(confirmManifesto));
////            manifestoRepository.updateShippingStatus(requests.getManifestoId(), requests.getShippingStatus());
////            response.setTitle(DriverConfirmConstant.CONFIRMMANIFESTO_UPDATE);
////            response.setCode(HttpStatus.OK);
////            response.setMessage(HttpStatus.OK.toString());
////            response.setData(confirmManifestoList);
////            response.setDeveloperMessage(DriverConfirmConstant.UPDATE_CONFIRMMANIFESTO_SUCCESS);
////        } catch (Exception ex) {
////            throw new GZException(
////                    DriverConfirmConstant.CONFIRMMANIFESTO_UPDATE,
////                    HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(),
////                    DriverConfirmConstant.UPDATE_CONFIRMMANIFESTO_NOTFOUND_ERROR + confirmManifestoId
////            );
////        }
////        return response;
////    }
////
////    //  Add ConfirmManifestoImage
////    public List<UploadFilesResponse> uploadFile(MultipartFile[] uploadingFiles,
////                                                DriverConfirmRequests requests,
////                                                int cofirmManifestoId
////    ) throws IOException, GZException {
////
////        byte[] bytes;
////        byte[] encodeBase64;
////        String base64Encoded;
////        List<UploadFilesResponse> uploadFilesResponsesList = new ArrayList<>();
////        UploadFilesResponse uploadFilesResponse;
////        for (MultipartFile uploadedFile : uploadingFiles) {
////            if (!uploadedFile.isEmpty()) {
////                ConfirmManifestoImage confirmManifestoImage = new ConfirmManifestoImage();
////                java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
////                if (imageValidator.oneImageValidator(uploadedFile)) {
////                    bytes = uploadedFile.getBytes();
////                    encodeBase64 = Base64.encodeBase64(bytes);
////                    base64Encoded = new String(encodeBase64, "UTF-8");
////                    confirmManifestoImage.setImageFile(base64Encoded);
////                    confirmManifestoImage.setManifestoId(requests.getManifestoId());
////                    confirmManifestoImage.setConfirmManifestoId(cofirmManifestoId);
////                    confirmManifestoImage.setUpdateDate(date.toString());
////                    confirmManifestoImage.setUpdateBy(requests.getUpdateBy());
////                    confirmManifestoImage = driverConfirmImageRepository.save(confirmManifestoImage);
////                    uploadFilesResponse = new UploadFilesResponse(
////                            confirmManifestoImage.getId(),
////                            confirmManifestoImage.getManifestoId(),
////                            confirmManifestoImage.getConfirmManifestoId(),
////                            confirmManifestoImage.getUpdateBy(),
////                            confirmManifestoImage.getImageFile());
////                    uploadFilesResponsesList.add(uploadFilesResponse);
////                }
////            } else {
////                throw new GZException(
////                        UploadFileConstant.FILE_LIST,
////                        HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(),
////                        UploadFileConstant.PUT_FILE_ERROR
////                );
////            }
////
////        }
////        return uploadFilesResponsesList;
////    }
////
////    //  Update ConfirmManifestoImage by id
////    public UploadFileResponse updateUploadFile(MultipartFile uploadedFile, int confirmManifestoImageId)
////            throws IOException, GZException {
////        UploadFileResponse response = new UploadFileResponse();
////        List<UploadFileResponse> uploadFilesList = new ArrayList<>();
////        UploadFilesResponse uploadFilesResponse;
////        byte[] bytes;
////        byte[] encodeBase64;
////        String base64Encoded;
////        if (!uploadedFile.isEmpty()) {
////            if (imageValidator.oneImageValidator(uploadedFile)) {
////                java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
////                ConfirmManifestoImage confirmManifestoImage = getConfirmManifestoImage(confirmManifestoImageId);
////                bytes = uploadedFile.getBytes();
////                encodeBase64 = Base64.encodeBase64(bytes);
////                base64Encoded = new String(encodeBase64, "UTF-8");
////                confirmManifestoImage.setImageFile(base64Encoded);
////                confirmManifestoImage.setUpdateDate(date.toString());
////                confirmManifestoImage = driverConfirmImageRepository.save(confirmManifestoImage);
////                uploadFilesResponse = new UploadFilesResponse(
////                        confirmManifestoImage.getId(),
////                        confirmManifestoImage.getManifestoId(),
////                        confirmManifestoImage.getConfirmManifestoId(),
////                        confirmManifestoImage.getUpdateBy(),
////                        confirmManifestoImage.getImageFile());
////                response.setData(uploadFilesResponse);
////                response.setDeveloperMessage(UploadFileConstant.PUT_FILE_SUCCESS);
////                response.setMessage(HttpStatus.OK.toString());
////                response.setTitle(UploadFileConstant.FILE_LIST);
////                response.setCode(HttpStatus.OK);
////                uploadFilesList.add(response);
////            } else {
////                throw new GZException(
////                        UploadFileConstant.FILE_LIST,
////                        HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(),
////                        UploadFileConstant.PUT_FILE_TYPE_ERROR
////                );
////            }
////
////        } else {
////            throw new GZException(
////                    UploadFileConstant.FILE_LIST,
////                    HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(),
////                    UploadFileConstant.PUT_FILE_ERROR
////            );
////        }
////        return response;
////    }
////
////    //  Update ConfirmManifestoImage by id
////    public UploadFileResponse deleteImageFile(int confirmManifestoImageId)
////            throws GZException {
////        UploadFileResponse response = new UploadFileResponse();
////        UploadFilesResponse uploadFilesResponse;
////        ConfirmManifestoImage confirmManifestoImage = getConfirmManifestoImage(confirmManifestoImageId);
////        uploadFilesResponse = new UploadFilesResponse(
////                confirmManifestoImage.getId(),
////                confirmManifestoImage.getManifestoId(),
////                confirmManifestoImage.getConfirmManifestoId(),
////                confirmManifestoImage.getUpdateBy(),
////                confirmManifestoImage.getImageFile());
////        response.setData(uploadFilesResponse);
////        driverConfirmImageRepository.delete(confirmManifestoImage);
////        response.setDeveloperMessage(UploadFileConstant.DELETE_FILE_SUCCESS);
////        response.setMessage(HttpStatus.OK.toString());
////        response.setTitle(UploadFileConstant.FILE_DELETE);
////        response.setCode(HttpStatus.OK);
////        return response;
////    }
////
////    //  Get ConfirmManifestoImage by confirmManifestoImageId
////    public ConfirmManifestoImage getConfirmManifestoImage(int confirmManifestoImageId) throws GZException {
////        Optional<ConfirmManifestoImage> optionConfirmManifestoImage = driverConfirmImageRepository
////                .findById(confirmManifestoImageId);
////        ConfirmManifestoImage confirmManifestoImage = null;
////        if (optionConfirmManifestoImage.isPresent()) {
////            confirmManifestoImage = optionConfirmManifestoImage.get();
////        } else {
////            throw new GZException(
////                    UploadFileConstant.FILE_LIST,
////                    HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(),
////                    UploadFileConstant.PUT_FILEID_NOT_FOUND_ERROR + confirmManifestoImageId
////            );
////        }
////        return confirmManifestoImage;
////    }
//}
