//package com.gz.pod.categories.shippedlist.driverconfirm;
//
//public class DriverConfirmConstant {
//    public static final String CONFIRMMANIFESTO_ADD = "Add confirm/reject shipping";
//    public static final String ADD_CONFIRMMANIFESTO_SUCCESS = "Add confirm/reject shipping success!";
//    public static final String ADD_CONFIRMMANIFESTO_ERROR = "Can not add , please check your data!";
//    public static final String ADD_REJECT_CONFIRMMANIFESTO_ERROR = "Can not add , please input reasonId and remark!";
//    public static final String ADD_CONFIRMMANIFESTO_FILE_ERROR = "Can not confirm shipping, please input file type .png/.jpeg!";
//
//    public static final String CONFIRMMANIFESTO_UPDATE = "Update confirm/reject shipping";
//    public static final String UPDATE_CONFIRMMANIFESTO_SUCCESS = "Update confirm/reject shipping success!";
//    public static final String UPDATE_REJECT_CONFIRMMANIFESTO_ERROR = "Can not update , please input reasonId and remark!";
//    public static final String UPDATE_CONFIRMMANIFESTO_NOTFOUND_ERROR = "Can not update , Not found confirm manifesto id = ";
//
//
//    private DriverConfirmConstant() {
//        throw new UnsupportedOperationException();
//    }
//
//}
