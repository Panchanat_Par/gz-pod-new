//package com.gz.pod.categories.shippedlist;
//
//import com.gz.pod.categories.shippedlist.requests.ShippedRequest;
//import com.gz.pod.categories.shippedlist.response.ShippedResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/shippedlist")
//@Api(value = "Get shipped", description = "Shipped management", produces = "application/json", tags = {"Shipped"})
//public class ShippedListController {
//
////    @Autowired
////    ShippedListService shippedListService;
////
////    @GetMapping
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ShippedResponse.class)})
////    public ResponseEntity getShippedList(@Valid ShippedRequest request) throws Exception {
////        return ResponseEntity.ok(shippedListService.getShippedList(request));
////    }
//}
