//package com.gz.pod.categories.shippedlist.managershipped;
//
//import com.gz.pod.entities.ConfirmManifesto;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import javax.transaction.Transactional;
//import java.sql.Timestamp;
//import java.util.List;
//
//@Repository
//public interface ManagerShippedRepository extends CrudRepository<ConfirmManifesto, Integer> {
//
//    @Query(value = "select cm from ConfirmManifesto cm where cm.actionBy = ?1 and cm.actionDate between ?2 and ?3")
//    List<ConfirmManifesto> getShippedlistByDriverID(int driverId, String startDay, String endDay);
//
//    @Query(value = "select cm from ConfirmManifesto cm where cm.manifestoId = ?1")
//    ConfirmManifesto getConfirmManifestoById(int manifestoId);
//
//    @Transactional
//    @Modifying
//    @Query(value = "update ConfirmManifesto set isConfirmByManager = ?2 " +
//            ", remarkByManager = ?3, reasonIdByManager = ?4 , updateDateByManager = ?5 where manifestoId = ?1")
//    void confirmShippedByManager(int manifestoId, int isConfirmByManager
//            , String remarkByManager, int reasonIdByManager, Timestamp updateDateByManager);
//
//}
