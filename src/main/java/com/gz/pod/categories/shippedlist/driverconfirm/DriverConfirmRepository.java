//package com.gz.pod.categories.shippedlist.driverconfirm;
//
//import com.gz.pod.entities.ConfirmManifesto;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface DriverConfirmRepository extends CrudRepository<ConfirmManifesto, Integer> {
//
//    @Query(value = " select c from ConfirmManifesto c " +
//            " where c.actionBy = ?1 " +
//            " and c.manifestoId in ?2")
//    List<ConfirmManifesto> getCountDriverConfirmShipped(int driverId, List<Integer> manafestoId);
//}
