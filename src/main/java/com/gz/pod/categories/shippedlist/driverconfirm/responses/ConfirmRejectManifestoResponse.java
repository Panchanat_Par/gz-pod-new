//package com.gz.pod.categories.shippedlist.driverconfirm.responses;
//
//import lombok.Data;
//import org.springframework.http.HttpStatus;
//
//import java.util.List;
//
//@Data
//public class ConfirmRejectManifestoResponse {
//    private String title;
//    private HttpStatus code;
//    private String message;
//    private String developerMessage;
//    private List<?> data;
//}
