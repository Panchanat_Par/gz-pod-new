//package com.gz.pod.categories.shippedlist.driverconfirm;
//
//import org.springframework.stereotype.Component;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.util.Arrays;
//import java.util.List;
//
//@Component
//public class ImageValidator {
//    private static final List<String> contentTypes = Arrays.asList("static/image/png", "static/image/jpeg", "static/image/jpg", "image/*");
//
//    // ImageValidator one file
//    public boolean oneImageValidator(MultipartFile file) {
//        String fileContentType = file.getContentType();
//        return contentTypes.contains(fileContentType);
//    }
//
//    // ImageValidator multiple file
//    public boolean mutipleImageValidators(MultipartFile[] files) {
//        Boolean checkFormateFile = false;
//        for (MultipartFile file : files) {
//            String fileContentType = file.getContentType();
//            if (contentTypes.contains(fileContentType)) {
//                checkFormateFile = true;
//            }
//        }
//        return checkFormateFile;
//    }
//}
