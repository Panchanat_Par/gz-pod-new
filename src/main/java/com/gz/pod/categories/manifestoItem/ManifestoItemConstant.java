package com.gz.pod.categories.manifestoItem;

public class ManifestoItemConstant {
    public static final String MANIFESTOITEM_DB = "manifestoItem";
    public static final String ACTION_UPDATEQTY = "updateQuantity";

    public static final String UPDATE_MANIFESTOITEM = "update manifesto item ";
    public static final String UPDATE_MANIFESTOITEM_SUCCESS = "update manifesto item Success";
    public static final String QUANTITY_IS_REQUEST = "quantity is request";

}
