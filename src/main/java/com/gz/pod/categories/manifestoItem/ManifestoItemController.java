package com.gz.pod.categories.manifestoItem;

import com.gz.pod.categories.manifestoItem.requests.ManifestoItemPutQtyRequest;
import com.gz.pod.response.GZResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("manifestoItem")
@Api(value = "ManifestoItem", description = "Manifesto management", produces = "application/json", tags = {"Manifesto"})
public class ManifestoItemController {

    @Autowired
    ManifestoItemService manifestoItemService;

    @PutMapping("/updateQuantity")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity updateQuantityItemByJob(@RequestBody @Valid ManifestoItemPutQtyRequest manifestoItemPutQtyRequest) throws Exception {
        return ResponseEntity.ok(manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest));
    }

}
