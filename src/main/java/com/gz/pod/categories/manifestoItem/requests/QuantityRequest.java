package com.gz.pod.categories.manifestoItem.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class QuantityRequest {
    @ApiParam(name = "refId")
    @NotNull(message = "refId is request")
    private String refId;
    @ApiParam(name = "quantity")
    @NotNull(message = "quantity is request")
    private Integer quantity;
    @ApiParam(name = "remark")
    private String remark;
}
