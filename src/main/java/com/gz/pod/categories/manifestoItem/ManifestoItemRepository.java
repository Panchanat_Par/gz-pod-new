package com.gz.pod.categories.manifestoItem;

import com.gz.pod.entities.ManifestoItem;
import com.gz.pod.entities.Manifestolist;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ManifestoItemRepository extends CrudRepository<ManifestoItem, Integer> {

    @Modifying
    @Transactional
    @Query("delete from ManifestoItem where manifestoId = ?1")
    void deleteByManifestoId(Integer manifestoId);

    List<ManifestoItem> findBymanifestoId(Integer manifestoId);

}