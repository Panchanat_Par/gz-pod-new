package com.gz.pod.categories.manifestoItem.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
public class ManifestoItemPutQtyRequest {

    @ApiParam(value = "update By")
    @NotNull(message = "updateBy is request")
    @Min(value = 1, message = "updateBy not 0")
    private Integer updateBy;
    @ApiParam(value = "fieldType")
    @NotNull(message = "fieldType is request")
    @Pattern(regexp = "(item)", message = "item Only")
    private String fieldType;
    @ApiParam(value = "boxes list")
    @NotNull(message = "quantities is request")
    private List<QuantityRequest> quantities;

}
