package com.gz.pod.categories.manifestoItem;

import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.GenericInformationRepository;
import com.gz.pod.categories.manifestoItem.requests.ManifestoItemPutQtyRequest;
import com.gz.pod.categories.manifestoItem.requests.QuantityRequest;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.GenericInformation;
import com.gz.pod.entities.ManifestoItem;
import com.gz.pod.entities.UserTypeMaster;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ManifestoItemService {

    @Autowired
    ManifestoItemRepository manifestoItemRepository;

    @Autowired
    CommonService commonService;

    @Autowired
    GenericInformationRepository genericInformationRepository;



    public GZResponse updateQuantityItemByJob(ManifestoItemPutQtyRequest manifestoItemPutQtyRequest) throws Exception {
        GZResponse response = new GZResponse();
        try {
            StringBuilder refIdNotFound = new StringBuilder();
            long checkRefIdRequest = manifestoItemPutQtyRequest.getQuantities().stream().filter(b -> b.getRefId() == null).count();
            long checkBoxQtyRequest = manifestoItemPutQtyRequest.getQuantities().stream().filter(b -> b.getQuantity() == null).count();
            long checkRemarkRequest = manifestoItemPutQtyRequest.getQuantities().stream().filter(b -> (b.getQuantity() == null || b.getQuantity() == 0) && (b.getRemark() == null || b.getRemark().equals(""))).count();
            long checkRemarkSoLong = manifestoItemPutQtyRequest.getQuantities().stream().filter(b ->  b.getRemark() != null && b.getRemark().length() > 1000).count();

            if(checkRefIdRequest > 0)
                return new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.REFID_IS_REQUEST
                );
            if(checkBoxQtyRequest > 0)
                return new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestoItemConstant.QUANTITY_IS_REQUEST
                );
            if(checkRemarkRequest > 0)
                return new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.REMARK_IS_REQUEST
                );
            if(checkRemarkSoLong > 0)
                return new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.REMARK_MUST_MAX_1000
                );

            for (QuantityRequest quantityRequest : manifestoItemPutQtyRequest.getQuantities()) {
                ManifestoItem manifestoItem = manifestoItemRepository.findOne(Integer.parseInt(quantityRequest.getRefId()));
                if(manifestoItem == null) {
                        String text;
                        text = quantityRequest.getRefId() + " , ";
                        refIdNotFound.append(text);
                } else {
                    UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(manifestoItemPutQtyRequest.getUpdateBy());
                    if(userTypeMaster == null)
                        return new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST,
                                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
                        );

                    java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

                    manifestoItem.setNewQuantity(quantityRequest.getQuantity());
                    manifestoItemRepository.save(manifestoItem);

                    if(quantityRequest.getRemark() != null) {
                        GenericInformation genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestoItemConstant.MANIFESTOITEM_DB, ManifestoItemConstant.ACTION_UPDATEQTY, manifestoItem.getId());
                        if (genericInformation != null) {
                            genericInformation.setValue(quantityRequest.getRemark());
                        } else {
                            genericInformation = new GenericInformation(ManifestoItemConstant.MANIFESTOITEM_DB, ManifestoItemConstant.ACTION_UPDATEQTY, manifestoItem.getId(), quantityRequest.getRemark(), userTypeMaster.getId(), date);
                        }
                        genericInformationRepository.save(genericInformation);
                    }
                }
            }

            response.setTitle(ManifestoItemConstant.UPDATE_MANIFESTOITEM);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            if(refIdNotFound.toString().equals(""))
                response.setDeveloperMessage(ManifestoItemConstant.UPDATE_MANIFESTOITEM_SUCCESS);
            else {
                String substring = refIdNotFound.toString().substring(0, refIdNotFound.length() - 2);
                response.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_NOT_FOUND + " : " +substring);
            }

        } catch (Exception e) {
            throw new GZException(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()  );
        }
        return response;
    }

}
