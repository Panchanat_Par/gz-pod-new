package com.gz.pod.categories.genericInformation;

public class GenericInformationConstant {
    public static final String RECEIVEDOCUMENT_DB = "receiveDocument";
    public static final String RECEIVEDOCUMENT_REFACTION = "receiveRemark";
}
