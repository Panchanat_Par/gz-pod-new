//package com.gz.pod.categories.customer.genericattribute;
//
//import com.gz.pod.entities.GenericAttribute;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//@Repository
//public interface GenericAttributeRepository extends CrudRepository<GenericAttribute, Integer> {
//
//    @Transactional
//    @Modifying
//    @Query("delete from GenericAttribute where EntityId = ?1")
//    void deleteByEntityId(int entityId);
//}
