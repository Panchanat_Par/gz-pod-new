//package com.gz.pod.categories.customer.requests;
//
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//import javax.validation.constraints.NotNull;
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//
//@Data
//public class CustomerRequest {
//    @ApiModelProperty(example = "LYNX Corporation", required = true)
//    private String companyName;
//    @ApiModelProperty(example = "1509901645364", required = true)
//    private String taxNo;
//    @ApiModelProperty(example = "Somchai", required = true)
//    private String name;
//    @ApiModelProperty(example = "Jaidee", required = true)
//    private String lastName;
//    @ApiModelProperty(example = "somchai@gmail.com")
//    private String email;
//    @ApiModelProperty(example = "somchai@gmail.com")
//    private String confirmEmail;
//    @ApiModelProperty(example = "0829384756", required = true)
//    private String tel;
//    @ApiModelProperty(example = "Somchai01", required = true)
//    private String password;
//    @ApiModelProperty(example = "Somchai01", required = true)
//    private String confirmPassword;
//    @ApiModelProperty(example = "Somchai01")
//    private String username;
//    private String passwordSalt;
//    private String adminComment;
//    private String systemName;
//    private String lastIpAddress;
//    private String customerNumber;
//    private String refQuotationNumber;
//    @NotNull(message = "passwordFormatId is require")
//    @ApiModelProperty(example = "1", required = true)
//    private int passwordFormatId;
//    private int typeCustomer;
//    @ApiModelProperty(example = "0", required = true)
//    private int affiliateId = 0;
//    @ApiModelProperty(example = "0", required = true)
//    private int vendorId = 0;
//    private int billingAddressId;
//    private int shippingAddressId;
//    @NotNull(message = "customerTypeId is require")
//    @ApiModelProperty(example = "0", required = true)
//    private int customerTypeId = 1;
//    private int somsBillingTypeId;
//    @ApiModelProperty(example = "1", allowableValues = "1,0", required = true)
//    @NotNull(message = "isTaxExempt is require")
//    private int isTaxExempt;
//    @ApiModelProperty(example = "1", allowableValues = "1,0", required = true)
//    @NotNull(message = "hasShoppingCartItems is require")
//    private int hasShoppingCartItems;
//    @ApiModelProperty(example = "1", allowableValues = "1,0", required = true)
//    @NotNull(message = "active is require")
//    private int active;
//    @ApiModelProperty(example = "0", allowableValues = "1,0", required = true)
//    @NotNull(message = "deleted is require")
//    private int deleted;
//    @ApiModelProperty(example = "1", allowableValues = "1,0", required = true)
//    private int isSystemAccount;
//    @ApiModelProperty(example = "0", allowableValues = "1,0", required = true)
//    @NotNull(message = "isCheckedCreditTerm is require")
//    private int isCheckedCreditTerm;
//    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z", required = true)
//    private Timestamp lastLoginDateUtc;
//    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z", required = true)
//    @NotNull(message = "lastActivityDateUtc is require")
//    private Timestamp lastActivityDateUtc;
//    @ApiModelProperty(example = "30000", required = true)
//    @NotNull(message = "creditLimit is require")
//    private BigDecimal creditLimit;
//    @ApiModelProperty(example = "30000", required = true)
//    @NotNull(message = "creditBalance is require")
//    private BigDecimal creditBalance;
//
//}
