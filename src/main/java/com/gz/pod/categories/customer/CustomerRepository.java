//package com.gz.pod.categories.customer;
//
//import com.gz.pod.entities.Customer;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface CustomerRepository extends CrudRepository<Customer, Integer> {
//
//    @Query("select max(id) from Customer")
//    int findByLastIdCustomer();
//
//}
