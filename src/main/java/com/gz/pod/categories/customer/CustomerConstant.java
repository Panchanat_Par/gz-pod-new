//package com.gz.pod.categories.customer;
//
//public class CustomerConstant {
//    public static final String TYPE_CUSTOMER = "TypeCustomer";
//    public static final String COMPANY_NAME = "CompanyName";
//    public static final String TAX_NO = "VatNumber";
//    public static final String FIRST_NAME = "FirstName";
//    public static final String LAST_NAME = "LastName";
//    public static final String TEL = "Phone";
//    public static final String KEY_GROUP = "Customer";
//    public static final int STORE_ID = 0;
//    public static final String ADD_CUSTOMER = "add new customer";
//    public static final String EDIT_CUSTOMER = "edit customer";
//    public static final String DELETE_CUSTOMER = "delete customer";
//    public static final String ADD_CUSTOMER_SUCCESS = "add new customer success!";
//    public static final String EDIT_CUSTOMER_SUCCESS = "edit customer success!";
//    public static final String DELETE_CUSTOMER_SUCCESS = "delete customer success!";
//
//    private CustomerConstant() {
//        throw new UnsupportedOperationException();
//    }
//
//}
