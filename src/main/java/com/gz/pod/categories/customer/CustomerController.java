//package com.gz.pod.categories.customer;
//
//import com.gz.pod.entities.Customer;
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.categories.customer.requests.CustomerRequest;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/customer")
//@Api(value = "Customer", description = "Customer management", produces = "application/json", tags = {"Customer"})
//public class CustomerController {
//    @Autowired
//    private CustomerService customerService;
//
//    @Autowired
//    public CustomerController(CustomerService customerService) {
//        this.customerService = customerService;
//    }
//
//    @PostMapping
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = Customer.class)})
//    public ResponseEntity<GZResponse> addCustomer(@RequestBody @Valid CustomerRequest customerRequest) throws Exception {
//        return ResponseEntity.ok(customerService.addNewCustomer(customerRequest));
//    }
//
//    @PutMapping("/{custId}")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = Customer.class)})
//    public ResponseEntity<GZResponse> editCustomer(
//            @ApiParam(value = "ไอดีลูกค้า", defaultValue = "2788")
//            @PathVariable int custId, @RequestBody @Valid CustomerRequest customerRequest) throws Exception {
//        return ResponseEntity.ok(customerService.editCustomer(custId, customerRequest));
//    }
//
//    @DeleteMapping("/{custId}")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = Customer.class)})
//    public ResponseEntity<GZResponse> deleteCustomer(@ApiParam(value = "ไอดีลูกค้า", defaultValue = "2788")
//                                                     @PathVariable int custId) throws Exception {
//        return ResponseEntity.ok(customerService.deleteCustomer(custId));
//    }
//}
