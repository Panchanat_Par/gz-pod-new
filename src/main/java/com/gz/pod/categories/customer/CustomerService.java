//package com.gz.pod.categories.customer;
//
//import com.gz.pod.entities.Customer;
//import com.gz.pod.entities.GenericAttribute;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponse;
//import com.gz.pod.categories.customer.genericattribute.GenericAttributeRepository;
//import com.gz.pod.categories.customer.requests.CustomerRequest;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//import java.util.UUID;
//
//@Service
//public class CustomerService {
//    @Autowired
//    private CustomerRepository customerRepository;
//    @Autowired
//    private GenericAttributeRepository genericAttributeRepository;
//
//    private ModelMapper modelMapper = new ModelMapper();
//
//    public GZResponse addNewCustomer(CustomerRequest customerRequest) throws GZException {
//        Customer customer;
//        GZResponse gzResponse = new GZResponse();
//        try {
//            customer = convertCustomerRequestToCustomer(customerRequest);
//            customer.setCustomerGuid(UUID.randomUUID().toString());
//            customer.setCreatedOnUtc(new Timestamp(System.currentTimeMillis()));
//            gzResponse.setData(customerRepository.save(customer));
//            gzResponse.setCode(HttpStatus.OK);
//            gzResponse.setMessage(HttpStatus.OK.toString());
//            gzResponse.setTitle(CustomerConstant.ADD_CUSTOMER);
//            gzResponse.setDeveloperMessage(CustomerConstant.ADD_CUSTOMER_SUCCESS);
//        } catch (Exception ex) {
//            throw new GZException(
//                    CustomerConstant.ADD_CUSTOMER,
//                    HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(),
//                    ex.getMessage()
//            );
//        }
//        return gzResponse;
//    }
//
//    public GZResponse editCustomer(int custId, CustomerRequest cust) throws GZException {
//        Customer customer;
//        GZResponse gzResponse = new GZResponse();
//        try {
//            customer = getCustomerById(custId);
//            customer.setEmail(cust.getEmail());
//            customer.setPassword(cust.getPassword());
//            List<GenericAttribute> attributeList = convertToListAttribute(cust);
//            for (int i = 0; i < attributeList.size(); i++) {
//                if (customer.getAttributeList().get(i).getKey().equals(attributeList.get(i).getKey())) {
//                    customer.getAttributeList().get(i).setValue(attributeList.get(i).getValue());
//                }
//            }
//            gzResponse.setData(customerRepository.save(customer));
//            gzResponse.setCode(HttpStatus.OK);
//            gzResponse.setMessage(HttpStatus.OK.toString());
//            gzResponse.setTitle(CustomerConstant.EDIT_CUSTOMER);
//            gzResponse.setDeveloperMessage(CustomerConstant.EDIT_CUSTOMER_SUCCESS);
//        } catch (Exception ex) {
//            throw new GZException(
//                    CustomerConstant.EDIT_CUSTOMER,
//                    HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(),
//                    ex.getMessage()
//            );
//        }
//        return gzResponse;
//    }
//
//    public GZResponse deleteCustomer(int custId) throws GZException {
//        Customer customer;
//        List<Customer> customerList = new ArrayList<>();
//        GZResponse gzResponse = new GZResponse();
//        customer = getCustomerById(custId);
//        customerList.add(customer);
//        gzResponse.setData(customerList);
//        gzResponse.setCode(HttpStatus.OK);
//        gzResponse.setMessage(HttpStatus.OK.toString());
//        gzResponse.setTitle(CustomerConstant.DELETE_CUSTOMER);
//        gzResponse.setDeveloperMessage(CustomerConstant.DELETE_CUSTOMER_SUCCESS);
//        try {
//            genericAttributeRepository.deleteByEntityId(custId);
//            customerRepository.delete(custId);
//        } catch (Exception ex) {
//            throw new GZException(
//                    CustomerConstant.DELETE_CUSTOMER,
//                    HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(),
//                    ex.getMessage()
//            );
//        }
//        return gzResponse;
//    }
//
//    public Customer getCustomerById(Integer id) {
//        Customer cus = customerRepository.findOne(id);
//        return cus;
//    }
//
//    public int getLastedIdFromCustomer() {
//        return (customerRepository.findByLastIdCustomer() + 1);
//    }
//
//    public Customer convertCustomerRequestToCustomer(CustomerRequest customerRequest) {
//        modelMapper.getConfiguration().setAmbiguityIgnored(true);
//        Customer customer = modelMapper.map(customerRequest, Customer.class);
//        List<GenericAttribute> attributeList = convertToListAttribute(customerRequest);
//        customer.setAttributeList(attributeList);
//        return customer;
//    }
//
//    public List<GenericAttribute> convertToListAttribute(CustomerRequest customerRequest) {
//        int entityId = getLastedIdFromCustomer();
//        String keyGroup = CustomerConstant.KEY_GROUP;
//        int storeId = CustomerConstant.STORE_ID;
//        List<GenericAttribute> attributeList = new ArrayList<>();
//        attributeList.add(new GenericAttribute(entityId, keyGroup, CustomerConstant.TYPE_CUSTOMER,
//                (customerRequest.getTypeCustomer() + ""), storeId));
//        attributeList.add(new GenericAttribute(entityId, keyGroup, CustomerConstant.COMPANY_NAME,
//                customerRequest.getCompanyName(), storeId));
//        attributeList.add(new GenericAttribute(entityId, keyGroup, CustomerConstant.TAX_NO,
//                customerRequest.getTaxNo(), storeId));
//        attributeList.add(new GenericAttribute(entityId, keyGroup, CustomerConstant.FIRST_NAME,
//                customerRequest.getName(), storeId));
//        attributeList.add(new GenericAttribute(entityId, keyGroup, CustomerConstant.LAST_NAME,
//                customerRequest.getLastName(), storeId));
//        attributeList.add(new GenericAttribute(entityId, keyGroup, CustomerConstant.TEL,
//                customerRequest.getTel(), storeId));
//        return attributeList;
//    }
//}
