package com.gz.pod.categories.jobStatus;

import com.gz.pod.entities.JobStatus;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface JobStatusRepository extends CrudRepository<JobStatus, Integer> {
    @Query(value = "SELECT top 1 * FROM pod.jobStatus j WHERE " +
            "CONVERT(date, createdDateUTC)= :deliveryDate and " +
            "jobId = :jobId Order By id Desc", nativeQuery = true)
    JobStatus findLastByjobIdAndDelivery(@Param("jobId") int jobId
            , @Param("deliveryDate") String deliveryDate);

//    @Query(value = "SELECT top 1 * FROM pod.jobStatus j WHERE " +
//            "jobId = :jobId Order By id Desc", nativeQuery = true)
//    JobStatus findLastByjobId(@Param("jobId") int jobId);

    @Query(value = "SELECT m.id, m.jobId, m.statusId, m.remark, m.createdBy, m.createdDateUTC, m.updatedBy, m.updatedDateUTC " +
            "       FROM  " +
            "       ( " +
            "           SELECT ROW_NUMBER() OVER (PARTITION BY jobId ORDER BY id DESC) AS rowNum " +
            "               , * " +
            "           FROM pod.jobStatus j " +
            "           WHERE CONVERT(date, j.createdDateUTC)=:deliveryDate " +
            "               and j.jobId In :jobIdList " +
            "       ) AS m " +
            "       WHERE m.rowNum = 1 " +
            "           and m.statusId In :statusIdList", nativeQuery = true)
    List<JobStatus> findJobIdByStatusIdListAndDelivery(@Param("jobIdList") List<Integer> jobIdList
            , @Param("statusIdList") List<Integer> statusIdList
            , @Param("deliveryDate") String deliveryDate);

    @Query(value = "SELECT top 1 * FROM pod.jobStatus j WHERE " +
            "CONVERT(date, createdDateUTC)= :deliveryDate " +
            "and jobId = :jobId " +
            "and statusId = :statusId " +
            "Order By id Asc", nativeQuery = true)
    JobStatus findFirstByjobIdAndStatusIdAndDelivery(@Param("jobId") int jobId
            , @Param("statusId") int statusId
            , @Param("deliveryDate") String deliveryDate);

    List<JobStatus> findByjobIdOrderByIdDesc(int jobId);

    @Query(value = "SELECT COUNT(*) " +
            "FROM pod.jobStatus j " +
            "WHERE j.id IN (SELECT MAX(jj.id) FROM pod.jobStatus jj " +
                            "WHERE CONVERT(date, createdDateUTC)= :deliveryDate and jobId IN :listJobId " +
                            "GROUP BY jobid" +
                            ") "+
            "AND statusId = :statusId "
            , nativeQuery = true)
    int findJobStatusDelete(@Param("listJobId") List<Integer> listJobId
            , @Param("deliveryDate") String deliveryDate
            , @Param("statusId") int statusId);

    @Query(value = " SELECT statusId FROM( " +
            "            select ROW_NUMBER() OVER(PARTITION BY j.jobId ORDER BY j.Id DESC)AS Row#, " +
            "             Id, statusId, jobId " +
            "            from pod.jobStatus j " +
            "            where " +
            "            j.jobId IN  :listJobId  AND " +
            "            CONVERT(date, j.createdDateUTC)= :deliveryDate ) A " +
            "            WHERE A.Row# = 1"
            , nativeQuery = true)
    List<Integer> findLastJobStatusByListStatusIgnoreDelete(@Param("listJobId") List<Integer>  listJobId
            , @Param("deliveryDate") String deliveryDate);

    @Query(value = "SELECT top 1 * FROM pod.jobStatus j WHERE " +
            "jobId = :jobId Order By id Desc", nativeQuery = true)
    JobStatus findLastByjobId(@Param("jobId") int jobId);

    @Query(value = "SELECT top 1 * FROM pod.jobStatus j WHERE " +
            "jobId = :jobId Order By id Desc", nativeQuery = true)
    JobStatus findLastStatusJob(@Param("jobId") int jobId);

    @Query(value = " SELECT jobId FROM( " +
            "            select ROW_NUMBER() OVER(PARTITION BY j.jobId ORDER BY j.Id DESC)AS Row#, " +
            "             Id, statusId, jobId " +
            "            from pod.jobStatus j " +
            "            where " +
            "            j.jobId IN  :listJobId  AND " +
            "            CONVERT(date, j.createdDateUTC)= :deliveryDate ) A " +
            "            WHERE A.Row# = 1 AND statusId IN :statusId"
            , nativeQuery = true)
    List<Integer> findLastJobStatusByListJobId(@Param("listJobId") List<Integer> listJobId
            , @Param("deliveryDate") String deliveryDate
            , @Param("statusId") List<Integer> statusId);

    @Query(value = " SELECT Id FROM( " +
            "            select ROW_NUMBER() OVER(PARTITION BY j.jobId ORDER BY j.Id DESC)AS Row#, " +
            "             Id, statusId, jobId " +
            "            from pod.jobStatus j " +
            "            where " +
            "            j.jobId = :jobId  AND " +
            "            CONVERT(date, j.createdDateUTC)= :deliveryDate ) A " +
            "            WHERE A.Row# = 2"
            , nativeQuery = true)
    List<Integer> findLastSecondJobStatusByJobIdReturnId(@Param("jobId") Integer jobId
            , @Param("deliveryDate") String deliveryDate
            );

    @Query(value = " SELECT statusId FROM( " +
            "            select ROW_NUMBER() OVER(PARTITION BY j.jobId ORDER BY j.Id DESC)AS Row#, " +
            "             Id, statusId, jobId " +
            "            from pod.jobStatus j " +
            "            where " +
            "            j.jobId IN  :listJobId  AND " +
            "            CONVERT(date, j.createdDateUTC)= :deliveryDate ) A "
            , nativeQuery = true)
    List<Integer> findJobStatusAllDay(@Param("listJobId") List<Integer>  listJobId
            , @Param("deliveryDate") String deliveryDate);


    @Query(value = " SELECT statusId FROM( " +
            "            select ROW_NUMBER() OVER(PARTITION BY j.jobId ORDER BY j.Id DESC)AS Row#, " +
            "             Id, statusId, jobId " +
            "            from pod.jobStatus j " +
            "            where " +
            "            j.jobId IN  :listJobId  AND " +
            "            CONVERT(date, j.createdDateUTC)= :deliveryDate ) A " +
            "            WHERE A.Row# = 1 AND statusId NOT IN :statusId AND statusId is NOT NULL"
            , nativeQuery = true)
    List<Integer> findLastJobStatusByListJobIdAndDeliveryDate(@Param("listJobId") List<Integer> listJobId
            , @Param("deliveryDate") String deliveryDate
            , @Param("statusId") List<Integer> statusId);

    @Query(value = " SELECT Id FROM( " +
            "            select ROW_NUMBER() OVER(PARTITION BY j.jobId ORDER BY j.Id DESC)AS Row#, " +
            "             Id, statusId, jobId " +
            "            from pod.jobStatus j " +
            "            where " +
            "            j.jobId IN  :listJobId  AND " +
            "            CONVERT(date, j.createdDateUTC)= :deliveryDate ) A " +
            "            WHERE statusId IN :statusId"
            , nativeQuery = true)
    List<Integer> findLastJobStatusByListJobIdAllDay(@Param("listJobId") List<Integer> listJobId
            , @Param("deliveryDate") String deliveryDate
            , @Param("statusId") List<Integer> statusId);


//    @Query("SELECT j.id FROM pod.jobStatus j " +
//            "WHERE jobId IN ( :listJobId )")
//    List<Integer> findIdStatusByListJobId(@Param("listJobId") List<Integer> listJobId);

}
