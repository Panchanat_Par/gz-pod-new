package com.gz.pod.categories.driver.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class DriverListRequest {
    @ApiModelProperty(required = true)
    @ApiParam(value = "ไอดีของพนักงานขับรถ", defaultValue = "1")
    @Pattern(regexp="^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$", message = "format date yyyy-MM-dd")
    private String deliveryDate;

    @ApiModelProperty(required = true)
    @NotNull(message = "login Id can't be null")
    @ApiParam(value = "login Id", defaultValue = "1")
    private Integer loginId;
}
