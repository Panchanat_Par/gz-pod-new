package com.gz.pod.categories.driver.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class DriverListByCheckerRequest {
    @ApiModelProperty(required = true)
    @ApiParam(value = "ไอดีของพนักงานขับรถ", defaultValue = "1")
    @NotNull(message = "loginId in request")
    private Integer loginId;
}
