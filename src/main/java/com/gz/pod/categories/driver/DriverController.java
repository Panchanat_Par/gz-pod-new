package com.gz.pod.categories.driver;

import com.gz.pod.categories.driver.requests.DriverListByCheckerRequest;
import com.gz.pod.categories.driver.requests.DriverListRequest;
import com.gz.pod.categories.driver.responses.DriverListResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("driver")
@Api(value = "Get Driver List", description = "Driver Management", produces = "application/json", tags = {"Driver"})
public class DriverController
{
    @Autowired
    DriverService driverService;

    @GetMapping("/list")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = DriverListResponse.class)})
    public ResponseEntity getDriverList(@Valid DriverListRequest driverListRequest) throws Exception {
        return ResponseEntity.ok(driverService.getDriverList(driverListRequest));
    }
}
