package com.gz.pod.categories.driver;

import com.gz.pod.categories.common.MasterStatusRepository;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.driver.requests.DriverListRequest;
import com.gz.pod.categories.driver.responses.DriverListResponse;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.manifestolist.ManifestolistService;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.*;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponsePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DriverService {

    @Autowired
    UserTypeMasterRepository userTypeMasterRepository;

    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    MasterStatusRepository masterStatusRepository;

    @Autowired
    JobStatusRepository jobStatusRepository;


    private final String regexCheckSortAZ = "^[A-Za-z0-9_.]+$";

    public GZResponsePage getDriverList(DriverListRequest driverListRequest) throws Exception
    {
        GZResponsePage gzResponsePage = new GZResponsePage();
        try {
            UserTypeMaster userLogin = userTypeMasterRepository.findByloginId(driverListRequest.getLoginId());
            if (userLogin == null ) {
                return new GZResponsePage(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
                );
            }
            else if (userLogin.getUserTypeId() != PODNumberStatusConstant.CHECKER ) {
                return new GZResponsePage(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), DriverConstant.NOT_CHECKER
                );
            }

            List<Integer> driverLists = manifestolistRepository.findByDeliveryDateGroupByDriver(driverListRequest.getDeliveryDate());
            if(driverLists == null || driverLists.isEmpty()) {
                return new GZResponsePage(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), DriverConstant.GET_DRIVER_LIST_NOT_FOUND
                );
            }
            List<DriverListResponse> driverListResponse = new ArrayList<>();
            List<MasterStatus> masterStatuses = (List<MasterStatus>) masterStatusRepository.findAll();
            String approval = masterStatuses.stream().filter(s -> s.getId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM).collect(Collectors.toList()).get(0).getName();
            String awaitingApproval = masterStatuses.stream().filter(s -> s.getId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER).collect(Collectors.toList()).get(0).getName();

            int countApproval = 0;
            int countAwaitingApproval = 0;
            driverLists = driverLists.stream().filter(s -> s != null).collect(Collectors.toList());
            for (Integer driverId : driverLists) {
                DriverListResponse getDriverListResponse = getDriverDetail(driverListRequest.getDeliveryDate(), driverId);
                if (getDriverListResponse.getName() != null ){
                    driverListResponse.add(getDriverListResponse);

                    if (getDriverListResponse.getStatus().equals(approval))
                    {
                        countApproval++;
                    }
                    else if ((getDriverListResponse.getStatus().equals(awaitingApproval)))
                    {
                        countAwaitingApproval++;
                    }
                }
            }

            List<DriverListResponse> driverListResponseAfterSort = driverListResponse.stream()
                    .filter(d -> !d.getName().toUpperCase().matches(regexCheckSortAZ))
                    .sorted(Comparator.comparing(DriverListResponse::getName))
                    .collect(Collectors.toList());

            driverListResponseAfterSort.addAll(driverListResponse.stream()
                    .filter(d -> d.getName().toUpperCase().matches(regexCheckSortAZ))
                    .sorted(Comparator.comparing(DriverListResponse::getName))
                    .collect(Collectors.toList()));

            gzResponsePage.setTotalItems(driverListResponseAfterSort.size());
            gzResponsePage.setTotalApproval(countApproval);
            gzResponsePage.setTotalWaitingApproval(countAwaitingApproval);
            gzResponsePage.setData(driverListResponseAfterSort);
            gzResponsePage.setCode(HttpStatus.OK);
            gzResponsePage.setTitle(DriverConstant.GET_DRIVER_LIST);
            gzResponsePage.setMessage(HttpStatus.OK.toString());
            gzResponsePage.setDeveloperMessage(DriverConstant.GET_DRIVER_LIST_SUCCESS);

        } catch (Exception e) {
            throw new GZException(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
            );
        }
        return gzResponsePage;
    }

    public DriverListResponse getDriverDetail(String deliveryDate, int driverId) {
        int countApproval = 0;
        int countWaitingApproval = 0;
        int checkIdToday ;

        List<Integer> statusId =  new ArrayList<>();
        statusId.add(PODNumberStatusConstant.JOB_DRIVER_SO_WAIT);
        statusId.add(PODNumberStatusConstant.JOB_DRIVER_CONFIRM);

        DriverListResponse driverListResponse1 = new DriverListResponse();
        List<MasterStatus> masterStatuses = (List<MasterStatus>) masterStatusRepository.findAll();

        String status ;
        List<Integer> listJobId = manifestolistRepository.findAllJobIdbyDriverId(deliveryDate, driverId);
        List<Integer> allLastJobStatus = jobStatusRepository.findLastJobStatusByListJobIdAndDeliveryDate(listJobId, deliveryDate,statusId);
        checkIdToday = allLastJobStatus.size();

        List<Integer> statusApproval = new ArrayList<>();
        statusApproval.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_REJECT);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_START);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_END);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM);
        statusApproval.add(PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT);
        statusApproval.add(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);

        List<Integer> statusAwaitingApproval = new ArrayList<>();
        statusAwaitingApproval.add(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        statusAwaitingApproval.add(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
        statusAwaitingApproval.add(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);

        for (int c : allLastJobStatus)
        {
            if (statusApproval.indexOf(c) != -1)
            {
                countApproval++;
            }
            else if (statusAwaitingApproval.indexOf(c) != -1)
            {
                countWaitingApproval ++;
            }
        }

        if(checkIdToday == countApproval && checkIdToday > 0 )
        {
            status = masterStatuses.stream().filter(s -> s.getId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM).collect(Collectors.toList()).get(0).getName();
        }
        else if (countWaitingApproval > 0 && checkIdToday > 0)
        {
            status = masterStatuses.stream().filter(s -> s.getId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER).collect(Collectors.toList()).get(0).getName();
        }
        else
            {
            status = "";
        }

        UserTypeMaster userTypeMaster = userTypeMasterRepository.findById(driverId);

        if(userTypeMaster != null && (userTypeMaster.getUserTypeId() == DriverConstant.DRIVER_TYPE || userTypeMaster.getUserTypeId() == DriverConstant.LF_DRIVER_TYPE) ) {
            driverListResponse1.setLoginId(userTypeMaster.getLoginId());
            driverListResponse1.setName(userTypeMaster.getName());
            driverListResponse1.setStatus(status);
        }

        return driverListResponse1 ;
    }
}
