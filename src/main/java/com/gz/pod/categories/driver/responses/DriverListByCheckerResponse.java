package com.gz.pod.categories.driver.responses;

import lombok.Data;

@Data
public class DriverListByCheckerResponse {

    private int loginId;
    private String driverName;

    public DriverListByCheckerResponse(int loginId, String driverName) {
        this.loginId = loginId;
        this.driverName = driverName;
    }

    public DriverListByCheckerResponse() {
    }
}
