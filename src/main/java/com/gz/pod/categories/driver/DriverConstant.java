package com.gz.pod.categories.driver;

public class DriverConstant {
    public static final String GET_DRIVER_LIST = "Get Driver list";
    public static final String GET_DRIVER_LIST_SUCCESS = "Get Driver list success";
    public static final String GET_DRIVER_LIST_NOT_FOUND = "Get Driver not found";
    public static final String NOT_CHECKER = "Cannot get driver list. Because you're not checker";
    public static final int DRIVER_TYPE = 1;
    public static final int LF_DRIVER_TYPE = 4;

}
