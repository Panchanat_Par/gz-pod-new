package com.gz.pod.categories.driver.responses;

import lombok.Data;

@Data
public class DriverListResponse {
    private int loginId;
    private String name;
    private String status;
}
