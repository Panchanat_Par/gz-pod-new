package com.gz.pod.categories.manifestoFile;

public class ManifestoFileConstant {

    public static final String UPDATE_MANIFESTOFILE = "Update save picture";
    public static final String UPDATE_MANIFESTOFILE_SUCCESS = "Update save picture Success";
    public static final String FILE_OVER_MAXIMUM_2_MB = "picture over size 2 MB";
    public static final String FILE_PNG_JPG_PDF_ONLY = "update file jpg,pdf,png only";

    public static final String GET_MANIFESTOFILE = "Get ManifestoFile";
    public static final String GET_MANIFESTOFILE_SUCCESS = "Get ManifestoFile Success";
    public static final String GET_MANIFESTOFILE_NOT_FOUND = "Get ManifestoFile Not Found";
    public static final String DEL_MANIFESTOFILE_SUCCESS = "Delete ManifestoFile Successful";
    public static final String NOT_OWNER_MANIFESTOFILE = "Cannot delete. Because your're not owner";
    public static final String NOT_USER_DRIVER = "Driver only to be upload";
    public static final String NOT_FOUND_MANIFESTOFILE = "Not found ManifestoFileId, Please check";




}
