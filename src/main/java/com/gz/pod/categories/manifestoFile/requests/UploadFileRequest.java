package com.gz.pod.categories.manifestoFile.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class UploadFileRequest {

    @ApiParam(value = "update By")
    @NotNull(message = "updateBy is request")
    @Min(value = 1, message = "updateBy not 0")
    private Integer updateBy;
    @ApiParam(value = "ใบสั่งซื้อ")
    @NotNull(message = "manifestoId is request")
    private Integer manifestoId;
    @ApiParam(value = "เอกสาร")
    @NotNull(message = "file is request")
    private MultipartFile file;
    @ApiParam(value = "rotate")
    @NotNull(message = "orientation is request")
    private Integer orientation;
}
