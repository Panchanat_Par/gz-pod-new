package com.gz.pod.categories.manifestoFile.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GetUploadFileRequest {

    @ApiParam(value = "ใบสั่งซื้อ")
    @NotNull(message = "manifestoFileId is request")
    private Integer manifestoFileId;

}
