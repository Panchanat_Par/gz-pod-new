package com.gz.pod.categories.manifestoFile.responses;

import lombok.Data;

@Data
public class GetUploadFileResponse {

    private byte[] file;

    public GetUploadFileResponse(byte[] file) {
        this.file = file;
    }
}
