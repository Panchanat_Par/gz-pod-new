package com.gz.pod.categories.manifestoFile;

import com.gz.pod.entities.JobStatus;
import com.gz.pod.entities.ManifestoFile;
import com.gz.pod.entities.Manifestolist;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ManifestoFileRepository extends CrudRepository<ManifestoFile, Integer> {

    List<ManifestoFile> findBymanifestoIdOrderByIdDesc(int manifestoId);

    @Query("select f.createBy from ManifestoFile f where id = :manifestoFileId")
    Integer findCreateBy(@Param("manifestoFileId") Integer manifestoFileId);

}
