package com.gz.pod.categories.manifestoFile;

import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.manifestoFile.requests.DeleteFileRequest;
import com.gz.pod.categories.manifestoFile.requests.GetUploadFileRequest;
import com.gz.pod.categories.manifestoFile.requests.UploadFileRequest;
import com.gz.pod.categories.manifestoFile.responses.GetUploadFileResponse;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.manifestolist.requests.ManifestoDetailRequest;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.ManifestoFile;
import com.gz.pod.entities.Manifestolist;
import com.gz.pod.entities.UserTypeMaster;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//

@Service
public class ManifestoFileService {

    @Autowired
    ManifestoFileRepository manifestoFileRepository;

    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    CommonService commonService;

    @Autowired
    UserTypeMasterRepository userTypeMasterRepository;

    private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg", "application/pdf");

    public GZResponse postFileByJob(UploadFileRequest uploadFileRequest) throws Exception {
        GZResponse response = new GZResponse();

        try {
            Manifestolist manifestolist = manifestolistRepository.findOne(uploadFileRequest.getManifestoId());
            if (manifestolist == null) {
                return new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_FOUND);
            }

            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(uploadFileRequest.getUpdateBy());
            if(userTypeMaster == null)
            {
                return new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);
            }
            if(userTypeMaster.getUserTypeId() != PODNumberStatusConstant.DRIVER) {
                return new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestoFileConstant.NOT_USER_DRIVER);
            }

            String fileContentType = uploadFileRequest.getFile().getContentType();
            if(!contentTypes.contains(fileContentType)) {
                return new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestoFileConstant.FILE_PNG_JPG_PDF_ONLY);
            }

            // Get length of file in bytes
            long fileSizeInBytes = uploadFileRequest.getFile().getSize();
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            long fileSizeInKB = fileSizeInBytes / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            long fileSizeInMB = fileSizeInKB / 1024;

            if (fileSizeInMB > 2) {
                return new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestoFileConstant.FILE_OVER_MAXIMUM_2_MB);
            }

            byte[] byteArr = rotateImage(uploadFileRequest.getFile(), uploadFileRequest.getOrientation(), fileContentType);
//            byte[] byteArr = uploadFileRequest.getFile().getBytes();

            Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

            ManifestoFile manifestoFile = new ManifestoFile(uploadFileRequest.getManifestoId(), byteArr, userTypeMaster.getId(), currentDate);
            ManifestoFile saved = manifestoFileRepository.save(manifestoFile);

            response.setTitle(ManifestoFileConstant.UPDATE_MANIFESTOFILE);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestoFileConstant.UPDATE_MANIFESTOFILE_SUCCESS);

            // create object to store in "data" response
            List<Object> storeID = new ArrayList<>();
            HashMap<String, String> returnIDAfterUpload = new HashMap<String, String>();
            returnIDAfterUpload.put("ID", String.valueOf(saved.getId()));
            storeID.add(returnIDAfterUpload);
            response.setData(storeID);
        } catch (Exception e) {
            throw new GZException(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }

        return response;

    }

    public byte[] rotateImage(MultipartFile file, int orientation, String typeFile) throws Exception {

        byte[] byteArr = file.getBytes();
        File outputFile = new File("LOCATION-FILE");
        String[] formatFIle = typeFile.split("/");

        try ( FileOutputStream outputStream = new FileOutputStream(outputFile)) {

            outputStream.write(byteArr);  //write the bytes and your done.

        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] imageInByte;
        try {

            BufferedImage originalImage = ImageIO.read(outputFile);
//            Metadata metadata = ImageMetadataReader.readMetadata(outputFile);
//            ExifIFD0Directory exifIFD0Directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
//            try {
//                orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
            if(originalImage != null) {
                int width = originalImage.getWidth();
                int height = originalImage.getHeight();

                AffineTransform affineTransform = new AffineTransform();

                switch (orientation) {
                    case 1:
                        break;
                    case 2: // Flip X
                        affineTransform.scale(-1.0, 1.0);
                        affineTransform.translate(-width, 0);
                        break;
                    case 3: // PI rotation
                        affineTransform.translate(width, height);
                        affineTransform.rotate(Math.PI);
                        break;
                    case 4: // Flip Y
                        affineTransform.scale(1.0, -1.0);
                        affineTransform.translate(0, -height);
                        break;
                    case 5: // - PI/2 and Flip X
                        affineTransform.rotate(-Math.PI / 2);
                        affineTransform.scale(-1.0, 1.0);
                        height = originalImage.getWidth();
                        width = originalImage.getHeight();
                        break;
                    case 6: // -PI/2 and -width
                        affineTransform.translate(height, 0);
                        affineTransform.rotate(Math.PI / 2);
                        height = originalImage.getWidth();
                        width = originalImage.getHeight();
                        break;
                    case 7: // PI/2 and Flip
                        affineTransform.scale(-1.0, 1.0);
                        affineTransform.translate(-height, 0);
                        affineTransform.translate(0, width);
                        affineTransform.rotate(3 * Math.PI / 2);
                        height = originalImage.getWidth();
                        width = originalImage.getHeight();
                        break;
                    case 8: // PI / 2
                        affineTransform.translate(0, width);
                        affineTransform.rotate(3 * Math.PI / 2);
                        height = originalImage.getWidth();
                        width = originalImage.getHeight();
                        break;
                    default:
                        break;
                }

                AffineTransformOp affineTransformOp = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BILINEAR);
                BufferedImage destinationImage = new BufferedImage(width, height, originalImage.getType());
                destinationImage = affineTransformOp.filter(originalImage, destinationImage);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(destinationImage, formatFIle[1], baos);
                imageInByte = baos.toByteArray();
            } else {
                imageInByte = new byte[] {};
            }

            outputFile.delete();

            return imageInByte;
        } catch (Exception ex) {
            throw new GZException(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
        }
    }

    public GZResponse getFileByJob(GetUploadFileRequest getUploadFileRequest) throws Exception {
        GZResponse response = new GZResponse();
        try {
            ManifestoFile manifestoFile = manifestoFileRepository.findOne(getUploadFileRequest.getManifestoFileId());
            if(manifestoFile != null) {
                response.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ManifestoFileConstant.GET_MANIFESTOFILE_SUCCESS);
                GetUploadFileResponse getUploadFileResponses = new GetUploadFileResponse(manifestoFile.getFileByte());
                response.setData(getUploadFileResponses);
            } else {
                response.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ManifestoFileConstant.GET_MANIFESTOFILE_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new GZException(ManifestoFileConstant.GET_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }

        return response;

    }


    public GZResponse deleteFileByJob (DeleteFileRequest request) {
        GZResponse response = new GZResponse();
        UserTypeMaster userLogin = userTypeMasterRepository.findByloginId(request.getLoginId());
        Integer driverId =  manifestoFileRepository.findCreateBy(request.getManifestoFileId());
        if (driverId == null ){
            response.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ManifestoFileConstant.NOT_FOUND_MANIFESTOFILE);
        }
            //only driver and owner
        else if (userLogin != null && userLogin.getUserTypeId() == ManifestolistConstant.DRIVER_ID && driverId == userLogin.getId()){
            manifestoFileRepository.delete(request.getManifestoFileId());
            response.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestoFileConstant.DEL_MANIFESTOFILE_SUCCESS);
        }
        else if (userLogin != null && (userLogin.getUserTypeId() == PODNumberStatusConstant.CHECKER || userLogin.getUserTypeId() == PODNumberStatusConstant.FINOPS  || driverId != userLogin.getId())) {
            response.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ManifestoFileConstant.NOT_OWNER_MANIFESTOFILE);
        }
        return response;
    }
}
