package com.gz.pod.categories.manifestoFile;

import com.gz.pod.categories.manifestoFile.requests.DeleteFileRequest;
import com.gz.pod.categories.manifestoFile.requests.GetUploadFileRequest;
import com.gz.pod.categories.manifestoFile.requests.UploadFileRequest;
import com.gz.pod.categories.manifestolist.requests.ManifestoDetailRequest;
import com.gz.pod.entities.Manifestolist;
import com.gz.pod.response.GZResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("manifestoFile")
@Api(value = "manifestoFile", description = "Manifesto management", produces = "application/json", tags = {"Manifesto"})
public class ManifestoFileController {

    @Autowired
    ManifestoFileService manifestoFileService;

    @PostMapping(value = "/uploadFile", consumes = {"multipart/form-data"})
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity postFileByJob(@Valid UploadFileRequest uploadFileRequest) throws Exception {
        return ResponseEntity.ok(manifestoFileService.postFileByJob(uploadFileRequest));
    }

    @GetMapping
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity getFileByJob(@Valid GetUploadFileRequest getUploadFileRequest) throws Exception {
        return ResponseEntity.ok(manifestoFileService.getFileByJob(getUploadFileRequest));
    }

    @DeleteMapping("/deleteFile")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity deleteFileByJob(@Valid DeleteFileRequest request) throws Exception {
        return ResponseEntity.ok(manifestoFileService.deleteFileByJob(request));
    }
}
