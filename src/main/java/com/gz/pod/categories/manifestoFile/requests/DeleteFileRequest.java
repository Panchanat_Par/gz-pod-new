package com.gz.pod.categories.manifestoFile.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeleteFileRequest {

    @ApiParam(value = "ใบสั่งซื้อ")
    @NotNull(message = "manifestoFileId is request")
    private Integer manifestoFileId;


    @ApiModelProperty(required = true)
    @NotNull(message = "login Id can't be null")
    @ApiParam(value = "login Id", defaultValue = "1")
    private Integer loginId;
}
