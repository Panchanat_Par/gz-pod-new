package com.gz.pod.categories.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.constants.Constant;
import com.gz.pod.entities.Config;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.ConfigResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ConfigService {

    public ConfigResponse getConfig() throws GZException {
        ConfigResponse configResponse = new ConfigResponse();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Config config = mapper.readValue(new File(Constant.CONFIG_LOCATION + "config.json"), Config.class);
            configResponse.setConfig(config);
        } catch (Exception e) {
            throw new GZException(Constant.CONFIG, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
            );
        }
        return configResponse;
    }


}
