package com.gz.pod.categories.config;

import com.gz.pod.entities.Config;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("api/v1/config")
@Api(value = "Config Service", description = "Config management",
        produces = "application/json", tags = {"Config Service"})
public class ConfigController {

    @Autowired
    ConfigService configService;

    @GetMapping
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = Config.class)})
    public ResponseEntity getConfig() throws Exception {
        return ResponseEntity.ok(configService.getConfig());
    }
}
