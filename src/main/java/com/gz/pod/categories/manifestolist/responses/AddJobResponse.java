package com.gz.pod.categories.manifestolist.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class AddJobResponse {
    private String manifestoType;
    private String jobId;

    public AddJobResponse(String manifestoType, String jobId) {
        this.manifestoType = manifestoType;
        this.jobId = jobId;
    }
}
