package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class DateShippingResponse {

    private String date;
    private Integer countPod;
    private Integer countSo;
    private Integer countLog;
    private Integer countInvoice;
    private List<DriverShippingResponse> driverList;
}
