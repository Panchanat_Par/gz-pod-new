package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BoxQtyRequest {
    @ApiParam(name = "refId")
    @NotNull(message = "refId is request")
    private Integer refId;
    @ApiParam(name = "box quantity")
    @NotNull(message = "boxQty is request")
    private Integer boxQty;
    @ApiParam(name = "remark")
    private String remark;
}
