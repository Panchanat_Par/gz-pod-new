package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UpdateDeleteJobRequest {
    @ApiParam(value = "เลขที่ ี user", defaultValue = "4")
    @NotNull(message = "loginId can't be null")
    @Min(value = 1, message = "loginId is require")
    private int loginId;

    @ApiModelProperty(required = true)
    @NotNull(message = "manifestoId can't be null")
    @ApiParam(value = "หมายเลขงาน", defaultValue = "1")
    private List<Integer> manifestoId;
}
