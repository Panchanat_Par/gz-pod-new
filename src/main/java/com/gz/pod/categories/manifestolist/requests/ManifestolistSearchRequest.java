package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ManifestolistSearchRequest {

    @ApiModelProperty(required = true)
    @ApiParam(value = "ไอดีของ checker", defaultValue = "1")
    @NotNull(message = "loginId in request")
    private Integer loginId;

    @ApiModelProperty(required = true)
    @Min(value = 1, message = "page must be greater than 1")
    @ApiParam(value = "หมายเลขหน้าที่ต้องการดู", defaultValue = "1")
    private Integer page = 1;

    @ApiParam(value = "วันที่ที่ต้องการดู", defaultValue = "2018-07-26")
    @Pattern(regexp="^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$", message = "format date yyyy-MM-dd")
    private String date;

    @Min(value = 1, message = "perPage must be greater than 1")
    @ApiParam(value = "จำนวนของที่ต้องการดูในแต่ละหน้า", defaultValue = "10")
    private Integer perPage = 50;

    @ApiParam(value = "fieldType", defaultValue = "driver")
    @Pattern(regexp = "(driver|job|route|sapCode)", message = "driver , job , sapCode or route Only")
    private String fieldType;

    @ApiParam(value = "value", defaultValue = "john")
    private String value;

}
