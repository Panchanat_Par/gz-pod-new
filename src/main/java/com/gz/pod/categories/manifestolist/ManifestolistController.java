package com.gz.pod.categories.manifestolist;

import com.gz.pod.categories.manifestolist.requests.*;
import com.gz.pod.response.GZManifestoListResponsePage;
import com.gz.pod.response.GZResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("manifestolist")
@Api(value = "Manifesto", description = "Manifesto management", produces = "application/json", tags = {"Manifesto"})
public class ManifestolistController {
    @Autowired
    ManifestolistService manifestolistService;

    @GetMapping
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZManifestoListResponsePage.class)})
    public ResponseEntity getShipping(@Valid ManifestolistRequest manifestolistRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.getManifestoList(manifestolistRequest));
    }

    @GetMapping("/checker")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZManifestoListResponsePage.class)})
    public ResponseEntity getShippingStatusList(@Valid ManifestolistCheckerRequest manifestolistCheckerRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.getManifestoListChecker(manifestolistCheckerRequest));
    }

    @GetMapping("/driverShipping")
    @TokenAuthentication
    public ResponseEntity getManifestoListChecker(@Valid DriverManifestolistCheckerRequest driverManifestolistCheckerRequest) {
        return ResponseEntity.ok(manifestolistService.getDriverManifestoListChecker(driverManifestolistCheckerRequest));
    }

    @PutMapping("/updateBox")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ManifestoPutBoxQtyRequest.class)})
    public  ResponseEntity updateManifestoBoxQty(@RequestBody @Valid ManifestoPutBoxQtyRequest request) throws Exception {
        return ResponseEntity.ok(manifestolistService.updateBoxManifestoByJob(request));
    }

    @GetMapping("/validateAddJob")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity validateAddJob(@Valid ValidateAddJobRequest validateAddJobRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.validateAddJob(validateAddJobRequest));
    }

    @PutMapping("/updateAddJob")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity updateAddJob(@RequestBody @Valid UpdateAddJobRequest updateAddJobRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.updateAddJob(updateAddJobRequest));
    }

    @GetMapping("/detail")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZManifestoListResponsePage.class)})
    public ResponseEntity getShippingDetail(@Valid ManifestoDetailRequest manifestoDetailRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.getManifestoDetailByManifestoId(manifestoDetailRequest));
    }

    @GetMapping("/validateDeleteJob")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity validateDeleteJob(@Valid ValidateDeleteJobRequest validateDeleteJobRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.validateDeleteJob(validateDeleteJobRequest));
    }

    @PutMapping("/updateDeleteJob")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity updateDeleteJob(@RequestBody @Valid UpdateDeleteJobRequest updateDeleteJobRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.updateDeleteJob(updateDeleteJobRequest));
    }

    @PostMapping("/rollback")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity rollbackManifesto(@RequestBody @Valid ManifestoRollbackRequest manifestoRollbackRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.rollbackManifesto(manifestoRollbackRequest));
    }

    @GetMapping("/search")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZManifestoListResponsePage.class)})
    public ResponseEntity getSearchShippinglist(@Valid ManifestolistSearchRequest ManifestolistSearchRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.getManifestoListChecker(ManifestolistSearchRequest));
    }

    @PostMapping("/saveShipping")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity saveManifestotoShippingList(@RequestBody @Valid AddManifestoRequest addManifestoRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.saveShipping(addManifestoRequest));
    }

    @PostMapping("/validateShipping")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GZResponse.class)})
    public ResponseEntity validateShippingList(@RequestBody @Valid AddManifestoRequest addManifestoRequest) throws Exception {
        return ResponseEntity.ok(manifestolistService.validateSaveShipping(addManifestoRequest));
    }
}
