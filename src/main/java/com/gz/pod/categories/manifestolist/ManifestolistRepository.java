package com.gz.pod.categories.manifestolist;

import com.gz.pod.entities.ManifestoOwner;
import com.gz.pod.entities.Manifestolist;
import com.gz.pod.entities.ManifestolistWithDriver;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface ManifestolistRepository extends CrudRepository<Manifestolist, Integer> {

    List<Manifestolist> findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(int driverId, String deliveryDate);

    @Query("select m from ManifestolistWithDriver m where driverId != 0 and podNumber is not null")
    List<ManifestolistWithDriver> findByPodNumberIsNotNullAndDriverIdIsNotNull();

    @Query("select m from ManifestolistWithDriver m where driverId != 0 and podNumber is not null and " +
            "( " +
            "( CONVERT(varchar(50), FulfillId) like %:job% or jobId like %:job% ) " +
            ")")
    List<ManifestolistWithDriver> findByPodNumberIsNotNullAndDriverIdIsNotNullAndSearch(
        @Param("job") String job
    );

    @Query("select m from Manifestolist m where podNumber is not null Order by podNumber desc")
    List<Manifestolist> findBypodNumberIsNotNullOrderBypodNumberDesc();

    @Query("select m from Manifestolist m where driverId = ?1 and podNumber is not null Order by podNumber desc")
    List<Manifestolist> findByDriverIdpodNumberIsNotNullOrderBypodNumberDesc(int driverId);

    List<Manifestolist> findByDeliveryDateAndPodNumberIsNotNull(String deliveryDate);

    List<Manifestolist> findAllBypodNumberOrderByIdAsc(String podNumber);

    List<Manifestolist> findAllBypodNumberAndDeliveryDateOrderByIdAsc(String podNumber, String deliveryDate);

    List<Manifestolist> findBypodNumberAndDeliveryDate(String podNumber, String deliveryDate);

    @Query("select m.id from Manifestolist  m " +
            "where m.deliveryDate = :deliveryDate " +
            "and m.delayDate IS NULL")
    List<Integer> findIdByDeliveryDateAndDelayDateIsNull(@Param("deliveryDate") String deliveryDate);

    @Query("select m.id from Manifestolist  m " +
            "where m.deliveryDate = :deliveryDate " +
            "AND m.delayDate IS NOT NULL")
    List<Integer> findIdByDeliveryDateAndDelayDateIsNotNull(@Param("deliveryDate") String deliveryDate);

    Manifestolist findTop1BypodNumberAndDeliveryDate(String podNumber, String deliveryDate);

    @Query("SELECT m.driverId " +
        " FROM " +
        "    Manifestolist m " +
        " WHERE (deliveryDate = ?1) " +
        " GROUP BY " +
        "    m.driverId")
    List<Integer> findByDeliveryDateGroupByDriver(@Param("deliveryDate") String deliveryDate);

    @Query("SELECT m.podNumber " +
            " FROM " +
            "    Manifestolist m " +
            " WHERE (deliveryDate = ?1 AND driverId = ?2) And (podNumber IS NOT NULL)" +
            " GROUP BY " +
            "    m.podNumber")
    List<String> findByDeliveryDateAndDriverIdAndPodNumberIsNotNullGroupByPodNumber(@Param("deliveryDate") String deliveryDate,@Param("driverId") int driverId);

    @Query("SELECT m.id " +
            " FROM " +
            "    Manifestolist m " +
            " " +
            " WHERE (deliveryDate = ?1 AND podNumber = ?2) " )
    List<Integer> findByDeliveryDateAndPodNumberWithManifestoId(@Param("deliveryDate") String deliveryDate, @Param("podNumber") String podNumber);

    @Query("SELECT m.id " +
            " FROM " +
            "    Manifestolist m " +
            " " +
            " WHERE (deliveryDate = ?1 AND driverId = ?2) And (podNumber IS NOT NULL)" )
    List<Integer> findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(@Param("deliveryDate") String deliveryDate,@Param("driverId") int driverId);

    List<Manifestolist> findAllBydriverIdAndDeliveryDate(int driverId, String deliveryDate);

    @Query("SELECT m.id " +
            " FROM " +
            "    Manifestolist m " +
            " " +
            " WHERE deliveryDate = :deliveryDate and driverId = :driverId" )
    List<Integer> findAllJobIdbyDriverId(@Param("deliveryDate") String deliveryDate, @Param("driverId")Integer driverId );

    @Query("SELECT m.id " +
            " FROM " +
            "    Manifestolist m " +
            " " +
            " WHERE deliveryDate = :deliveryDate and podNumber = :podNumber" )
    List<Integer> findAllJobIdbyPodNumber(@Param("deliveryDate") String deliveryDate, @Param("podNumber")String podNumber );

    @Query("SELECT m.delayDate " +
            " FROM " +
            "    Manifestolist m " +
            " " +
            " WHERE m.id = :id" )
    String findDelayDatebyJobId(@Param("id")int id );


    @Query("select max(m.podNumber) from Manifestolist  m " +
            "where MONTH(m.deliveryDate) = :month " +
            "   and YEAR(m.deliveryDate) = :year " +
            "   and convert(INT, SUBSTRING(m.podNumber, 1, 2)) = :month " +
            "   and convert(INT, SUBSTRING(m.podNumber, 3, 4)) = :year")
    String getMaxPODNumber(@Param("month") int month,@Param("year") int year);


    @Query("select m from Manifestolist m where podNumber is null and deliveryDate = :deliveryDate")
    List<Manifestolist> getManifestolistPODNumberNull(@Param("deliveryDate") String deliveryDate);

    @Query("SELECT m FROM Manifestolist m " +
            "WHERE podNumber IS NULL " +
            "and deliveryDate = :deliveryDate " +
            "and id IN :manifestoIdList")
    List<Manifestolist> getManifestolistByIdListPODNumberNull(@Param("deliveryDate") String deliveryDate
            ,@Param("manifestoIdList") List<Integer> manifestoIdList);

    @Query("select m from Manifestolist m where driverId = :driverId and deliveryDate = :deliveryDate and shipToCode = :shipToCode")
    List<Manifestolist> getExitingPODNumber(@Param("driverId") int driverId,@Param("deliveryDate") String deliveryDate,@Param("shipToCode") String shipToCode);

    @Modifying
    @Transactional
    @Query("delete from Manifestolist where jobId = ?1 And manifestoType <> 'Log'")
    void deleteByJobId(String jobId);

    @Modifying
    @Transactional
    @Query("delete from Manifestolist " +
            "where fulfillId = ?1 AND " +
            "manifestoType = 'Log'")
    void deleteByFulfillId(int fulfillId);

    @Modifying
    @Transactional
    @Query("delete from Manifestolist where podFlag = 'N' or podFlag = 'U'")
    void deletePodFlagNotUpdate();

    @Query("select m from Manifestolist m where podFlag = 'N' or podFlag = 'U'")
    List<Manifestolist> findManifestolistPODFlagNotOld();

    @Transactional
    @Modifying
    @Query(value = "update Manifestolist set podNumber = ?2 " +
            " where Id = ?1")
    void updatePODNumberById(int id, String podNumber);

    @Transactional
    @Modifying
    @Query(value = "update Manifestolist set podFlag = 'O' " +
            " where podFlag <> 'O'")
    void updatePODFlag();

    @Query("select m from Manifestolist m where (m.isGetItem = 0 or m.isGetItem is null) and m.manifestoType <> 'Log'")
    List<Manifestolist> findManifestolistNotGetItem();

    @Query("select m from Manifestolist m where jobId = :soNumber and m.manifestoType <> 'Log'")
    List<Manifestolist> findManifestoBySoNumber(@Param("soNumber") String soNumber);

    @Query("select m from Manifestolist m where jobId = :soNumber and m.manifestoType != 'Log'")
    Manifestolist findManifestoBySoNumberNotLog(@Param("soNumber") String soNumber);

    @Query("select m from Manifestolist m " +
            "where fulfillId = :fulfillId AND " +
            "manifestoType = 'Log'")
    Manifestolist findByFulfillId(@Param("fulfillId") int fulfillId);

    @Query("select m from Manifestolist m where Id = :Id")
    Manifestolist findAllManifestolistById(@Param("Id") int Id);

    @Query("select m.id from Manifestolist m where podNumber = :podNumber")
    List<Integer> findIdbypodNumber(@Param("podNumber") String podNumber);


    List<Manifestolist> findBydriverIdAndDeliveryDateAndPodNumber(int driverId, String deliveryDate, String podNumber);

    Manifestolist findByidAndDeliveryDate(int id, String deliveryDate);

    @Query("select m.podNumber from Manifestolist m where driverId = :driverId AND deliveryDate = :deliveryDate")
    List<String> findPodNumberBydriverIdAndDeliveryDate(@Param("driverId") int driverId, @Param("deliveryDate") String deliveryDate);

    //search
    List<Manifestolist> findBypodNumberContaining(String podNumber);
    List<Manifestolist> findByrouteNameContainingAndPodNumberNotNull(String routeMasterId);
    List<Manifestolist> findBysapCustCodeContainingAndPodNumberNotNullAndDeliveryDateAfter(String sapCode, String dateAfter);

    @Query("select m from ManifestolistWithDriver m where podNumber is not null and driverId != 0 and" +
            " routeName like %:routeName%")
    List<ManifestolistWithDriver> findJobWithDriverByrouteNameContaining(@Param("routeName") String routeName);

    @Query("select m from ManifestolistWithDriver m where podNumber is not null and driverId != 0 and " +
            " sapCustCode like %:sapCustCode%")
    List<ManifestolistWithDriver> findJobWithDriverBySapCustCodeContaining(@Param("sapCustCode") String sapCustCode);

    @Query("select m from Manifestolist m where driverId IN ( :listDriverId ) AND podNumber is NOT NULL ")
    List<Manifestolist> findByListDriverId(@Param("listDriverId")List<Integer> listDriverId);

    @Query("select m from ManifestolistWithDriver m where driverId IN ( :listDriverId ) AND podNumber is NOT NULL ")
    List<ManifestolistWithDriver> findJobWithDriverByListDriverId(@Param("listDriverId")List<Integer> listDriverId);

    @Query("select m from ManifestoOwner m where id IN ( :listId ) AND deliveryDate = :deliveryDate ")
    List<ManifestoOwner> findDriveIdByListId(@Param("listId")List<Integer> listId, @Param("deliveryDate") String deliveryDate);

    @Query("select podNumber from Manifestolist m where ( CONVERT(varchar(50), FulfillId) like %:job% or " +
            "jobId like %:job% ) AND " +
            "podNumber is NOT NULL " +
            "group by podNumber ")
    List<String> findByfulfillIdOrJobIdContaining(@Param("job")String job);

    @Query("select m from Manifestolist m where podNumber IN ( :listPodnumber ) ")
    List<Manifestolist> findByListPodNumber(@Param("listPodnumber")List<String> listPodnumber);

    @Query("select m.podNumber from Manifestolist m where id IN ( :ids ) group by podNumber ")
    List<String> findByListManifestoId(@Param("ids")List<Integer> ids);

    @Transactional
    @Modifying
    @Query(value = "update Manifestolist set delayDate = ?2 " +
            " where Id in ?1")
    void updateDelayDateById(List<Integer> idList, String delayDate);

    @Transactional
    @Modifying
    @Query(value = "update Manifestolist set deliveryDate = ?2 " +
            " where Id in ?1")
    void updateDeliveryDateById(List<Integer> idList, String deliveryDate);
}