package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class ShippingResponse {

    private String customerName;
    private String shipToAddress;
    private String podNumber;
    private String status;
    private String remark;
    private String userRemark;
    private Boolean flgDelay;
    private List<ManifestoResponse> data;

    public ShippingResponse() {
    }
}
