package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class DateShippingListResponse {

    private int countPodNumber;
    private int countSo;
    private int countLog;
    private int countInvoice;
    private String date;
    List<ShippingResponse> shippingList;

}
