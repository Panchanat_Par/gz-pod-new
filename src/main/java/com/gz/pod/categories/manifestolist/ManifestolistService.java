package com.gz.pod.categories.manifestolist;

import com.gz.pod.categories.MasterReasonRejectManifesto.MasterReasonRejectManifestoRepository;
import com.gz.pod.categories.common.*;
import com.gz.pod.categories.common.responses.ManifestoOwnerResponse;
import com.gz.pod.categories.datafromsoms.DataFromSOMSConstant;
import com.gz.pod.categories.datafromsoms.DataFromSOMSService;
import com.gz.pod.categories.driver.DriverService;
import com.gz.pod.categories.driver.responses.DriverListResponse;
import com.gz.pod.categories.genericInformation.GenericInformationConstant;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestoFile.ManifestoFileRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemConstant;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestolist.requests.*;
import com.gz.pod.categories.manifestolist.responses.*;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.podNumberStatus.responses.SaveShippingResponse;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentConstant;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentRepository;
import com.gz.pod.entities.*;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZManifestoListResponsePage;
import com.gz.pod.response.GZResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ManifestolistService {
    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    CommonService commonService;

    @Autowired
    DataFromSOMSService dataFromSOMSService;

    @Autowired
    ManifestoItemRepository manifestoItemRepository;

    @Autowired
    JobStatusRepository jobStatusRepository;

    @Autowired
    ManifestoFileRepository manifestoFileRepository;

    @Autowired
    GenericInformationRepository genericInformationRepository;

    @Autowired
    MasterReasonRejectManifestoRepository masterReasonRejectManifestoRepository;

    @Autowired
    ReceiveDocumentRepository receiveDocumentRepository;

    @Autowired
    UserTypeMasterRepository userTypeMasterRepository;

    @Autowired
    MasterStatusRepository masterStatusRepository;

    @Autowired
    DriverService driverService;

    private boolean checkerConfirmStatus;

    private final String regexCheckSortAZ = "^[A-Za-z0-9_.]+$";

    public GZManifestoListResponsePage getManifestoList(ManifestolistRequest request) throws GZException {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();
        int pageSize = 0;
        int totalPage = 0;
        List<GroupByDateResponse> groupByDateResponse = new ArrayList<>();
        try {
            if (request.getDate() == null) {
                request.setDate(java.time.LocalDate.now().toString());
            }
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(request.getLoginId());
            if (userTypeMaster == null) {
                return new GZManifestoListResponsePage(ManifestolistConstant.SHIPPING_LIST, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
                );
            }

            List<Manifestolist> list = manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(userTypeMaster.getId(), request.getDate());

                if (!list.isEmpty())
                {

                    // set Data and get last status
                    List<ManifestoResponse> listManisfesto = list.stream().map(
                            shipping -> new ManifestoResponse(
                                    shipping.getDriverId(),
                                    shipping.getId(),
                                    shipping.getManifestoType(),
                                    shipping.getJobId(),
                                    shipping.getCustomerName(),
                                    shipping.getShipToCode(),
                                    shipping.getShipToName(),
                                    shipping.getShipToAddress(),
                                    shipping.getAddressType(),
                                    shipping.getBoxQty(),
                                    shipping.getNewBoxQty(),
                                    shipping.getItemQty(),
                                    shipping.getSKUQty(),
                                    shipping.getRemark(),
                                    shipping.getRouteName(),
                                    shipping.getPodNumber(),
                                    shipping.getLogType(),
                                    shipping.getFulfillId(),
                                    shipping.getDelayDate() != null ? shipping.getDelayDate() : shipping.getDeliveryDate(),
                                    shipping.getContactPerson(),
                                    shipping.getInvoiceNo(  ),
                                    shipping.getNote(),
                                    getLastJobStatus(shipping.getId(), shipping.getDeliveryDate()),
                                    shipping.getDelayDate()
                            )
                    ).collect(Collectors.toList());
                    // countApprovalItem ByGroup เพราะว่า ต้องการจะรู้ว่าทั้งหมดมีกี่ใบที่ approval ไม่ approval
                    List<Integer> countApprovalItemByGroup = getCountApprovalItemByGroup(listManisfesto);
                    List<ManifestoResponse> listManifestoDelay = new ArrayList<>();
                    List<ManifestoResponse> listManifestoDelayTemp;
                    List<ManifestoResponse> listManifestoTemp;
                    // find list item podItem (Start)
                    List<ManifestoResponse> podStart = new ArrayList<>();
                    // create object to get data
                    List<Object> storeDataPODResponse = new ArrayList<>();
                switch (request.getTab()) {
                    case ManifestolistConstant.SHIPPING:

                        listManisfesto = listManisfesto.stream().filter(m ->
                                (m.getJobStatus() != null &&
                                        m.getJobStatus().getMasterStatus() != null &&
                                        (
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_CONFIRM ||
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE ||
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER ||
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_APPROVE ||
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM
                                        )
                                )
                        ).collect(Collectors.toList());
                        break;
                    case ManifestolistConstant.TODAYLIST:

                        listManisfesto = listManisfesto.stream().filter(m ->
                                m.getJobStatus() != null &&
                                        m.getJobStatus().getMasterStatus() != null &&
                                        m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_SO_WAIT
                        ).collect(Collectors.toList());
                        break;
                    case ManifestolistConstant.DELIVERED:
                        listManisfesto = listManisfesto.stream().filter(m ->
                                m.getJobStatus() != null &&
                                (
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY ||
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY ||
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY ||
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM ||
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT
                                )
                        ).collect(Collectors.toList());

                        break;
                    case ManifestolistConstant.SHIPPED:
                        List<ManifestoResponse> listManisfestoForPage = listManisfesto.stream().filter( m ->
                                m.getJobStatus() != null && (
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM ||
                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT
                                )
                        ).collect(Collectors.toList()).stream()
                                .sorted(Comparator.comparing(ManifestoResponse::getDeliveryDate).reversed().thenComparing(ManifestoResponse::getPodNumber).reversed())
                                .collect(Collectors.toList());

                        PagedListHolder<ShippingResponse> dataForPage = manifestoGroupPagedList(listManisfestoForPage, request.getPage(), request.getPerPage(), userTypeMaster);
                        pageSize = dataForPage.getPageSize();
                        totalPage = dataForPage.getPageCount();

                        listManisfestoForPage = prepareManifestoAddPage(listManisfestoForPage, request.getPerPage(), request.getPage());

                        // find podStart when delay date = null
                        podStart = (listManisfesto.stream().filter(m ->
                                (m.getJobStatus() != null &&
                                        m.getJobStatus().getMasterStatus() != null &&(
                                        m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_START ||
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_END
                                )
                                )
                        ).collect(Collectors.toList()));
                        // push podStart detail to object
                        response.setPodNumberStatusStart(false);
                        if (!podStart.isEmpty()){
                            HashMap<String, String> podStartResponse = new HashMap<String, String>();
                            podStartResponse.put("podNumber", podStart.get(0).getPodNumber());
                            podStartResponse.put("customerName", podStart.get(0).getCustomerName());
                            storeDataPODResponse.add(podStartResponse);
                            response.setPodNumberStart(storeDataPODResponse);
                            response.setPodNumberStatusStart(true);
                        }

                        if(listManisfestoForPage.isEmpty() && podStart.isEmpty()) {
                            return new GZManifestoListResponsePage(ManifestolistConstant.SHIPPING_LIST, HttpStatus.OK,
                                    HttpStatus.OK.toString(), ManifestolistConstant.MANIFESTO_NOT_FOUND
                            );
                        }else if(listManisfestoForPage.isEmpty() && !podStart.isEmpty()){
                            //add page
                            response.setCurrentPage(request.getPage());
                            response.setTitle(ManifestolistConstant.SHIPPING_LIST);
                            response.setCode(HttpStatus.OK);
                            response.setMessage(HttpStatus.OK.toString());
                            response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);
                            response.setCheckerConfirmStatus(checkerConfirmStatus);
                            response.setPageSize(request.getPerPage());
                            response.setTotalPage(0);
                            response.setCurrentPage(request.getPage());
                            return response;
                        }

                        // find shipped list when delay != deliveryDate
                        listManifestoDelay = listManisfestoForPage.stream().filter(m ->
                            (m.getDelayDate() != null) &&  (!m.getDeliveryDate().equalsIgnoreCase(request.getDate())) &&
                                    (m.getJobStatus() != null &&
                                            m.getJobStatus().getMasterStatus() != null && (
                                                    m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM ||
                                                    m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_REJECT
                                            )
                                    )
                        ).collect(Collectors.toList());

                        listManifestoDelayTemp = listManisfesto.stream().filter(m ->
                                (m.getDelayDate() != null) && (!m.getDeliveryDate().equalsIgnoreCase(request.getDate())) &&
                                        (m.getJobStatus() != null &&
                                                m.getJobStatus().getMasterStatus() != null && (
                                                m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM ||
                                                        m.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_REJECT
                                        )
                                        )
                        ).collect(Collectors.toList());

                        listManisfesto = listManisfestoForPage.stream().filter(m ->
                                ( m.getDelayDate() == null ||
                                        m.getDeliveryDate().equalsIgnoreCase(request.getDate())
                                ) &&
                                        m.getJobStatus() != null &&
                                        (
                                                m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM ||
                                                        m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT
                                        )
                        ).collect(Collectors.toList());

                        listManifestoTemp = listManisfesto.stream().filter(m ->
                                ( m.getDelayDate() == null ||
                                    m.getDeliveryDate().equalsIgnoreCase(request.getDate())
                                ) &&
                                m.getJobStatus() != null &&
                                        (
                                                m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM ||
                                                m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT
                                        )
                                ).collect(Collectors.toList());

                        if(!listManifestoDelay.isEmpty())
                        {
                            Map<String, List<ManifestoResponse>> map3 = listManifestoDelay.stream().collect(Collectors.groupingBy(ManifestoResponse::getDelayDate));
                            Set<String> dateGroupedKeySet3 = map3.keySet();
                            List<String> delayDate = dateGroupedKeySet3.stream().sorted(Comparator.comparing(s -> s)).collect(Collectors.toList());

                            for (String delayDateIndex : delayDate){
                                GroupByDateResponse groupByDateResponse1 = new GroupByDateResponse();
                                List<ManifestoResponse> delayListShipping = listManifestoDelay.stream().filter(m -> m.getDelayDate().equalsIgnoreCase(delayDateIndex)).collect(Collectors.toList());
                                List<ManifestoResponse> delayListShippingTemp = listManifestoDelayTemp.stream().filter(m -> m.getDelayDate().equalsIgnoreCase(delayDateIndex)).collect(Collectors.toList());
                                // group by podNumber
                                List<ShippingResponse> shippingListDelay = groupByPODNumber(delayListShipping, request.getTab(), userTypeMaster);
                                List<ShippingResponse> shippingListDelayTemp = groupByPODNumber(delayListShippingTemp, request.getTab(), userTypeMaster);
                                groupByDateResponse1.setDeliveryDate(delayDateIndex);
                                groupByDateResponse1.setManifestoList(shippingListDelay);
                                List<Integer> countItemByGroup = getCountItemByGroup(shippingListDelayTemp);
                                groupByDateResponse1.setCountSo(countItemByGroup.get(0));
                                groupByDateResponse1.setCountLog(countItemByGroup.get(1));
                                groupByDateResponse1.setCountInvoice(countItemByGroup.get(2));
                                groupByDateResponse1.setCountPODNumber(shippingListDelayTemp.size());
                                groupByDateResponse.add(groupByDateResponse1);
                            }

                        }


                        if (!listManisfesto.isEmpty()){
                            // groupItem when delayDate = null
                            GroupByDateResponse groupByDateResponse2 = new GroupByDateResponse();
                            // group by podNumber
                            List<ShippingResponse> shippingList = groupByPODNumber(listManisfesto, request.getTab(), userTypeMaster);
                            List<ShippingResponse> shippingListTemp = groupByPODNumber(listManifestoTemp, request.getTab(), userTypeMaster);
                            groupByDateResponse2.setDeliveryDate(listManisfesto.get(0).getDeliveryDate());
                            groupByDateResponse2.setManifestoList(shippingList);
                            List<Integer> countItemByGroup = getCountItemByGroup(shippingListTemp);
                            groupByDateResponse2.setCountSo(countItemByGroup.get(0));
                            groupByDateResponse2.setCountLog(countItemByGroup.get(1));
                            groupByDateResponse2.setCountInvoice(countItemByGroup.get(2));
                            groupByDateResponse2.setCountPODNumber(shippingListTemp.size());
                            groupByDateResponse.add(groupByDateResponse2);
                        }
                        break;
                }
                    PagedListHolder<?> listResponseAddPage ;
                if(!request.getTab().equalsIgnoreCase(ManifestolistConstant.SHIPPED)) {
                    if (listManisfesto.isEmpty()) {
                        response.setCountApprovalSo(countApprovalItemByGroup.get(0));
                        response.setCountApprovalLog(countApprovalItemByGroup.get(1));
                        response.setCountNotApprovalSo(countApprovalItemByGroup.get(2));
                        response.setCountNotApprovalLog(countApprovalItemByGroup.get(3));
                        response.setCheckerConfirmStatus(checkerConfirmStatus);
                        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);
                        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
                        response.setCode(HttpStatus.OK);
                        response.setMessage(HttpStatus.OK.toString());
                        return response;
                    }
                    listManisfesto.stream().sorted(Comparator.comparing(ManifestoResponse::getDeliveryDate).reversed().thenComparing(ManifestoResponse::getPodNumber).reversed())
                            .collect(Collectors.toList());

                    Map<String, List<ManifestoResponse>> mappedJobsByPODNumber = listManisfesto.stream().collect(Collectors.groupingBy(b -> b.getPodNumber()));
                    Set<String> groupByPodNumber = mappedJobsByPODNumber.keySet();
                    List<String> listPodNumber = groupByPodNumber.stream().sorted(Comparator.comparing(s -> s.toString()).reversed()).collect(Collectors.toList());

                    List<ShippingResponse> podNumberResponseList = new ArrayList<>();
                    for (String podNumber:
                            listPodNumber) {
                        List<ManifestoResponse> temp = listManisfesto.stream().filter(m -> m.getPodNumber().equalsIgnoreCase(podNumber)).collect(Collectors.toList());
                        podNumberResponseList.add(groupByPODNumber(temp, request.getTab(), userTypeMaster).get(0));
                    }

                    // countItem ByGroup
                    List<Integer> countItemByGroup = getCountItemByGroup(podNumberResponseList);
                    // set response
                    response.setCountSo(countItemByGroup.get(0));
                    response.setCountLog(countItemByGroup.get(1));
                    response.setCountInvoice(countItemByGroup.get(2));
                    response.setCountApprovalSo(countApprovalItemByGroup.get(0));
                    response.setCountApprovalLog(countApprovalItemByGroup.get(1));
                    response.setCountNotApprovalSo(countApprovalItemByGroup.get(2));
                    response.setCountNotApprovalLog(countApprovalItemByGroup.get(3));
                    response.setCountPodNumber(podNumberResponseList.size());

                    listResponseAddPage = new PagedListHolder<>(podNumberResponseList);
                    listResponseAddPage.setPage(request.getPage()-1);
                    listResponseAddPage.setPageSize(request.getPerPage());
                    response.setPageSize(listResponseAddPage.getPageSize());
                    response.setTotalPage(listResponseAddPage.getPageCount());
                    response.setData(listResponseAddPage.getPageList());
                } else {
                    response.setPageSize(pageSize);
                    response.setTotalPage(totalPage);
                    response.setData(groupByDateResponse);
                }

                //add page
                response.setCurrentPage(request.getPage());
                response.setTitle(ManifestolistConstant.SHIPPING_LIST);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);
                response.setCheckerConfirmStatus(checkerConfirmStatus);

            } else {
                return new GZManifestoListResponsePage(ManifestolistConstant.SHIPPING_LIST, HttpStatus.OK,
                        HttpStatus.OK.toString(), ManifestolistConstant.MANIFESTO_NOT_FOUND
                );
            }
        } catch (Exception e) {
            throw new GZException(ManifestolistConstant.SHIPPING_LIST, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
            );
        }
        return response;
    }

    private List<ManifestoResponse> prepareManifestoAddPage(List<ManifestoResponse> listManisfestoForPage, Integer perPage, Integer page) {
        int startData = (page * perPage) - perPage;
        int size = 0;
        String podNumber = null;
        String delay = "";
        List<ManifestoResponse> responses = new ArrayList<>();
        for (int i = startData; i < listManisfestoForPage.size(); i++) {
            String oldDelay = listManisfestoForPage.get(i).getDelayDate() == null ? "" : listManisfestoForPage.get(i).getDelayDate();
            if(size == perPage && (!podNumber.equals(listManisfestoForPage.get(i).getPodNumber()) || !delay.equals(oldDelay) ) ) {
                break;
            }
            if(podNumber == null || !podNumber.equalsIgnoreCase(listManisfestoForPage.get(i).getPodNumber()) || !delay.equalsIgnoreCase(listManisfestoForPage.get(i).getDelayDate()) ) {
                size++;
                podNumber = listManisfestoForPage.get(i).getPodNumber();
                delay = oldDelay;
            }
            responses.add(listManisfestoForPage.get(i));
        }
        return responses;
    }

    public GZResponse getManifestoListChecker(ManifestolistSearchRequest request) {
        GZResponse response = new GZResponse();
        try {
            // check user
            UserTypeMaster currentUser = commonService.getUserTypeMasterForReturn(request.getLoginId());
            if (currentUser == null) {
                response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
                return response;
            }

            // if user not checker or finop
            if (currentUser.getUserTypeId() != PODNumberStatusConstant.CHECKER && currentUser.getUserTypeId() != PODNumberStatusConstant.FINOPS) {
                response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_ALLOW);
                return response;
            }

            List<ManifestolistWithDriver> manifestoList = new ArrayList<>();

            // filter search criteria
            String textSearch = request.getValue();
            String jobSearch = "";
            List<Integer> driverIds = new ArrayList<>();
            if (request.getFieldType() != null && request.getValue() != null) {
                switch (request.getFieldType()) {
                case "driver":
                    List<UserTypeMaster> userTypeMasters = userTypeMasterRepository.findBynameContainingAndUserTypeId(textSearch, PODNumberStatusConstant.DRIVER);
                    if (!userTypeMasters.isEmpty()) {
                        driverIds = userTypeMasters.stream().map(u -> u.getId()).collect(Collectors.toList());
                    }

                    // get manifesto list by driver ids
                    if (!driverIds.isEmpty()) {
                        manifestoList = manifestolistRepository.findJobWithDriverByListDriverId(driverIds);
                    } else {
                        manifestoList = new ArrayList<>();
                    }
                    break;

                    case "job":
                        jobSearch = request.getValue();
                        manifestoList = manifestolistRepository.findByPodNumberIsNotNullAndDriverIdIsNotNull();
                        break;

                    case "route":
                        manifestoList = manifestolistRepository.findJobWithDriverByrouteNameContaining(request.getValue());
                        break;

                    case "sapCode":
                        manifestoList = manifestolistRepository.findJobWithDriverBySapCustCodeContaining(request.getValue());
                        break;
                }
            } else { // get all if no search
                manifestoList = manifestolistRepository.findByPodNumberIsNotNullAndDriverIdIsNotNull();
            }

            // set original delivery date
            manifestoList = manifestoList.stream()
                    .map(m -> {
                        m.setOriginDeliveryDate(m.getDeliveryDate());
                        return m;
                    })
                    .collect(Collectors.toList());

            // replace delivery date
            manifestoList = getFormattedDelayDateJobList(manifestoList, jobSearch);

            // sort all jobs
            List<ManifestolistWithDriver> sortedManifestoList = manifestoList.stream()
                    .filter(b -> b.getUserTypeMaster() != null)
                    .filter(b -> {
                        if (request.getDate() != null) {
                            return request.getDate().equals(b.getDeliveryDate());
                        }
                        return true;
                    })
                    .sorted(Comparator.comparing(ManifestolistWithDriver::getDeliveryDate).reversed()
                        .thenComparing(b -> b.getUserTypeMaster().getName()))
                    .collect(Collectors.toList());

            // Check is empty
            if (sortedManifestoList.isEmpty()) {
                response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);
                return response;
            }

            // Map data to POD level
            Map<String, List<ManifestolistWithDriver>> mappedPods = sortedManifestoList.stream()
                    .collect(Collectors.groupingBy(b -> b.getDeliveryDate() + ":" + b.getUserTypeMaster().getName() + ":" + b.getPodNumber()));

            // set podgroup list
            List<PODGroup> podGroupList = new ArrayList<>();
            for (Map.Entry<String, List<ManifestolistWithDriver>> mapPod:
                mappedPods.entrySet()){

                String[] ks = mapPod.getKey().split(":");

                PODGroup p = new PODGroup();
                p.setDate(ks[0]);
                p.setDriverName(ks[1]);
                p.setPodNumber(ks[2]);

                p.setJobList(mapPod.getValue());
                podGroupList.add(p);
            }

            // sort pog group
            List<PODGroup> firstSortedPODGroupList = podGroupList.stream()
                    .sorted(
                            Comparator.comparing(PODGroup::getDate).reversed()
                                    .thenComparing(PODGroup::getDriverName)
                                    .thenComparing(Collections.reverseOrder(Comparator.comparing(PODGroup::getPodNumber)))
                    )
                    .collect(Collectors.toList());

            List<PODGroup> sortedPODGroupList = new ArrayList<>();

            Map<String, List<PODGroup>> groupDateForSort =  firstSortedPODGroupList.stream().collect(Collectors.groupingBy(PODGroup::getDate));
            List<String> sortedDateKey = groupDateForSort.keySet().stream().sorted(Comparator.comparing(s -> s.toString()).reversed()).collect(Collectors.toList());
            for (String sk:
                    sortedDateKey) {

                List<PODGroup> firstGroup = groupDateForSort.get(sk).stream().filter(d -> !d.getDriverName().toUpperCase().matches(regexCheckSortAZ))
                        .sorted(Comparator.comparing(PODGroup::getDriverName))
                        .collect(Collectors.toList());
                List<PODGroup> secondGroup = groupDateForSort.get(sk).stream().filter(d -> d.getDriverName().toUpperCase().matches(regexCheckSortAZ))
                        .sorted(Comparator.comparing(PODGroup::getDriverName))
                        .collect(Collectors.toList());

                List<PODGroup> listForDate = new ArrayList<>();
                listForDate.addAll(firstGroup);
                listForDate.addAll(secondGroup);
                sortedPODGroupList.addAll(listForDate);
            }

            // pagination
            PagedListHolder<PODGroup> podPageHolder = new PagedListHolder<>(sortedPODGroupList);
            podPageHolder.setPageSize(request.getPerPage());
            podPageHolder.setPage(request.getPage() - 1);

            // prevent exceed page number
            if (request.getPage() > podPageHolder.getPageCount()) {
                response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ManifestolistConstant.EXCEED_PAGE_NUMBER);

                CheckerShippingStatusPageResponse pageData = new CheckerShippingStatusPageResponse();
                pageData.setTotalPage(podPageHolder.getPageCount());
                pageData.setTotalItems(sortedPODGroupList.size());
                pageData.setPageSize(podPageHolder.getPageSize());
                response.setData(pageData);

                return response;
            }

            List<PODGroup> pagePODGroup = podPageHolder.getPageList();

            List<DateShippingResponse> dList = new ArrayList<>();

            // group by date
            Map<String, List<PODGroup>> mappedDate = pagePODGroup.stream()
                    .collect(Collectors.groupingBy(b -> b.getDate()));
            for (Map.Entry<String, List<PODGroup>> mapDate :
                    mappedDate.entrySet()) {
                DateShippingResponse das = new DateShippingResponse();
                das.setDate(mapDate.getKey());

                // group by driverList
                Map<String, List<PODGroup>> mappedDriver = mapDate.getValue().stream()
                        .collect(Collectors.groupingBy(p -> p.getDriverName()));

                List<DriverShippingResponse> driverList = new ArrayList<>();

                // stack of podRes by date
                List<ShippingResponse> podResStack = new ArrayList<>();

                for (Map.Entry<String, List<PODGroup>> mapDriver:
                    mappedDriver.entrySet()){
                    DriverShippingResponse driverShippingResponse = new DriverShippingResponse();
                    driverShippingResponse.setDriverName(mapDriver.getKey());

                    // set driver info by first element
                    driverShippingResponse.setDriverId(mapDriver.getValue().get(0).getJobList().get(0).getDriverId());

                    // create job list
                    List<ManifestoResponse> jobResponseList = new ArrayList<>();
                    for (PODGroup g: mapDriver.getValue()
                    ){
                        List<ManifestoResponse> jobResponseListByPOD = g.getJobList().stream()
                                .map(shipping -> new ManifestoResponse(
                                    shipping.getDriverId(),
                                    shipping.getId(),
                                    shipping.getManifestoType(),
                                    shipping.getJobId(),
                                    shipping.getCustomerName(),
                                    shipping.getShipToCode(),
                                    shipping.getShipToName(),
                                    shipping.getShipToAddress(),
                                    shipping.getAddressType(),
                                    shipping.getBoxQty(),
                                    shipping.getNewBoxQty(),
                                    shipping.getItemQty(),
                                    shipping.getSKUQty(),
                                    shipping.getRemark(),
                                    shipping.getRouteName(),
                                    shipping.getPodNumber(),
                                    shipping.getLogType(),
                                    shipping.getFulfillId(),
                                    shipping.getDeliveryDate(),
                                    shipping.getContactPerson(),
                                    shipping.getInvoiceNo(),
                                    shipping.getNote(),
                                    getLastJobStatus(shipping.getId(), shipping.getOriginDeliveryDate()),
                                    shipping.getDelayDate()
                                )
                                ).collect(Collectors.toList());

                        jobResponseList.addAll(jobResponseListByPOD);
                    }

                    // group pod
                    List<ShippingResponse> podByDriver = groupByPODNumber(jobResponseList, ManifestolistConstant.CHECKER_APPROVED, currentUser);
                    podByDriver = podByDriver.stream().sorted(Comparator.comparing(ShippingResponse::getPodNumber).reversed()).collect(Collectors.toList());
                    driverShippingResponse.setShippingList(podByDriver);

                    // add to stack
                    podResStack.addAll(podByDriver);

                    driverList.add(driverShippingResponse);
                }
                // sort driver name
                driverList = driverList.stream().sorted(Comparator.comparing(b -> b.getDriverName())).collect(Collectors.toList());

                List<DriverShippingResponse> firstGroup = driverList.stream().filter(d -> !d.getDriverName().toUpperCase().matches(regexCheckSortAZ))
                        .sorted(Comparator.comparing(DriverShippingResponse::getDriverName))
                        .collect(Collectors.toList());
                List<DriverShippingResponse> secondGroup = driverList.stream().filter(d -> d.getDriverName().toUpperCase().matches(regexCheckSortAZ))
                        .sorted(Comparator.comparing(DriverShippingResponse::getDriverName))
                        .collect(Collectors.toList());

                driverList.clear();
                driverList.addAll(firstGroup);
                driverList.addAll(secondGroup);

                das.setDriverList(driverList);

                // count
                List<ManifestoResponse> jobListDate = sortedManifestoList.stream()
                        .filter(b -> b.getDeliveryDate().equals(mapDate.getKey()))
                        .map(shipping -> new ManifestoResponse(
                                        shipping.getDriverId(),
                                        shipping.getId(),
                                        shipping.getManifestoType(),
                                        shipping.getJobId(),
                                        shipping.getCustomerName(),
                                        shipping.getShipToCode(),
                                        shipping.getShipToName(),
                                        shipping.getShipToAddress(),
                                        shipping.getAddressType(),
                                        shipping.getBoxQty(),
                                        shipping.getNewBoxQty(),
                                        shipping.getItemQty(),
                                        shipping.getSKUQty(),
                                        shipping.getRemark(),
                                        shipping.getRouteName(),
                                        shipping.getPodNumber(),
                                        shipping.getLogType(),
                                        shipping.getFulfillId(),
                                        shipping.getDeliveryDate(),
                                        shipping.getContactPerson(),
                                        shipping.getInvoiceNo(),
                                        shipping.getNote(),
                                        null,
                                        shipping.getDelayDate()
                                )
                        ).collect(Collectors.toList());
                List<ShippingResponse> podGroupListPerDay = groupByPODNumber(jobListDate, ManifestolistConstant.CHECKER_APPROVED, currentUser);

                List<Integer> countList = getCountItemByGroup(podGroupListPerDay);
                das.setCountPod(podGroupListPerDay.size());
                das.setCountSo(countList.get(0));
                das.setCountLog(countList.get(1));
                das.setCountInvoice(countList.get(2));

                dList.add(das);
            }

            dList = dList.stream().sorted(Comparator.comparing(DateShippingResponse::getDate).reversed()).collect(Collectors.toList());

            // resposne
            CheckerShippingStatusPageResponse resData = new CheckerShippingStatusPageResponse();
            resData.setDateList(dList);

            resData.setCurrentPage(request.getPage());
            resData.setTotalPage(podPageHolder.getPageCount());
            resData.setTotalItems(sortedPODGroupList.size());
            resData.setPageSize(podPageHolder.getPageSize());

            response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);
            response.setData(resData);

        } catch (Exception ex) {
            response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.getMessage());
        }
        return response;
    }

    private List<ManifestolistWithDriver> getFormattedDelayDateJobList(List<ManifestolistWithDriver> jobList, String jobSearch) {
        List<ManifestolistWithDriver> formattedList = new ArrayList<>();

        // group by pod
        Map<String, List<ManifestolistWithDriver>> mappedPod = jobList.stream()
                .collect(Collectors.groupingBy(ManifestolistWithDriver::getPodNumber));

        jobSearch = jobSearch != null && !jobSearch.isEmpty() ? jobSearch : "";

        // replaced date
        for (Map.Entry<String, List<ManifestolistWithDriver>> mapPod :
                mappedPod.entrySet()) {

            List<ManifestolistWithDriver> jobListByPod = mapPod.getValue();

            boolean include = false;

            // get delay list
            List<String> delayList = new ArrayList<>();
            for (ManifestolistWithDriver mani:
                    mapPod.getValue()) {

                // filter search
                if ((mani.getJobId() != null && mani.getJobId().contains(jobSearch)) ||
                        (mani.getFulfillId() != null && (mani.getFulfillId() + "").contains(jobSearch))
                ) {
                    include = true;
                }

                if (mani.getDelayDate() != null) {
                    delayList.add(mani.getDelayDate());
                }
            }

            // if not found by search criteria
            if (!include) {
                continue;
            }

            // if has delay then replace delivery date by minimum delay date to jobs
            if (!delayList.isEmpty()) {
                String minimumDelayDate = delayList.stream().sorted(Comparator.comparing(String::toString)).collect(Collectors.toList()).get(0);

                jobListByPod = jobListByPod.stream()
                        .map(m -> {
                            m.setDeliveryDate(minimumDelayDate);
                            return m;
                        }).collect(Collectors.toList());
            }

            formattedList.addAll(jobListByPod);
        }

        return formattedList;
    }

    public GZResponse getManifestoListChecker(ManifestolistCheckerRequest request) {
        ManifestolistSearchRequest searchRequest = new ManifestolistSearchRequest();
        BeanUtils.copyProperties(request, searchRequest);
        return getManifestoListChecker(searchRequest);
    }

    public PagedListHolder<ShippingResponse> manifestoGroupPagedList(List<ManifestoResponse> listManisfestoForPage, Integer page, Integer perPage, UserTypeMaster userTypeMaster) {
        List<ShippingResponse> shippingListDelay = groupByPODNumber(listManisfestoForPage, ManifestolistConstant.SHIPPED, userTypeMaster);

        PagedListHolder<ShippingResponse> newDataForPage = new PagedListHolder<>(shippingListDelay);
        newDataForPage.setPage(page - 1);
        newDataForPage.setPageSize(perPage);
        return newDataForPage;
    }

    public Integer checkShowStatusPODNumber(List<ManifestoResponse> manifestoListInApprove, int role, String tab) {
        String deliveryDate;
        if(tab.equalsIgnoreCase(ManifestolistConstant.CHECKER_APPROVED) || tab.equalsIgnoreCase(ManifestolistConstant.CHECKER_WAIT) || tab.equalsIgnoreCase(ManifestolistConstant.FINOPS) || tab.equalsIgnoreCase(ManifestolistConstant.CHECKER)) {
            manifestoListInApprove = manifestoListInApprove.stream().filter(m -> m.getJobStatus() != null && m.getJobStatus().getStatusId() != PODNumberStatusConstant.JOB_DRIVER_SO_WAIT && m.getJobStatus().getStatusId() != PODNumberStatusConstant.JOB_DRIVER_CONFIRM).collect(Collectors.toList());
            if (manifestoListInApprove.isEmpty()) {
                return null;
            }
            deliveryDate = manifestoListInApprove.get(0).getDeliveryDate();
        } else {
            manifestoListInApprove = manifestoListInApprove.stream().filter(m -> m.getJobStatus() != null && m.getJobStatus().getStatusId() != PODNumberStatusConstant.JOB_DRIVER_SO_WAIT).collect(Collectors.toList());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            deliveryDate = formatter.format(new Date());
        }
        if(manifestoListInApprove.isEmpty()) {
            return null;
        }

        if(!manifestoListInApprove.isEmpty()) {
            // status confirm podNumber and reject
            int podNumberDriverConfirm = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_CONFIRM).collect(Collectors.toList()).size();
            int podNumberDriverReject = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM).collect(Collectors.toList()).size();

            if(podNumberDriverConfirm == manifestoListInApprove.size()) {
                return PODNumberStatusConstant.JOB_DRIVER_CONFIRM;
            } else if(podNumberDriverReject == manifestoListInApprove.size()) {
                return PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM;
            } else if(podNumberDriverConfirm + podNumberDriverReject == manifestoListInApprove.size()) {
                return PODNumberStatusConstant.JOB_DRIVER_CONFIRM;
            }

            // status checker approve and not approve
            int podNumberCheckerWaiting = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER).collect(Collectors.toList()).size();
            int podNumberCheckerConfirm = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_APPROVE).collect(Collectors.toList()).size();
            int podNumberCheckerNotConfirm = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE).collect(Collectors.toList()).size();

            if( (podNumberCheckerWaiting > 0 || podNumberCheckerConfirm > 0 || podNumberCheckerNotConfirm > 0 ) && role == PODNumberStatusConstant.DRIVER ) {
                return PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER;
            } else if(podNumberCheckerWaiting > 0) {
                return PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER;
            } else if(podNumberCheckerConfirm > 0) {
                return PODNumberStatusConstant.JOB_CHECKER_APPROVE;
            } else if(podNumberCheckerNotConfirm > 0 && podNumberCheckerNotConfirm == manifestoListInApprove.size()) {
                return PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE;
            }

            // status confirm after approve and start end
            int confirm = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM).collect(Collectors.toList()).size();
            if(confirm > 0)
                return PODNumberStatusConstant.JOB_CHECKER_CONFIRM;
            int driverReject = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_REJECT).collect(Collectors.toList()).size();
            if(driverReject > 0 && driverReject == manifestoListInApprove.size())
                return PODNumberStatusConstant.JOB_DRIVER_REJECT;
            int jobDriverStart = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_START).collect(Collectors.toList()).size();
            if(jobDriverStart > 0)
                return PODNumberStatusConstant.JOB_DRIVER_START;
            int jobDriverEnd = manifestoListInApprove.stream().filter(m -> m.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_DRIVER_END).collect(Collectors.toList()).size();
            if(jobDriverEnd > 0)
                return PODNumberStatusConstant.JOB_DRIVER_END;

            // status delivered
            List<Integer> jobStatusId = new ArrayList<>();
            if(tab.equalsIgnoreCase(ManifestolistConstant.CHECKER_APPROVED) || tab.equalsIgnoreCase(ManifestolistConstant.CHECKER_WAIT) || tab.equalsIgnoreCase(ManifestolistConstant.FINOPS) || tab.equalsIgnoreCase(ManifestolistConstant.CHECKER))
            {
                jobStatusId = manifestoListInApprove.stream().map(m -> m.getJobStatus().getStatusId()).collect(Collectors.toList());
            }
            else
            {
                List<Integer> manifestoLists = manifestolistRepository.findAllBypodNumberAndDeliveryDateOrderByIdAsc(manifestoListInApprove.get(0).getPodNumber(), deliveryDate).stream().map(m -> m.getId()).collect(Collectors.toList());
                if (!manifestoLists.isEmpty())
                {
                    jobStatusId = jobStatusRepository.findLastJobStatusByListStatusIgnoreDelete(manifestoLists, deliveryDate);
                }
            }

            int podNumberEnd = jobStatusId.stream().filter(m -> m == PODNumberStatusConstant.JOB_DRIVER_END ).collect(Collectors.toList()).size();
            int podNumberConfirmAll = jobStatusId.stream().filter(m -> m == PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY || m == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM).collect(Collectors.toList()).size();
            int podNumberRejectAll = jobStatusId.stream().filter(m -> m == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY || m == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT).collect(Collectors.toList()).size();
            int podNumberPartial = jobStatusId.stream().filter(m ->
                 (
                        m == PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY ||
                        m == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY ||
                        m == PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY ||
                        m == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM ||
                        m == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT ||
                        m == PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM ||
                        m == PODNumberStatusConstant.JOB_DRIVER_REJECT ||
                        m == PODNumberStatusConstant.JOB_CHECKER_APPROVE ||
                        m == PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE
                 )

            ).collect(Collectors.toList()).size();

            if(podNumberEnd > 0) {
                return PODNumberStatusConstant.JOB_DRIVER_END;
            } else if(podNumberConfirmAll == manifestoListInApprove.size()) {
                return PODNumberStatusConstant.PODNUMBERSTATUS_DRIVER_CONFIRMALLDELIVERY;
            }
            else if(podNumberRejectAll == manifestoListInApprove.size()) {
                return PODNumberStatusConstant.PODNUMBERSTATUS_DRIVER_REJECTALLDELIVERY;
            }
            else if(podNumberPartial >= manifestoListInApprove.size()) {
                return PODNumberStatusConstant.PODNUMBERSTATUS_DRIVER_PARTIALDELIVERY;
            }
        }

        return null;
    }

    public GZResponse getDriverManifestoListChecker(DriverManifestolistCheckerRequest request) {
        GZResponse response = new GZResponse();
        try {
            if (request.getDate() == null) {
                request.setDate(java.time.LocalDate.now().toString());
            }

            // Checker current user
            UserTypeMaster currentUser = commonService.getUserTypeMasterForReturn(request.getLoginId());
            if (currentUser == null) {
                response.setTitle(ManifestolistConstant.SHIPPING_LIST);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
                return response;
            }

            // Validate user
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(request.getDriverId());
            if (userTypeMaster == null) {
                response.setTitle(ManifestolistConstant.SHIPPING_LIST);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
                return response;
            }

            // Get manifesto list
            List<Manifestolist> manifestolistList = manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(userTypeMaster.getId(), request.getDate());

            // Construct response
            // set Data and get last status
            List<ManifestoResponse> listManisfesto = manifestolistList.stream().map(
                    shipping -> new ManifestoResponse(
                            shipping.getDriverId(),
                            shipping.getId(),
                            shipping.getManifestoType(),
                            shipping.getJobId(),
                            shipping.getCustomerName(),
                            shipping.getShipToCode(),
                            shipping.getShipToName(),
                            shipping.getShipToAddress(),
                            shipping.getAddressType(),
                            shipping.getBoxQty(),
                            shipping.getNewBoxQty(),
                            shipping.getItemQty(),
                            shipping.getSKUQty(),
                            shipping.getRemark(),
                            shipping.getRouteName(),
                            shipping.getPodNumber(),
                            shipping.getLogType(),
                            shipping.getFulfillId(),
                            shipping.getDeliveryDate(),
                            shipping.getContactPerson(),
                            shipping.getInvoiceNo(),
                            shipping.getNote(),
                            getLastJobStatus(shipping.getId(), shipping.getDeliveryDate()),
                            shipping.getDelayDate()
                    )
            ).collect(Collectors.toList());

            // filter manifesto by jobstatus
            Integer[] visibleShippingStatusList = {
                    PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER,
                    PODNumberStatusConstant.JOB_CHECKER_APPROVE,
                    PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE
            };

            Integer[] visibleShippedStatusList = {
                    PODNumberStatusConstant.JOB_CHECKER_CONFIRM,
                    PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM,
                    PODNumberStatusConstant.JOB_DRIVER_REJECT,
                    PODNumberStatusConstant.JOB_DRIVER_START,
                    PODNumberStatusConstant.JOB_DRIVER_END,
                    PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY,
                    PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY,
                    PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY,
                    PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM,
                    PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT
            };

            switch (request.getTab()) {
                case ManifestolistConstant.CHECKER_TAB_WAIT: {
                    listManisfesto = listManisfesto.stream()
                            .filter(b -> b.getJobStatus() != null &&
                                    Arrays.asList(visibleShippingStatusList).indexOf(b.getJobStatus().getStatusId()) != -1)
                            .collect(Collectors.toList());
                    break;
                }

                case ManifestolistConstant.CHECKER_TAB_APPROVE: {
                    listManisfesto = listManisfesto.stream()
                            .filter(b -> b.getJobStatus() != null &&
                                    Arrays.asList(visibleShippedStatusList).indexOf(b.getJobStatus().getStatusId()) != -1)
                            .collect(Collectors.toList());
                    break;
                }
            }

            // Check is empty
            if (listManisfesto.isEmpty()) {
                response.setTitle(ManifestolistConstant.SHIPPING_LIST);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(PODNumberStatusConstant.MANIFESTO_NOT_FOUND);
                return response;
            }

            // filter delay and normarl list
            List<ManifestoResponse> normalList = listManisfesto.stream()
                    .filter(b -> b.getDelayDate() == null || b.getDelayDate().equals(b.getDeliveryDate()))
                    .collect(Collectors.toList());

            List<ManifestoResponse> delayList = listManisfesto.stream()
                    .filter(b -> b.getDelayDate() != null && !b.getDelayDate().equals(b.getDeliveryDate()))
                    .collect(Collectors.toList());

            // group by date
            Map<String, List<ManifestoResponse>> mapDate = delayList.stream()
                    .collect(Collectors.groupingBy(b -> b.getDelayDate()));
            if (!normalList.isEmpty()) { // if normal date is not empty then add to the map
                mapDate.put(request.getDate(), normalList);
            }

            // sort date
            List<String> sortedDateKeys = mapDate.keySet().stream().sorted(Comparator.comparing(s -> s)).collect(Collectors.toList());

            // set tab name
            String tab = ManifestolistConstant.CHECKER_WAIT;
            if (request.getTab().equals(ManifestolistConstant.CHECKER_TAB_APPROVE)) {
                tab = ManifestolistConstant.CHECKER_APPROVED;
            }

            // construct data
            List<DateShippingListResponse> dataList = new ArrayList<>();
            for(String key: sortedDateKeys) {
                // group pod
                List<ShippingResponse> podNumberResponseList = mapDate.get(key).isEmpty() ?
                        new ArrayList<>()
                        :
                        groupByPODNumber(mapDate.get(key), tab, currentUser);

                DateShippingListResponse dateShippingListResponse = new DateShippingListResponse();
                dateShippingListResponse.setCountPodNumber(podNumberResponseList.size());
                dateShippingListResponse.setShippingList(podNumberResponseList);

                // count so invoice log
                List<Integer> countItemByGroup = getCountItemByGroup(podNumberResponseList);
                dateShippingListResponse.setCountSo(countItemByGroup.get(0));
                dateShippingListResponse.setCountLog(countItemByGroup.get(1));
                dateShippingListResponse.setCountInvoice(countItemByGroup.get(2));

                // driver information
                dateShippingListResponse.setDate(key);

                dataList.add(dateShippingListResponse);
            }

            DriverListResponse driverListResponse = driverService.getDriverDetail(request.getDate(), userTypeMaster.getId());

            DriverManifestoCheckerResponse resData = new DriverManifestoCheckerResponse();
            resData.setDate(request.getDate());
            resData.setDriverName(userTypeMaster.getName());
            resData.setDriverStatus(driverListResponse.getStatus());
            resData.setDateShippingList(dataList);

            // set message
            response.setTitle(ManifestolistConstant.SHIPPING_LIST);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

            response.setData(resData);
        } catch (Exception ex) {
            response.setTitle(ManifestolistConstant.SHIPPING_LIST);
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }
        return response;
    }

    public List<ShippingResponse> groupByPODNumber(List<ManifestoResponse> listManisfesto, String tab, UserTypeMaster userTypeMaster) {
        // group jobs by pod number
        Map<String, List<ManifestoResponse>> mappedJobsByPODNumber = listManisfesto.stream().collect(Collectors.groupingBy(b -> b.getPodNumber()));

        List<ShippingResponse> podList = new ArrayList<>();
        for (Map.Entry<String, List<ManifestoResponse>> jobsByPODNumber:
            mappedJobsByPODNumber.entrySet()) {
            ShippingResponse pod = new ShippingResponse();

            // set pod number
            pod.setPodNumber(jobsByPODNumber.getKey());
            // set pod fields by first job of pod
            pod.setCustomerName(jobsByPODNumber.getValue().get(0).getCustomerName());
            pod.setShipToAddress(jobsByPODNumber.getValue().get(0).getShipToAddress());

            // sort jobs so, log
            List<ManifestoResponse> logList = jobsByPODNumber.getValue().stream()
                    .filter(b -> b.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
                    .sorted(Comparator.comparing(b -> b.getFulfillId()))
                    .collect(Collectors.toList());
            List<ManifestoResponse> soList = jobsByPODNumber.getValue().stream()
                    .filter(b -> !b.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
//                    .sorted(Comparator.comparing(b -> Long.parseLong(b.getJobId())))
                    .sorted(Comparator.comparing(b -> b.getJobId()))
                    .collect(Collectors.toList());
            List<ManifestoResponse> sortedJobList = new ArrayList<>();
            sortedJobList.addAll(soList);
            sortedJobList.addAll(logList);

            // set pod status
            List<ManifestoResponse> manifestoListByPodNumber = jobsByPODNumber.getValue();
            Integer podNumberStatusResponse = checkShowStatusPODNumber(manifestoListByPodNumber, userTypeMaster.getUserTypeId(), tab);
            if(podNumberStatusResponse != null ) {
                MasterStatus masterStatus = masterStatusRepository.findOne(podNumberStatusResponse);
                if(masterStatus != null )
                    pod.setStatus(masterStatus.getName());
            } else {
                if (jobsByPODNumber.getValue().get(0).getJobStatus() != null && jobsByPODNumber.getValue().get(0).getJobStatus().getMasterStatus() != null) {
                    pod.setStatus(jobsByPODNumber.getValue().get(0).getJobStatus().getMasterStatus().getName());
                }
            }
            // set constant flag delay
            pod.setFlgDelay(findStatusDelayDate(jobsByPODNumber.getValue(),tab));

            // set pod remarks
            if(podNumberStatusResponse != null &&
                    (podNumberStatusResponse == PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM ||
                            podNumberStatusResponse == PODNumberStatusConstant.JOB_DRIVER_REJECT ||
                            podNumberStatusResponse == PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE
                    )
            )  {
                pod.setRemark(jobsByPODNumber.getValue().get(0).getJobStatus().getRemark());
                if (jobsByPODNumber.getValue().get(0).getJobStatus().getUserTypeMaster() != null) {
                    pod.setUserRemark(jobsByPODNumber.getValue().get(0).getJobStatus().getUserTypeMaster().getName());
                }
            }

            // set job list
            List<ManifestoResponse> jobList = new ArrayList<>();
            for (ManifestoResponse job:
                    sortedJobList){

                String rejectRemark = "";
                GenericInformation genericInformation = null;
                if (job.getJobStatus() != null) {
                    genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN, job.getJobStatus().getId());
                }

                if(genericInformation != null) {
                    MasterReasonRejectManifesto masterReasonRejectManifesto = masterReasonRejectManifestoRepository.findOne(Integer.parseInt(genericInformation.getValue()));
                    if (masterReasonRejectManifesto != null)
                        rejectRemark = masterReasonRejectManifesto.getDescription();
                    else
                        rejectRemark = PODNumberStatusConstant.REMARK_REASON_ID_NOT_FOUND;
                }

                // set flag attachFile
                ManifestoResponse jobResponse;
                if (job.getManifestoType() != null && job.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG)) {
                    // construct job response
                    ManifestoResponse logResponse = new ManifestoResponse(
                            job.getManifestoId(),
                            job.getManifestoType(),
                            job.getRemark(),
                            job.getFulfillId(),
                            job.getJobId(),
                            job.getInvoiceNo(),
                            job.getIssueType()
                    );
                    logResponse.setRejectRemark(rejectRemark);
                    jobResponse = logResponse;
                } else {
                    ManifestoResponse manifestoDetailResponse = new ManifestoResponse(
                            job.getManifestoId(),
                            job.getManifestoType(),
                            job.getNewBoxQty(),
                            job.getItemQty(),
                            job.getSkuQty(),
                            job.getRemark(),
                            job.getJobId(),
                            job.getInvoiceNo(),
                            job.getIssueType()
                    );
                    manifestoDetailResponse.setRejectRemark(rejectRemark);
                    jobResponse = manifestoDetailResponse;
                }

                // set more info
                // set job status
                if (job.getJobStatus() != null && job.getJobStatus().getMasterStatus() != null) {
                    jobResponse.setStatus(job.getJobStatus().getMasterStatus().getName());
                }

                // set flg attachFile
                jobResponse.setFlgAttachFile(checkAttachFile(job));

                // set job remarks
                if (tab.equalsIgnoreCase(ManifestolistConstant.DELIVERED) || tab.equals(ManifestolistConstant.CHECKER_WAIT) || tab.equals(ManifestolistConstant.CHECKER_APPROVED)) {
                    if (job.getJobStatus() != null) {
                        jobResponse.setRemarkStatus(job.getJobStatus().getRemark());
                        jobResponse.setUserRemarkStatus(job.getJobStatus().getUserTypeMaster() != null ? job.getJobStatus().getUserTypeMaster().getName(): null);
                    }

                    ManifestoCheckerResponse tmpMani = getJobRemarkDetailForChecker(job, userTypeMaster);
                    job.setRejectRemark(tmpMani.getRejectRemark());
                    if (tab.equals(ManifestolistConstant.CHECKER_APPROVED) || tab.equalsIgnoreCase(ManifestolistConstant.DELIVERED)) {
                        jobResponse.setReceived(tmpMani.isReceived());
                        jobResponse.setReceiveRemark(tmpMani.getReceiveRemark());
                        jobResponse.setUserReceive(tmpMani.getUserReceive());
                    }
                }
                jobList.add(jobResponse);
            }
            pod.setData(jobList);

            // add pod to list
            podList.add(pod);
        }
        return podList;
    }

    private Boolean checkAttachFile(ManifestoResponse job) {
        int status = job.getJobStatus() != null ? job.getJobStatus().getStatusId() : 0;
        if(status == PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY ||
                status == PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY ||
                status == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM
        ) {
            List<ManifestoFile> manifestoFile = manifestoFileRepository.findBymanifestoIdOrderByIdDesc(job.getManifestoId());
            if(manifestoFile.isEmpty())
                return true;
        }
        return false;
    }

    public JobStatus getLastJobStatus(int manifestoId, String deliveryDate) {
        return jobStatusRepository.findLastByjobIdAndDelivery(manifestoId, deliveryDate);
    }

    public String PrepareUserName(int id) {
        String result = "";
        try {
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterById(ManifestolistConstant.SHIPPING_LIST, id);
            if (userTypeMaster != null) {
                result = userTypeMaster.getName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public GZResponse validateUpdateBoxManifestoByJob(ManifestoPutBoxQtyRequest request) {
        GZResponse response = new GZResponse();
        try {
            response.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_BYJOBID);

            Manifestolist manifesto = manifestolistRepository.findOne(request.getJobId());
            if (manifesto != null) {
                UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(request.getUpdateBy());
                if (userTypeMaster != null) {
                    if (userTypeMaster.getId() == manifesto.getDriverId()) {
                        response.setDeveloperMessage(ManifestolistConstant.VALIDATE_UPDATE_MANIFESTO_BOXQTY_SUCCESS);
                    } else {
                        response.setCode(HttpStatus.BAD_REQUEST);
                        response.setMessage(HttpStatus.BAD_REQUEST.toString());
                        response.setDeveloperMessage(ManifestolistConstant.VALIDATE_UPDATE_MANIFESTO_BOXQTY_JOBCHANGED);
                    }
                } else {
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
                }
            } else {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_NOT_FOUND);
            }
        } catch (Exception ex) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }

        return response;
    }

    public GZResponse updateBoxManifestoByJob(ManifestoPutBoxQtyRequest request) {
        GZResponse response = new GZResponse();
        try {
            response.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_BYJOBID);
            response = validateUpdateBoxManifestoByJob(request);

            if (response.getDeveloperMessage().equals(ManifestolistConstant.VALIDATE_UPDATE_MANIFESTO_BOXQTY_SUCCESS)) {
                java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

                // Check manifesto
                Manifestolist manifesto = manifestolistRepository.findOne(request.getJobId());

                manifesto.setNewBoxQty(request.getBoxQty());
                manifesto.setUpdatedBy(manifesto.getDriverId());
                manifesto.setUpdatedDate(date);
                manifestolistRepository.save(manifesto);

                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_SUCCESS);
            }

            return response;
        } catch (Exception ex) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
        }
        return response;
    }

    public GZResponse validateAddJob(ValidateAddJobRequest request) throws GZException {
        GZResponse response;
        try {
            response = fncValidateAddJob(request);
        }
        catch (Exception ex) {
            throw new GZException(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB
                    , HttpStatus.BAD_REQUEST
                    , HttpStatus.BAD_REQUEST.toString()
                    , ex.getMessage()
            );
        }

        response.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        return response;
    }

    public GZResponse updateAddJob(UpdateAddJobRequest request) throws GZException {
        GZResponse response;
        try {
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(request.getDriverId());
            String nowDate = java.time.LocalDate.now().toString();

            ValidateAddJobRequest validateAddJobRequest = new ValidateAddJobRequest();
            validateAddJobRequest.setRefModuleType(request.getRefModuleType());
            validateAddJobRequest.setRefId(request.getRefId());
            validateAddJobRequest.setDriverId(request.getDriverId());

            response = fncValidateAddJob(validateAddJobRequest);

            if (response.getDeveloperMessage().equals(ManifestolistConstant.VALIDATE_ADD_JOB_SUCCESS))
            {
                GZResponse<Manifestolist> manifestoResponse = getManifestoByRefType(request.getRefId(), request.getRefModuleType());

                Manifestolist manifestolist = manifestoResponse.getData();
                manifestolist.setDriverId(userTypeMaster.getId());
                manifestolist.setDeliveryDate(nowDate);
                manifestolist.setPodNumber(null);
                manifestolist.setDelayDate(null);
                manifestolist.setNewBoxQty(manifestolist.getBoxQty());
                manifestolistRepository.save(manifestolist);

                ///Re generate PODNumber
                dataFromSOMSService.UpdatePODNumber(nowDate, PODNumberStatusConstant.JOB_DRIVER_CONFIRM);

                //Gen response
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ManifestolistConstant.UPDATE_ADD_JOB_SUCCESS);
            }
        } catch (Exception ex) {
            throw new GZException(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB
                    , HttpStatus.BAD_REQUEST
                    , HttpStatus.BAD_REQUEST.toString()
                    , ex.getMessage()
            );
        }
        response.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        return response;
    }

    public GZResponse<Manifestolist> getManifestoByRefType(String refId, String refType) {
        GZResponse<Manifestolist> response = new GZResponse();
        Manifestolist manifesto = null;
        try {
            // In case of QR code
            if (refType.equals(ManifestolistConstant.REF_MODULE_TYPE_QRCODE)) {

                // Get data from SOMS
                GZResponse somsResponse = commonService.getRefDocByQRCodeFromSOMS(refId);

                // Check type of data
                if(somsResponse.getDeveloperMessage().equals(ReceiveDocumentConstant.QRNUMBER_FOUND)) {
                    HashMap dataMap = (HashMap) somsResponse.getData();
                    String docName = (String)dataMap.get("docName");
                    String docRef = (String)dataMap.get("docRef");

                    refId = docRef;
                    if (docName.equalsIgnoreCase(ManifestolistConstant.REF_MODULE_TYPE_SO)) {
                        refType = ManifestolistConstant.REF_MODULE_TYPE_SO;
                    } else if(docName.equalsIgnoreCase(ManifestolistConstant.REF_MODULE_TYPE_LOG)) {
                        refType = ManifestolistConstant.REF_MODULE_TYPE_LOG;
                    } else {
                        response.setCode(HttpStatus.BAD_REQUEST);
                        response.setMessage(HttpStatus.BAD_REQUEST.toString());
                        response.setDeveloperMessage(ManifestolistConstant.UNKNOWN_REF_MODULE_TYPE);
                        return response;
                    }
                } else {
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);
                    return response;
                }
            }

            // Get manifesto by ref type
            if (refType.equals(ManifestolistConstant.REF_MODULE_TYPE_SO)) {
                // find manifesto by so number
                manifesto = manifestolistRepository.findManifestoBySoNumberNotLog(refId);
            } else if(refType.equals(ManifestolistConstant.REF_MODULE_TYPE_LOG)) {
                // find manifesto by log number
                manifesto = manifestolistRepository.findByFulfillId(Integer.parseInt(refId));
            } else {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ManifestolistConstant.UNKNOWN_REF_MODULE_TYPE);
                return response;
            }

            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_SEARCH_SUCCESS);
            response.setData(manifesto);
        } catch (NumberFormatException numEx) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);
            response.setData(manifesto);
        } catch (Exception ex) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.getMessage());
            response.setData(manifesto);
        }

        return response;
    }

    public GZResponse fncValidateAddJob(ValidateAddJobRequest request) throws Exception {
        GZResponse response = new GZResponse();
        UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(request.getDriverId());
        String nowDate = java.time.LocalDate.now().toString();
        AddJobResponse addJobResponse = new AddJobResponse(request.getRefModuleType(), request.getRefId());
        response.setData(addJobResponse);

        if (userTypeMaster == null) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
        } else {

            // Get manifesto
            GZResponse<Manifestolist> manifestoResponse = getManifestoByRefType(request.getRefId(), request.getRefModuleType());
            if (!manifestoResponse.getDeveloperMessage().equals(ManifestolistConstant.MANIFESTO_SEARCH_SUCCESS)) {
                return manifestoResponse;
            }
            Manifestolist manifestolist = manifestoResponse.getData();
            if (manifestolist == null) {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);
            } else {
                // Validate add job
                response = checkAddJob(manifestolist, userTypeMaster.getId(), nowDate);

                // Add ref type and number
                if (manifestolist.getManifestoType().equalsIgnoreCase(ManifestolistConstant.REF_MODULE_TYPE_LOG)) {
                    addJobResponse.setManifestoType(ManifestolistConstant.REF_MODULE_TYPE_LOG);
                    addJobResponse.setJobId(manifestolist.getFulfillId() + "");
                } else {
                    addJobResponse.setManifestoType(ManifestolistConstant.REF_MODULE_TYPE_SO);
                    addJobResponse.setJobId(manifestolist.getJobId());
                }
                response.setData(addJobResponse);
            }
        }
        return response;
    }

    public GZResponse checkAddJob(Manifestolist manifestolist, int driverId, String deliveryDate) {
        GZResponse response = new GZResponse();

        // Check job status
        JobStatus checkJobStatus = jobStatusRepository.findLastByjobId(manifestolist.getId());
        List<Integer> addableJobStatusList = Arrays.asList(
                PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM,
                PODNumberStatusConstant.JOB_DRIVER_CONFIRM,
                PODNumberStatusConstant.JOB_DRIVER_SO_WAIT,
                PODNumberStatusConstant.JOB_DRIVER_REJECT
        );

        List<Integer> completedJobStatusList = Arrays.asList(
                PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY,
                PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY,
                PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY,
                PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM,
                PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT
        );

        String createdDateStr = "";
        if (checkJobStatus != null && checkJobStatus.getCreatedDateUTC() != null) {
            Date createdDate = new Date();
            createdDate.setTime(checkJobStatus.getCreatedDateUTC().getTime());
            createdDateStr = (new SimpleDateFormat("yyyy-MM-dd")).format(createdDate);
        }

        // Check can change job
        boolean canChange = false;
        if (checkJobStatus != null) {
            // current date
            if (createdDateStr.startsWith(deliveryDate)) {
                // status must be state before confirm job
                canChange = addableJobStatusList.contains(checkJobStatus.getStatusId());
            } else{
                // status must not be completed
                canChange = !completedJobStatusList.contains(checkJobStatus.getStatusId());
            }

        } else {
            canChange = true;
        }

        if (canChange) {
            Integer[] duplicateCheckStatusList = {
                    PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM,
                    PODNumberStatusConstant.JOB_DRIVER_CONFIRM,
                    PODNumberStatusConstant.JOB_DRIVER_REJECT
            };

            Integer[] checkerCannotAddJobStatusList = {
                    PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER,
                    PODNumberStatusConstant.JOB_CHECKER_APPROVE,
                    PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE
            };

            // Check driver jobs
            List<Manifestolist> driverManifestoList = manifestolistRepository.findAllBydriverIdAndDeliveryDate(driverId, deliveryDate);
            for (Manifestolist mani: driverManifestoList)
            {
                // Check duplicate job
                if (mani.getDriverId() == manifestolist.getDriverId()
                        && checkJobStatus != null
                        && checkJobStatus.getCreatedBy() != null
                        && checkJobStatus.getCreatedBy() == manifestolist.getDriverId()
                        && Arrays.asList(duplicateCheckStatusList).indexOf(checkJobStatus.getStatusId()) != -1
                        && createdDateStr.startsWith(deliveryDate)) // must be added in current date
                {
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_DUPLICATE);
                    return response;
                }

                JobStatus jobStatus = jobStatusRepository.findLastByjobIdAndDelivery(mani.getId(), deliveryDate);

                // Check each job status
                if (jobStatus != null
                        && checkJobStatus != null
                        && Arrays.asList(checkerCannotAddJobStatusList).indexOf(jobStatus.getStatusId()) != -1)
                {
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_UNABLE_TO_ADD);
                    return response;
                }
            }

            // Pass validation
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_SUCCESS);
        }
        else{
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_UNABLE_TO_ADD);
        }
        return response;
    }

    public GZResponse getManifestoDetailByManifestoId(ManifestoDetailRequest request) throws GZException {

        GZResponse response = new GZResponse();
        try {
            Manifestolist manifesto = manifestolistRepository.findAllManifestolistById(request.getManifestoId());
            if (manifesto == null) {
                return new GZResponse(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                        ManifestolistConstant.GET_DETAIL_ERROR, ManifestolistConstant.GET_DETAIL_ERROR
                );
            }
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(request.getLoginId());
            if (userTypeMaster == null) {
                return new GZResponse(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
                );
            }
            if(userTypeMaster.getUserTypeId() == ManifestolistConstant.DRIVER_ID && manifesto.getDriverId() != userTypeMaster.getId() ) {
                return new GZResponse(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
                );
            }

            String receiveRemark = "";
            String nameUserReceive = "";
            boolean isReceive = false;
            if(userTypeMaster.getUserTypeId() != PODNumberStatusConstant.DRIVER) {
                int statusReceiveId = userTypeMaster.getUserTypeId() == PODNumberStatusConstant.CHECKER ? PODNumberStatusConstant.RECEIVE_CHECKER_CONFIRM : PODNumberStatusConstant.RECEIVE_FINOP_CONFIRM;
                ReceiveDocument receiveDocument = receiveDocumentRepository.findByManifestoIdAndStatusId(manifesto.getId(), statusReceiveId);
                if (receiveDocument != null) {
                    isReceive = true;
                    receiveRemark = PrepareGeneric(receiveDocument.getId(), GenericInformationConstant.RECEIVEDOCUMENT_DB, GenericInformationConstant.RECEIVEDOCUMENT_REFACTION);
                    if(receiveDocument.getUserTypeMaster() != null)
                        nameUserReceive = receiveDocument.getUserTypeMaster().getName();
                }
            }

            List<FileResponse> fileResponse = manifestoFileRepository.findBymanifestoIdOrderByIdDesc(manifesto.getId()).stream().map(m -> new FileResponse(m.getId())).collect(Collectors.toList());


            if (!manifesto.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG)) {
                ManifestoDetailResponse detail = new ManifestoDetailResponse();
                detail.setJobId(manifesto.getJobId());
                detail.setManifestoId(manifesto.getId());
                detail.setManifestoType(manifesto.getManifestoType());
                detail.setNewBoxQty(manifesto.getNewBoxQty());
                detail.setItemQty(manifesto.getItemQty());
                detail.setSkuQty(manifesto.getSKUQty());
                detail.setRouteName(manifesto.getRouteName());
                detail.setCustomerName(manifesto.getCustomerName());
                detail.setShipToAddress(manifesto.getShipToAddress());
                detail.setRemark(manifesto.getRemark());
                detail.setOrderTotalInVat(manifesto.getOrderTotalInVat());

                detail.setNameUserUpdateRemark(PrepareNameUserUpdateByJob(manifesto.getId(), manifesto.getDeliveryDate()));
                detail.setContactPerson(manifesto.getContactPerson());
                detail.setFiles(fileResponse);
                detail.setReceive(isReceive);
                detail.setReceiveRemark(receiveRemark);
                detail.setNameUserReceive(nameUserReceive);

                if (manifesto.getOrderTotalInVat() != null) {
                    BigDecimal vat = new BigDecimal(calVat(manifesto.getOrderTotalInVat().doubleValue()));
                    vat = vat.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                    detail.setVat(vat);
                    BigDecimal TotalExVat = new BigDecimal(calTotalExVat(manifesto.getOrderTotalInVat().doubleValue()));
                    TotalExVat = TotalExVat.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                    detail.setTotalExVatTH(TotalExVat);
                }
                detail.setDriverName(PrepareUserName(manifesto.getDriverId()));
                detail.setBoxRemark(PrepareGeneric(manifesto.getId(), ManifestolistConstant.MANIFESTO_DB, ManifestolistConstant.ACTION_UPDATEBOXQTY));
                JobStatus jobStatusList = jobStatusRepository.findLastByjobIdAndDelivery(manifesto.getId(), manifesto.getDeliveryDate());
                if (jobStatusList != null) {
                    detail.setStatus(PrepareJobStatusDesc(manifesto.getId(), manifesto.getDeliveryDate()));
                    detail.setRemarkStatus(PrepareRemarkJobStatus(manifesto.getId(), manifesto.getDeliveryDate()));
                    detail.setRejectRemark(PrepareGeneric(jobStatusList.getId(), ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN));
                }
                else if (jobStatusList == null){
                    detail.setStatus("");
                    detail.setRemarkStatus("");
                    detail.setRejectRemark("");
                }
                List<ManifestoItem> manifestoItem = manifestoItemRepository.findBymanifestoId(manifesto.getId());
                if (!manifestoItem.isEmpty()) {
                    List<ManifestoItemlResponse> manifestoItemList = new ArrayList<>();
                    for (ManifestoItem item : manifestoItem) {
                        ManifestoItemlResponse maniItem = new ManifestoItemlResponse();
                        maniItem.setId(item.getId());
                        maniItem.setProductCode(item.getProductCode());
                        maniItem.setProductName(item.getProductName());
                        maniItem.setUom(item.getUom());
                        maniItem.setQuantity(item.getQuantity());
                        maniItem.setQuantitySend(item.getNewQuantity());
                        maniItem.setTotalExVat(item.getAmount());
                        maniItem.setRemark(PrepareGeneric(item.getId(), ManifestoItemConstant.MANIFESTOITEM_DB, ManifestoItemConstant.ACTION_UPDATEQTY));
                        manifestoItemList.add(maniItem);
                    }
                    detail.setItems(manifestoItemList);
                }
                response.setData(detail);

            } else {
                ManifestoLogDetailResponse logDetail = new ManifestoLogDetailResponse();
                JobStatus jobStatusList = jobStatusRepository.findLastByjobIdAndDelivery(manifesto.getId(), manifesto.getDeliveryDate());
                logDetail.setJobId(manifesto.getJobId());
                logDetail.setManifestoId(manifesto.getId());
                logDetail.setManifestoType(manifesto.getManifestoType());
                logDetail.setShipToAddress(manifesto.getShipToAddress());
                logDetail.setRemark(manifesto.getRemark());
                logDetail.setSapCustCode(manifesto.getSapCustCode());
                logDetail.setInvoiceNo(manifesto.getInvoiceNo());
                logDetail.setIssueType(manifesto.getLogType());
                logDetail.setDriverName(PrepareUserName(manifesto.getDriverId()));
                logDetail.setFulfillId(manifesto.getFulfillId());
                logDetail.setNameUserUpdateRemark(PrepareNameUserUpdateByJob(manifesto.getId(), manifesto.getDeliveryDate()));
                logDetail.setContactPerson(manifesto.getContactPerson());
                logDetail.setFiles(fileResponse);
                logDetail.setReceive(isReceive);
                logDetail.setReceiveRemark(receiveRemark);
                logDetail.setNameUserReceive(nameUserReceive);
                logDetail.setCustomerName(manifesto.getCustomerName());
                if (jobStatusList != null) {
                    logDetail.setLogRemark(PrepareGeneric(jobStatusList.getId(), ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS));
                    logDetail.setRejectRemark(PrepareGeneric(jobStatusList.getId(), ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN));
                    logDetail.setRemarkStatus(PrepareRemarkJobStatus(manifesto.getId(), manifesto.getDeliveryDate()));
                    logDetail.setStatus(PrepareJobStatusDesc(manifesto.getId(), manifesto.getDeliveryDate()));
                    logDetail.setStatus(PrepareJobStatusDesc(manifesto.getId(), manifesto.getDeliveryDate()));

                }
                else if (jobStatusList == null) {
                    logDetail.setLogRemark("");
                    logDetail.setRejectRemark("");
                    logDetail.setRemarkStatus("");
                    logDetail.setStatus("");

                }

                response.setData(logDetail);
            }

            response.setTitle(ManifestolistConstant.GET_DETAIL);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(ManifestolistConstant.GET_DETAIL_SUCCESS);

        } catch (Exception ex) {
            throw new GZException(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), ex.toString()
            );
        }
        return response;
    }

    public String PrepareNameUserUpdateByJob(int id, String deliveryDate) throws Exception {
        String name = null;
        JobStatus jobStatus = jobStatusRepository.findLastByjobIdAndDelivery(id, deliveryDate);
        if (jobStatus == null)
            name = "";
        else {
            if (jobStatus.getUserTypeMaster() != null)
                name = jobStatus.getUserTypeMaster().getName();
        }
        return name;
    }

    private Double calVat(Double getOrderTotalInVat) {
        Double result = 0.0;
        if (getOrderTotalInVat > 0) {
            Double exVat = getOrderTotalInVat / 1.07;
            result = getOrderTotalInVat - exVat;

        }
        return result;
    }

    private Double calTotalExVat(Double getOrderTotalInVat) {
        Double result = 0.0;
        if (getOrderTotalInVat > 0) {
            Double exVat = getOrderTotalInVat / 1.07;
            result = exVat;
        }
        return result;
    }

    public String PrepareJobStatusDesc(int manifestoId, String deliveryDate) {
        String status;
        JobStatus jobStatusList = jobStatusRepository.findLastByjobIdAndDelivery(manifestoId, deliveryDate);
        if (jobStatusList == null)
            status = null;
        else
            status = jobStatusList.getMasterStatus().getName();
        return status;
    }

    public String PrepareGeneric(int refId, String refModule, String refAction) {
        GenericInformation genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(refModule, refAction, refId);
        String jobRemarkReject = "";
        if (genericInformation != null) {
            jobRemarkReject = genericInformation.getValue();
            if (refAction.equals(ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN)) {
                if (!CommonService.isInteger(jobRemarkReject))
                    jobRemarkReject = PODNumberStatusConstant.REMARK_REASON_ID_NOT_FOUND;
                else {
                    MasterReasonRejectManifesto masterReasonRejectManifesto = masterReasonRejectManifestoRepository.findOne(Integer.parseInt(jobRemarkReject));
                    if (masterReasonRejectManifesto != null)
                        jobRemarkReject = masterReasonRejectManifesto.getDescription();
                    else
                        jobRemarkReject = PODNumberStatusConstant.REMARK_REASON_ID_NOT_FOUND;
                }
            }
        }
        return jobRemarkReject;
    }

    public String PrepareRemarkJobStatus(int manifestoId, String deliveryDate) {
        String remark;
        JobStatus jobStatusList = jobStatusRepository.findLastByjobIdAndDelivery(manifestoId, deliveryDate);
        if (jobStatusList == null)
            remark = null;
        else
            remark = jobStatusList.getRemark();
        return remark;
    }

    public GZResponse validateDeleteJob(ValidateDeleteJobRequest request)
    {
        GZResponse response = new GZResponse();
        try
        {
            response = fncValidateCanDelete(request, "Validate");
        }
        catch (Exception ex) {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.getMessage());
        }

        return response;
    }

    public GZResponse updateDeleteJob(UpdateDeleteJobRequest request)
    {
        GZResponse response = new GZResponse();
        try
        {
            ValidateDeleteJobRequest requestValid = new ValidateDeleteJobRequest();
            requestValid.setLoginId(request.getLoginId());
            requestValid.setManifestoId(request.getManifestoId());

            response = fncValidateCanDelete(requestValid, "Update");

            String deliveryDate = java.time.LocalDate.now().toString();

            if(response.getDeveloperMessage().equals((ManifestolistConstant.VALIDATE_AND_DELETE_JOB_CAN_DELETE)))
            {
                java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

                for (Integer manifestoId : request.getManifestoId())
                {
                    JobStatus jobStatus = jobStatusRepository.findFirstByjobIdAndStatusIdAndDelivery(manifestoId, PODNumberStatusConstant.JOB_DRIVER_SO_WAIT, deliveryDate);

                    Manifestolist manifesto = manifestolistRepository.findOne(manifestoId);
                    manifesto.setNewBoxQty(manifesto.getBoxQty());
                    manifesto.setDriverId(jobStatus==null ? 0 : jobStatus.getCreatedBy());
                    manifesto.setPodNumber(null);
                    manifestolistRepository.save(manifesto);

                    JobStatus jobStatusForSave = new JobStatus();
                    jobStatusForSave.setJobId(manifestoId);
                    jobStatusForSave.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_WAIT);
                    jobStatusForSave.setCreatedBy((int)response.getData());
                    jobStatusForSave.setCreatedDateUTC(date);
                    jobStatusRepository.save(jobStatusForSave);
                }

                dataFromSOMSService.UpdatePODNumber(deliveryDate);

                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(ManifestolistConstant.UPDATE_DELETE_JOB_SUCCESS);
                response.setData(null);
            }
        }
        catch (Exception ex)
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(ex.toString());
            response.setData(null);
        }

        return response;
    }

    private GZResponse fncValidateCanDelete(ValidateDeleteJobRequest request, String type)
    {
        GZResponse response = new GZResponse();
        response.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);

        ///Fine driverId from loginId
        UserTypeMaster userTypeMaster = userTypeMasterRepository.findByloginId(request.getLoginId());
        if(userTypeMaster != null)
        {
            ///Validate Owner manifesto
            List<ManifestoOwnerResponse> manifestoOwnerList = commonService.validateOwnerByManifestoId(request.getManifestoId(), userTypeMaster.getId());

            if(manifestoOwnerList.size() == request.getManifestoId().size())
            {
                Integer cntFalse = manifestoOwnerList.stream().filter(m -> !m.isOwner()).collect(Collectors.toList()).size();
                if(cntFalse == 0)
                {
                    String deliveryDate = java.time.LocalDate.now().toString();
                    for (Integer manifestoId : request.getManifestoId())
                    {
                        JobStatus jobStatus = jobStatusRepository.findLastByjobIdAndDelivery(manifestoId, deliveryDate);

                        if(jobStatus == null ||
                                (jobStatus.getStatusId() != PODNumberStatusConstant.JOB_DRIVER_CONFIRM
                                && jobStatus.getStatusId() != PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM)
                        )
                        {
                            response.setCode(HttpStatus.BAD_REQUEST);
                            response.setMessage(HttpStatus.BAD_REQUEST.toString());
                            response.setDeveloperMessage(ManifestolistConstant.VALIDATE_AND_DELETE_JOB_CANNOTDELETE);

                            return response;
                        }
                    }

                    response.setCode(HttpStatus.OK);
                    response.setMessage(HttpStatus.OK.toString());
                    response.setDeveloperMessage(ManifestolistConstant.VALIDATE_AND_DELETE_JOB_CAN_DELETE);
                    if(type.equals("Update"))
                    {
                        response.setData(userTypeMaster.getId());
                    }
                }
                else
                {
                    response.setCode(HttpStatus.BAD_REQUEST);
                    response.setMessage(HttpStatus.BAD_REQUEST.toString());
                    response.setDeveloperMessage(CommonConstant.THERE_ARE_SOME_MANIFESTO_NOT_YOURS);
                    response.setData(manifestoOwnerList.stream().filter(m -> !m.isOwner()).collect(Collectors.toList()));
                }
            }
            else
            {
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(ManifestolistConstant.VALIDATE_AND_DELETE_JOB_NOTFOUND);
            }
        }
        else
        {
            response.setCode(HttpStatus.BAD_REQUEST);
            response.setMessage(HttpStatus.BAD_REQUEST.toString());
            response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);
        }

        return response;
    }

    public GZResponse rollbackManifesto(ManifestoRollbackRequest manifestoRollbackRequest) {
        GZResponse response = new GZResponse();

        // rollback quantity item
        List<ManifestoItem> manifestoItemList = manifestoItemRepository.findBymanifestoId(manifestoRollbackRequest.getManifestoId());
        for (ManifestoItem manifestoItem : manifestoItemList) {
            manifestoItem.setNewQuantity(manifestoItem.getQuantity());

            GenericInformation genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestoItemConstant.MANIFESTOITEM_DB, ManifestoItemConstant.ACTION_UPDATEQTY, manifestoItem.getId());
            if (genericInformation != null) {
                genericInformationRepository.delete(genericInformation);
            }
            manifestoItemRepository.save(manifestoItem);
        }

        // rollback file
        List<ManifestoFile> manifestoFileList = manifestoFileRepository.findBymanifestoIdOrderByIdDesc(manifestoRollbackRequest.getManifestoId());
        for (ManifestoFile manifestoFile : manifestoFileList) {
            manifestoFileRepository.delete(manifestoFile);
        }

        // rollback jobStatus
        final List<Integer> contentTypes = Arrays.asList(
                ManifestolistConstant.JOB_DRIVER_CONFIRMALLDELIVERY,
                ManifestolistConstant.JOB_DRIVER_REJECTALLDELIVERY,
                ManifestolistConstant.JOB_DRIVER_PARTIALDELIVERY,
                PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT,
                PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM
        );

        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());
        JobStatus jobStatus = jobStatusRepository.findLastByjobIdAndDelivery(manifestoRollbackRequest.getManifestoId(), currentDate.toString());
        if (contentTypes.contains(jobStatus.getStatusId())) {

            GenericInformation genericInformation = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestolistConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS, jobStatus.getId());
            if (genericInformation != null) {
                genericInformationRepository.delete(genericInformation);
            }

            jobStatusRepository.delete(jobStatus);
        }

        response.setTitle(ManifestolistConstant.ROLLBACK);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.ROLLBACK_SUCCESS);

        return response;
    }

    public List<Integer> getCountItemByGroup(List<ShippingResponse> podNumberResponseList) {
        int countSo = 0;
        int countLog = 0;
        int countInvoice = 0;
        List<Integer> countItemByGroup = new ArrayList<>();
        Set<String> invoiceNotReplace = new HashSet<>();
        for (ShippingResponse shippingResponse : podNumberResponseList) {
            for (ManifestoResponse manifestoResponse : shippingResponse.getData()) {
                if (manifestoResponse.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
                    countLog++;
                else
                    countSo++;

                if (manifestoResponse.getInvoiceNo() != null && !manifestoResponse.getInvoiceNo().equals("") && !manifestoResponse.getInvoiceNo().equals(null))
                    for(String invoice : manifestoResponse.getInvoiceNo().split(","))
                        invoiceNotReplace.add(invoice);
            }
        }
        countInvoice = invoiceNotReplace.size();
        countItemByGroup.add(countSo);
        countItemByGroup.add(countLog);
        countItemByGroup.add(countInvoice);
        return countItemByGroup;
    }

    public List<Integer> getCountApprovalItemByGroup(List<ManifestoResponse> listManisfesto) {
        checkerConfirmStatus = false;
        int countApprovalSo = 0;
        int countApprovalLog = 0;
        int countNotApprovalSo = 0;
        int countNotApprovalLog = 0;
        List<Integer> countItemApprovalByGroups = new ArrayList<>();
        for (ManifestoResponse manifestoResponse : listManisfesto) {
            if (manifestoResponse.getJobStatus() != null && manifestoResponse.getJobStatus().getMasterStatus() != null &&
                    (
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_REJECT ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_START ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_END ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM ||
                            manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT
                    )
            )
            {
                if (manifestoResponse.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
                    countApprovalLog++;
                else
                    countApprovalSo++;
                checkerConfirmStatus = true;
            } else if (manifestoResponse.getJobStatus() != null && manifestoResponse.getJobStatus().getMasterStatus() != null
                    && manifestoResponse.getJobStatus().getMasterStatus().getId() == PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM)
            {
                if (manifestoResponse.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG))
                    countNotApprovalLog++;
                else
                    countNotApprovalSo++;
            }

        }
        countItemApprovalByGroups.add(countApprovalSo);
        countItemApprovalByGroups.add(countApprovalLog);
        countItemApprovalByGroups.add(countNotApprovalSo);
        countItemApprovalByGroups.add(countNotApprovalLog);
        return countItemApprovalByGroups;
    }

    public SaveShippingResponse validateSaveShipping(AddManifestoRequest addManifestoRequest) throws Exception {
        SaveShippingResponse saveShippingResponse;

        try {
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(addManifestoRequest.getLoginId());
            if(userTypeMaster == null) {
                return new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);
            }

            List<ManifestoOwnerResponse> manifestoOwnerResponseList = commonService.validateOwnerByManifestoId(addManifestoRequest.getManifestoIds(), userTypeMaster.getId());
            if(manifestoOwnerResponseList.isEmpty())
            {
                return new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.MANIFESTO_NOT_FOUND);
            }
            else if(!manifestoOwnerResponseList.stream().filter(m -> !m.isOwner()).collect(Collectors.toList()).isEmpty() || manifestoOwnerResponseList.size() != addManifestoRequest.getManifestoIds().size())
            {
                List<Integer> manifestoIds = manifestoOwnerResponseList.stream().filter(m -> m.isOwner()).map(m -> m.getManifestoId()).collect(Collectors.toList());
                List<Integer> manifestoIdsLogNotOwner = manifestoOwnerResponseList.stream().filter(m -> !m.isOwner() && m.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG)).map(m -> m.getFulfillId()).collect(Collectors.toList());
                List<String> manifestoIdsSoNotOwner = manifestoOwnerResponseList.stream().filter(m -> !m.isOwner() && !m.getManifestoType().equalsIgnoreCase(ManifestolistConstant.LOG)).map(m -> m.getJobId()).collect(Collectors.toList());
                int countSoNotOwner = manifestoIdsSoNotOwner.size();
                int countLogNotOwner = manifestoIdsLogNotOwner.size();

                saveShippingResponse = new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_FAILED);
                saveShippingResponse.setDataIsSelected(manifestoIds);
                saveShippingResponse.setDataLogNotIsSelected(manifestoIdsLogNotOwner);
                saveShippingResponse.setDataSoNotIsSelected(manifestoIdsSoNotOwner);
                saveShippingResponse.setCountSoNotOwner(countSoNotOwner);
                saveShippingResponse.setCountLogNotOwner(countLogNotOwner);

                return saveShippingResponse;
            } else {
                saveShippingResponse = new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.OK,
                        HttpStatus.OK.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_SUCCESS);
            }

        } catch (Exception e) {
            throw new GZException(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }

        return saveShippingResponse;
    }

    public SaveShippingResponse saveShipping(AddManifestoRequest addManifestoRequest) throws Exception {
        SaveShippingResponse saveShippingResponse;
        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

        try {
            UserTypeMaster userTypeMaster = commonService.getUserTypeMasterForReturn(addManifestoRequest.getLoginId());
            if(userTypeMaster == null) {
                return new SaveShippingResponse(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);
            }

            SaveShippingResponse validateShipping = validateSaveShipping(addManifestoRequest);

            if(validateShipping.getCode() == HttpStatus.OK) {
                for (int manifestoId : addManifestoRequest.getManifestoIds() ) {
                    jobStatusRepository.save(
                            new JobStatus(manifestoId, PODNumberStatusConstant.JOB_DRIVER_CONFIRM, null, userTypeMaster.getId(), currentDate)
                    );
                }
                saveShippingResponse = new SaveShippingResponse(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.OK,
                        HttpStatus.OK.toString(), ManifestolistConstant.SAVE_MANIFESTO_SUCCESS);
            } else {
                saveShippingResponse = validateShipping;
            }
        } catch (Exception e) {
            throw new GZException(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), e.getMessage());
        }

        return saveShippingResponse;
    }

    public ManifestoCheckerResponse getJobRemarkDetailForChecker(ManifestoResponse manifestolistResponse, UserTypeMaster userTypeMaster) {
        ManifestoCheckerResponse mani = new ManifestoCheckerResponse();

        // get reject remark
        String rejectRemark = null;
        GenericInformation rejectRemarkGen = null;
         if (manifestolistResponse.getJobStatus() != null) {
                rejectRemarkGen = genericInformationRepository.findByRefModuleAndRefActionAndRefId(ManifestolistConstant.JOBSTATUS_DB,
                    ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN,
                    manifestolistResponse.getJobStatus().getId());
        }

        if (rejectRemarkGen != null) {

            // get reject remark description
            try{
                MasterReasonRejectManifesto reasonReject = masterReasonRejectManifestoRepository.findOne(Integer.parseInt(rejectRemarkGen.getValue()));
                if (reasonReject != null) {
                    rejectRemark = reasonReject.getDescription();
                } else {
                    rejectRemark = null;
                }
            } catch (Exception ex) {
                rejectRemark = null;
            }
        }

        // get receive doc
        ReceiveDocument receiveDocument = null;
        if (userTypeMaster != null) {
            int receiveStatusId = PODNumberStatusConstant.RECEIVE_CHECKER_CONFIRM;
            if (userTypeMaster.getUserTypeId() == PODNumberStatusConstant.CHECKER) {
                receiveStatusId = PODNumberStatusConstant.RECEIVE_CHECKER_CONFIRM;
            } else if (userTypeMaster.getUserTypeId() == PODNumberStatusConstant.FINOPS) {
                receiveStatusId = PODNumberStatusConstant.RECEIVE_FINOP_CONFIRM;
            }
            receiveDocument = receiveDocumentRepository.findByManifestoIdAndStatusId(manifestolistResponse.getManifestoId(), receiveStatusId);
        }

        String receiveRemark = null;
        String userReceive = null;
        boolean received = false;
        if (receiveDocument != null) {
            received = true;

            // get remark
            GenericInformation receiveGen = genericInformationRepository
                    .findByRefModuleAndRefActionAndRefId(GenericInformationConstant.RECEIVEDOCUMENT_DB,
                            GenericInformationConstant.RECEIVEDOCUMENT_REFACTION,
                            receiveDocument.getId());

            if (receiveGen != null) {
                receiveRemark = receiveGen.getValue();
            }

            if (receiveDocument.getUserTypeMaster() != null) {
                mani.setUserReceive(receiveDocument.getUserTypeMaster().getName());
            }
        }

        mani.setReceived(received);
        mani.setReceiveRemark(receiveRemark);
        mani.setRejectRemark(rejectRemark);

        return mani;
    }

    public boolean findStatusDelayDate(List<ManifestoResponse> listManisfesto, String tab){
        // set now date
        String nowDate = java.time.LocalDate.now().toString();
        boolean flgDelay = false;
        boolean checkDelay = false;
        // set array list for check flg delay
        List<Boolean> storeFlgDelay = new ArrayList<>();
        for (ManifestoResponse subList : listManisfesto) {


            // check time
            String timeStamp = new SimpleDateFormat("HH:mm").format(new Date());
            String[] hourMin = timeStamp.split(":");
            int hour = Integer.parseInt(hourMin[0]);
            int mins = Integer.parseInt(hourMin[1]);
            int timeNow = (hour * 60) + mins;
            // time from soms
            hourMin = DataFromSOMSConstant.CHECK_TIME_DELAY.split(":");
            hour = Integer.parseInt(hourMin[0]);
            mins = Integer.parseInt(hourMin[1]);
            int checkTimeDelay = (hour * 60) + mins;
            // check flg delay reference time
            if (timeNow >= checkTimeDelay)
            {
                if (subList.getDelayDate() != null)
                    flgDelay = true;
            }
            else
                {
                // set pod delay
                if (tab.equalsIgnoreCase(ManifestolistConstant.SHIPPING) || tab.equalsIgnoreCase(ManifestolistConstant.CHECKER_TAB_WAIT))
                {
                    flgDelay = false;
                }
                else if (subList.getDelayDate() != null)
                {
                    if (!subList.getDelayDate().equalsIgnoreCase(nowDate))
                    {
                        flgDelay = true;
                    }
                    else
                    {
                        if (subList.getJobStatus() != null && subList.getJobStatus().getStatusId() == PODNumberStatusConstant.JOB_CHECKER_CONFIRM)
                        {
                            flgDelay = true;
                        }
                    }
                }
            }
            storeFlgDelay.add(flgDelay);
        }
        if (storeFlgDelay.contains(true)){
            checkDelay = true;
        }
        return checkDelay;
    }
}