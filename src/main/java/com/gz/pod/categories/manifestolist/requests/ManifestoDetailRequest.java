package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ManifestoDetailRequest {
    @ApiModelProperty(required = true)
    @NotNull(message = "manifestoId can't be null")
    @ApiParam(value = "Id ของ ใบงาน", defaultValue = "1")
    private Integer manifestoId;

    @ApiModelProperty(required = true)
    @NotNull(message = "login Id can't be null")
    @ApiParam(value = "login Id", defaultValue = "1")
    private Integer loginId;
}
