package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ManifestoDetailResponse {
    private String jobId;
    private Integer manifestoId;
    private String manifestoType;
    private Integer newBoxQty;
    private String boxRemark;
    private Integer itemQty;
    private Integer skuQty;
    private String routeName;
    private String customerName;
    private String shipToAddress;
    private String remark;
    private String remarkStatus;
    private BigDecimal orderTotalInVat;
    private BigDecimal vat;
    private BigDecimal totalExVatTH;
    private String status;
    private String driverName;
    private String rejectRemark;
    private List<ManifestoItemlResponse> items;
    private String nameUserUpdateRemark;
    private boolean isReceive;
    private String receiveRemark;
    private String nameUserReceive;
    private String contactPerson;
    private List<FileResponse> files;

    public ManifestoDetailResponse() {
    }
}
