package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
public class ManifestoItemlResponse {

    private int id;
    private String productCode;
    private String productName;
    private String uom;
    private Integer quantity;
    private Integer quantitySend;
    private BigDecimal totalExVat;
    private String remark;


    public ManifestoItemlResponse() {
    }

}
