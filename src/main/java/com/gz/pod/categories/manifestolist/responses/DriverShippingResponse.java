package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class DriverShippingResponse {

    private Integer driverId;
    private String driverName;
    private List<ShippingResponse> shippingList;

}
