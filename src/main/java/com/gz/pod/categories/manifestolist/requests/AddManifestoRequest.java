package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddManifestoRequest {
    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "manifestoIds is request")
    private List<Integer> manifestoIds;

    @ApiModelProperty(required = true)
    @ApiParam(value = "เลขที่", defaultValue = "1")
    @NotNull(message = "loginId is request")
    private Integer loginId;

}
