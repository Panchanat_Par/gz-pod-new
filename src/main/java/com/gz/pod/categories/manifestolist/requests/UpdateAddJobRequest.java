package com.gz.pod.categories.manifestolist.requests;

import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class UpdateAddJobRequest {
    @NotNull(message = "field not null Ex: so, log, qrCode")
    @Pattern(regexp = "(" + ManifestolistConstant.REF_MODULE_TYPE_SO + "|" +
            ManifestolistConstant.REF_MODULE_TYPE_LOG + "|" +
            ManifestolistConstant.REF_MODULE_TYPE_QRCODE + ")",

            message = "Value must be " + ManifestolistConstant.REF_MODULE_TYPE_SO + ", " +
                    ManifestolistConstant.REF_MODULE_TYPE_LOG + ", " +
                    ManifestolistConstant.REF_MODULE_TYPE_QRCODE + " only")
    private String refModuleType;

    @ApiModelProperty(required = true)
    @NotNull(message = "refId can't be null")
    @ApiParam(value = "หมายเลขงาน", defaultValue = "1")
    private String refId;

    @ApiParam(value = "เลขที่คนขับรถ", defaultValue = "4")
    @NotNull(message = "driverId can't be null")
    @Min(value = 1, message = "driverId is require")
    private int driverId;
}
