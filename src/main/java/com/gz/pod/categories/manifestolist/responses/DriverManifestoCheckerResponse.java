package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class DriverManifestoCheckerResponse {
    private String date;
    private String driverName;
    private String driverStatus;
    private List<DateShippingListResponse> dateShippingList;
}
