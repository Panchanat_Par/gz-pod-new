package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class ManifestoRollbackRequest {
    @ApiParam(value = "fieldType")
    @NotNull(message = "manifestoId is request")
    private Integer manifestoId;
}
