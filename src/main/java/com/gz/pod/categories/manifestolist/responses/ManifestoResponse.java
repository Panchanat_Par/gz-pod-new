package com.gz.pod.categories.manifestolist.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gz.pod.entities.JobStatus;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ManifestoResponse {
    private Integer driverId;
    private int manifestoId;
    private String manifestoType;
    private String jobId;
    private String customerName;
    private String shipToCode;
    private String shipToName;
    private String shipToAddress;
    private String addressType;
    private Integer boxQty;
    private Integer newBoxQty;
    private Integer itemQty;
    private Integer skuQty;
    private String remark;
    private String routeName;
    private String podNumber;
    private String issueType;
    private Integer fulfillId;
    private String deliveryDate;
    private String contactPerson;
    private String invoiceNo;
    private String note;
    private JobStatus jobStatus;
    private String remarkForChecker;
    private String status = "";
    private String delayDate;

    private String remarkStatus;
    private String userRemarkStatus;
    private boolean received;
    private String receiveRemark;
    private String rejectRemark;
    private String userReceive;
    private Boolean flgAttachFile;

    public ManifestoResponse() {
    }

    public ManifestoResponse(Integer driverId, int manifestoId, String manifestoType, String jobId, String customerName, String shipToCode, String shipToName, String shipToAddress, String addressType, Integer boxQty, Integer newBoxQty, Integer itemQty, Integer skuQty, String remark, String routeName, String podNumber, String issueType, Integer fulfillId, String deliveryDate, String contactPerson, String invoiceNo, String note, JobStatus jobStatus, String delayDate) {
        this.driverId = driverId;
        this.manifestoId = manifestoId;
        this.manifestoType = manifestoType;
        this.jobId = jobId;
        this.customerName = customerName;
        this.shipToCode = shipToCode;
        this.shipToName = shipToName;
        this.shipToAddress = shipToAddress;
        this.addressType = addressType;
        this.boxQty = boxQty;
        this.newBoxQty = newBoxQty;
        this.itemQty = itemQty;
        this.skuQty = skuQty;
        this.remark = remark;
        this.routeName = routeName;
        this.podNumber = podNumber;
        this.issueType = issueType;
        this.fulfillId = fulfillId;
        this.deliveryDate = deliveryDate;
        this.contactPerson = contactPerson;
        this.invoiceNo = invoiceNo;
        this.note = note;
        this.jobStatus = jobStatus;
        this.delayDate = delayDate;
    }

    public ManifestoResponse(int manifestoId, String manifestoType, Integer newBoxQty, Integer itemQty, Integer skuQty, String remark, String jobId, String invoiceNo, String issueType){
        this.manifestoId = manifestoId;
        this.manifestoType = manifestoType;
        this.jobId = jobId;
        this.newBoxQty = newBoxQty;
        this.itemQty = itemQty;
        this.skuQty = skuQty;
        this.remark = remark;
        this.invoiceNo = invoiceNo;
        this.issueType = issueType;
    }


    public ManifestoResponse(int manifestoId, String manifestoType, String remark, Integer fulfillId, String jobId, String invoiceNo, String issueType){
        this.manifestoId = manifestoId;
        this.manifestoType = manifestoType;
        this.jobId = jobId;
        this.remark = remark;
        this.fulfillId = fulfillId;
        this.invoiceNo = invoiceNo;
        this.issueType = issueType;
    }

}
