package com.gz.pod.categories.manifestolist.responses;

import com.gz.pod.entities.ManifestolistWithDriver;
import lombok.Data;

import java.util.List;

@Data
public class PODGroup {

    private String podNumber;
    private String date;
    private String driverName;
    private List<ManifestolistWithDriver> jobList;

}
