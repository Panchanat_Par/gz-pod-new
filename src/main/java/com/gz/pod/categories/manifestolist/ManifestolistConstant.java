package com.gz.pod.categories.manifestolist;

import com.gz.pod.entities.Manifestolist;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ManifestolistConstant {

    public static final String UPDATE_MANIFESTO_BOXQTY_PODNUMBER = "Update Manifesto boxQty by POD Number";
    public static final String UPDATE_MANIFESTO_BOXQTY_BYJOBID = "Update Manifesto boxQty by Jod Id";
    public static final String UPDATE_MANIFESTO_BOXQTY_SUCCESS = "Update Manifesto Success";
    public static final String UPDATE_MANIFESTO_BOXQTY_NOT_FOUND = "Manifesto Not Found";
    public static final String REFID_IS_REQUEST = "refId is request";
    public static final String REMARK_IS_REQUEST = "remark is request when boxQty = 0";
    public static final String REMARK_MUST_MAX_1000 = "Remark must be less than 1000 characters";

    public static final String VALIDATE_UPDATE_MANIFESTO_BOXQTY_JOBCHANGED = "Validate.Update.Manifesto.BoxQty.JobChanged";
    public static final String VALIDATE_UPDATE_MANIFESTO_BOXQTY_SUCCESS = "Validate.Update.Manifesto.BoxQty.Success";

    public static final String LOG = "log";
    public static final String ORDER = "order";

    public static final String MANIFESTO_DB = "manifesto";
    public static final String JOBSTATUS_DB = "jobStatus";
    public static final String REF_ACTION_UPDATEJOBSTATUS = "updateJobStatus";
    public static final String REF_ACTION_UPDATEJOBSTATUS_DROPDOWN = "updateJobStatusRejectDropdown";

    public static final String ACTION_UPDATEBOXQTY = "updateBoxQty";

    public static final String REF_MODULE_TYPE_SO = "so";
    public static final String REF_MODULE_TYPE_LOG = "log";
    public static final String REF_MODULE_TYPE_QRCODE = "qrCode";


    public static final String SHIPPING_LIST = "Manifesto List";
    public static final String GET_LIST_SUCCESS = "get list success!";
    public static final String GET_LIST_ERROR = "Manifesto list not found, please check your data!";
    public static final int DRIVER_ID = 1;

    public static final String VALIDATE_AND_UPDATE_ADD_JOB = "Validate and Update Add Job";

    public static final String VALIDATE_ADD_JOB_NOTFOUND = "Job.Is.Not.Found";
    public static final String VALIDATE_ADD_JOB_SUCCESS = "Job.Can.Change.DriverId";
    public static final String VALIDATE_ADD_JOB_UNABLE_TO_ADD = "Job.Unable.To.Add";
    public static final String VALIDATE_ADD_JOB_DUPLICATE = "Job.Can.Not.Change.Duplicate";

    public static final String UPDATE_ADD_JOB_SUCCESS = "Update Add Job Success";
    public static final String UNKNOWN_REF_MODULE_TYPE = "RefModuleType must be " + REF_MODULE_TYPE_SO + ", " +
            REF_MODULE_TYPE_LOG + ", " + REF_MODULE_TYPE_QRCODE + " only";
    public static final String GET_DETAIL_SUCCESS = "get Detail success!";
    public static final String GET_DETAIL_ERROR = "Manifesto Detail not found, please check your data!";
    public static final String GET_DETAIL = "Shipping Detail";
    public static final String SHIPPING = "shipping";
    public static final String TODAYLIST = "todayList";
    public static final String SHIPPED = "shipped";
    public static final String DELIVERED = "delivered";
    public static final String CHECKER = "checker";
    public static final String CHECKER_WAIT = "checkerWait";
    public static final String CHECKER_APPROVED = "checkerApproved";
    public static final String FINOPS = "finops";
    public static final String CHECKER_TAB_WAIT = "wait";
    public static final String CHECKER_TAB_APPROVE = "approved";
    public static final String POD_DRIVER_AWAIT = "POD.Driver.Status.AwaitingApproval";
    public static final String POD_CHECKER_AWAIT = "POD.Checker.Status.AwaitingApproval";
    public static final String POD_CHECKER_APPROVAL = "POD.Checker.Status.Approval";

    public static final String VALIDATE_AND_DELETE_JOB = "Validate and Delete Job";
    public static final String VALIDATE_AND_DELETE_JOB_NOTFOUND = "ManifestoId.Is.Not.Found";
    public static final String VALIDATE_AND_DELETE_JOB_CAN_DELETE = "ManifestoId.Can.Delete";
    public static final String VALIDATE_AND_DELETE_JOB_CANNOTDELETE = "ManifestoId Can Not Delete Job";
    public static final String UPDATE_DELETE_JOB_SUCCESS = "ManifestoId Delete Job Success";
    public static final String UPDATE_UNDELETE_JOB_REMARK_NULL = "Remark is not null or Empty";

    public static final String UPDATE_MANIFESTO_STAUTS = "Update Manifesto Status";
    public static final String UPDATE_MANIFESTO_STAUTS_SUCCESS = "Update Manifesto Status Success";
    public static final String MANIFESTO_ITEM_NOT_FOUND = "Manifesto item not found";
    public static final String MANIFESTO_NOT_FOUND = "Manifesto not found";
    public static final String FIELDTYPE_NOT_FOUND = "fieldType Not Found";

    public static final int JOB_DRIVER_CONFIRMALLDELIVERY = 211;
    public static final int JOB_DRIVER_REJECTALLDELIVERY = 212;
    public static final int JOB_DRIVER_PARTIALDELIVERY = 213;


    public static final String MANIFESTO_SEARCH = "Search";
    public static final String MANIFESTO_SEARCH_SUCCESS = "Search Success";

    public static final String SHIPPING_STATUS_CHECKER = "Shipping Status Checker";
    public static final String SHIPPING_STATUS_CHECKER_SUCCESS = "Shipping Status Checker Success";
    public static final String EXCEED_PAGE_NUMBER = "Exceed Page Number";

    public static final String SAVE_MANIFESTO = "Add manifesto to shipping";
    public static final String VALIDATE_MANIFESTO = "validate add to shipping";
    public static final String SAVE_MANIFESTO_NOT_OWNER = "manifesto not owner";
    public static final String SAVE_MANIFESTO_SUCCESS = "Add manifesto to shipping success";


    public static final String ROLLBACK = "rollback manifesto ";
    public static final String ROLLBACK_SUCCESS = "rollback manifesto Success";

    public static Manifestolist MANIFESTO_RESPONSE = new Manifestolist("1", "2018-07-26", "1111",
            1, "SOMS Order", "",
            "SM01", 1, "1",
            "Test Test", "502100", "Test ja Test ja", "Test moo 1",
            1, 1, 1, "remark", 1,"",new BigDecimal(1000),
            "201810-0001","",1,1,"","","","",0,"", "contactPerson","2018-07-18");
    public static Manifestolist MANIFESTO_RESPONSE_2 = new Manifestolist("1", "2018-07-26", "1111",
            1, "SOMS Order", "",
            "SM01", 1, "1",
            "Test Test", "502100", "Test ja Test ja", "Test moo 1",
            1, 1, 1, "remark", 1,"",new BigDecimal(1000),
            "201810-0002","",1,1,"","","","",0,"", "contactPerson","2018-07-20");
    public static Manifestolist MANIFESTO_RESPONSE_LOG = new Manifestolist("1", "2018-07-26", "1111",
            1, "log", "001",
            "SM01", 1, "1",
            "Test Test", "502100", "Test ja Test ja", "Test moo 1",
            1, 1, 1, "remark", 1,"",new BigDecimal(1000),
            "201810-0001","",1,1,"","","","",0,"", "contactPerson",  null);

    public static Manifestolist MANIFESTO_RESPONSE_3 = new Manifestolist("1", "2019-01-11", "1111",
            1, "log", "001",
            "SM01", 1, "1",
            "Test Test", "502100", "Test ja Test ja", "Test moo 1",
            1, 1, 1, "remark", 1,"",new BigDecimal(1000),
            "201810-0001","",1,1,"","","","",0,"", "contactPerson",  "2019-01-10");

    private ManifestolistConstant() {

    }

    public static List<Manifestolist> GET_MANIFESTO_LIST() {
        List<Manifestolist> manifestoList = new ArrayList<>();
        manifestoList.add(MANIFESTO_RESPONSE);
        manifestoList.add(MANIFESTO_RESPONSE_LOG);
        manifestoList.add(MANIFESTO_RESPONSE_2);
        return manifestoList;
    }


}
