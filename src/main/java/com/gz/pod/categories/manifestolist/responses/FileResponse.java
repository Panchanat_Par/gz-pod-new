package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

@Data
public class FileResponse {

    private int manifestoFileId;

    public FileResponse(int manifestoFileId) {
        this.manifestoFileId = manifestoFileId;
    }
}
