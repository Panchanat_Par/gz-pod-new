package com.gz.pod.categories.manifestolist.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
public class ManifestoPutBoxQtyRequest {
    @ApiParam(name = "jobId")
    @NotNull(message = "jobId is request")
    private Integer jobId;

    @ApiParam(name = "box quantity")
    @NotNull(message = "boxQty is request")
    @Min(value = 1, message = "boxQty not 0")
    private Integer boxQty;

    @ApiParam(value = "update By")
    @NotNull(message = "updateBy is request")
    @Min(value = 1, message = "updateBy not 0")
    private Integer updateBy;
}
