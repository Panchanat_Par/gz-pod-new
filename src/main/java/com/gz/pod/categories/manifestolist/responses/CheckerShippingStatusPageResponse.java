package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class CheckerShippingStatusPageResponse {
    private Integer totalItems;
    private Integer pageSize;
    private Integer currentPage;
    private Integer totalPage;
    List<DateShippingResponse> dateList;
}
