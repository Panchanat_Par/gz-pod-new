package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ManifestoLogDetailResponse {
    private String jobId;
    private Integer manifestoId;
    private String manifestoType;
    private String sapCustCode;
    private String customerName;
    private String shipToAddress;
    private String remark;
    private String logRemark;
    private String remarkStatus;
    private String InvoiceNo;
    private String status;
    private String issueType;
    private String driverName;
    private String rejectRemark;
    private int fulfillId;
    private String nameUserUpdateRemark;
    private boolean isReceive;
    private String receiveRemark;
    private String nameUserReceive;
    private String contactPerson;
    private List<FileResponse> files;


    public ManifestoLogDetailResponse() {
    }
}
