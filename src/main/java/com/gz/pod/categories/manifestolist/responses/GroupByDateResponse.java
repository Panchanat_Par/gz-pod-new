package com.gz.pod.categories.manifestolist.responses;

import lombok.Data;

import java.util.List;

@Data
public class GroupByDateResponse {
    private String deliveryDate;
    private int countSo;
    private int countLog;
    private int countInvoice;
    private int countPODNumber;
    private List<ShippingResponse> manifestoList;
}
