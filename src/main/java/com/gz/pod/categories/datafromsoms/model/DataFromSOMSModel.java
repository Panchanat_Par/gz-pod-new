package com.gz.pod.categories.datafromsoms.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;


@Data
public class DataFromSOMSModel {
    @JsonProperty("ManifestoId")
    private int ManifestoId;
    @JsonProperty("DeliveryDate")
    private String DeliveryDate;
    @JsonProperty("AddressType")
    private String AddressType;
    @JsonProperty("BillNo")
    private String BillNo;
    @JsonProperty("BoxQty")
    private Integer BoxQty;
    @JsonProperty("CustomerName")
    private String CustomerName;
    @JsonProperty("DocumentNumber")
    private String DocumentNumber;
    @JsonProperty("DONumber")
    private String DONumber;
    @JsonProperty("DriverId")
    private int DriverId;
    @JsonProperty("ItemQty")
    private Integer ItemQty;
    @JsonProperty("JobId")
    private String JobId;
    @JsonProperty("ManifestoType")
    private String ManifestoType;
    @JsonProperty("OrderTotalInVat")
    private BigDecimal OrderTotalInVat;
    @JsonProperty("Remark")
    private String Remark;
    @JsonProperty("RouteMasterId")
    private int RouteMasterId;
    @JsonProperty("RouteMasterName")
    private String RouteMasterName;
    @JsonProperty("ShipToCode")
    private String ShipToCode;
    @JsonProperty("ShippingStatus")
    private String ShippingStatus;
    @JsonProperty("SKUQty")
    private Integer SKUQty;
    @JsonProperty("SONo")
    private String SONo;
    @JsonProperty("ShipToName")
    private String ShipToName;
    @JsonProperty("ShipToAddress")
    private String ShipToAddress;
    @JsonProperty("ShipmentNo")
    private String ShipmentNo;
    @JsonProperty("PODFlag")
    private String PODFlag;
    @JsonProperty("logType")
    private String logType;
    @JsonProperty("CreatedDate")
    private String CreatedDate;
    @JsonProperty("SAPCustCode")
    private String SAPCustCode;
    @JsonProperty("Invoice")
    private String Invoice;
    @JsonProperty("FulfillId")
    private Integer FulfillId;
    @JsonProperty("ContactPerson")
    private String ContactPerson;
    @JsonProperty("DelayDate")
    private String DelayDate;

    public DataFromSOMSModel() {}
}