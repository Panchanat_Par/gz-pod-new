package com.gz.pod.categories.datafromsoms.requests;

import lombok.Data;

import java.util.List;

@Data
public class JobStatusToSOMSRequest {

    private String refId;
    private String refModuleType;
    private String remark;
    private Integer statusId;
    private String createdDate;
    private Integer driverId;
    private List<PicIdRequest> picIds;

    public JobStatusToSOMSRequest(String refId, String refModuleType, String remark, Integer statusId, String createdDate, Integer driverId) {
        this.refId = refId;
        this.refModuleType = refModuleType;
        this.remark = remark;
        this.statusId = statusId;
        this.createdDate = createdDate;
        this.driverId = driverId;
    }

    public JobStatusToSOMSRequest(String refId, String refModuleType, String remark, Integer statusId, String createdDate, Integer driverId, List<PicIdRequest> picIds) {
        this.refId = refId;
        this.refModuleType = refModuleType;
        this.remark = remark;
        this.statusId = statusId;
        this.createdDate = createdDate;
        this.driverId = driverId;
        this.picIds = picIds;
    }
}
