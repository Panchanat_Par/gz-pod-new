package com.gz.pod.categories.datafromsoms.requests;

import lombok.Data;

@Data
public class PicIdRequest {
    private int id;

    public PicIdRequest(int id) {
        this.id = id;
    }
}
