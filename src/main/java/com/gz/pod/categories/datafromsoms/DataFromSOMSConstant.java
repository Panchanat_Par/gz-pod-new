package com.gz.pod.categories.datafromsoms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DataFromSOMSConstant {
    public static String PrivateKey;
    public static String URL_SOMS_API;
    public static int NUM_GET_ITEM;

    @Value("${PrivateKey}")
    public void setPrivateKey(String PrivateKey) {
        PrivateKey = PrivateKey;
    }

    @Value("${URL_SOMS_API}")
    public void setUrlSomsApi(String urlSomsApi) {
        URL_SOMS_API = urlSomsApi;
    }

    @Value("${NUM_GET_ITEM}")
    public void setNumGetItem(int numGetItem) {
        NUM_GET_ITEM = numGetItem;
    }

    // config job stamp and change delivery date for delay date
    public static final String CHANGE_DELIVERY_DATE = "0 30 23 * * *";
    public static final String STAMP_DELAY_DATE = "0 15 17 * * *";

    // config jog get data from SOMS
    public static final String GET_MANIFESTO_FROM_SOMS = "0 0 4-5 * * *";

    // config time to check flg delay
    public static final String CHECK_TIME_DELAY = "17:15";

    public static final String TRANSFER_SUCCESS = "Transfer data success";
    public static final String TRANSFER_DATA = "Transfer data from soms";
    public static final String MANIFESTO_NOT_FOUND = "apimanifesto.response.manifestolist.notfound";

    public static final String GET_ITEM  ="[\n" +
            "    {\n" +
            "        \"SO\": \"SO100\",\n" +
            "        \"SomsManifestoItem\": [\n" +
            "            {\n" +
            "                \"SO\": \"SO100\",\n" +
            "                \"SOItem\": \"20\",\n" +
            "                \"MatDoc\": \"11111\",\n" +
            "                \"DocDate\": \"2018-09-14T00:00:00\",\n" +
            "                \"PostDate\": \"2018-09-14T00:00:00\",\n" +
            "                \"ProductCode\": \"11111\",\n" +
            "                \"MaterialCode\": \"11111\",\n" +
            "                \"ProductName\": \"Test mock data\",\n" +
            "                \"UOM\": \"EA\",\n" +
            "                \"Quantity\": 100,\n" +
            "                \"Amount\": 1,\n" +
            "                \"Id\": 7\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "]";
}
