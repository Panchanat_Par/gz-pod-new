package com.gz.pod.categories.datafromsoms;

import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.datafromsoms.responses.DataFromSOMSResponse;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("somsmanifesto")
@Api(value = "Get Data From SOMS", description = "Data From SOMS management", produces = "application/json", tags = {"SOMS"})
public class DataFromSOMSController {
    @Autowired
    DataFromSOMSService dataFromSOMSService;

    @GetMapping
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = DataFromSOMSResponse.class)})
    public ResponseEntity ImportManifestoList(@Valid DataFromSOMSRequest dataFromSOMSRequest) throws Exception {
        return ResponseEntity.ok(dataFromSOMSService.importManifestoList(dataFromSOMSRequest));
    }

    @Scheduled(cron = DataFromSOMSConstant.GET_MANIFESTO_FROM_SOMS)
    public void ImportManifestoListJob() throws Exception {
        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        dataFromSOMSService.importManifestoList(dataFromSOMSRequest);
    }
}
