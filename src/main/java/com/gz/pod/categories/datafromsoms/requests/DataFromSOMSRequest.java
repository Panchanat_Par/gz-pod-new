package com.gz.pod.categories.datafromsoms.requests;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class DataFromSOMSRequest {
    @ApiParam(value = "วันที่ที่ต้องการดู", defaultValue = "2018-07-26")
    private String deliveryDate;
}
