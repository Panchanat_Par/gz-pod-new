package com.gz.pod.categories.datafromsoms;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.datafromsoms.model.DataFromSOMSModel;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.JobStatusToSOMSRequest;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.JobStatus;
import com.gz.pod.entities.ManifestoItem;
import com.gz.pod.entities.Manifestolist;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponsePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

@Service
public class DataFromSOMSService {

    @Autowired
    ManifestolistRepository manifestolistRepository;

    @Autowired
    ManifestoItemRepository manifestoItemRepository;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    JobStatusRepository jobStatusRepository;

    @Autowired
    CommonService commonService;

    public DataFromSOMSService (ManifestolistRepository manifestolistRepository,
                                RestTemplate restTemplate,
                                ManifestoItemRepository manifestoItemRepository,
                                JobStatusRepository jobStatusRepository)
    {
        this.manifestolistRepository = manifestolistRepository;
        this.restTemplate = restTemplate;
        this.manifestoItemRepository = manifestoItemRepository;
        this.jobStatusRepository = jobStatusRepository;
    }

    private ObjectMapper mapper = new ObjectMapper();

    public GZResponsePage importManifestoList(DataFromSOMSRequest request) throws GZException {
        GZResponsePage response = new GZResponsePage();
        try {
            if (request.getDeliveryDate() == null) {
                request.setDeliveryDate(java.time.LocalDate.now().toString());
            }
            List<Manifestolist>  getNotOld =  manifestolistRepository.findManifestolistPODFlagNotOld();
            if(!getNotOld.isEmpty())
            {
                for ( Manifestolist mani :getNotOld)
                {
                    manifestoItemRepository.deleteByManifestoId(mani.getId());
                }
            }
            manifestolistRepository.deletePodFlagNotUpdate();

            String[] payloads = new String[]{};
            final String secretKey = commonService.generateSignature(payloads);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type","application/json");
            headers.set("secretKey",secretKey);
            Map body = new HashMap();
            HttpEntity<?> entity = new HttpEntity<>(body,headers);
            ResponseEntity<String> res = restTemplate.exchange(DataFromSOMSConstant.URL_SOMS_API + "API/GetListManifesto", HttpMethod.POST, entity, String.class);


            if(res.getStatusCode() == HttpStatus.OK)
            {
                JsonNode tree = mapper.readTree(res.getBody());
                JsonNode message = tree.path("Message");
                if(!message.isMissingNode())
                {
                    if(message.asText().equalsIgnoreCase(DataFromSOMSConstant.MANIFESTO_NOT_FOUND))
                        UpdatePODNumber(request.getDeliveryDate(), PODNumberStatusConstant.JOB_DRIVER_SO_WAIT);

                    throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                            message.asText(),""
                    );
                }
                List<DataFromSOMSModel> dataFromSOMSModelList = mapper.readValue(res.getBody(), mapper.getTypeFactory().constructCollectionType(List.class, DataFromSOMSModel.class));
                for (DataFromSOMSModel data : dataFromSOMSModelList)
                {
                    Manifestolist manifesto = new Manifestolist(
                            data.getDocumentNumber(),
                            data.getDeliveryDate(),
                            data.getDONumber(),
                            data.getDriverId(),
                            data.getManifestoType(),
                            data.getShipmentNo(),
                            data.getJobId(),
                            data.getRouteMasterId(),
                            data.getRouteMasterName(),
                            data.getCustomerName(),
                            data.getShipToCode(),
                            data.getShipToName(),
                            data.getShipToAddress(),
                            data.getBoxQty(),
                            data.getItemQty(),
                            data.getSKUQty(),
                            data.getRemark(),
                            null,
                            data.getAddressType(),
                            data.getOrderTotalInVat(),
                            null,
                            "",
                            data.getBoxQty(),
                            0,
                            data.getSAPCustCode(),
                            data.getInvoice(),
                            data.getLogType(),
                            "",
                            data.getFulfillId(),
                            data.getPODFlag(),
                            data.getContactPerson(),
                            data.getDelayDate()
                    );
                    if(data.getPODFlag().equals("N"))
                    {
                        manifestolistRepository.save(manifesto);
                    }
                    else if (data.getPODFlag().equals("U"))
                    {
                        List<Manifestolist> getDataForDelete;
                        if(data.getManifestoType().equalsIgnoreCase("log")) {
                            manifestolistRepository.deleteByFulfillId(data.getFulfillId());
                        } else {
                            getDataForDelete =  manifestolistRepository.findManifestoBySoNumber(data.getJobId());
                            if(!getDataForDelete.isEmpty())
                            {
                                for ( Manifestolist mani :getDataForDelete)
                                {
                                    manifestoItemRepository.deleteByManifestoId(mani.getId());
                                }
                            }
                            manifestolistRepository.deleteByJobId(data.getJobId());
                        }

                        manifestolistRepository.save(manifesto);
                    }
                }
                UpdatePODNumber(request.getDeliveryDate(), PODNumberStatusConstant.JOB_DRIVER_SO_WAIT);
                String[] payloads2 = new String[]{};
                final String secretKey2 = commonService.generateSignature(payloads2);
                headers.set("secretKey",secretKey2);
                HttpEntity<?> entityUpdate = new HttpEntity<>(body,headers);
                ResponseEntity<String> resUpdate = restTemplate.exchange(DataFromSOMSConstant.URL_SOMS_API + "API/UpdatePODFlagManifesto", HttpMethod.PUT, entityUpdate, String.class);
                if(resUpdate.getStatusCode() != HttpStatus.OK)
                {
                    JsonNode treeUp = mapper.readTree(res.getBody());
                    JsonNode messageUp = treeUp.path("Message");
                    if(!message.isMissingNode())
                    {
                        throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                                "Error update POD flag " + messageUp.asText(), res.getBody()
                        );
                    }
                }
                GZResponsePage resAddItem = importManifestoItem();
                if(resAddItem.getCode() != HttpStatus.OK)
                {
                        throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                                "Error Add Item " + resAddItem.getMessage(), resAddItem.getDeveloperMessage()
                        );
                }
                manifestolistRepository.updatePODFlag();
                response.setTitle(DataFromSOMSConstant.TRANSFER_DATA);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(DataFromSOMSConstant.TRANSFER_SUCCESS);

            }
            else
            {
                throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), res.getBody()
                );
            }

        } catch (Exception ex) {
            throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
            );
        }
        return response;
    }

    public void updatePODNumber(Manifestolist manifestolist, String DeliveryDate) {
        if(manifestolist.getShipToCode() != null && !manifestolist.getShipToCode().equalsIgnoreCase(""))
        {
            List<Manifestolist> exitingPODNumber =  manifestolistRepository.getExitingPODNumber(manifestolist.getDriverId(),manifestolist.getDeliveryDate(),manifestolist.getShipToCode());
            if(exitingPODNumber != null && !exitingPODNumber.isEmpty())
            {
                Manifestolist maniHasPodNum = exitingPODNumber.stream()
                        .filter(podNumberStatus -> podNumberStatus.getPodNumber() != null)
                        .findFirst()
                        .orElse(null);
                if(maniHasPodNum != null && maniHasPodNum.getPodNumber() != null) /// maniHasPodNum.getPodNumber() > 0 (Old logic for interger)
                {
                    manifestolist.setPodNumber(maniHasPodNum.getPodNumber());
                }
                else
                {
                    manifestolist.setPodNumber(getLastedPODNumber(DeliveryDate));
                }
            }
            else
            {
                manifestolist.setPodNumber(getLastedPODNumber(DeliveryDate));
            }
        }
        else
        {
            manifestolist.setPodNumber(getLastedPODNumber(DeliveryDate));
        }
        manifestolistRepository.updatePODNumberById(manifestolist.getId(),manifestolist.getPodNumber());
    }

    public void UpdatePODNumber(String DeliveryDate, List<Integer> manifestoIdList) {
        List<Manifestolist> manifestoList =  manifestolistRepository.getManifestolistByIdListPODNumberNull(DeliveryDate, manifestoIdList);
        for (Manifestolist mani : manifestoList)
        {
            updatePODNumber(mani, DeliveryDate);
        }
    }

    public void UpdatePODNumber(String DeliveryDate) {
        List<Manifestolist> manifestoList =  manifestolistRepository.getManifestolistPODNumberNull(DeliveryDate);
        for (Manifestolist mani : manifestoList)
        {
            updatePODNumber(mani, DeliveryDate);
        }
    }
    
    public void UpdatePODNumber(String DeliveryDate, int jobStatusId) {
        List<Manifestolist> manifestoList =  manifestolistRepository.getManifestolistPODNumberNull(DeliveryDate);
        for (Manifestolist mani : manifestoList)
        {
            updatePODNumber(mani, DeliveryDate);
            JobStatus jobStatus = new JobStatus(mani.getId(), jobStatusId, null, mani.getDriverId(), new java.sql.Timestamp(new java.util.Date().getTime()));
            jobStatusRepository.save(jobStatus);
        }
    }

    public String getLastedPODNumber(String DeliveryDate)
    {
        String result;
        String dateRel = DeliveryDate.replace("-","");
        String strMonth = dateRel.substring(4,6);
        String strYear = dateRel.substring(0,4);
        int month = parseInt(strMonth);
        int year = parseInt(strYear);
        String getMax = manifestolistRepository.getMaxPODNumber(month,year);

        if(getMax != null)
        {
            String[] getMaxSplit = getMax.split("-");
            int cntNum = parseInt(getMaxSplit[getMaxSplit.length - 1]) + 1;
            result = getMaxSplit[0] + "-" + String.format("%04d",cntNum);
        }
        else
        {
            String dateReplace = DeliveryDate.replace("-","");
            result = dateReplace.substring(4, 6) + dateReplace.substring(0, 4) + "-" +"0001";
        }

        return (result);
    }

    public GZResponsePage importManifestoItem() throws GZException {
        GZResponsePage response = new GZResponsePage();
        int getItem = DataFromSOMSConstant.NUM_GET_ITEM;
        int startItem = 0;
        int endItem = 0;

        try {
            List<Manifestolist> manifestolistNotGetItem = manifestolistRepository.findManifestolistNotGetItem();
            int totalItem = manifestolistNotGetItem.size();
            if(!manifestolistNotGetItem.isEmpty()) {
                List<Manifestolist> manifestolist;
                int roundGet = 1;
                while (startItem < totalItem) {
                    if (getItem > 0) {
                        endItem = endItem + getItem;
                        if (endItem > totalItem) {
                            manifestolist = manifestolistNotGetItem.subList(startItem, totalItem);
                        } else {
                            manifestolist = manifestolistNotGetItem.subList(startItem, endItem);
                        }
                        startItem = startItem + getItem;
                    } else {
                        manifestolist = manifestolistNotGetItem;
                        startItem = startItem + totalItem;
                    }
                    if (!manifestolist.isEmpty()) {
                        List<String> jobIdList = manifestolist.stream().map(Manifestolist::getJobId).collect(Collectors.toList());

                        String[] payloads = new String[]{};

                        final String secretKey = commonService.generateSignature(payloads);

                        HttpHeaders headers = new HttpHeaders();
                        headers.set("Content-Type", "application/json");
                        headers.set("secretKey", secretKey);
                        Map body = new HashMap();
                        HttpEntity<?> entity = new HttpEntity<>(body, headers);
                        ResponseEntity<String> res = restTemplate.exchange(DataFromSOMSConstant.URL_SOMS_API + "API/GetSomsManifestoItemBySo?SO=" + String.join(",", jobIdList), HttpMethod.GET, entity, String.class);
                        if (res.getStatusCode() == HttpStatus.OK) {
                            ObjectMapper mapper = new ObjectMapper();
                            JsonNode tree = mapper.readTree(res.getBody());
                            if (tree.isArray()) {
                                for (JsonNode node : tree) {
                                    JsonNode itemNode = node.path("SomsManifestoItem");
                                    JsonNode SO = node.path("SO");
                                    if (itemNode.isArray() && (!SO.isMissingNode())) {
                                        for (JsonNode it : itemNode) {
                                            String so = it.path("SO").asText();
                                            String soItem = it.path("SOItem").asText();
                                            String matDoc = it.path("MatDoc").asText();
                                            String docDate = it.path("DocDate").asText();
                                            String postDate = it.path("PostDate").asText();
                                            String productCode = it.path("ProductCode").asText();
                                            String materialCode = it.path("MaterialCode").asText();
                                            String uom = it.path("UOM").asText();
                                            String productName = it.path("ProductName").asText();
                                            Integer quantity = it.path("Quantity").asInt(0);
                                            BigDecimal amount = new BigDecimal(it.path("Amount").asText());

                                            ManifestoItem manifestoItem = new ManifestoItem();
                                            manifestoItem.setSoNumber(so);
                                            manifestoItem.setSoItem(soItem);
                                            manifestoItem.setMatDoc(matDoc);
                                            manifestoItem.setDocDate(docDate);
                                            manifestoItem.setPostDate(postDate);
                                            manifestoItem.setProductCode(productCode);
                                            manifestoItem.setMaterialCode(materialCode);
                                            manifestoItem.setUom(uom);
                                            manifestoItem.setProductName(productName);
                                            manifestoItem.setQuantity(quantity);
                                            manifestoItem.setNewQuantity(quantity);
                                            manifestoItem.setAmount(amount);

                                            manifestoItem.setRoundGet(roundGet);
                                            manifestoItemRepository.save(manifestoItem);

                                            Manifestolist mani = manifestolist.stream()
                                                    .filter(x -> x.getJobId().equals(SO.asText()))
                                                    .findFirst().orElse(null);
                                            if (mani != null) {
                                                manifestoItem.setManifestoId(mani.getId());
                                                manifestoItemRepository.save(manifestoItem);
                                            }
                                        }
                                    }
                                }

                                roundGet++;
                            }
                        } else {
                            ObjectMapper mapper = new ObjectMapper();
                            JsonNode tree = mapper.readTree(res.getBody());
                            JsonNode message = tree.path("Message");
                            throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                                    HttpStatus.BAD_REQUEST.toString(), message.toString()
                            );
                        }
                    }
                }
                for (Manifestolist mani : manifestolistNotGetItem) {
                    mani.setIsGetItem(1);
                    manifestolistRepository.save(mani);
                }
            }
            response.setTitle(DataFromSOMSConstant.TRANSFER_DATA);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(DataFromSOMSConstant.TRANSFER_SUCCESS);

        } catch (Exception ex) {
            throw new GZException(DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
            );
        }
        return response;
    }

    public void callAPIPostStampStatusOrder(JobStatusToSOMSRequest jobStatusToSOMSRequest) throws Exception {
        try {
            String[] payloads = new String[]{jobStatusToSOMSRequest.getRefId(),jobStatusToSOMSRequest.getRefModuleType() };
            final String secretKey = commonService.generateSignature(payloads);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type","application/json");
            headers.set("secretKey",secretKey);
            Map body = new HashMap();
            body.put("RefId", jobStatusToSOMSRequest.getRefId());
            body.put("RefModuleType", jobStatusToSOMSRequest.getRefModuleType());
            body.put("Remark", jobStatusToSOMSRequest.getRemark());
            body.put("StatusId", jobStatusToSOMSRequest.getStatusId());
            body.put("CreatedDate", jobStatusToSOMSRequest.getCreatedDate());
            body.put("DriverId", jobStatusToSOMSRequest.getDriverId());
            body.put("PicId", jobStatusToSOMSRequest.getPicIds());
            HttpEntity<?> entity = new HttpEntity<>(body,headers);

            ResponseEntity<String> res = restTemplate.exchange(DataFromSOMSConstant.URL_SOMS_API + "API/UpdateManifestoTracking",
                    HttpMethod.POST,
                    entity,
                    String.class);
            if(res.getStatusCode() != HttpStatus.OK)
            {
                throw new GZException( DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), res.getBody() );
            }
        } catch (Exception ex) {
            throw new GZException( DataFromSOMSConstant.TRANSFER_DATA, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), ex.getMessage() );
        }

    }


}
