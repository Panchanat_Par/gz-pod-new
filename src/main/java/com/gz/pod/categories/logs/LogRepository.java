package com.gz.pod.categories.logs;

import com.gz.pod.entities.SOMSManifestoTracking;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface LogRepository extends CrudRepository<SOMSManifestoTracking, Integer> {
    @Query(value = "select s from SOMSManifestoTracking s where s.docType = ?1 and s.docRef = ?2 and s.createBy = ?3 " +
            "and s.statusId = ?4")
    SOMSManifestoTracking checkLog(String docType, String docRef, int createBy, int statusId);

    @Query(value = "select s from SOMSManifestoTracking s where s.docType = ?1 and s.docRef = ?2 and s.createBy = ?3 " +
            "and s.statusId = ?4 and s.saleOrderItem = ?5")
    SOMSManifestoTracking checkLogPerItem(String docType, String docRef, int createBy, int statusId, int itemId);

    @Transactional
    @Modifying
    @Query(value = "update SOMSManifestoTracking set updateBy = ?1, updateDate = ?2  where id = ?3")
    void updateLog(int updateBy, String updateDate, int id);
}