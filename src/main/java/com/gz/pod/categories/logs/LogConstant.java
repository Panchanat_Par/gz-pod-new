package com.gz.pod.categories.logs;

public class LogConstant {

    public static final String ADD_NEW_LOG = "add new log";
    public static final String ADD_LOG_SUCCESS = "add new log success!";
    public static final String ADD_LOG_ERROR = "can not add new log!";
    public static final String UPDATE_LOG = "update log";
    public static final String UPDATE_LOG_SUCCESS = "update log success!";
    public static final String UPDATE_LOG_ERROR = "can not update log!";
    public static final String LOGS = "LOGS";

    private LogConstant(){}
}
