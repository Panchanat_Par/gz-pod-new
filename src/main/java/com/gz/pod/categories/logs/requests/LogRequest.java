package com.gz.pod.categories.logs.requests;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class LogRequest {

    @NotNull(message = "docType can't be null")
    private String docType;

    @NotNull(message = "docRef can't be null")
    private String docRef;

    @Min(value = 1, message = "actor id must be greater than 1")
    @NotNull(message = "actor id can't be null")
    private int createBy;

    @Min(value = 1, message = "status id must be greater than 1")
    @NotNull(message = "status id can't be null")
    private int statusId;

    @Pattern(regexp = "[0-9]+", message = "saleOrderItemID must be positive number")
    private int saleOrderItemID = 0;

    public LogRequest(){}

    public LogRequest(String docType, String docRef, int createBy, int statusId) {
        this.docType = docType;
        this.docRef = docRef;
        this.createBy = createBy;
        this.statusId = statusId;
    }

    public LogRequest(String docType, String docRef, int createBy, int statusId, int saleOrderItemID) {
        this.docType = docType;
        this.docRef = docRef;
        this.createBy = createBy;
        this.statusId = statusId;
        this.saleOrderItemID = saleOrderItemID;
    }

}