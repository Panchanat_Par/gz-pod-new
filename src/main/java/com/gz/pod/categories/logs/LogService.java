package com.gz.pod.categories.logs;

import com.gz.pod.categories.logs.requests.LogRequest;
import com.gz.pod.entities.SOMSManifestoTracking;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class LogService {

    @Autowired
    LogRepository logRepository;

    public LogService(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    public GZResponse setLog(LogRequest request) throws GZException {
        GZResponse response = new GZResponse();

        //set log per item
        if (request.getSaleOrderItemID() != 0) {
            try {
                SOMSManifestoTracking checkLogPerItem = logRepository.checkLogPerItem(
                        request.getDocType(), request.getDocRef(), request.getCreateBy(),
                        request.getStatusId(), request.getSaleOrderItemID());
                response = setData(checkLogPerItem, request);

            } catch (Exception e) {
                throw new GZException(LogConstant.LOGS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), e.getMessage()
                );
            }
        }

        //Set log per SO
        if (request.getSaleOrderItemID() == 0) {
            try {
                SOMSManifestoTracking checkLog = logRepository.checkLog(
                        request.getDocType(), request.getDocRef(), request.getCreateBy(), request.getStatusId());
                response = setData(checkLog, request);

            } catch (Exception e) {
                throw new GZException(LogConstant.LOGS, HttpStatus.BAD_REQUEST,
                        HttpStatus.BAD_REQUEST.toString(), e.getMessage()
                );
            }
        }
        return response;
    }

    public GZResponse setLogs(SOMSManifestoTracking request, String title, String devMessage) throws GZException {
        GZResponse response = new GZResponse();
        try {
            logRepository.save(request);
            response.setData(request);
            response.setTitle(title);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setDeveloperMessage(devMessage);
        } catch (Exception e) {
            throw new GZException(LogConstant.ADD_NEW_LOG, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
            );
        }
        return response;
    }

    public GZResponse updateLogs(int updateBy, String updateDate, SOMSManifestoTracking checkLog,
                                 String title, String devMessage) throws GZException {
        GZResponse response = new GZResponse();
        try {
            logRepository.updateLog(updateBy, updateDate, checkLog.getId());
            SOMSManifestoTracking data = checkLog;
            data.setUpdateBy(updateBy);
            data.setUpdateDate(updateDate);
            response.setData(data);
            response.setCode(HttpStatus.OK);
            response.setMessage(HttpStatus.OK.toString());
            response.setTitle(title);
            response.setDeveloperMessage(devMessage);
        } catch (Exception e) {
            throw new GZException(LogConstant.UPDATE_LOG, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), e.getMessage()
            );
        }
        return response;
    }

    public GZResponse setData(SOMSManifestoTracking checkLog, LogRequest request) throws GZException {
        GZResponse response = new GZResponse();
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        String dateStr = date.toString();
        if (checkLog == null) {
            SOMSManifestoTracking somsManifestoTracking = new SOMSManifestoTracking(
                    request.getDocType(), request.getDocRef(),
                    request.getCreateBy(), dateStr,
                    request.getStatusId(), request.getSaleOrderItemID()
            );
            response = setLogs(somsManifestoTracking, LogConstant.ADD_NEW_LOG, LogConstant.ADD_LOG_SUCCESS);
        }
        if (checkLog != null) {
            response = updateLogs(request.getCreateBy(), dateStr, checkLog,
                    LogConstant.UPDATE_LOG, LogConstant.UPDATE_LOG_SUCCESS);
        }
        return response;
    }

}
