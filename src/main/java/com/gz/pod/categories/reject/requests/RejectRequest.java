package com.gz.pod.categories.reject.requests;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RejectRequest {
    @NotNull(message = "Type reject can't not be null")
    @ApiModelProperty(required = true)
    @ApiParam(value = "ประเภท reject", defaultValue = "1")
    private Integer typeReject;
}
