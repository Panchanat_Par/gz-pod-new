package com.gz.pod.categories.reject;

public class RejectConstant {
    public static final String REJECT_LIST = "Reject list!";
    public static final String GET_REJECT_SUCCESS = "Get reason reject list success!";
    public static final String GET_REJECT_ERROR = "Get reason reject list is not found , Type reason reject not correctly!";

    private RejectConstant() {

    }
}
