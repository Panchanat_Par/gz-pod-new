package com.gz.pod.categories.reject;

import com.gz.pod.categories.reject.Respones.RejectResponse;
import com.gz.pod.entities.Reject;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import com.gz.pod.categories.reject.requests.RejectRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RejectService {

    @Autowired
    RejectRepository rejectRepository;

    public GZResponse getRejectList() throws GZException {
        GZResponse response = new GZResponse();
        try {
            List<Reject> rejects = (List<Reject>) rejectRepository.findAll();

            if(rejects == null || rejects.isEmpty()){
                response.setTitle(RejectConstant.REJECT_LIST);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(RejectConstant.GET_REJECT_ERROR);
            }
            else
            {
                List<RejectResponse> rejectsResponse = rejects.stream().map(
                        rj -> new RejectResponse(
                                rj.getId(),
                                rj.getDescription()
                        )
                ).collect(Collectors.toList());

                response.setTitle(RejectConstant.REJECT_LIST);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(RejectConstant.GET_REJECT_SUCCESS);
                response.setData(rejectsResponse);
            }
        } catch (Exception ex) {
            throw new GZException(
                    RejectConstant.REJECT_LIST,
                    HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(),
                    RejectConstant.GET_REJECT_ERROR
            );
        }
        return response;
    }

    public GZResponse getRejectListByType(RejectRequest rejectRequest) throws GZException {
        GZResponse response = new GZResponse();
        try {
            List<Reject> rejects = rejectRepository.getRejectListByTypeReject(rejectRequest.getTypeReject());

            if(rejects == null || rejects.isEmpty()){
                response.setTitle(RejectConstant.REJECT_LIST);
                response.setCode(HttpStatus.BAD_REQUEST);
                response.setMessage(HttpStatus.BAD_REQUEST.toString());
                response.setDeveloperMessage(RejectConstant.GET_REJECT_ERROR);
            }
            else
            {
                List<RejectResponse> rejectsResponse = rejects.stream().map(
                        rj -> new RejectResponse(
                                rj.getId(),
                                rj.getDescription()
                        )
                ).collect(Collectors.toList());

                response.setTitle(RejectConstant.REJECT_LIST);
                response.setCode(HttpStatus.OK);
                response.setMessage(HttpStatus.OK.toString());
                response.setDeveloperMessage(RejectConstant.GET_REJECT_SUCCESS);
                response.setData(rejectsResponse);
            }
        } catch (Exception ex) {
            throw new GZException(
                    RejectConstant.REJECT_LIST,
                    HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(),
                    ex.getMessage()
            );
        }
        return response;
    }

}
