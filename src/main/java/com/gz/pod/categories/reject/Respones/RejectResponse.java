package com.gz.pod.categories.reject.Respones;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RejectResponse {
    private int id;
    private String description;

    public RejectResponse(int id, String description)
    {
        this.id = id;
        this.description = description;
    }

    public RejectResponse()
    {
    }
}
