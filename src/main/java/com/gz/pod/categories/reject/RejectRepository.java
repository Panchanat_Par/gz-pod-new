package com.gz.pod.categories.reject;

import com.gz.pod.entities.Reject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RejectRepository extends CrudRepository<Reject, Integer> {

    @Query(value = "select r from Reject r " +
            "where r.typeReject = ?1")
    List<Reject> getRejectListByTypeReject(int typeReject);

    @Query(value = "select r from Reject r " +
            "where r.id = ?1")
    Reject getRejectById(int id);
}
