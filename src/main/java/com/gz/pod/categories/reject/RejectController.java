package com.gz.pod.categories.reject;

import com.gz.pod.entities.Reject;
import com.gz.pod.response.GZResponse;
import com.gz.pod.categories.reject.requests.RejectRequest;
import com.starter.api.annotation.TokenAuthentication;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("getRejectReason/")
@Api(value = "Reject", description = "Reject management", produces = "application/json", tags = {"Reject"})
public class RejectController {
    @Autowired
    RejectService rejectService;

    @GetMapping(value = "rejectList")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = Reject.class)})
    public ResponseEntity<GZResponse> getRejectList() throws Exception {
        return ResponseEntity.ok(rejectService.getRejectList());
    }

    @GetMapping(value = "rejectListByType")
    @TokenAuthentication
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = Reject.class)})
    public ResponseEntity<GZResponse> getRejectListByType(@Valid RejectRequest rejectRequest) throws Exception {
        return ResponseEntity.ok(rejectService.getRejectListByType(rejectRequest));
    }
}
