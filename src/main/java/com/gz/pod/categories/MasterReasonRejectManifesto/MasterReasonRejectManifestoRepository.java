package com.gz.pod.categories.MasterReasonRejectManifesto;

import com.gz.pod.entities.MasterReasonRejectManifesto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterReasonRejectManifestoRepository extends CrudRepository<MasterReasonRejectManifesto, Integer> {

//    @Query("SELECT r.description FROM MasterReasonRejectManifesto r " +
//            "WHERE id =: id")
//    String findDescToReject(@Param("id") Integer id);
}
