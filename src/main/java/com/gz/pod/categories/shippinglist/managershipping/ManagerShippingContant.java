//package com.gz.pod.categories.shippinglist.managershipping;
//
//public class ManagerShippingContant {
//    public static final String SHIPPING_LIST = "Shipping List";
//    public static final String GET_LIST_SUCCESS = "get shipping list success!";
//    public static final String GET_LIST_ERROR = "shipping list not found, please check your data!";
//    public static final String CONFIRM_SHIPPING_MANAGER = "Confirm shipping by manager";
//    public static final String CONFIRM_SHIPPING_SUCCESS = "Confirm shipping success!";
//    public static final String REMARK_IS_EMPTY = "remark can't empty, please set remark when reject.";
//    public static final int MANIFESTO_ID = 9;
//    public static final int IS_CONFIRM_BY_MANAGER = 1;
//    public static final int IS_REJECT_BY_MANAGER = 1;
//    public static final String REMARK_CONFIRM = null;
//    public static final String REMARK_REJECT = "products destroy!";
//    public static final int REASON_CONFIRM = 0;
//    public static final int REASON_REJECT = 5;
//
//    ManagerShippingContant() {
//
//    }
//}
