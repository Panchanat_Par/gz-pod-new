//package com.gz.pod.categories.shippinglist.requests;
//
//import io.swagger.annotations.ApiModelProperty;
//import io.swagger.annotations.ApiParam;
//import lombok.Data;
//
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Pattern;
//
//@Data
//public class ShippinglistRequest {
//
//    @ApiModelProperty(required = true)
//    @Min(value = 1, message = "page must be greater than 1")
//    @ApiParam(value = "หมายเลขหน้าที่ต้องการดู", defaultValue = "1")
//    private Integer page = 1;
//
//    @ApiModelProperty(required = true)
//    @NotNull(message = "driver id can't be null")
//    @ApiParam(value = "ไอดีของพนักงานขับรถ", defaultValue = "1")
//    private Integer driverId;
//
//    @ApiParam(value = "วันที่ที่ต้องการดู", defaultValue = "2018-07-26")
//    @NotNull(message = "date can't be null")
//    private String date;
//
//    @Min(value = 1, message = "perPage must be greater than 1")
//    @ApiParam(value = "จำนวนของที่ต้องการดูในแต่ละหน้า", defaultValue = "10")
//    private Integer perPage = 10;
//
//    @Pattern(regexp = "^[0-9]{1,2} *$", message = "status SO must be 0-9 and between 1 and 2 digits")
//    @ApiParam(value = "สถานะของ so", defaultValue = "3")
//    private String statusSO;
//}
