//package com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist;
//
//import com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist.requests.GetHistoryConfirmShippinglistRequest;
//import com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist.responses.GetHistoryConfirmShippinglistResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/shippinglist")
//@Api(value = "Get confirm shipping history", description = "History confirm shipping management"
//        , produces = "application/json", tags = {"History Confirm Shipping"})
//public class GetHistoryConfirmShippinglistController {
//
//    @Autowired
//    GetHistoryConfirmShippinglistService getHistoryConfirmShippinglistService;
//
//    @GetMapping("/historyconfirmshippinglist")
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = GetHistoryConfirmShippinglistResponse.class)})
//    public ResponseEntity getShippedlist(@Valid GetHistoryConfirmShippinglistRequest request) throws Exception {
//        return ResponseEntity.ok(getHistoryConfirmShippinglistService.getHistoryShippinglist(request));
//    }
//}
