//package com.gz.pod.categories.shippinglist.managershipping;
////
////import com.gz.pod.categories.logs.LogService;
////import com.gz.pod.categories.logs.requests.LogRequest;
////import com.gz.pod.categories.reject.RejectRepository;
////import com.gz.pod.categories.shippedlist.managershipped.ManagerShippedContant;
////import com.gz.pod.categories.shippinglist.managershipping.model.ListShipping;
////import com.gz.pod.categories.shippinglist.managershipping.requests.ManagerConfirmShippingRequest;
////import com.gz.pod.categories.shippinglist.managershipping.requests.ManagerShippingRequest;
////import com.gz.pod.categories.shippinglist.managershipping.responses.ManagerShippingResponse;
////import com.gz.pod.constants.LogConstants;
////import com.gz.pod.entities.ConfirmShippingList;
////import com.gz.pod.entities.Manifesto;
////import com.gz.pod.exceptions.GZException;
////import com.gz.pod.response.GZResponse;
////import com.gz.pod.response.GZResponsePage;
////import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.beans.support.PagedListHolder;
////import org.springframework.http.HttpStatus;
////import org.springframework.stereotype.Service;
////
////import java.text.DecimalFormat;
////import java.util.ArrayList;
////import java.util.List;
////
////@Service
//public class ManagerShippingService {
////
////    @Autowired
////    ManagerShippingRepository managerShippingRepository;
////
////    @Autowired
////    RejectRepository rejectRepository;
////
////    @Autowired
////    ManifestoRepository manifestoRepository;
////
////    @Autowired
////    LogService logService;
////
////    public ManagerShippingService(ManagerShippingRepository managerShippingRepository) {
////        this.managerShippingRepository = managerShippingRepository;
////    }
////
////    private String remark;
////    private int reason;
////    private int hasRemarkWhenReject = 0;
////
////    public GZResponsePage getShippingList(ManagerShippingRequest request) throws GZException {
////        GZResponsePage response = new GZResponsePage();
////        if (request.getDay() == null) {
////            request.setDay(java.time.LocalDate.now().toString());
////        }
////        try {
////            List<Manifesto> list = manifestoRepository
////                    .getShippinglistByDriverIdDateShpippingStatus(
////                            request.getDriverId(),
////                            request.getDay() + "T00:00:00.000"
////                    );
////            List<ManagerShippingResponse> mapConfirmArray = new ArrayList<>();
////            for (int i = 0; i < list.size(); i++) {
////                List<ConfirmShippingList> confirmList = managerShippingRepository
////                        .getConfirmListByDriverID(
////                                request.getDriverId(),
////                                list.get(i).getId()
////                        );
////                ManagerShippingResponse mapConfirmToManifesto = new ManagerShippingResponse();
////                mapConfirmToManifesto.setDriverId(list.get(i).getDriverId());
////                mapConfirmToManifesto.setManifestoId(list.get(i).getId());
////                mapConfirmToManifesto.setDONumber(list.get(i).getDONumber());
////                mapConfirmToManifesto.setManifestoType(list.get(i).getManifestoType());
////                mapConfirmToManifesto.setCustomerName(list.get(i).getCustomerName());
////                mapConfirmToManifesto.setAddressType(list.get(i).getAddressType());
////                mapConfirmToManifesto.setBoxQty(list.get(i).getBoxQty());
////                mapConfirmToManifesto.setItemQty(list.get(i).getItemQty());
////                mapConfirmToManifesto.setSKUQty(list.get(i).getSKUQty());
////                double vat = Double.parseDouble(String.valueOf(list.get(i).getOrderTotalInVat()));
////                DecimalFormat df = new DecimalFormat("#,###.00");
////                mapConfirmToManifesto.setOrderTotalInVat(df.format(vat));
////                mapConfirmToManifesto.setRemark(list.get(i).getRemark());
////                mapConfirmToManifesto.setRouteMaster(list.get(i).getMasterRoute().getRouteMaster());
////                mapConfirmToManifesto.setRouteName(list.get(i).getMasterRoute().getRouteName());
////                mapConfirmToManifesto.setBillNo(list.get(i).getManifestoCstm().getBillNo());
////                mapConfirmToManifesto.setSONo(list.get(i).getManifestoCstm().getSONo());
////                mapConfirmToManifesto.setDocumentNumber(list.get(i).getDocumentNumber());
////                mapConfirmToManifesto.setShippingStatus(list.get(i).getShippingStatus());
////                if (confirmList.isEmpty()) {
////                    mapConfirmToManifesto.setIsConfirm(null);
////                    mapConfirmToManifesto.setRemarkReject(null);
////                    mapConfirmToManifesto.setReasonId(null);
////                    mapConfirmToManifesto.setReasonString(null);
////                    mapConfirmToManifesto.setActionBy(null);
////                    mapConfirmToManifesto.setIsConfirmByManager(null);
////                    mapConfirmToManifesto.setRemarkByManager(null);
////                    mapConfirmToManifesto.setReasonIdByManager(null);
////                } else {
////                    mapConfirmToManifesto.setIsConfirm(confirmList.get(0).getIsConfirm());
////                    mapConfirmToManifesto.setRemarkReject(confirmList.get(0).getRemark());
////                    mapConfirmToManifesto.setReasonId(confirmList.get(0).getReasonId());
////                    if (confirmList.get(0).getReasonId() != 0) {
////                        String reasonString = rejectRepository.getRejectById(confirmList.get(0).getReasonId())
////                                .getDescription();
////                        mapConfirmToManifesto.setReasonString(reasonString);
////                    } else {
////                        mapConfirmToManifesto.setReasonString(null);
////                    }
////                    mapConfirmToManifesto.setActionBy(confirmList.get(0).getActionBy());
////                    mapConfirmToManifesto.setIsConfirmByManager(confirmList.get(0).getIsConfirmByManager());
////                    mapConfirmToManifesto.setRemarkByManager(confirmList.get(0).getRemarkByManager());
////                    mapConfirmToManifesto.setReasonIdByManager(confirmList.get(0).getReasonIdByManager());
////                }
////                mapConfirmArray.add(mapConfirmToManifesto);
////
////            }
////
////            PagedListHolder<ManagerShippingResponse> setPage = new PagedListHolder<>(mapConfirmArray);
////            if (!mapConfirmArray.isEmpty()) {
////                setPage.setPage(request.getPage() - 1);
////                setPage.setPageSize(request.getPerPage());
////                response.setPageSize(request.getPerPage());
////                response.setTitle(ManagerShippedContant.SHIPPED_LIST);
////                response.setCode(HttpStatus.OK);
////                response.setMessage(HttpStatus.OK.toString());
////                response.setDeveloperMessage(ManagerShippedContant.GET_LIST_SUCCESS);
////                response.setCurrentPage(request.getPage());
////                response.setTotalPage(setPage.getPageCount());
////                response.setPageSize(setPage.getPageSize());
////                response.setTotalItems(setPage.getNrOfElements());
////                response.setData(setPage.getPageList());
////            } else {
////                throw new GZException(ManagerShippingContant.SHIPPING_LIST, HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(), ManagerShippedContant.GET_LIST_ERROR
////                );
////            }
////        } catch (Exception ex) {
////            throw new GZException(ManagerShippingContant.SHIPPING_LIST, HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
////            );
////        }
////        return response;
////    }
////
////    public GZResponse confirmShippingByManager(ManagerConfirmShippingRequest managerConfirmRequest) throws GZException {
////        GZResponse gzResponse = new GZResponse();
////        List<ListShipping> listShippings = managerConfirmRequest.getListShipping();
////        List<ListShipping> listShippingsResponse = new ArrayList<>();
////        try {
////            for (int i = 0; i < listShippings.size(); i++) {
////                ListShipping response = new ListShipping();
////                LogRequest log = new LogRequest();
////                Manifesto manifestoData = manifestoRepository.getManifestoById(listShippings.get(i).getManifestoId());
////                log.setDocType(manifestoData.getManifestoType());
////                log.setDocRef(manifestoData.getJobId());
////                log.setCreateBy(0);  //id of checker
////                if (listShippings.get(i).getIsConfirmByManager() == 0) {
////                    if (listShippings.get(i).getRemarkByManager() != null) {
////                        remark = listShippings.get(i).getRemarkByManager();
////                        reason = listShippings.get(i).getReasonIdByManager();
////                    } else {
////                        hasRemarkWhenReject = 1;
////                        throw new GZException(ManagerShippingContant.CONFIRM_SHIPPING_MANAGER, HttpStatus.BAD_REQUEST,
////                                HttpStatus.BAD_REQUEST.toString(), ManagerShippingContant.REMARK_IS_EMPTY
////                        );
////                    }
////                    log.setStatusId(LogConstants.checker_cancel.getValue());
////
////                } else if (listShippings.get(i).getIsConfirmByManager() == 1) {
////                    remark = null;
////                    reason = ManagerShippingContant.REASON_CONFIRM;
////                    log.setStatusId(LogConstants.checker_confirm.getValue());
////                }
////                managerShippingRepository.confirmShippingByManager(listShippings.get(i).getManifestoId(),
////                        listShippings.get(i).getIsConfirmByManager(), remark, reason,
////                        new java.sql.Timestamp(new java.util.Date().getTime()));
////                gzResponse.setTitle(ManagerShippingContant.CONFIRM_SHIPPING_MANAGER);
////                response.setManifestoId(listShippings.get(i).getManifestoId());
////                response.setIsConfirmByManager(listShippings.get(i).getIsConfirmByManager());
////                response.setReasonIdByManager(listShippings.get(i).getReasonIdByManager());
////                response.setRemarkByManager(listShippings.get(i).getRemarkByManager());
////                listShippingsResponse.add(response);
////                logService.setLog(log);
////
////            }
////            gzResponse.setData(listShippingsResponse);
////            gzResponse.setCode(HttpStatus.OK);
////            gzResponse.setMessage(HttpStatus.OK.toString());
////            gzResponse.setDeveloperMessage(ManagerShippingContant.CONFIRM_SHIPPING_SUCCESS);
////        } catch (Exception ex) {
////            if (hasRemarkWhenReject == 1) {
////                throw new GZException(ManagerShippingContant.CONFIRM_SHIPPING_MANAGER, HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(), ManagerShippingContant.REMARK_IS_EMPTY
////                );
////            } else {
////                throw new GZException(ManagerShippingContant.CONFIRM_SHIPPING_MANAGER, HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
////                );
////            }
////        }
////        return gzResponse;
////    }
//}
