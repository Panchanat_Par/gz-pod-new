//package com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist;
//
//import com.gz.pod.entities.ConfirmShippingList;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface GetHistoryConfirmShippinglistRepository extends CrudRepository<ConfirmShippingList, Integer> {
//    @Query(value = "select c from ConfirmShippingList c where c.actionBy = ?1 and c.confirmDate between ?2 and ?3")
//    List<ConfirmShippingList> getHistoryConfirmShippinglist(int driverId, String startDate, String endDate);
//}
