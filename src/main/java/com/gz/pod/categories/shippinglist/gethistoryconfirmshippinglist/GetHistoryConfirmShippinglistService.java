//package com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist;
//
//import com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist.requests.GetHistoryConfirmShippinglistRequest;
//import com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist.responses.GetHistoryConfirmShippinglistResponse;
//import com.gz.pod.entities.ConfirmShippingList;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponsePage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.support.PagedListHolder;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Service
//public class GetHistoryConfirmShippinglistService {
//
//    @Autowired
//    GetHistoryConfirmShippinglistRepository getHistoryConfirmShippinglistRepository;
//
//    @Autowired
//    GetReasonDescriptionRepository getReasonDescriptionRepository;
//
//    public GZResponsePage getHistoryShippinglist(GetHistoryConfirmShippinglistRequest request) throws GZException {
//        GZResponsePage responsePage = new GZResponsePage();
//        if(request.getDate() == null){
//            request.setDate(java.time.LocalDate.now().toString());
//        }
//        try {
//            List<ConfirmShippingList> list = getHistoryConfirmShippinglistRepository
//                    .getHistoryConfirmShippinglist(request.getDriverId(),
//                            request.getDate() + "T00:00:00.000",
//                            request.getDate() + "T23:59:59.999");
//            List<GetHistoryConfirmShippinglistResponse> res = list.stream().map(
//                    history -> new GetHistoryConfirmShippinglistResponse(
//                            history.getManifestoId(),
//                            history.getIsConfirm(),
//                            history.getRemark(),
//                            history.getReasonId(),
//                            history.getActionBy(),
//                            history.getConfirmDate(),
//                            getReasonDescriptionRepository.findOne(history.getReasonId()).getDescription()
//                    )
//            ).collect(Collectors.toList());
//            PagedListHolder<GetHistoryConfirmShippinglistResponse> setPage = new PagedListHolder<>(res);
//            if (setPage.getNrOfElements() == 0) {
//                throw new GZException(GetHistoryConfirmShippinglistConstant.HISTORY_LIST, HttpStatus.BAD_REQUEST,
//                        HttpStatus.BAD_REQUEST.toString(), GetHistoryConfirmShippinglistConstant.GET_LIST_ERROR
//                );
//            } else {
//                setPage.setPage(request.getPage() - 1);
//                setPage.setPageSize(request.getPerPage());
//                responsePage.setTitle(GetHistoryConfirmShippinglistConstant.HISTORY_LIST);
//                responsePage.setCode(HttpStatus.OK);
//                responsePage.setMessage(HttpStatus.OK.toString());
//                responsePage.setDeveloperMessage(GetHistoryConfirmShippinglistConstant.GET_LIST_SUCCESS);
//                responsePage.setCurrentPage(request.getPage());
//                responsePage.setTotalPage(setPage.getPageCount());
//                responsePage.setPageSize(setPage.getPageSize());
//                responsePage.setTotalItems(setPage.getNrOfElements());
//                responsePage.setData(setPage.getPageList());
//            }
//
//        } catch (Exception ex){
//            throw new GZException(GetHistoryConfirmShippinglistConstant.HISTORY_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
//            );
//        }
//        return responsePage;
//
//    }
//}
