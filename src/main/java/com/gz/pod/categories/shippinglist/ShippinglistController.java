//package com.gz.pod.categories.shippinglist;
//
//import com.gz.pod.categories.shippinglist.requests.ShippinglistRequest;
//import com.gz.pod.categories.shippinglist.responses.ShippinglistResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/shippinglist")
//@Api(value = "Get shipping", description = "Shipping management", produces = "application/json", tags = {"Shipping"})
//public class ShippinglistController {
//    @Autowired
//    ShippinglistService shippinglistService;
//
//    @GetMapping
//    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ShippinglistResponse.class)})
//    public ResponseEntity getShippinglist(@Valid ShippinglistRequest shippinglistRequest) throws Exception {
//        return ResponseEntity.ok(shippinglistService.getShippingListByDriverId(shippinglistRequest));
//    }
//}
