//package com.gz.pod.categories.shippinglist.confirmshippinglist;
//
//
//import com.gz.pod.categories.logs.LogService;
//import com.gz.pod.categories.logs.requests.LogRequest;
//import com.gz.pod.categories.shippinglist.confirmshippinglist.model.RejectModel;
//import com.gz.pod.categories.shippinglist.confirmshippinglist.requests.ConfirmShippinglistRequest;
//import com.gz.pod.constants.LogConstants;
//import com.gz.pod.entities.ConfirmShippingList;
//import com.gz.pod.entities.Manifesto;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponsePage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class ConfirmShippinglistService {
//
////    @Autowired
////    ConfirmShippinglistRepositoty confirmShippinglistRepositoty;
////    @Autowired
////    LogService logService;
////
////    public GZResponsePage confirmRejectShippinglist(ConfirmShippinglistRequest request, int driverId) throws GZException {
////        List<ConfirmShippingList> confirmShippingLists = new ArrayList<>();
////        GZResponsePage response = new GZResponsePage();
////        try {
////            if (!request.getConfirm().isEmpty() || !request.getReject().isEmpty()) {
////                java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
////                if (!request.getConfirm().isEmpty()) {
////                    for (Integer manifestoId : request.getConfirm()) {
////                        Manifesto manifesto = manifestoRepository.getManifestoById(manifestoId);
////                        LogRequest logData = new LogRequest(
////                                manifesto.getManifestoType(),
////                                manifesto.getJobId(),
////                                driverId, LogConstants.request_confirm_SO.getValue());
////                        ConfirmShippingList confirmShippingList = new ConfirmShippingList();
////                        confirmShippingList.setManifestoId(manifestoId);
////                        confirmShippingList.setIsConfirm(ConfirmShippinglistConstant.STATUS_DRIVER_CONFIRM_OR_REJECT);
////                        confirmShippingList.setRemark(null);
////                        confirmShippingList.setReasonId(
////                                ConfirmShippinglistConstant.STATUS_DRIVER_NOT_CONFIRM_OR_REJECT);
////                        confirmShippingList.setActionBy(driverId);
////                        confirmShippingList.setConfirmDate(date.toString());
////                        confirmShippingLists.add(confirmShippinglistRepositoty.save(confirmShippingList));
////                        manifestoRepository.updateShippingStatus(
////                                manifestoId, ConfirmShippinglistConstant.STATUS_DRIVER_CONFIRM_OR_REJECT);
////                        logService.setLog(logData);
////                    }
////                }
////                if (!request.getReject().isEmpty()) {
////                    for (RejectModel rejectObject : request.getReject()) {
////                        Manifesto manifesto = manifestoRepository.getManifestoById(rejectObject.getId());
////                        LogRequest logData = new LogRequest(
////                                manifesto.getManifestoType(),
////                                manifesto.getJobId(),
////                                driverId, LogConstants.request_cancel_SO.getValue());
////                        ConfirmShippingList confirmShippingList = new ConfirmShippingList();
////                        confirmShippingList.setManifestoId(rejectObject.getId());
////                        confirmShippingList.setIsConfirm(
////                                ConfirmShippinglistConstant.STATUS_DRIVER_NOT_CONFIRM_OR_REJECT);
////                        confirmShippingList.setRemark(rejectObject.getRemark());
////                        confirmShippingList.setReasonId(rejectObject.getReason());
////                        confirmShippingList.setActionBy(driverId);
////                        confirmShippingList.setConfirmDate(date.toString());
////                        confirmShippingLists.add(confirmShippinglistRepositoty.save(confirmShippingList));
////                        manifestoRepository.updateShippingStatus(
////                                rejectObject.getId(), ConfirmShippinglistConstant.STATUS_DRIVER_CONFIRM_OR_REJECT);
////                        logService.setLog(logData);
////                    }
////                }
////                response.setTitle(ConfirmShippinglistConstant.CONFIRM_SHIPPING_LIST);
////                response.setCode(HttpStatus.OK);
////                response.setMessage(HttpStatus.OK.toString());
////                response.setDeveloperMessage(ConfirmShippinglistConstant.POST_CONFIRM_SUCCESS);
////                response.setData(confirmShippingLists);
////            } else {
////                throw new GZException(ConfirmShippinglistConstant.CONFIRM_SHIPPING_LIST, HttpStatus.BAD_REQUEST,
////                        HttpStatus.BAD_REQUEST.toString(), ConfirmShippinglistConstant.POST_CONFIRM_ERROR
////                );
////            }
////
////        } catch (Exception ex) {
////            throw new GZException(ConfirmShippinglistConstant.CONFIRM_SHIPPING_LIST, HttpStatus.BAD_REQUEST,
////                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
////            );
////        }
////
////        return response;
////    }
//}
