//package com.gz.pod.categories.shippinglist.responses;
//
//import lombok.Data;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//
//@Data
//public class ShippinglistResponse {
//
//    private Integer driverId;
//    private int manifestoId;
//    private String dONumber;
//    private String manifestoType;
//    private String jobId;
//    private String customerName;
//    private String shipToCode;
//    private String addressType;
//    private Integer boxQty;
//    private Integer itemQty;
//    private Integer sKUQty;
//    private String orderTotalInVat;
//    private String remark;
//    private String routeMaster;
//    private String routeName;
//    private String billNo;
//    private String sONo;
//    private String documentNumber;
//    private int shippingStatus;
//
//    @SuppressWarnings("squid:S00107")
//    public ShippinglistResponse(Integer driverId, int manifestoId, String dONumber, String manifestoType,
//                                String jobId, String customerName, String shipToCode, String addressType,
//                                Integer boxQty, Integer itemQty, Integer sKUQty, BigDecimal orderTotalInVat,
//                                String remark, String routeMaster, String routeName, String billNo, String sONo,
//                                String documentNumber, int shippingStatus) {
//        this.driverId = driverId;
//        this.manifestoId = manifestoId;
//        this.dONumber = dONumber;
//        this.manifestoType = manifestoType;
//        this.jobId = jobId;
//        this.customerName = customerName;
//        this.shipToCode = shipToCode;
//        this.addressType = addressType;
//        this.boxQty = boxQty;
//        this.itemQty = itemQty;
//        this.sKUQty = sKUQty;
//        double vat = Double.parseDouble(String.valueOf(orderTotalInVat));
//        DecimalFormat df = new DecimalFormat("#,###.00");
//        this.orderTotalInVat = df.format(vat);
//        this.remark = remark;
//        this.routeMaster = routeMaster;
//        this.routeName = routeName;
//        this.billNo = billNo;
//        this.sONo = sONo;
//        this.documentNumber = documentNumber;
//        this.shippingStatus = shippingStatus;
//    }
//
//    public ShippinglistResponse() {
//    }
//}
//
//
