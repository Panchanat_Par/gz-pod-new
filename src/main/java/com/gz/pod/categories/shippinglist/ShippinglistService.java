//package com.gz.pod.categories.shippinglist;
//
//import com.gz.pod.categories.shippinglist.requests.ShippinglistRequest;
//import com.gz.pod.categories.shippinglist.responses.ShipToCodeResponse;
//import com.gz.pod.categories.shippinglist.responses.ShippinglistResponse;
//import com.gz.pod.entities.Manifesto;
//import com.gz.pod.exceptions.GZException;
//import com.gz.pod.response.GZResponsePage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.support.PagedListHolder;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
//@Service
//public class ShippinglistService {
//
//    @Autowired
//    ShippinglistRepository shippinglistRepository;
//
//    public ShippinglistService(ShippinglistRepository shippinglistRepository) {
//        this.shippinglistRepository = shippinglistRepository;
//    }
//
//    public GZResponsePage getShippingListByDriverId(ShippinglistRequest request) throws GZException {
//        GZResponsePage response = new GZResponsePage();
//        try {
//            if (request.getDate() == null) {
//                request.setDate(java.time.LocalDate.now().toString());
//            }
//            List<Manifesto> list = shippinglistRepository.getShippingList(request.getDriverId(),
//                    request.getDate(), Integer.parseInt(request.getStatusSO()));
//
//            List<ShippinglistResponse> shl = list.stream().map(
//                    shipping -> new ShippinglistResponse(
//                            shipping.getDriverId(),
//                            shipping.getId(),
//                            shipping.getDONumber(),
//                            shipping.getManifestoType(),
//                            shipping.getJobId(),
//                            shipping.getCustomerName(),
//                            shipping.getShipToCode(),
//                            shipping.getShipToAddress(),
//                            shipping.getBoxQty(),
//                            shipping.getItemQty(),
//                            shipping.getSKUQty(),
//                            shipping.getOrderTotalInVat(),
//                            shipping.getRemark(),
//                            shipping.getMasterRoute().getRouteMaster(),
//                            shipping.getMasterRoute().getRouteName(),
//                            shipping.getManifestoCstm().getBillNo(),
//                            shipping.getManifestoCstm().getSONo(),
//                            shipping.getDocumentNumber(),
//                            shipping.getShippingStatus()
//                    )
//            ).collect(Collectors.toList());
//
//            List<ShipToCodeResponse> shipToCodeResponseList = convertListGroupByShipTOCode(shl);
//
//            PagedListHolder<ShipToCodeResponse> setPage = new PagedListHolder<>(shipToCodeResponseList);
//            if (setPage.getNrOfElements() == 0) {
//                throw new GZException(ShippingConstant.SHIPPING_LIST, HttpStatus.BAD_REQUEST,
//                        HttpStatus.BAD_REQUEST.toString(), ShippingConstant.GET_LIST_ERROR
//                );
//            } else {
//                setPage.setPage(request.getPage() - 1);
//                setPage.setPageSize(request.getPerPage());
//                response.setTitle(ShippingConstant.SHIPPING_LIST);
//                response.setCode(HttpStatus.OK);
//                response.setMessage(HttpStatus.OK.toString());
//                response.setDeveloperMessage(ShippingConstant.GET_LIST_SUCCESS);
//                response.setCurrentPage(request.getPage());
//                response.setTotalPage(setPage.getPageCount());
//                response.setPageSize(setPage.getPageSize());
//                response.setTotalItems(setPage.getNrOfElements());
//                response.setData(setPage.getPageList());
//            }
//        } catch (Exception ex) {
//            throw new GZException(ShippingConstant.SHIPPING_LIST, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage()
//            );
//        }
//        return response;
//    }
//
//    public List<ShipToCodeResponse> convertListGroupByShipTOCode(List<ShippinglistResponse> list) {
//        List<ShipToCodeResponse> snList = new ArrayList<>();
//        List<ShippinglistResponse> spList = new ArrayList<>();
//        String shipToCode = list.get(0).getShipToCode();
//
//        // We can use this logic when list order by shipToCode.
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i).getShipToCode().equals(shipToCode)) {
//                spList.add(list.get(i));
//                if (i == list.size() - 1) {
//                    ShipToCodeResponse sn = new ShipToCodeResponse();
//                    sn.setShipToCode(list.get(i).getShipToCode());
//                    sn.setStatus(list.get(i).getShippingStatus());
//                    sn.setData(spList);
//                    snList.add(sn);
//                }
//            } else { // If shipToCode not same old value.
//                shipToCode = list.get(i).getShipToCode();
//                i--; // Then we can get old value that duplicate by position - 1.
//                ShipToCodeResponse sn = new ShipToCodeResponse();
//                sn.setShipToCode(list.get(i).getShipToCode());
//                sn.setStatus(list.get(i).getShippingStatus());
//                sn.setData(spList);
//                snList.add(sn);
//                spList = new ArrayList<>(); // Clear shippingList to next shipToCode.
//            }
//        }
//        return snList;
//    }
//}