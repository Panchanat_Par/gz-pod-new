//package com.gz.pod.categories.shippinglist.managershipping;
//
//import com.gz.pod.categories.shippinglist.managershipping.requests.ManagerConfirmShippingRequest;
//import com.gz.pod.categories.shippinglist.managershipping.requests.ManagerShippingRequest;
//import com.gz.pod.categories.shippinglist.managershipping.responses.ManagerShippingResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@RestController
//@RequestMapping("api/v1/shippinglist/manager")
//@Api(value = "Manager Confirm shipping", description = "Manager Confirm shipping management", produces = "application/json", tags = {"Confirm Shipping/Manager"})
//public class ManagerShippingController {
////
////    @Autowired
////    ManagerShippingService managerShippingService;
////
////    @GetMapping
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ManagerShippingResponse.class)})
////    public ResponseEntity getShippinglist(@Valid ManagerShippingRequest request) throws Exception {
////        return ResponseEntity.ok(managerShippingService.getShippingList(request));
////    }
////
////    @PutMapping("/confirmShippingByManager")
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ManagerShippingResponse.class)})
////    public ResponseEntity confirmShippingByManager(@Valid @RequestBody ManagerConfirmShippingRequest request) throws Exception {
////        return ResponseEntity.ok(managerShippingService.confirmShippingByManager(request));
////    }
//
//}
