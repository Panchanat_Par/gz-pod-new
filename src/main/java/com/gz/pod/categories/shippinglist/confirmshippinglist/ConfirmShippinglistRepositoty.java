//package com.gz.pod.categories.shippinglist.confirmshippinglist;
//
//import com.gz.pod.entities.ConfirmShippingList;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ConfirmShippinglistRepositoty extends CrudRepository<ConfirmShippingList, Integer> {
//
//    @Query(value = " select c from ConfirmShippingList c " +
//            " where c.isConfirmByManager = ?1 " +
//            " and c.manifestoId in ?2 " +
//            " and c.isConfirm = ?3")
//    List<ConfirmShippingList> getManagerConfirmShipping(
//            int isConfirmByManager, List<Integer> manafestoId, int isConfirm);
//}
