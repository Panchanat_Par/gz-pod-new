//package com.gz.pod.categories.shippinglist.managershipping.model;
//
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//import javax.validation.constraints.Max;
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//
//@Data
//public class ListShipping {
//    @NotNull(message = "ManifestoId is require")
//    @ApiModelProperty(example = "9", required = true)
//    private Integer manifestoId;
//    @NotNull(message = "isConfirmByManager is require")
//    @Min(value = 0, message = "isConfirmByManager must be number 0 : reject or 1 : confirm")
//    @Max(value = 1, message = "isConfirmByManager must be number 0 : reject or 1 : confirm")
//    @ApiModelProperty(example = "0", allowableValues = "1,0", required = true)
//    private Integer isConfirmByManager;
//    @ApiModelProperty(example = "ของชำรุด")
//    private String remarkByManager;
//    @ApiModelProperty(example = "1")
//    private int reasonIdByManager = 0;
//
//    public ListShipping(@NotNull(message = "ManifestoId is require") Integer manifestoId,
//                        @NotNull(message = "isConfirmByManager is require")
//                        @Min(value = 0, message = "isConfirmByManager must be number 0 : reject or 1 : confirm")
//                        @Max(value = 1, message = "isConfirmByManager must be number 0 : reject or 1 : confirm")
//                                Integer isConfirmByManager,
//                        String remarkByManager,
//                        int reasonIdByManager) {
//        this.manifestoId = manifestoId;
//        this.isConfirmByManager = isConfirmByManager;
//        this.remarkByManager = remarkByManager;
//        this.reasonIdByManager = reasonIdByManager;
//    }
//
//    public ListShipping() {
//    }
//}
