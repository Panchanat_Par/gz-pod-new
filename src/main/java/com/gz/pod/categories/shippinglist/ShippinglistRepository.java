//package com.gz.pod.categories.shippinglist;
//
//import com.gz.pod.entities.Manifesto;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ShippinglistRepository extends CrudRepository<Manifesto, Integer> {
//    @Query(value = "select m from Manifesto m inner join m.masterRoute mr " +
//            "where m.driverId = ?1 and m.deliveryDate = ?2 and m.shippingStatus = ?3 " +
//            "order by m.shipToCode")
//    List<Manifesto> getShippingList(int driverId, String date, int statusSO);
//}