//package com.gz.pod.categories.shippinglist.confirmshippinglist;
//
//import com.gz.pod.categories.shippinglist.confirmshippinglist.requests.ConfirmShippinglistRequest;
//import com.gz.pod.entities.ConfirmShippingList;
//import com.gz.pod.response.GZResponsePage;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//
//@RestController
//@RequestMapping("api/v1/confirmshippinglist")
//@Api(value = "Driver Confirm shipping", description = "Driver Confirm shipping management"
//        , produces = "application/json", tags = {"Confirm Shipping/Driver"})
//public class ConfirmShippinglistController {
//
////    @Autowired
////    ConfirmShippinglistService confirmShippinglistService;
////
////    @PostMapping(value = "/confirm/{driverId}")
////    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = ConfirmShippingList.class)})
////    public ResponseEntity<GZResponsePage> confirmShipping(
////            @ApiParam(value = "ไอดีพนักงานขับรถ", defaultValue = "1") @PathVariable int driverId,
////            @RequestBody @Valid ConfirmShippinglistRequest request) throws Exception {
////        return ResponseEntity.ok(confirmShippinglistService.confirmRejectShippinglist(request, driverId));
////    }
//}
