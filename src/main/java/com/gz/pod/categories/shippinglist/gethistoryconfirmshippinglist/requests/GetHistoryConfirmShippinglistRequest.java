//package com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist.requests;
//
//import io.swagger.annotations.ApiModelProperty;
//import io.swagger.annotations.ApiParam;
//import lombok.Data;
//
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//
//@Data
//public class GetHistoryConfirmShippinglistRequest {
//
//    @NotNull(message = "Driver Id can not be null!")
//    @ApiModelProperty(example = "1", required = true)
//    @ApiParam(value = "รหัสพนักงานขับรถ", defaultValue = "1")
//    private int driverId;
//    @ApiModelProperty(example = "2018-07-12")
//    @ApiParam(value = "วันที่ที่ต้องการดู", defaultValue = "2018-07-12")
//    private String date;
//    @ApiModelProperty(example = "1")
//    @ApiParam(value = "หมายเลขหน้าที่ต้องการดู", defaultValue = "1")
//    @Min(value = 1, message = "page must be greater than 1")
//    private Integer page = 1;
//
//    @Min(value = 1, message = "perPage must be greater than 1")
//    private Integer perPage = 10;
//}
