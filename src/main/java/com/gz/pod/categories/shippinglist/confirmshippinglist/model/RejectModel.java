//package com.gz.pod.categories.shippinglist.confirmshippinglist.model;
//
//
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//import javax.validation.constraints.NotNull;
//
//@Data
//public class RejectModel {
//
//    @NotNull(message = "Driver id can not be null!")
//    @ApiModelProperty(example = "10", required = true)
//    private int id;
//
//    @NotNull(message = "Reason id can not be null!")
//    @ApiModelProperty(example = "2", required = true)
//    private int reason;
//
//    @NotNull(message = "Remark can not be null!")
//    @ApiModelProperty(example = "ของชำรุด", required = true)
//    private String remark;
//
//    public RejectModel(@NotNull(message = "Driver id can not be null!") int id,
//                       @NotNull(message = "Reason id can not be null!") int reason,
//                       @NotNull(message = "Remark can not be null!") String remark) {
//        this.id = id;
//        this.reason = reason;
//        this.remark = remark;
//    }
//
//}
