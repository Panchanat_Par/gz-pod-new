//package com.gz.pod.categories.shippinglist;
//
//import com.gz.pod.categories.shippinglist.responses.ShipToCodeResponse;
//import com.gz.pod.categories.shippinglist.responses.ShippinglistResponse;
//import com.gz.pod.entities.Manifesto;
//import com.gz.pod.entities.ManifestoCstm;
//import com.gz.pod.entities.MasterRoute;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//
//public class ShippingConstant {
//
//    public static final String SHIPPING_LIST = "Shipping List";
//    public static final String GET_LIST_SUCCESS = "get shipping list success!";
//    public static final String GET_LIST_ERROR = "shipping list not found, please check your data!";
//    public static final int DRIVER_ID = 1;
//    public static final String DATE = "2018-07-26";
//    public static final String STATUS_SO = "3";
//    public static final String SHIP_TO_CODE_1 = "502100";
//    public static final String SHIP_TO_CODE_2 = "502110";
//    public static final int PAGE = 1;
//    public static final int PER_PAGE = 10;
//    public static ShippinglistResponse SHIPPING_RESPONSE = new ShippinglistResponse(1, 1, "DOC001",
//            "SOMS Order", "21000234", "Test Name", "502100",
//            "785 moo1", 1, 2, 3, BigDecimal.valueOf(1), "Test remark", "1",
//            "R002", "222", "SO001", "MN001", 3);
//    public static Manifesto MANIFESTO_RESPONSE = new Manifesto(1, "MN001", "2018-07-26",
//            "DOC001", 1, "SOMS Order",
//            "SM01", "502195454", 1,
//            "Test Test", "502100", "Test ja Test ja", "Test moo 1",
//            1, 1, 1, "remark", "Test",
//            "addressType", BigDecimal.valueOf(1), 3, new MasterRoute(), new ManifestoCstm());
//
//    private ShippingConstant() {
//
//    }
//
//    public static List<ShipToCodeResponse> GET_SHIPTOCODE_LIST() {
//        List<ShipToCodeResponse> shipToCodeResponseList = new ArrayList<>();
//        ShipToCodeResponse st1 = new ShipToCodeResponse();
//        st1.setShipToCode(SHIP_TO_CODE_1);
//        st1.setStatus(Integer.parseInt(STATUS_SO));
//        st1.setData(GET_SHIPPING_LIST());
//        ShipToCodeResponse st2 = new ShipToCodeResponse();
//        st1.setShipToCode(SHIP_TO_CODE_2);
//        st1.setStatus(Integer.parseInt(STATUS_SO));
//        st1.setData(GET_SHIPPING_LIST());
//        shipToCodeResponseList.add(st1);
//        shipToCodeResponseList.add(st2);
//        return shipToCodeResponseList;
//    }
//
//    public static List<ShippinglistResponse> GET_SHIPPING_LIST() {
//        List<ShippinglistResponse> shippinglistResponseList = new ArrayList<>();
//        ShippinglistResponse ship = SHIPPING_RESPONSE;
//        shippinglistResponseList.add(ship);
//        return shippinglistResponseList;
//    }
//
//    public static List<Manifesto> GET_MANIFESTO_LIST() {
//        List<Manifesto> manifestoList = new ArrayList<>();
//        manifestoList.add(MANIFESTO_RESPONSE);
//        manifestoList.add(MANIFESTO_RESPONSE);
//        return manifestoList;
//    }
//
//
//}
