//package com.gz.pod.categories.shippinglist.managershipping;
//
//import com.gz.pod.entities.ConfirmShippingList;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import javax.transaction.Transactional;
//import java.sql.Timestamp;
//import java.util.List;
//
//@Repository
//public interface ManagerShippingRepository extends CrudRepository<ConfirmShippingList, Integer> {
//
//    @Query(value = "select c from ConfirmShippingList c " +
//            "where c.actionBy = ?1 and c.manifestoId = ?2")
//    List<ConfirmShippingList> getConfirmListByDriverID(int driverId, Integer manifestoId);
//
//    @Query(value = "select cm from ConfirmShippingList cm where cm.manifestoId = ?1")
//    ConfirmShippingList getConfirmManifestoById(int manifestoId);
//
//    @Transactional
//    @Modifying
//    @Query(value = "update ConfirmShippingList set isConfirmByManager = ?2 " +
//            ", remarkByManager = ?3, reasonIdByManager = ?4 , updateDateByManager = ?5 where manifestoId = ?1")
//    void confirmShippingByManager(int manifestoId, int isConfirmByManager
//            , String remarkByManager, int reasonIdByManager, Timestamp updateDateByManager);
//}
