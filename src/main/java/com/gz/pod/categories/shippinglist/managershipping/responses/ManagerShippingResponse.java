//package com.gz.pod.categories.shippinglist.managershipping.responses;
//
//import lombok.Data;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//import java.util.Objects;
//
//@Data
//public class ManagerShippingResponse {
//
//    private Integer driverId;
//    private int manifestoId;
//    private String dONumber;
//    private String manifestoType;
//    private String customerName;
//    private String addressType;
//    private Integer boxQty;
//    private Integer itemQty;
//    private Integer sKUQty;
//    private String orderTotalInVat;
//    private String remark;
//    private String routeMaster;
//    private String routeName;
//    private String billNo;
//    private String sONo;
//    private String documentNumber;
//    private int shippingStatus;
//    private Integer isConfirm;
//    private String remarkReject;
//    private Integer reasonId;
//    private String reasonString;
//    private Integer actionBy;
//    private Integer isConfirmByManager;
//    private String remarkByManager;
//    private Integer reasonIdByManager;
//    private String shipToCode;
//
//    @SuppressWarnings("squid:S00107")
//    public ManagerShippingResponse(Integer driverId, int manifestoId, String dONumber, String manifestoType,
//                                   String customerName, String addressType, Integer boxQty, Integer itemQty,
//                                   Integer sKUQty, BigDecimal orderTotalInVat, String remark, String routeMaster,
//                                   String routeName, String billNo, String sONo, String documentNumber,
//                                   int shippingStatus, Integer isConfirm, String remarkReject, Integer reasonId,
//                                   String reasonString, Integer actionBy, Integer isConfirmByManager,
//                                   String remarkByManager, Integer reasonIdByManager, String shipToCode) {
//        this.driverId = driverId;
//        this.manifestoId = manifestoId;
//        this.dONumber = dONumber;
//        this.manifestoType = manifestoType;
//        this.customerName = customerName;
//        this.addressType = addressType;
//        this.boxQty = boxQty;
//        this.itemQty = itemQty;
//        this.sKUQty = sKUQty;
//
//        double vat = Double.parseDouble(String.valueOf(orderTotalInVat));
//        DecimalFormat df = new DecimalFormat("#,###.00");
//        this.orderTotalInVat = df.format(vat);
//
//        this.remark = remark;
//        this.routeMaster = routeMaster;
//        this.routeName = routeName;
//        this.billNo = billNo;
//        this.sONo = sONo;
//        this.documentNumber = documentNumber;
//        this.shippingStatus = shippingStatus;
//        this.isConfirm = isConfirm;
//        this.remarkReject = remarkReject;
//        this.reasonId = reasonId;
//        this.reasonString = reasonString;
//        this.actionBy = actionBy;
//        this.isConfirmByManager = isConfirmByManager;
//        this.remarkByManager = remarkByManager;
//        this.reasonIdByManager = reasonIdByManager;
//        this.shipToCode = shipToCode;
//    }
//
//    public ManagerShippingResponse() {
//
//    }
//
//    public void setDriverId(Integer driverId) {
//        this.driverId = driverId;
//    }
//
//    public void setManifestoId(int manifestoId) {
//        this.manifestoId = manifestoId;
//    }
//
//    public void setDONumber(String dONumber) {
//        this.dONumber = Objects.toString(dONumber, "");
//    }
//
//    public void setManifestoType(String manifestoType) {
//        this.manifestoType = Objects.toString(manifestoType, "");
//    }
//
//    public void setCustomerName(String customerName) {
//        this.customerName = Objects.toString(customerName, "");
//    }
//
//    public void setAddressType(String addressType) {
//        this.addressType = Objects.toString(addressType, "");
//    }
//
//    public void setBoxQty(Integer boxQty) {
//        this.boxQty = boxQty;
//    }
//
//    public void setItemQty(Integer itemQty) {
//        this.itemQty = itemQty;
//    }
//
//    public void setSKUQty(Integer sKUQty) {
//        this.sKUQty = sKUQty;
//    }
//
//    public void setOrderTotalInVat(String orderTotalInVat) {
//        this.orderTotalInVat = Objects.toString(orderTotalInVat, "");
//
//    }
//
//    public void setRemark(String remark) {
//        this.remark = Objects.toString(remark, "");
//
//    }
//
//    public void setRouteMaster(String routeMaster) {
//        this.routeMaster = Objects.toString(routeMaster, "");
//
//    }
//
//    public void setRouteName(String routeName) {
//        this.routeName = Objects.toString(routeName, "");
//
//    }
//
//    public void setBillNo(String billNo) {
//        this.billNo = Objects.toString(billNo, "");
//
//    }
//
//    public void setSONo(String sONo) {
//        this.sONo = Objects.toString(sONo, "");
//
//    }
//
//    public void setDocumentNumber(String documentNumber) {
//        this.documentNumber = Objects.toString(documentNumber, "");
//
//    }
//
//    public void setShippingStatus(int shippingStatus) {
//        this.shippingStatus = shippingStatus;
//    }
//
//    public void setIsConfirm(Integer isConfirm) {
//        this.isConfirm = isConfirm;
//    }
//
//    public void setRemarkReject(String remarkReject) {
//        this.remarkReject = Objects.toString(remarkReject, "");
//
//    }
//
//    public void setReasonId(Integer reasonId) {
//        this.reasonId = reasonId;
//    }
//
//    public void setReasonString(String reasonString) {
//        this.reasonString = Objects.toString(reasonString, "");
//
//    }
//
//    public void setActionBy(Integer actionBy) {
//        this.actionBy = actionBy;
//    }
//
//    public void setIsConfirmByManager(Integer isConfirmByManager) {
//        this.isConfirmByManager = isConfirmByManager;
//    }
//
//    public void setRemarkByManager(String remarkByManager) {
//        this.remarkByManager = Objects.toString(remarkByManager, "");
//
//    }
//
//    public void setReasonIdByManager(Integer reasonIdByManager) {
//        this.reasonIdByManager = reasonIdByManager;
//    }
//
//    public void setShipToCode(String shipToCode) {
//        this.shipToCode = Objects.toString(shipToCode, "");
//    }
//}