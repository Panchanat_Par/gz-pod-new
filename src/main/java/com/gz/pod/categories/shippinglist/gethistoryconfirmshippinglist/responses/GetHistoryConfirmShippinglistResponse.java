//package com.gz.pod.categories.shippinglist.gethistoryconfirmshippinglist.responses;
//
//import lombok.Data;
//
//@Data
//public class GetHistoryConfirmShippinglistResponse {
//
//    private int manifestoId;
//    private int isConfirm;
//    private String remark;
//    private int reasonId;
//    private int actionBy;
//    private String confirmDate;
//    private String reasonText;
//
//    public GetHistoryConfirmShippinglistResponse(int manifestoId, int isConfirm, String remark, int reasonId,
//                                                 int actionBy, String confirmDate, String reasonText) {
//        this.manifestoId = manifestoId;
//        this.isConfirm = isConfirm;
//        this.remark = remark;
//        this.reasonId = reasonId;
//        this.actionBy = actionBy;
//        this.confirmDate = confirmDate;
//        this.reasonText = reasonText;
//    }
//
//    public GetHistoryConfirmShippinglistResponse(){}
//}
