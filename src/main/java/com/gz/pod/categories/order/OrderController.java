package com.gz.pod.categories.order;

import com.gz.pod.response.GZResponse;
import com.gz.pod.categories.order.requests.OrderRequest;
import com.gz.pod.categories.order.responses.OrderResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/order")
@Api(value = "Order", description = "Order management", produces = "application/json", tags = {"Order"})
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success", response = OrderResponse.class)})
    public ResponseEntity<GZResponse<OrderResponse>> addOrder(@RequestBody @Valid OrderRequest orderRequest) throws Exception {
        return ResponseEntity.ok(orderService.addOrderInSOMS(orderRequest));
    }

}
