package com.gz.pod.categories.order.orderitem.requests;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Data
public class OrderItemRequest {
    @ApiModelProperty(example = "5", required = true)
    @NotNull(message = "orderId can not be null")
    private int orderId;
    @ApiModelProperty(example = "30dd879c-ee2f-11db-8314-0800200c9a66")
    private String orderItemGuid = UUID.randomUUID().toString();
    @ApiModelProperty(example = "1320158", required = true)
    @NotNull(message = "productCode can not be null")
    private String productCode;
    @ApiModelProperty(example = "1", required = true)
    @NotNull(message = "quantity can not be null")
    private int quantity;
    @ApiModelProperty(example = "2100.0000", required = true)
    @NotNull(message = "unitPriceInclTax can not be null")
    private BigDecimal unitPriceInclTax;
    @ApiModelProperty(example = "1962.6200", required = true)
    @NotNull(message = "unitPriceExclTax can not be null")
    private BigDecimal unitPriceExclTax;
    @ApiModelProperty(example = "2100.0000", required = true)
    @NotNull(message = "priceInclTax can not be null")
    private BigDecimal priceInclTax;
    @ApiModelProperty(example = "1962.6200", required = true)
    @NotNull(message = "priceExclTax can not be null")
    private BigDecimal priceExclTax;
    @ApiModelProperty(example = "0.0000")
    private BigDecimal discountAmountInclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal discountAmountExclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal originalProductCost = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "id is integer")
    private String attributeDescription;
    @ApiModelProperty(example = "id")
    private String attributesXml;
    @ApiModelProperty(example = "0")
    private int downloadCount = 0;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private int isDownloadActivated = 0;
    @ApiModelProperty(example = "0")
    private int licenseDownloadId = 0;
    @ApiModelProperty(example = "120.0000")
    private BigDecimal itemWeight;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp rentalStartDateUtc;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp rentalEndDateUtc;
    @NotNull(message = "productID can not be null")
    @ApiModelProperty(example = "15399", required = true)
    private int productID;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp deliveryDate;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private int isFreeItem = 0;
    @ApiModelProperty(example = "0")
    private int sortable = 0;
    @ApiModelProperty(example = "KAIJU")
    private String fmssku;
    @ApiModelProperty(example = "F5140")
    private String shipToCode;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private int isEdit = 0;
    @ApiModelProperty(example = "0")
    private int discountId;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private int canSaleTypeX = 0;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private int isApproveSurge = 0;
}
