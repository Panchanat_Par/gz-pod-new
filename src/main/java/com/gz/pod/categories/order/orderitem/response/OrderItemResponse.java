package com.gz.pod.categories.order.orderitem.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItemResponse {
    private int orderId;
    private String productCode;
    private int quantity;
    private BigDecimal priceInclTax;
    private BigDecimal priceExclTax;
}
