package com.gz.pod.categories.order;

import com.gz.pod.entities.SOMSOrder;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import com.gz.pod.categories.order.requests.OrderRequest;
import com.gz.pod.categories.order.responses.OrderResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository){
        this.orderRepository = orderRepository;
    }

    private ModelMapper modelMapper = new ModelMapper();

    public GZResponse<OrderResponse> addOrderInSOMS(OrderRequest orderRequest) throws GZException {
        GZResponse response = new GZResponse();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        try {
            SOMSOrder somsOrder = orderRepository.save(modelMapper.map(orderRequest, SOMSOrder.class));
            somsOrder.setCustomerPODate(getCustomerPODate());
            OrderResponse orderResponse = modelMapper.map(somsOrder, OrderResponse.class);
            response.setTitle(OrderConstant.ADD_ORDER);
            response.setMessage(HttpStatus.OK.toString());
            response.setCode(HttpStatus.OK);
            response.setDeveloperMessage(OrderConstant.ADD_ORDER_SUCCESS);
            response.setData(orderResponse);
        } catch (Exception ex) {
            throw new GZException(OrderConstant.ADD_ORDER, HttpStatus.BAD_REQUEST,
                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
        }
        return response;
    }

    private Timestamp getCustomerPODate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, 7);
        return new Timestamp(calendar.getTimeInMillis());
    }

}
