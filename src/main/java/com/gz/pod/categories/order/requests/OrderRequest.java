package com.gz.pod.categories.order.requests;

import com.gz.pod.categories.order.orderitem.requests.OrderItemRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
public class OrderRequest {
    @ApiModelProperty(example = "4")
    private Integer storeId = 4;
    @ApiModelProperty(example = "30dd879c-ee2f-11db-8314-0800200c9a66")
    private String orderGuid = UUID.randomUUID().toString();
    @ApiModelProperty(example = "4")
    private Integer customerId;
    @ApiModelProperty(example = "117615", value = "117615", required = true)
    @NotNull(message = "customerNumber can not be null")
    private String customerNumber;
    @ApiModelProperty(example = "1")
    private Integer billingAddressId;
    @ApiModelProperty(example = "1")
    private Integer shippingAddressId;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer pickUpInStore = 0;
    @ApiModelProperty(example = "10")
    private Integer orderStatusId = 10;
    @ApiModelProperty(example = "0")
    private Integer shippingStatusId = 0;
    @ApiModelProperty(example = "0")
    private Integer paymentStatusId = 0;
    @ApiModelProperty(example = "SCB")
    private String paymentMethodSystemName;
    @ApiModelProperty(example = "THB")
    private String customerCurrencyCode = "THB";
    @ApiModelProperty(example = "1")
    private BigDecimal currencyRate = BigDecimal.valueOf(1);
    @ApiModelProperty(example = "0")
    private Integer customerTaxDisplayTypeId = 0;
    @ApiModelProperty(example = "7.0000")
    private String vatNumber = "7.0000";
    @ApiModelProperty(example = "2100.0000", required = true)
    @NotNull(message = "orderSubtotalInclTax can not be null")
    private BigDecimal orderSubtotalInclTax;
    @ApiModelProperty(example = "1962.6200", required = true)
    @NotNull(message = "orderSubtotalExclTax can not be null")
    private BigDecimal orderSubtotalExclTax;
    @ApiModelProperty(example = "0.0000")
    private BigDecimal orderSubTotalDiscountInclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal orderSubTotalDiscountExclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal orderShippingInclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal orderShippingExclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal paymentMethodAdditionalFeeInclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0.0000")
    private BigDecimal paymentMethodAdditionalFeeExclTax = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "7.0000")
    private String taxRates = "7.0000";
    @NotNull(message = "orderTax can not be null")
    @ApiModelProperty(example = "7.0000", required = true)
    private BigDecimal orderTax;
    @ApiModelProperty(example = "0.0000")
    private BigDecimal orderDiscount = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "2100.0000")
    @NotNull(message = "orderTotal can not be null")
    private BigDecimal orderTotal;
    @ApiModelProperty(example = "0.0000")
    private BigDecimal refundedAmount = BigDecimal.valueOf(0);
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer rewardPointsWereAdded = 0;
    @ApiModelProperty(example = "id")
    private String checkoutAttributeDescription;
    @ApiModelProperty(example = "id")
    private String checkoutAttributesXml;
    @ApiModelProperty(example = "0")
    private Integer customerLanguageId = 0;
    @ApiModelProperty(example = "0")
    private Integer affiliateId = 0;
    @ApiModelProperty(example = "127.0.0.1")
    private String customerIp;
    @ApiModelProperty(example = "0")
    private Integer allowStoringCreditCardNumber = 0;
    @ApiModelProperty(example = "VISA")
    private String cardType;
    @ApiModelProperty(example = "SOMCHAI")
    private String cardName;
    @ApiModelProperty(example = "1234-1234-5432-5343")
    private String cardNumber;
    @ApiModelProperty(example = "SOMCHAI")
    private String maskedCreditCardNumber;
    @ApiModelProperty(example = "123")
    private String cardCvv2;
    @ApiModelProperty(example = "05")
    private String cardExpirationMonth;
    @ApiModelProperty(example = "2020")
    private String cardExpirationYear;
    @ApiModelProperty(example = "0FRw#ef")
    private String authorizationTransactionId;
    @ApiModelProperty(example = "FRTED#4")
    private String authorizationTransactionCode;
    @ApiModelProperty(example = "FREe#f4")
    private String authorizationTransactionResult;
    @ApiModelProperty(example = "FDWE4fd3")
    private String captureTransactionId;
    @ApiModelProperty(example = "gTGT424#")
    private String captureTransactionResult;
    @ApiModelProperty(example = "TGTGTR45")
    private String subscriptionTransactionId;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp paidDateUtc;
    @ApiModelProperty(example = "PUT")
    private String shippingMethod;
    @ApiModelProperty(example = "ADEN")
    private String shippingRateComputationMethodSystemName;
    @ApiModelProperty(example = "5")
    private String customValuesXml;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer deleted = 0;
    @ApiModelProperty(example = "1")
    private Integer createdBy;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp createdOnUtc = new Timestamp(System.currentTimeMillis());
    @ApiModelProperty(example = "1")
    private Integer updatedBy;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp updatedOnUtc;
    @ApiModelProperty(example = "SOMS-2013/32")
    private String orderNumber;
    @ApiModelProperty(example = "SOMS-2020")
    private String billingNumber;
    @ApiModelProperty(example = "2018-05-23T18:25:43.511Z")
    private Timestamp pickupDate;
    @ApiModelProperty(example = "06:43")
    private String pickupTime;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer returnCredit = 0;
    @ApiModelProperty(example = "0")
    private String exportStatus;
    @ApiModelProperty(example = "EWE323")
    private String quotationNumber;
    @ApiModelProperty(example = "GZ33494")
    private String voucherCode;
    @ApiModelProperty(example = "0")
    private String cancelRemark;
    @ApiModelProperty(example = "0")
    private String transferBankStatus;
    @ApiModelProperty(example = "0")
    private Integer transferBankPictureId;
    @ApiModelProperty(example = "get pen")
    private String transferBankRemark;
    @ApiModelProperty(example = "0")
    private String purchaseOrderNumber;
    @ApiModelProperty(example = "0")
    private String generatedDocNumber;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer hasBillingAddress = 0;
    @ApiModelProperty(example = "1")
    private Integer placeOrderId = 1;
    @ApiModelProperty(example = "0")
    private String specialInstruction;
    @ApiModelProperty(example = "0")
    private String customerPONumber;
    @ApiModelProperty(example = "456789086")
    private String sODocNumber;
    @ApiModelProperty(example = "Owner cat")
    private String shipToContact;
    @ApiModelProperty(example = "1")
    private Integer empBookingId = 1;
    @ApiModelProperty(example = "kaewta@gmail.com")
    private String customerCCEmail;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer isReceiveCCEmail = 0;
    @ApiModelProperty(example = "0")
    private Integer webOrderId = 0;
    @ApiModelProperty(example = "0")
    private String fIDocNumber;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer isShippingFee;
    @ApiModelProperty(example = "0")
    private Integer esolutionDepartmentId;
    @ApiModelProperty(example = "0")
    private Integer esolutionCostCenterId;
    @ApiModelProperty(example = "0")
    private Integer hubSpotDealId;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer readyForFMS = 0;
    @ApiModelProperty(example = "0")
    private Integer reworkRemark = 0;
    @ApiModelProperty(example = "EWEW5454")
    private String uniqueCustomerPONumber;
    @ApiModelProperty(example = "0", allowableValues = "1,0")
    private Integer customerPOIsFullSO = 0;
    @ApiModelProperty(example = "5", allowableValues = "1,0")
    private Integer isMonthlyInvoice;
    private Integer orderRef;
    private List<OrderItemRequest> orderItems;
}
