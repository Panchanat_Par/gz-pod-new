package com.gz.pod.categories.order.orderitem;

import com.gz.pod.categories.order.orderitem.requests.OrderItemRequest;
import com.gz.pod.categories.order.orderitem.response.OrderItemResponse;

import java.math.BigDecimal;

public class OrderItemConstant {
    public static final int ORDER_ID = 5;
    public static final String PRODUCT_CODE = "1320158";
    public static final int QUANTITY = 1;
    public static final BigDecimal UNIT_PRICE_INCL_TAX = BigDecimal.valueOf(2100.0000);
    public static final BigDecimal UNIT_PRICE_EXCL_TAX = BigDecimal.valueOf(1962.6200);
    public static final BigDecimal PRICE_INCL_TAX = BigDecimal.valueOf(2100.0000);
    public static final BigDecimal PRICE_EXCL_TAX = BigDecimal.valueOf(1962.6200);
    public static final int PRODUCT_ID = 15399;

    private OrderItemConstant() {
        throw new IllegalStateException("OrderItemConstant class");
    }

    public static OrderItemRequest getOrderItemRequest() {
        OrderItemRequest orderItemRequest = new OrderItemRequest();
        orderItemRequest.setOrderId(OrderItemConstant.ORDER_ID);
        orderItemRequest.setProductCode(OrderItemConstant.PRODUCT_CODE);
        orderItemRequest.setQuantity(OrderItemConstant.QUANTITY);
        orderItemRequest.setUnitPriceInclTax(OrderItemConstant.UNIT_PRICE_INCL_TAX);
        orderItemRequest.setUnitPriceExclTax(OrderItemConstant.UNIT_PRICE_EXCL_TAX);
        orderItemRequest.setPriceInclTax(OrderItemConstant.PRICE_INCL_TAX);
        orderItemRequest.setPriceExclTax(OrderItemConstant.PRICE_EXCL_TAX);
        orderItemRequest.setProductID(OrderItemConstant.PRODUCT_ID);
        return orderItemRequest;
    }

    public static OrderItemResponse getOrderItemResponse() {
        OrderItemResponse orderItemResponse = new OrderItemResponse();
        orderItemResponse.setOrderId(OrderItemConstant.ORDER_ID);
        orderItemResponse.setProductCode(OrderItemConstant.PRODUCT_CODE);
        orderItemResponse.setQuantity(OrderItemConstant.QUANTITY);
        orderItemResponse.setPriceInclTax(OrderItemConstant.PRICE_INCL_TAX);
        orderItemResponse.setPriceExclTax(OrderItemConstant.PRICE_EXCL_TAX);
        return orderItemResponse;
    }
}
