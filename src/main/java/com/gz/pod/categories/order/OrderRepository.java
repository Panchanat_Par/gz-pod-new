package com.gz.pod.categories.order;

import com.gz.pod.entities.SOMSOrder;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<SOMSOrder, Integer> {
}
