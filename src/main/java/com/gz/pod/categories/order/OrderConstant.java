package com.gz.pod.categories.order;

import com.gz.pod.categories.order.orderitem.OrderItemConstant;
import com.gz.pod.categories.order.orderitem.requests.OrderItemRequest;
import com.gz.pod.categories.order.orderitem.response.OrderItemResponse;
import com.gz.pod.categories.order.requests.OrderRequest;
import com.gz.pod.categories.order.responses.OrderResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderConstant {
    public static final String ADD_ORDER = "Add New Order";
    public static final String ADD_ORDER_SUCCESS = "Add new order in SOMS success!";
    public static final String CUSTOMER_NUMBER = "117615";
    public static final BigDecimal ORDER_SUBTOTAL_INCL_TAX = BigDecimal.valueOf(2100.0000);
    public static final BigDecimal ORDER_SUBTOTAL_EXCL_TAX = BigDecimal.valueOf(1962.6200);
    public static final BigDecimal ORDER_TAX = BigDecimal.valueOf(137.3800);
    public static final BigDecimal ORDER_TOTAL = BigDecimal.valueOf(2100.0000);

    private OrderConstant() {
        throw new IllegalStateException("OrderConstant class");
    }

    public static OrderRequest getOrderRequest() {
        List<OrderItemRequest> orderItemRequestList = new ArrayList<>();
        orderItemRequestList.add(OrderItemConstant.getOrderItemRequest());
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setCustomerNumber(OrderConstant.CUSTOMER_NUMBER);
        orderRequest.setOrderSubtotalInclTax(OrderConstant.ORDER_SUBTOTAL_INCL_TAX);
        orderRequest.setOrderSubtotalExclTax(OrderConstant.ORDER_SUBTOTAL_EXCL_TAX);
        orderRequest.setOrderSubTotalDiscountExclTax(OrderConstant.ORDER_SUBTOTAL_EXCL_TAX);
        orderRequest.setOrderTax(OrderConstant.ORDER_TAX);
        orderRequest.setOrderTotal(OrderConstant.ORDER_TOTAL);
        orderRequest.setOrderItems(orderItemRequestList);
        return orderRequest;
    }

    public static OrderResponse getOrderResponse() {
        List<OrderItemResponse> orderItemResponseList = new ArrayList<>();
        orderItemResponseList.add(OrderItemConstant.getOrderItemResponse());
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setCustomerNumber(OrderConstant.CUSTOMER_NUMBER);
        orderResponse.setOrderTax(OrderConstant.ORDER_TAX);
        orderResponse.setOrderTotal(OrderConstant.ORDER_TOTAL);
        orderResponse.setOrderItems(orderItemResponseList);
        return orderResponse;
    }

}
