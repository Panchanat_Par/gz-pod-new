package com.gz.pod.categories.order.orderitem;

import com.gz.pod.entities.SOMSOrderItem;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemRepository extends CrudRepository<SOMSOrderItem, Integer> {
}
