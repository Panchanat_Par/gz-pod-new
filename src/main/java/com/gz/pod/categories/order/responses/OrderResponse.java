package com.gz.pod.categories.order.responses;

import com.gz.pod.categories.order.orderitem.response.OrderItemResponse;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class OrderResponse {
    private String customerNumber;
    private BigDecimal orderTax;
    private BigDecimal orderTotal;
    private List<OrderItemResponse> orderItems = new ArrayList<>();
}
