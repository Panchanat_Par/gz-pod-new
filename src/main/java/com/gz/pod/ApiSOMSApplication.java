package com.gz.pod;

import com.gz.pod.configs.CreateConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import org.springframework.boot.web.support.SpringBootServletInitializer;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
@SpringBootApplication
public class ApiSOMSApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ApiSOMSApplication.class, args);
        createConfig();
    }

    public static void createConfig() {
        CreateConfig createConfig = new CreateConfig();
        createConfig.createConfig();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ApiSOMSApplication.class);
    }
}
