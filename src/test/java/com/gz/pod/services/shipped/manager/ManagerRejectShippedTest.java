//package com.gz.pod.services.shipped.manager;
//
//import com.gz.pod.categories.shippedlist.managershipped.ManagerShippedContant;
//import com.gz.pod.categories.shippedlist.managershipped.ManagerShippedRepository;
//import com.gz.pod.categories.shippedlist.managershipped.ManagerShippedService;
//import com.gz.pod.categories.shippedlist.managershipped.requests.ManagerConfirmRequest;
//import com.gz.pod.categories.shippedlist.managershipped.responses.ManagerConfirmResponse;
//import com.gz.pod.entities.ConfirmManifesto;
//import com.gz.pod.response.GZResponse;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.modelmapper.ModelMapper;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import static org.mockito.Mockito.*;
//
//@RunWith(SpringRunner.class)
//public class ManagerRejectShippedTest {
//
//    private ManagerShippedService managerShippedService;
//    private ModelMapper modelMapper = new ModelMapper();
//
//    @Mock
//    private ManagerShippedRepository managerShippedRepository;
//
//    @Before
//    public void setUp() throws Exception {
//        managerShippedService = new ManagerShippedService(managerShippedRepository);
//    }
//
//    @Test
//    public void shouldReturnRejectStatusWhenRejectSuccess() throws Exception {
//        //Arrange
//        ManagerConfirmRequest request = new ManagerConfirmRequest();
//        request.setManifestoId(ManagerShippedContant.MANIFESTO_ID);
//        request.setIsConfirmByManager(ManagerShippedContant.IS_REJECT_BY_MANAGER);
//        request.setReasonIdByManager(ManagerShippedContant.REASON_REJECT);
//        request.setRemarkByManager(ManagerShippedContant.REMARK_REJECT);
//
//        ManagerConfirmResponse response = new ManagerConfirmResponse();
//        response.setManifestoId(ManagerShippedContant.MANIFESTO_ID);
//        response.setIsConfirmByManager(ManagerShippedContant.IS_REJECT_BY_MANAGER);
//        response.setRemarkByManager(ManagerShippedContant.REMARK_REJECT);
//        response.setReasonIdByManager(ManagerShippedContant.REASON_REJECT);
//
//        when(managerShippedRepository.getConfirmManifestoById(request.getManifestoId()))
//                .thenReturn(modelMapper.map(response, ConfirmManifesto.class));
//
//        //Action
//        GZResponse gzResponse = managerShippedService.confirmShippedByManager(request);
//
//        //Assert
//        verify(managerShippedRepository, times(1)).getConfirmManifestoById(request.getManifestoId());
//        Assert.assertEquals(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER, gzResponse.getTitle());
//
//    }
//}
