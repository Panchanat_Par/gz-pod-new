package com.gz.pod.services.receiveDocument;

import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.GenericInformationRepository;
import com.gz.pod.categories.common.MasterStatusRepository;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentConstant;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentRepository;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentService;
import com.gz.pod.categories.receiveDocument.requests.UpdateReceiveDocument;
import com.gz.pod.categories.receiveDocument.requests.ValidateReceiveDocument;
import com.gz.pod.entities.*;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Matchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class ReceiveDocumentServiceTest {
    @InjectMocks
    private ReceiveDocumentService receiveDocumentService;

    @Mock
    private CommonService commonService;

    @Mock
    private MasterStatusRepository masterStatusRepository;

    @Mock
    private UserTypeMasterRepository userTypeMasterRepository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private ReceiveDocumentRepository receiveDocumentRepository;

    @Mock
    private JobStatusRepository jobStatusRepository;

    @Mock
    private GenericInformationRepository genericInformationRepository;

    @Test
    public void validateReceiveDocumentCaseValidateCustomerNotFound() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(411);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseValidateCustomerNotHavePermission() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(411);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(1);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(1);
        userType.setName("Driver");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.USER_NOTHAVE_PERMISSION);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseMappingStatusAndUserStatusRequestNotFound() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(411);

        //////// Mock customer
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(null);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.STATUS_REQUEST_NOTFOUND);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseMappingStatusAndUserStatusRequestCheckerStatusNotMatch() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(423);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(2);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////

        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.STATUS_AND_USER_NOT_MATCH);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseMappingStatusAndUserStatusRequestFinOpsStatusNotMatch() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(423);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(3);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(3);
        userType.setName("FinOps");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(3);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////

        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.STATUS_AND_USER_NOT_MATCH);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseMappingStatusAndUserStatusRequestFinOpsStatusMatch() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(431);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(3);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(3);
        userType.setName("FinOps");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(3);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////

        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage("java.lang.NullPointerException");

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseQRCodeCatch() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(421);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(2);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////

        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage("java.lang.NullPointerException");

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseQRCodeNotFound() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(421);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(2);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////

        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        //////// QRCodeMock
        String qrResponse = "    {\n" +
                "        \"Message\": \"" + ReceiveDocumentConstant.QRNUMBER_NOTFOUND + "\"" +
                "    }\n";

        String jsonItem = qrResponse;
        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.REFID_LENGTH_LESS_THAN_51);

        ///Case more than 50
        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        request.setRefId("GZQ-1-1");
        expect.setDeveloperMessage(ReceiveDocumentConstant.QRNUMBER_NOTFOUND);

        actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        //////// QRCodeMock2
        String qrResponse2 = "    {\n" +
                "        \"Message\": \"Test\"" +
                "    }\n";

        String jsonItem2 = qrResponse2;
        ResponseEntity<String> responseEntityGet2 = new ResponseEntity<String>(jsonItem2, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet2);
        /////////////////////////////

        GZResponse expect2 = new GZResponse();
        expect2.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect2.setCode(HttpStatus.BAD_REQUEST);
        expect2.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect2.setDeveloperMessage("Test");

        GZResponse actual2 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect2.getTitle(), actual2.getTitle());
        Assert.assertEquals(expect2.getCode(), actual2.getCode());
        Assert.assertEquals(expect2.getMessage(), actual2.getMessage());
        Assert.assertEquals(expect2.getDeveloperMessage(), actual2.getDeveloperMessage());
    }

    @Test
    public void validateReceiveDocumentCaseQRCodeFindManifestoNotFound() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(421);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(2);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////

        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        //////// Mock QRCode
        String qrResponse = "    {\n" +
                "        \"DocRef\": \"1000239\"" +
                "    }\n";

        String jsonItem = qrResponse;
        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        //////// Mock Manifesto
        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog("1000239")).thenReturn(null);

        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.JOBID_FROM_SOMS_MANIFESTO_NOTFOUND);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());


    }

    @Test
    public void validateAndUpdateReceiveDocument() {
        ValidateReceiveDocument request = new ValidateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(421);

        //////// Mock customer 1
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(60);
        userTypeMaster.setUserTypeId(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(request.getLoginId())).thenReturn(userTypeMaster);

        UserType userType = new UserType();
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);
        /////////////////////////////

        //////// Mock mapping status
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(421);
        masterStatus.setName("Receive.Checker.Status.Confirm");
        masterStatus.setModuleType("ReceiveDocument");
        masterStatus.setUserTypeId(2);
        Mockito.when(masterStatusRepository.findById(request.getStatusId())).thenReturn(masterStatus);
        /////////////////////////////














        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        //////// Mock QRCode
        String qrResponse = "    {\n" +
                "        \"DocRef\": \"23010\"," +
                "        \"DocName\": \"Log\"" +
                "    }\n";

        String jsonItem = qrResponse;
        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        //////// Mock Manifesto
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(303);
        Mockito.when(manifestolistRepository.findByFulfillId(23010)).thenReturn(manifestolist);
        /////////////////////////////

        GZResponse expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.MANIFESTO_NOTFOUND);

        GZResponse actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());











        //////// Mock customer 2
        Mockito.when(userTypeMasterRepository.findById(userTypeMaster.getId())).thenReturn(userTypeMaster);
        /////////////////////////////

        //////// Mock QRCode
        qrResponse = "    {\n" +
                "        \"DocRef\": \"1000239\"," +
                "        \"DocName\": \"SO\"" +
                "    }\n";

        jsonItem = qrResponse;
        responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        //////// Mock Manifesto
        manifestolist = new Manifestolist();
        manifestolist.setId(303);
        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog("1000239")).thenReturn(manifestolist);
        /////////////////////////////

        expect = new GZResponse();
        expect.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ReceiveDocumentConstant.MANIFESTO_NOTFOUND);

        actual = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        //////// Mock ManifestoById
        Manifestolist manifestoList2 = new Manifestolist();
        manifestoList2.setId(303);
        manifestoList2.setJobId("1000239");
        manifestoList2.setManifestoType("Log");
        Mockito.when(manifestolistRepository.findAllManifestolistById(manifestolist.getId())).thenReturn(manifestoList2);
        /////////////////////////////

        //////// Mock ReceiveDocument
        ReceiveDocument receiveDocument = new ReceiveDocument();
        Mockito.when(receiveDocumentRepository.findByDeliveryDateGroupByDriver(303, request.getStatusId())).thenReturn(receiveDocument);
        /////////////////////////////

        GZResponse expect2 = new GZResponse();
        expect2.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect2.setCode(HttpStatus.BAD_REQUEST);
        expect2.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect2.setDeveloperMessage(ReceiveDocumentConstant.EVER_RECEIVE);

        GZResponse actual2 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect2.getTitle(), actual2.getTitle());
        Assert.assertEquals(expect2.getCode(), actual2.getCode());
        Assert.assertEquals(expect2.getMessage(), actual2.getMessage());
        Assert.assertEquals(expect2.getDeveloperMessage(), actual2.getDeveloperMessage());

        manifestoList2.setManifestoType("Log");
        manifestoList2.setFulfillId(1002);
        Mockito.when(manifestolistRepository.findAllManifestolistById(manifestolist.getId())).thenReturn(manifestoList2);

        actual2 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect2.getTitle(), actual2.getTitle());
        Assert.assertEquals(expect2.getCode(), actual2.getCode());
        Assert.assertEquals(expect2.getMessage(), actual2.getMessage());
        Assert.assertEquals(expect2.getDeveloperMessage(), actual2.getDeveloperMessage());

        manifestoList2.setManifestoType("SOMS Order");
        Mockito.when(manifestolistRepository.findAllManifestolistById(manifestolist.getId())).thenReturn(manifestoList2);

        actual2 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect2.getTitle(), actual2.getTitle());
        Assert.assertEquals(expect2.getCode(), actual2.getCode());
        Assert.assertEquals(expect2.getMessage(), actual2.getMessage());
        Assert.assertEquals(expect2.getDeveloperMessage(), actual2.getDeveloperMessage());

        //////// Mock ReceiveDocument NULL
        Mockito.when(receiveDocumentRepository.findByDeliveryDateGroupByDriver(303, request.getStatusId())).thenReturn(null);
        /////////////////////////////

        //////// Mock jobStatus
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(null);
        /////////////////////////////

        GZResponse expect3 = new GZResponse();
        expect3.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect3.setCode(HttpStatus.BAD_REQUEST);
        expect3.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect3.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual3 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect3.getTitle(), actual3.getTitle());
        Assert.assertEquals(expect3.getCode(), actual3.getCode());
        Assert.assertEquals(expect3.getMessage(), actual3.getMessage());
        Assert.assertEquals(expect3.getDeveloperMessage(), actual3.getDeveloperMessage());

        ////////////////
        UserTypeMaster user = new UserTypeMaster();
        user.setUserTypeId(PODNumberStatusConstant.DRIVERUPC);
        Mockito.when(userTypeMasterRepository.findById(0)).thenReturn(user);

        expect3.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect3.setCode(HttpStatus.OK);
        expect3.setMessage(HttpStatus.OK.toString());
        expect3.setDeveloperMessage(ReceiveDocumentConstant.CAN_RECEIVE);

        actual3 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect3.getTitle(), actual3.getTitle());
        Assert.assertEquals(expect3.getCode(), actual3.getCode());
        Assert.assertEquals(expect3.getMessage(), actual3.getMessage());
        Assert.assertEquals(expect3.getDeveloperMessage(), actual3.getDeveloperMessage());
        /////////////////

        ////////////////
        user.setUserTypeId(PODNumberStatusConstant.DRIVER);
        Mockito.when(userTypeMasterRepository.findById(0)).thenReturn(user);

        expect3.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect3.setCode(HttpStatus.BAD_REQUEST);
        expect3.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect3.setDeveloperMessage(ReceiveDocumentConstant.STATUS_CANNOT_RECEIVE);

        actual3 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect3.getTitle(), actual3.getTitle());
        Assert.assertEquals(expect3.getCode(), actual3.getCode());
        Assert.assertEquals(expect3.getMessage(), actual3.getMessage());
        Assert.assertEquals(expect3.getDeveloperMessage(), actual3.getDeveloperMessage());
        /////////////////

        //////// Mock jobStatus
        JobStatus jobStatus = new JobStatus();
        jobStatus.setStatusId(211);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        GZResponse expect4 = new GZResponse();
        expect4.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect4.setCode(HttpStatus.OK);
        expect4.setMessage(HttpStatus.OK.toString());
        expect4.setDeveloperMessage(ReceiveDocumentConstant.CAN_RECEIVE);

        GZResponse actual4 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect4.getTitle(), actual4.getTitle());
        Assert.assertEquals(expect4.getCode(), actual4.getCode());
        Assert.assertEquals(expect4.getMessage(), actual4.getMessage());
        Assert.assertEquals(expect4.getDeveloperMessage(), actual4.getDeveloperMessage());

        //////// Mock jobStatus
        jobStatus.setStatusId(212);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        GZResponse actual5 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect4.getTitle(), actual5.getTitle());
        Assert.assertEquals(expect4.getCode(), actual5.getCode());
        Assert.assertEquals(expect4.getMessage(), actual5.getMessage());
        Assert.assertEquals(expect4.getDeveloperMessage(), actual5.getDeveloperMessage());

        //////// Mock jobStatus
        jobStatus.setStatusId(213);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        GZResponse actual6 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect4.getTitle(), actual6.getTitle());
        Assert.assertEquals(expect4.getCode(), actual6.getCode());
        Assert.assertEquals(expect4.getMessage(), actual6.getMessage());
        Assert.assertEquals(expect4.getDeveloperMessage(), actual6.getDeveloperMessage());

        //////// Mock jobStatus
        jobStatus.setStatusId(216);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        GZResponse actual7 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect4.getTitle(), actual7.getTitle());
        Assert.assertEquals(expect4.getCode(), actual7.getCode());
        Assert.assertEquals(expect4.getMessage(), actual7.getMessage());
        Assert.assertEquals(expect4.getDeveloperMessage(), actual7.getDeveloperMessage());

        //////// Mock jobStatus
        jobStatus.setStatusId(217);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        GZResponse actual8 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect4.getTitle(), actual8.getTitle());
        Assert.assertEquals(expect4.getCode(), actual8.getCode());
        Assert.assertEquals(expect4.getMessage(), actual8.getMessage());
        Assert.assertEquals(expect4.getDeveloperMessage(), actual8.getDeveloperMessage());

        //////// Mock jobStatus
        jobStatus.setStatusId(100);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        GZResponse actual9 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect3.getTitle(), actual9.getTitle());
        Assert.assertEquals(expect3.getCode(), actual9.getCode());
        Assert.assertEquals(expect3.getMessage(), actual9.getMessage());
        Assert.assertEquals(expect3.getDeveloperMessage(), actual9.getDeveloperMessage());

        ////// Case Valida Checker Before FinOps
        //////// Mock customer 2
        userType.setId(3);
        userType.setName("FinOps");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);

        expect3.setDeveloperMessage(ReceiveDocumentConstant.STATUS_CANNOT_RECEIVE);

        actual9 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect3.getTitle(), actual9.getTitle());
        Assert.assertEquals(expect3.getCode(), actual9.getCode());
        Assert.assertEquals(expect3.getMessage(), actual9.getMessage());
        Assert.assertEquals(expect3.getDeveloperMessage(), actual9.getDeveloperMessage());



        //// Case Manual Receive
        userType.setId(2);
        userType.setName("Checker");
        Mockito.when(commonService.getUserType(userTypeMaster.getUserTypeId())).thenReturn(userType);

        request.setRefId("303");
        request.setRefModuleType("manifestoId");

        GZResponse actual10 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect3.getTitle(), actual10.getTitle());
        Assert.assertEquals(expect3.getCode(), actual10.getCode());
        Assert.assertEquals(expect3.getMessage(), actual10.getMessage());
        Assert.assertEquals(expect3.getDeveloperMessage(), actual10.getDeveloperMessage());

        //// Case ReModule Not Found
        request.setRefId("303");
        request.setRefModuleType("test");

        GZResponse expect5 = new GZResponse();
        expect5.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expect5.setCode(HttpStatus.BAD_REQUEST);
        expect5.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect5.setDeveloperMessage(ReceiveDocumentConstant.REFMODULE_NOTFOUND);

        GZResponse actual11 = receiveDocumentService.validateReceiveDocumentMain(request);
        Assert.assertEquals(expect5.getTitle(), actual11.getTitle());
        Assert.assertEquals(expect5.getCode(), actual11.getCode());
        Assert.assertEquals(expect5.getMessage(), actual11.getMessage());
        Assert.assertEquals(expect5.getDeveloperMessage(), actual11.getDeveloperMessage());


        //////////////////Unit Test Case Update Success
        UpdateReceiveDocument requestUpdate = new UpdateReceiveDocument();
        requestUpdate.setStatusId(421);
        requestUpdate.setLoginId(60);
        requestUpdate.setRefId("GZQ-1-1");
        requestUpdate.setRefModuleType("qrCode");
        requestUpdate.setRemark("test");

        //////// Mock jobStatus
        jobStatus.setStatusId(216);
        Mockito.when(jobStatusRepository.findLastByjobId(303)).thenReturn(jobStatus);
        /////////////////////////////

        //////// Mock ReceiveDocument
        ReceiveDocument receiveDocument2 = new ReceiveDocument();
        receiveDocument2.setId(333);
        /////////////////////////////

        GenericInformation genericInformation = new GenericInformation();
        Mockito.when(receiveDocumentRepository.save(Mockito.any(ReceiveDocument.class))).thenReturn(receiveDocument2);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        GZResponse expectUpdate = new GZResponse();
        expectUpdate.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expectUpdate.setCode(HttpStatus.OK);
        expectUpdate.setMessage(HttpStatus.OK.toString());
        expectUpdate.setDeveloperMessage(ReceiveDocumentConstant.RECEIVE_SUCCESS);

        GZResponse actualUpdate = receiveDocumentService.updateReceiveDocumentMain(requestUpdate);
        Assert.assertEquals(expectUpdate.getTitle(), actualUpdate.getTitle());
        Assert.assertEquals(expectUpdate.getCode(), actualUpdate.getCode());
        Assert.assertEquals(expectUpdate.getMessage(), actualUpdate.getMessage());
        Assert.assertEquals(expectUpdate.getDeveloperMessage(), actualUpdate.getDeveloperMessage());

        //////////////////Unit Test Case Update Catch
        Mockito.when(receiveDocumentRepository.save(Mockito.any(ReceiveDocument.class))).thenReturn(null);

        GZResponse expectUpdate2 = new GZResponse();
        expectUpdate2.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        expectUpdate2.setCode(HttpStatus.BAD_REQUEST);
        expectUpdate2.setMessage(HttpStatus.BAD_REQUEST.toString());
        expectUpdate2.setDeveloperMessage("java.lang.NullPointerException");

        GZResponse actualUpdate2 = receiveDocumentService.updateReceiveDocumentMain(requestUpdate);
        Assert.assertEquals(expectUpdate2.getTitle(), actualUpdate2.getTitle());
        Assert.assertEquals(expectUpdate2.getCode(), actualUpdate2.getCode());
        Assert.assertEquals(expectUpdate2.getMessage(), actualUpdate2.getMessage());
        Assert.assertEquals(expectUpdate2.getDeveloperMessage(), actualUpdate2.getDeveloperMessage());
    }
}