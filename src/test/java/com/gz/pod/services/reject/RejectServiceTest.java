package com.gz.pod.services.reject;

import com.gz.pod.categories.reject.RejectConstant;
import com.gz.pod.categories.reject.RejectRepository;
import com.gz.pod.categories.reject.RejectService;
import com.gz.pod.categories.reject.requests.RejectRequest;
import com.gz.pod.entities.Reject;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RejectServiceTest {
    @InjectMocks
    private RejectService rejectService;

    @Mock
    private RejectRepository rejectRepository;

    @Test
    public void getRejectListCaseGetSuccess() throws Exception {
        List<Reject> rejectsList = new ArrayList<>();
        Reject reject = new Reject();
        reject.setId(1);
        reject.setDescription("TestDes");
        reject.setTypeReject(2);

        rejectsList.add(reject);

        Mockito.when(rejectRepository.findAll()).thenReturn(rejectsList);

        GZResponse expect = new GZResponse();
        expect.setTitle(RejectConstant.REJECT_LIST);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(RejectConstant.GET_REJECT_SUCCESS);

        GZResponse actual = rejectService.getRejectList();
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getRejectListCaseGetEmpty() throws Exception {
        GZResponse expect = new GZResponse();
        expect.setTitle(RejectConstant.REJECT_LIST);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(RejectConstant.GET_REJECT_ERROR);

        GZResponse actual = rejectService.getRejectList();
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getRejectListByTypeCaseGetSuccess() throws Exception {
        RejectRequest rejectRequest = new RejectRequest();
        rejectRequest.setTypeReject(1);

        List<Reject> rejectsList = new ArrayList<>();
        Reject reject = new Reject();
        reject.setId(1);
        reject.setDescription("TestDes");
        reject.setTypeReject(2);

        rejectsList.add(reject);

        Mockito.when(rejectRepository.getRejectListByTypeReject(Mockito.anyInt())).thenReturn(rejectsList);

        GZResponse expect = new GZResponse();
        expect.setTitle(RejectConstant.REJECT_LIST);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(RejectConstant.GET_REJECT_SUCCESS);

        GZResponse actual = rejectService.getRejectListByType(rejectRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getRejectListByTypeCaseGetEmpty() throws Exception {
        RejectRequest rejectRequest = new RejectRequest();
        rejectRequest.setTypeReject(1);

        GZResponse expect = new GZResponse();
        expect.setTitle(RejectConstant.REJECT_LIST);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(RejectConstant.GET_REJECT_ERROR);

        GZResponse actual = rejectService.getRejectListByType(rejectRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }
}
