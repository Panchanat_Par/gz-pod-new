//package com.gz.pod.services.shipping;
//
//import com.gz.pod.categories.shippinglist.ShippingConstant;
//import com.gz.pod.categories.shippinglist.ShippinglistRepository;
//import com.gz.pod.categories.shippinglist.ShippinglistService;
//import com.gz.pod.categories.shippinglist.requests.ShippinglistRequest;
//import com.gz.pod.response.GZResponsePage;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//@RunWith(MockitoJUnitRunner.class)
//public class ShippingListTest {
//
//    private ShippinglistService shippinglistService;
//
//
//    @Mock
//    ShippinglistRepository shippinglistRepository;
//
//    @Before
//    public void setup() throws Exception {
//        shippinglistService = new ShippinglistService(shippinglistRepository);
//    }
//
//    @Test
//    public void shouldReturnShippingListWhenGetSuccess() throws Exception {
//        //Arrange
//        ShippinglistRequest request = new ShippinglistRequest();
//        request.setDriverId(ShippingConstant.DRIVER_ID);
//        request.setDate(ShippingConstant.DATE);
//        request.setStatusSO(ShippingConstant.STATUS_SO);
//
//        when(shippinglistRepository.getShippingList(ShippingConstant.DRIVER_ID, ShippingConstant.DATE,
//                Integer.parseInt(ShippingConstant.STATUS_SO)))
//                .thenReturn(ShippingConstant.GET_MANIFESTO_LIST());
//
//        //Action
//        GZResponsePage gzResponse = shippinglistService.getShippingListByDriverId(request);
//
//        //Assert
//        verify(shippinglistRepository, times(1)).getShippingList(ShippingConstant.DRIVER_ID,
//                ShippingConstant.DATE, Integer.parseInt(ShippingConstant.STATUS_SO));
//        Assert.assertEquals(ShippingConstant.SHIPPING_LIST, gzResponse.getTitle());
//
//    }
//
//}
