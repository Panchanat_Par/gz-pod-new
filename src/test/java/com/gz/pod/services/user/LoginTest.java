//package com.gz.soms.api.services.user;
//
//import UserConstant;
//import UserService;
//import UserRequest;
//import GZException;
//import GZResponse;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.springframework.http.HttpStatus;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//public class LoginTest {
//
//    @Mock
//    private UserService userService;
//
//    @Test
//    public void loginUserWithUsernameTrue() throws GZException {
//        UserRequest userRequest = new UserRequest();
//        GZResponse gzResponse = new GZResponse();
//        gzResponse.setTitle(UserConstant.LOGIN);
//        gzResponse.setMessage(HttpStatus.OK.toString());
//        gzResponse.setCode(HttpStatus.OK);
//        gzResponse.setDeveloperMessage(UserConstant.LOGINSUCCESS);
//        userRequest.setUsername(UserConstant.USERNAMETRUE);
//        userRequest.setPassword(UserConstant.PWTRUE);
//        try {
//            Mockito.when(userService.loginUser(userRequest)).thenReturn(gzResponse);
//        } catch (Exception ex) {
//            throw new GZException(UserConstant.LOGIN, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
//        }
//    }
//
//    @Test
//    public void loginUserWithEmailTrue() throws GZException {
//        UserRequest userRequest = new UserRequest();
//        GZResponse gzResponse = new GZResponse();
//        gzResponse.setTitle(UserConstant.LOGIN);
//        gzResponse.setMessage(HttpStatus.OK.toString());
//        gzResponse.setCode(HttpStatus.OK);
//        gzResponse.setDeveloperMessage(UserConstant.LOGINSUCCESS);
//        userRequest.setUsername(UserConstant.EMAILTRUE);
//        userRequest.setPassword(UserConstant.PWTRUE);
//        try {
//            Mockito.when(userService.loginUser(userRequest)).thenReturn(gzResponse);
//        } catch (Exception ex) {
//            throw new GZException(UserConstant.LOGIN, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
//        }
//    }
//
//    @Test
//    public void loginUserWithUsernameFalse() throws GZException {
//        UserRequest userRequest = new UserRequest();
//        userRequest.setUsername(UserConstant.USERNAMEFALSE);
//        userRequest.setPassword(UserConstant.PWTRUE);
//        try {
//            GZResponse gzResponse = new GZResponse();
//            gzResponse.setTitle(UserConstant.LOGIN);
//            gzResponse.setMessage(HttpStatus.OK.toString());
//            gzResponse.setCode(HttpStatus.OK);
//            gzResponse.setDeveloperMessage(UserConstant.LOGINSUCCESS);
//            Mockito.when(userService.loginUser(userRequest)).thenReturn(gzResponse);
//        } catch (Exception ex) {
//            throw new GZException(UserConstant.LOGIN, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
//        }
//    }
//
//    @Test
//    public void loginUserWithEmailFalse() throws GZException {
//        UserRequest userRequest = new UserRequest();
//        GZResponse gzResponse = new GZResponse();
//        gzResponse.setTitle(UserConstant.LOGIN);
//        gzResponse.setMessage(HttpStatus.OK.toString());
//        gzResponse.setCode(HttpStatus.OK);
//        gzResponse.setDeveloperMessage(UserConstant.LOGINSUCCESS);
//        userRequest.setUsername(UserConstant.EMAILFALSE);
//        userRequest.setPassword(UserConstant.PWTRUE);
//        try {
//            Mockito.when(userService.loginUser(userRequest)).thenReturn(gzResponse);
//        } catch (Exception ex) {
//            throw new GZException(UserConstant.LOGIN, HttpStatus.BAD_REQUEST,
//                    HttpStatus.BAD_REQUEST.toString(), ex.getMessage());
//        }
//    }
//
//
//
//}
