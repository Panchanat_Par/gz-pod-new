package com.gz.pod.services.manifestoItem;

import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.GenericInformationRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemConstant;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemService;
import com.gz.pod.categories.manifestoItem.requests.ManifestoItemPutQtyRequest;
import com.gz.pod.categories.manifestoItem.requests.QuantityRequest;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.GenericInformation;
import com.gz.pod.entities.ManifestoItem;
import com.gz.pod.entities.UserTypeMaster;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ManifestoItemServiceTest {

    @InjectMocks
    private ManifestoItemService manifestoItemService;

    @Mock
    private ManifestoItemRepository manifestoItemRepository;

    @Mock
    private CommonService commonService;

    @Mock
    private GenericInformationRepository genericInformationRepository;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(manifestoItemService);
    }

    @Test
    public void updateQuantityItemByJobReturnSuccess () throws Exception {
        GZResponse expect = new GZResponse();
        GZResponse actual;

        ManifestoItemPutQtyRequest manifestoItemPutQtyRequest = new ManifestoItemPutQtyRequest();
        List<QuantityRequest> quantityRequests = new ArrayList<>();
        QuantityRequest quantityRequest = new QuantityRequest();
        quantityRequest.setQuantity(1);
        quantityRequest.setRefId("1");
        quantityRequest.setRemark("remark");
        quantityRequests.add(quantityRequest);
        quantityRequest = new QuantityRequest();
        quantityRequest.setQuantity(2);
        quantityRequest.setRefId("2");
        quantityRequest.setRemark("remark2");
        quantityRequests.add(quantityRequest);
        manifestoItemPutQtyRequest.setQuantities(quantityRequests);
        manifestoItemPutQtyRequest.setUpdateBy(1);
        manifestoItemPutQtyRequest.setFieldType("item");

        //case ManifestoItem Null
        Mockito.when(manifestoItemRepository.findOne(Mockito.anyInt())).thenReturn(null);
        expect.setTitle(ManifestoItemConstant.UPDATE_MANIFESTOITEM);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_NOT_FOUND + " : 1 , 2 " );

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
        //END

        ManifestoItem manifestoItem = new ManifestoItem("DOC-SO", "soItem", "123456", "docDate", "postDate", "12546",
                "1554577", "product", "กล่อง", 10, 10, BigDecimal.ZERO, "remak", 3);
        Mockito.when(manifestoItemRepository.findOne(Mockito.anyInt())).thenReturn(manifestoItem);

        //case getUserTypeMasterForReturn Null
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);
        expect.setTitle(ManifestoItemConstant.UPDATE_MANIFESTOITEM);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND );

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
        //END

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(2);
        userTypeMaster.setName("test");
        userTypeMaster.setLoginId(2);
        userTypeMaster.setUserTypeId(1);

        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());


        GenericInformation genericInformation = new GenericInformation(ManifestoItemConstant.MANIFESTOITEM_DB, ManifestoItemConstant.ACTION_UPDATEQTY,
                1, "remark", 1, date);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(manifestoItemRepository.save(Mockito.any(ManifestoItem.class))).thenReturn(manifestoItem);
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        expect.setTitle(ManifestoItemConstant.UPDATE_MANIFESTOITEM);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestoItemConstant.UPDATE_MANIFESTOITEM_SUCCESS);

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateQuantityItemByJobCaseRefIdNullReturnThrows () throws Exception {
        ManifestoItemPutQtyRequest manifestoItemPutQtyRequest = new ManifestoItemPutQtyRequest();
        GZResponse actual;
        List<QuantityRequest> quantityRequests = new ArrayList<>();
        QuantityRequest quantityRequest = new QuantityRequest();
        quantityRequest.setQuantity(1);
        quantityRequest.setRemark("remark");
        quantityRequests.add(quantityRequest);
        manifestoItemPutQtyRequest.setQuantities(quantityRequests);
        manifestoItemPutQtyRequest.setUpdateBy(1);
        manifestoItemPutQtyRequest.setFieldType("item");

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        GZException expect = new GZException(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.REFID_IS_REQUEST);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateQuantityItemByJobCaseQuantityNullReturnThrows () throws Exception {
        ManifestoItemPutQtyRequest manifestoItemPutQtyRequest = new ManifestoItemPutQtyRequest();
        GZResponse actual;
        List<QuantityRequest> quantityRequests = new ArrayList<>();
        QuantityRequest quantityRequest = new QuantityRequest();
        quantityRequest.setRefId("1");
        quantityRequest.setRemark("remark");
        quantityRequest.setQuantity(null);
        quantityRequests.add(quantityRequest);
        manifestoItemPutQtyRequest.setQuantities(quantityRequests);
        manifestoItemPutQtyRequest.setUpdateBy(1);
        manifestoItemPutQtyRequest.setFieldType("item");

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        GZResponse expect = new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), ManifestoItemConstant.QUANTITY_IS_REQUEST);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateQuantityItemByJobCaseRemarkIsReqWhenQuantityZeroReturnThrows () throws Exception {
        ManifestoItemPutQtyRequest manifestoItemPutQtyRequest = new ManifestoItemPutQtyRequest();
        GZResponse actual;
        List<QuantityRequest> quantityRequests = new ArrayList<>();
        QuantityRequest quantityRequest = new QuantityRequest();
        quantityRequest.setRefId("1");
        quantityRequest.setQuantity(0);
        quantityRequests.add(quantityRequest);
        manifestoItemPutQtyRequest.setQuantities(quantityRequests);
        manifestoItemPutQtyRequest.setUpdateBy(1);
        manifestoItemPutQtyRequest.setFieldType("item");

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        GZResponse expect = new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.REMARK_IS_REQUEST);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateQuantityItemByJobCaseRemarkMust1000CReturnThrows () throws Exception {
        ManifestoItemPutQtyRequest manifestoItemPutQtyRequest = new ManifestoItemPutQtyRequest();
        GZResponse actual;
        List<QuantityRequest> quantityRequests = new ArrayList<>();
        QuantityRequest quantityRequest = new QuantityRequest();
        quantityRequest.setRefId("1");
        quantityRequest.setQuantity(0);
        quantityRequest.setRemark("remarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremarkremark");
        quantityRequests.add(quantityRequest);
        manifestoItemPutQtyRequest.setQuantities(quantityRequests);
        manifestoItemPutQtyRequest.setUpdateBy(1);
        manifestoItemPutQtyRequest.setFieldType("item");

        actual = manifestoItemService.updateQuantityItemByJob(manifestoItemPutQtyRequest);
        GZResponse expect = new GZResponse(ManifestoItemConstant.UPDATE_MANIFESTOITEM, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.REMARK_MUST_MAX_1000);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }
}
