package com.gz.pod.services.manifesto;

import com.gz.pod.categories.MasterReasonRejectManifesto.MasterReasonRejectManifestoRepository;
import com.gz.pod.categories.common.*;
import com.gz.pod.categories.common.responses.ManifestoOwnerResponse;
import com.gz.pod.categories.datafromsoms.DataFromSOMSService;
import com.gz.pod.categories.driver.DriverService;
import com.gz.pod.categories.driver.responses.DriverListResponse;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestoFile.ManifestoFileRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemConstant;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.manifestolist.ManifestolistService;
import com.gz.pod.categories.manifestolist.requests.*;
import com.gz.pod.categories.manifestolist.responses.ManifestoCheckerResponse;
import com.gz.pod.categories.manifestolist.responses.ManifestoResponse;
import com.gz.pod.categories.manifestolist.responses.ShippingResponse;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.podNumberStatus.responses.SaveShippingResponse;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentConstant;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentRepository;
import com.gz.pod.entities.*;
import com.gz.pod.response.GZManifestoListResponsePage;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)
public class ManifestolistServiceTest {

    @InjectMocks
    private ManifestolistService manifestolistService;

    @Mock
    private CommonService commonService;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private DataFromSOMSService dataFromSOMSService;

    @Mock
    private GenericInformationRepository genericInformationRepository;

    @Mock
    private JobStatusRepository jobStatusRepository;

    @Mock
    private ManifestoItemRepository manifestoItemRepository;

    @Mock
    private ManifestoFileRepository manifestoFileRepository;

    @Mock
    private MasterReasonRejectManifestoRepository masterReasonRejectManifestoRepository;

    @Mock
    private ReceiveDocumentRepository receiveDocumentRepository;

    @Mock
    private UserTypeMasterRepository userTypeMasterRepository;

    @Mock
    private MasterStatusRepository masterStatusRepository;

    @Mock
    private DriverService driverService;

    @Test
    public void validateUpdateBoxManifesto() throws Exception
    {
        ManifestoPutBoxQtyRequest manifestoPutBoxQtyRequest = new ManifestoPutBoxQtyRequest();

        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setDriverId(1);
        manifestolist.setPodNumber("092018-0001");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(2);
        userTypeMaster.setName("test");
        userTypeMaster.setLoginId(2);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(manifestolistRepository.save(manifestolist)).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);

        GZResponse expect = new GZResponse();

        ///Case Manifesto not found
        expect.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_BYJOBID);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_NOT_FOUND);

        manifestoPutBoxQtyRequest.setUpdateBy(2);
        manifestoPutBoxQtyRequest.setBoxQty(1);
        manifestoPutBoxQtyRequest.setJobId(100);

        GZResponse actual = manifestolistService.updateBoxManifestoByJob(manifestoPutBoxQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case User not found
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        expect.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_BYJOBID);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        actual = manifestolistService.updateBoxManifestoByJob(manifestoPutBoxQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case job changed
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_UPDATE_MANIFESTO_BOXQTY_JOBCHANGED);

        actual = manifestolistService.updateBoxManifestoByJob(manifestoPutBoxQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case update boxQty success
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setLoginId(2);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        expect.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_BYJOBID);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_SUCCESS);

        actual = manifestolistService.updateBoxManifestoByJob(manifestoPutBoxQtyRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }


    @Test
    public void ManifestoDetailWhenGetSuccess() throws Exception {
        //Arrange
        ManifestoDetailRequest request = new ManifestoDetailRequest();
        request.setManifestoId(ManifestolistConstant.DRIVER_ID);
        request.setLoginId(1);

        UserTypeMaster  userTypeMaster  = new UserTypeMaster();
        userTypeMaster.setName("1");
        userTypeMaster.setId(1);
        userTypeMaster.setUserTypeId(3);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setStatusId(1);
        jobStatus.setRemark("Job Remark");
        jobStatus.setCreatedBy(1);

        MasterStatus masterStatus = new  MasterStatus(1,"pod","JOB.Driver.Status.Confirm");
//        masterStatus.setId(1);
//        masterStatus.setName("JOB.Driver.Status.Confirm");
//        masterStatus.setUserTypeId(1);

        jobStatus.setUserTypeMaster(userTypeMaster);
        jobStatus.setMasterStatus(masterStatus);

        List<ManifestoFile> manifestoFileList = new ArrayList<>();

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("remark");
        genericInformation.setId(1);
        genericInformation.setRefAction("updateBox");
        genericInformation.setRefModule("manifesto");

        List<ManifestoItem> ManifestoItemList = new ArrayList<>();
        ManifestoItem ManifestoItem = new ManifestoItem();
        ManifestoItem.setId(1);
        ManifestoItem.setAmount(new BigDecimal(1));
        ManifestoItem.setManifestoId(1);
        ManifestoItemList.add(ManifestoItem);

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setId(1);
        masterReasonRejectManifesto.setDescription("test");

        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(),Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);
        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(ManifestolistConstant.MANIFESTO_RESPONSE);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);
        Mockito.when(manifestoFileRepository.findBymanifestoIdOrderByIdDesc(Mockito.anyInt())).thenReturn(manifestoFileList);
        Mockito.when(manifestoItemRepository.findBymanifestoId(Mockito.anyInt())).thenReturn(ManifestoItemList);

        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(ManifestolistConstant.MANIFESTO_RESPONSE);

        ReceiveDocument receiveDocument = new ReceiveDocument(1, 1,1,1,  new java.sql.Timestamp(new java.util.Date().getTime()));
        receiveDocument.setUserTypeMaster(userTypeMaster);
        Mockito.when(receiveDocumentRepository.findByManifestoIdAndStatusId(Mockito.anyInt(), Mockito.anyInt())).thenReturn(receiveDocument);
        GZResponse actual = manifestolistService.getManifestoDetailByManifestoId(request);
        GZResponse expect = new GZResponse(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
        );
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        actual = manifestolistService.getManifestoDetailByManifestoId(request);
        expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.GET_DETAIL);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.GET_DETAIL_SUCCESS);
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        GZResponse actualLog = manifestolistService.getManifestoDetailByManifestoId(request);
        GZResponse expectLog = new GZResponse();
        expectLog.setCode(HttpStatus.OK);
        expectLog.setMessage(HttpStatus.OK.toString());
        Assert.assertEquals(expectLog.getCode(), actualLog.getCode());
        Assert.assertEquals(expectLog.getMessage(), actualLog.getMessage());

        // No jobStatus
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(null);
        actual = manifestolistService.getManifestoDetailByManifestoId(request);
        expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.GET_DETAIL);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.GET_DETAIL_SUCCESS);
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(ManifestolistConstant.MANIFESTO_RESPONSE);
        actualLog = manifestolistService.getManifestoDetailByManifestoId(request);
        expectLog = new GZResponse();
        expectLog.setCode(HttpStatus.OK);
        expectLog.setMessage(HttpStatus.OK.toString());
        Assert.assertEquals(expectLog.getCode(), actualLog.getCode());
        Assert.assertEquals(expectLog.getMessage(), actualLog.getMessage());
    }

    @Test
    public void getManifestoDetailFailed() throws Exception {
        GZResponse response;
        // not found
        response = new GZResponse(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                ManifestolistConstant.GET_DETAIL_ERROR, ManifestolistConstant.GET_DETAIL_ERROR);

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt()))
                .thenReturn(null);

        ManifestoDetailRequest request = new ManifestoDetailRequest();
        request.setLoginId(1);
        request.setManifestoId(1);

        GZResponse actual = manifestolistService.getManifestoDetailByManifestoId(request);
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // user not owner
        Manifestolist manifestolist = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist);
        manifestolist.setDriverId(2);

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt()))
            .thenReturn(manifestolist);

        UserTypeMaster  userTypeMaster  = new UserTypeMaster();
        userTypeMaster.setName("1");
        userTypeMaster.setId(1);
        userTypeMaster.setUserTypeId(ManifestolistConstant.DRIVER_ID);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt()))
                .thenReturn(userTypeMaster);

        response = new GZResponse(ManifestolistConstant.GET_DETAIL, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND
        );

        actual = manifestolistService.getManifestoDetailByManifestoId(request);

        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateAddJobCaseCanUpdate() throws Exception {
        ValidateAddJobRequest validateAddJobRequest = new ValidateAddJobRequest();
        validateAddJobRequest.setRefModuleType("so");
        validateAddJobRequest.setRefId("3");
        validateAddJobRequest.setDriverId(77);

        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(3);
        manifestolist.setJobId("3");
        manifestolist.setDriverId(3);
        manifestolist.setPodNumber("092018-0012");
        manifestolist.setDeliveryDate("2018-09-23");
        manifestolist.setManifestoType("So");

        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(77)).thenReturn(null);

        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = manifestolistService.validateAddJob(validateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case 2
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(7);
        Mockito.when(commonService.getUserTypeMasterForReturn(77)).thenReturn(userTypeMaster);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_SUCCESS);

        actual = manifestolistService.validateAddJob(validateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case 3
        String nowDate = java.time.LocalDate.now().toString();

        Manifestolist manifesto = new Manifestolist();

        List<Manifestolist> manifestoList = new ArrayList<>();
        manifestoList.add(manifesto);
        Mockito.when(manifestolistRepository.findAllBydriverIdAndDeliveryDate(userTypeMaster.getId(), nowDate))
                .thenReturn(manifestoList);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_INCOMPLETED);
        jobStatus.setCreatedDateUTC(Timestamp.valueOf(java.time.LocalDateTime.now()));

        Mockito.when(jobStatusRepository.findLastByjobId(Mockito.anyInt())).thenReturn(jobStatus);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_UNABLE_TO_ADD);

        actual = manifestolistService.validateAddJob(validateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateAddJobCaseNotFound() throws Exception {
        ValidateAddJobRequest validateAddJobRequest = new ValidateAddJobRequest();
        validateAddJobRequest.setRefModuleType("so");
        validateAddJobRequest.setRefId("3");
        validateAddJobRequest.setDriverId(77);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(7);

        Mockito.when(commonService.getUserTypeMasterForReturn(anyInt())).thenReturn(userTypeMaster);
        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(null);

        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);

        GZResponse actual = manifestolistService.validateAddJob(validateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateAddJobCaseSONumberNull() throws Exception {
        ValidateAddJobRequest validateAddJobRequest = new ValidateAddJobRequest();

        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = manifestolistService.validateAddJob(validateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateAddJobCaseUpdateFailed() throws Exception {
        UpdateAddJobRequest updateAddJobRequest = new UpdateAddJobRequest();
        updateAddJobRequest.setRefModuleType("so");
        updateAddJobRequest.setRefId("3");
        updateAddJobRequest.setDriverId(33);

        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(3);
        manifestolist.setJobId("3");
        manifestolist.setDriverId(3);
        manifestolist.setPodNumber("092018-0012");
        manifestolist.setDeliveryDate("2018-09-23");
        manifestolist.setManifestoType("So");

        // User not found
        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);

        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = manifestolistService.updateAddJob(updateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Manifesto not found
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(null);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(manifestolistRepository.save(manifestolist)).thenReturn(manifestolist);
        doNothing().when(dataFromSOMSService).UpdatePODNumber(Mockito.anyString(), Mockito.anyInt());

        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);

        actual = manifestolistService.updateAddJob(updateAddJobRequest);

        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Wrong ref type
        ExpectedException thrown = ExpectedException.none();
        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(manifestolist);
        updateAddJobRequest.setRefModuleType("xxx");

        expect.setDeveloperMessage(ManifestolistConstant.UNKNOWN_REF_MODULE_TYPE);

        actual = manifestolistService.updateAddJob(updateAddJobRequest);
        thrown.expectMessage(ManifestolistConstant.UNKNOWN_REF_MODULE_TYPE);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Add duplicate job
        updateAddJobRequest.setRefModuleType("so");
        updateAddJobRequest.setRefId("3");
        updateAddJobRequest.setDriverId(3);

        List<Manifestolist> manifestolists = new ArrayList<>();
        manifestolist.setDriverId(3);
        manifestolists.add(manifestolist);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setJobId(3);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_CONFIRM);
        jobStatus.setCreatedBy(3);
        jobStatus.setCreatedDateUTC(Timestamp.valueOf(java.time.LocalDateTime.now()));

        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(manifestolist);
        Mockito.when(manifestolistRepository.findAllBydriverIdAndDeliveryDate(Mockito.anyInt(), Mockito.anyString()))
            .thenReturn(manifestolists);
        Mockito.when(jobStatusRepository.findLastByjobId(Mockito.anyInt()))
                .thenReturn(jobStatus);

        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_DUPLICATE);
        actual = manifestolistService.updateAddJob(updateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // too long log number
        updateAddJobRequest.setRefModuleType("log");
        updateAddJobRequest.setRefId("213123123123123123");
        updateAddJobRequest.setDriverId(3);

        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);
        actual = manifestolistService.updateAddJob(updateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateAddJobCaseUnableToChange() {

        // Job to be added
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setDriverId(1);
        manifestolist.setJobId("10");

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setCreatedBy(0);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_WAIT);

        // Driver jobs
        Manifestolist holdingManifesto = new Manifestolist();
        holdingManifesto.setId(2);
        holdingManifesto.setDriverId(1);
        holdingManifesto.setJobId("20");

        JobStatus jobStatus1 = new JobStatus();
        jobStatus1.setId(2);
        jobStatus1.setJobId(2);
        jobStatus1.setCreatedBy(1);
        jobStatus1.setStatusId(PODNumberStatusConstant.JOB_CHECKER_APPROVE);

        List<Manifestolist> manifestoList = new ArrayList<>();
        manifestoList.add(holdingManifesto);

        Mockito.when(jobStatusRepository.findLastByjobId(Mockito.eq(1))).thenReturn(jobStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.eq(2), Mockito.anyString())).thenReturn(jobStatus1);
        Mockito.when(manifestolistRepository.findAllBydriverIdAndDeliveryDate(Mockito.anyInt(), Mockito.anyString()))
                .thenReturn(manifestoList);

        GZResponse expect = new GZResponse();
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_UNABLE_TO_ADD);

        GZResponse actual = manifestolistService.checkAddJob(manifestolist, 1, "nowDate");
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getManifestolistByQrCode() throws Exception {
        ValidateAddJobRequest validateAddJobRequest = new ValidateAddJobRequest();
        validateAddJobRequest.setRefModuleType("qrCode");
        validateAddJobRequest.setRefId("3");
        validateAddJobRequest.setDriverId(33);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        // Failed
        GZResponse somFailedResponse = new GZResponse();
        somFailedResponse.setCode(HttpStatus.BAD_REQUEST);
        somFailedResponse.setMessage(HttpStatus.BAD_REQUEST.toString());
        somFailedResponse.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);

        Mockito.when(commonService.getRefDocByQRCodeFromSOMS(Mockito.anyString()))
            .thenReturn(somFailedResponse);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        GZResponse expect = new GZResponse();
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_NOTFOUND);

        GZResponse actual = manifestolistService.getManifestoByRefType("3", "qrCode");
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Success
        // SO
        HashMap data = new HashMap();
        data.put("docRef", "3");
        data.put("docName", "so");

        GZResponse somResponse = new GZResponse();
        somResponse.setCode(HttpStatus.OK);
        somResponse.setMessage(HttpStatus.OK.toString());
        somResponse.setDeveloperMessage(ReceiveDocumentConstant.QRNUMBER_FOUND);
        somResponse.setData(data);

        Mockito.when(commonService.getRefDocByQRCodeFromSOMS(Mockito.anyString()))
                .thenReturn(somResponse);
        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString()))
                .thenReturn(null);
        Mockito.when(manifestolistRepository.findByFulfillId(Mockito.anyInt()))
                .thenReturn(null);

        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.MANIFESTO_SEARCH_SUCCESS);
        actual = manifestolistService.getManifestoByRefType("3", "qrCode");
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Log
        data.put("docName", "log");
        actual = manifestolistService.getManifestoByRefType("3", "qrCode");
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Other type (failed)
        data.put("docName", "other");

        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.UNKNOWN_REF_MODULE_TYPE);

        actual = manifestolistService.getManifestoByRefType("3", "qrCode");
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateAddJobCaseUpdateSuccess() throws Exception {
        // Add by so
        UpdateAddJobRequest updateAddJobRequest = new UpdateAddJobRequest();
        updateAddJobRequest.setRefModuleType("so");
        updateAddJobRequest.setRefId("3");
        updateAddJobRequest.setDriverId(33);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(3);
        manifestolist.setJobId("3");
        manifestolist.setDriverId(3);
        manifestolist.setPodNumber("092018-0012");
        manifestolist.setDeliveryDate("2018-09-23");
        manifestolist.setManifestoType("So");

        Mockito.when(manifestolistRepository.findManifestoBySoNumberNotLog(Mockito.anyString())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(manifestolistRepository.save(manifestolist)).thenReturn(manifestolist);
        doNothing().when(dataFromSOMSService).UpdatePODNumber(Mockito.anyString(), Mockito.anyInt());

        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.UPDATE_ADD_JOB_SUCCESS);

        GZResponse actual = manifestolistService.updateAddJob(updateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        // Add by log
        updateAddJobRequest.setRefModuleType("log");

        manifestolist.setFulfillId(3);
        manifestolist.setManifestoType("Log");

        Mockito.when(manifestolistRepository.findByFulfillId(Mockito.anyInt())).thenReturn(manifestolist);

        expect.setDeveloperMessage(ManifestolistConstant.UPDATE_ADD_JOB_SUCCESS);

        actual = manifestolistService.updateAddJob(updateAddJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateAndUpdateDeleteJobCaseCanDelete() {
        ///Case User not found
        ValidateDeleteJobRequest validateDeleteJobRequest = new ValidateDeleteJobRequest();

        List<Integer> manifestoList = new ArrayList<>();
        manifestoList.add(301);
        manifestoList.add(302);
        validateDeleteJobRequest.setLoginId(70);
        validateDeleteJobRequest.setManifestoId(manifestoList);

        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = manifestolistService.validateDeleteJob(validateDeleteJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case manifestoOwnerList not found
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(70);
        userTypeMaster.setName("driver1");
        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMaster);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_AND_DELETE_JOB_NOTFOUND);

        actual = manifestolistService.validateDeleteJob(validateDeleteJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case Jobs don't have not owner
        ManifestoOwnerResponse manifestoOwner1 = new ManifestoOwnerResponse();
        manifestoOwner1.setManifestoType("SO");
        manifestoOwner1.setRefId("1001");
        manifestoOwner1.setManifestoId(301);
        manifestoOwner1.setOwner(true);

        ManifestoOwnerResponse manifestoOwner2 = new ManifestoOwnerResponse();
        manifestoOwner2.setManifestoType("Log");
        manifestoOwner2.setRefId("1002");
        manifestoOwner2.setManifestoId(302);
        manifestoOwner2.setOwner(false);

        List<ManifestoOwnerResponse> manifestoOwnerList = new ArrayList<>();
        manifestoOwnerList.add(manifestoOwner1);
        manifestoOwnerList.add(manifestoOwner2);

        Mockito.when(commonService.validateOwnerByManifestoId(validateDeleteJobRequest.getManifestoId(), userTypeMaster.getId())).thenReturn(manifestoOwnerList);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(CommonConstant.THERE_ARE_SOME_MANIFESTO_NOT_YOURS);

        actual = manifestolistService.validateDeleteJob(validateDeleteJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case job Status cannot delete
        manifestoOwner2.setManifestoType("Log");
        manifestoOwner2.setRefId("1002");
        manifestoOwner2.setManifestoId(302);
        manifestoOwner2.setOwner(true);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        expect.setCode(HttpStatus.BAD_REQUEST);
        expect.setMessage(HttpStatus.BAD_REQUEST.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_AND_DELETE_JOB_CANNOTDELETE);

        actual = manifestolistService.validateDeleteJob(validateDeleteJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case can delete
        String deliveryDate = java.time.LocalDate.now().toString();
        JobStatus jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_CONFIRM);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(301, deliveryDate)).thenReturn(jobStatus);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(302, deliveryDate)).thenReturn(jobStatus);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.VALIDATE_AND_DELETE_JOB_CAN_DELETE);

        actual = manifestolistService.validateDeleteJob(validateDeleteJobRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        ///Case Update Success
        UpdateDeleteJobRequest request = new UpdateDeleteJobRequest();
        request.setLoginId(validateDeleteJobRequest.getLoginId());
        request.setManifestoId(validateDeleteJobRequest.getManifestoId());

        Manifestolist manifesto = new Manifestolist();
        manifesto.setDriverId(8);
        manifesto.setBoxQty(15);

        jobStatus.setCreatedBy(13);

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifesto);
        Mockito.when(jobStatusRepository.findFirstByjobIdAndStatusIdAndDelivery(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        expect.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.UPDATE_DELETE_JOB_SUCCESS);

        actual = manifestolistService.updateDeleteJob(request);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void rollBackManifestoReturnSuccess() throws Exception {
        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.ROLLBACK);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.ROLLBACK_SUCCESS);

        ManifestoRollbackRequest manifestoRollbackRequest = new ManifestoRollbackRequest();
        manifestoRollbackRequest.setManifestoId(1);

        List<ManifestoItem> manifestoItemList = new ArrayList<>();
        ManifestoItem manifestoItem = new ManifestoItem("DOC-SO", "soItem", "123456", "docDate", "postDate", "12546",
                "1554577", "product", "กล่อง", 10, 10, BigDecimal.ZERO, "remak", 3);
        manifestoItemList.add(manifestoItem);

        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

        GenericInformation genericInformation = new GenericInformation(ManifestoItemConstant.MANIFESTOITEM_DB, ManifestoItemConstant.ACTION_UPDATEQTY,
                1, "remark", 1, date);

        List<ManifestoFile> manifestoFileList = new ArrayList<>();
        ManifestoFile manifestoFile = new ManifestoFile();
        manifestoFile.setId(1);
        manifestoFile.setManifestoId(1);
        manifestoFileList.add(manifestoFile);

        JobStatus jobStatus = new JobStatus(1, 211, "renarj",
                1, date);

        Mockito.when(manifestoItemRepository.findBymanifestoId(Mockito.anyInt())).thenReturn(manifestoItemList);
        Mockito.when(manifestoItemRepository.save(Mockito.any(ManifestoItem.class))).thenReturn(manifestoItem);
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        doNothing().when(genericInformationRepository).delete(genericInformation);
        Mockito.when(manifestoFileRepository.findBymanifestoIdOrderByIdDesc(Mockito.anyInt())).thenReturn(manifestoFileList);
        doNothing().when(manifestoFileRepository).delete(manifestoFile);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);
        doNothing().when(jobStatusRepository).delete(jobStatus);

        GZResponse actual = manifestolistService.rollbackManifesto(manifestoRollbackRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

//    @Test
//    public void getPerPage() {
//        List<Manifestolist> list = new ArrayList<>();
//        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
//                1, "1", "SOMS Order",
//                "SM01", 1, "1",
//                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
//                1, 1, 1, "remark", 1,
//                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson","");
//        list.add(manifestolist);
//        list.add(manifestolist);
//
//        List<Manifestolist> actual = manifestolistService.getPerPage(list, 1, 10);
//        Assert.assertEquals(list.get(0), actual.get(0));
//    }
//
//    @Test
//    public void SortByApproval() {
//        List<PODNumberResponse> podNumberResponseList = new ArrayList<>();
//        podNumberResponseList.add(new PODNumberResponse("102018-1001", "2018-10-11", ManifestolistConstant.POD_DRIVER_AWAIT, ManifestolistConstant.POD_CHECKER_AWAIT, "customer",
//                "shipTo", "route", "remarkDriver", "remarkChecker", "driverName", "namveUserRemark", "nameUserRemarkChecker"));
//        podNumberResponseList.add(new PODNumberResponse("102018-1001", "2018-10-11", ManifestolistConstant.POD_DRIVER_AWAIT, ManifestolistConstant.POD_CHECKER_AWAIT, "customer",
//                "shipTo2", "route2", "remarkDriver2", "remarkChecker2", "driverName", "namveUserRemark", "nameUserRemarkChecker"));
//
//        List<PODNumberResponse> actual = manifestolistService.SortByApproval(podNumberResponseList);
//        Assert.assertEquals(podNumberResponseList.get(0), actual.get(0));
//    }
//
//    @Test
//    public void SortByApprovalNoPOD_AWAIT() {
//        List<PODNumberResponse> podNumberResponseList = new ArrayList<>();
//        podNumberResponseList.add(new PODNumberResponse("102018-1001", "2018-10-11", "", "", "customer",
//                "shipTo", "route", "remarkDriver", "remarkChecker", "driverName", "namveUserRemark", "nameUserRemarkChecker"));
//        podNumberResponseList.add(new PODNumberResponse("102018-1001", "2018-10-11", "1", "2", "customer",
//                "shipTo2", "route2", "remarkDriver2", "remarkChecker2", "driverName", "namveUserRemark", "nameUserRemarkChecker"));
//
//        List<PODNumberResponse> actual = manifestolistService.SortByApproval(podNumberResponseList);
//        Assert.assertEquals(podNumberResponseList.get(0), actual.get(0));
//    }
//
//    @Test
//    public void SortByApprovalByStatusNull() {
//        List<PODNumberResponse> podNumberResponseList = new ArrayList<>();
//        podNumberResponseList.add(new PODNumberResponse("102018-1001", "2018-10-11", "", "", "customer",
//                "shipTo", "route", "remarkDriver", "remarkChecker", "driverName", "namveUserRemark", "nameUserRemarkChecker"));
//        podNumberResponseList.add(new PODNumberResponse("102018-1001", "2018-10-11", null, null, "customer",
//                "shipTo2", "route2", "remarkDriver2", "remarkChecker2", "driverName", "namveUserRemark" , "nameUserRemarkChecker"));
//
//        List<PODNumberResponse> actual = manifestolistService.SortByApproval(podNumberResponseList);
//        Assert.assertEquals(podNumberResponseList.get(0), actual.get(0));
//    }

    @Test
    public void PrepareUserName() throws Exception {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setName("bay");

        Mockito.when(commonService.getUserTypeMasterById(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);

        String actual = manifestolistService.PrepareUserName(1);
        Assert.assertEquals(userTypeMaster.getName(), actual);

    }

    @Test
    public void PrepareGenericCaseREMARK_REASON_ID_NOT_FOUND() {
        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);
        genericInformation.setRefAction("updateBox");
        genericInformation.setRefModule("manifesto");

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setId(1);
        masterReasonRejectManifesto.setDescription("test");

        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(null);

        String actual = manifestolistService.PrepareGeneric(1, ManifestolistConstant.MANIFESTO_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN);
        Assert.assertEquals(PODNumberStatusConstant.REMARK_REASON_ID_NOT_FOUND, actual);

        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);
        actual = manifestolistService.PrepareGeneric(1, ManifestolistConstant.MANIFESTO_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN);
        Assert.assertEquals(masterReasonRejectManifesto.getDescription(), actual);

    }

    @Test
    public void PrepareRemarkJobStatusCaseNull() {
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(null);
        String actual = manifestolistService.PrepareRemarkJobStatus(1, "2018-10-10");
        Assert.assertEquals(null, actual);
    }

    @Test
    public void PrepareJobStatusDescCaseNull() {
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(null);
        String actual = manifestolistService.PrepareJobStatusDesc(1, "2018-10-10");
        Assert.assertEquals(null, actual);
    }

    @Test
    public void saveShippingReturnSuccess() throws Exception {
        SaveShippingResponse expect;

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        ManifestoOwnerResponse data = new ManifestoOwnerResponse();
        data.setManifestoId(1);
        data.setManifestoType("Order");
        data.setOwner(true);
        data.setRefId("test");
        manifestoOwnerResponseList.add(data);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(new JobStatus());

        expect = new SaveShippingResponse(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestolistConstant.SAVE_MANIFESTO_SUCCESS);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        AddManifestoRequest addManifestoRequest = new AddManifestoRequest();
        addManifestoRequest.setManifestoIds(manifestoIds);
        addManifestoRequest.setLoginId(1);

        SaveShippingResponse actual = manifestolistService.saveShipping(addManifestoRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void saveShippingReturnFalseCaseUserNotFound() throws Exception {

        UserTypeMaster userTypeMaster = null;
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        SaveShippingResponse expect = new SaveShippingResponse(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        AddManifestoRequest addManifestoRequest = new AddManifestoRequest();
        addManifestoRequest.setManifestoIds(manifestoIds);
        addManifestoRequest.setLoginId(1);

        SaveShippingResponse actual = manifestolistService.saveShipping(addManifestoRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateSaveShippingReturnFalseCaseManifestoNotFound() throws Exception {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);

        SaveShippingResponse expect = new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.MANIFESTO_NOT_FOUND);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        AddManifestoRequest addManifestoRequest = new AddManifestoRequest();
        addManifestoRequest.setManifestoIds(manifestoIds);
        addManifestoRequest.setLoginId(1);

        SaveShippingResponse actual = manifestolistService.validateSaveShipping(addManifestoRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateSaveShippingReturnFalseNotOwer() throws Exception {
        SaveShippingResponse expect;

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        ManifestoOwnerResponse data = new ManifestoOwnerResponse();
        data.setManifestoId(1);
        data.setManifestoType("Order");
        data.setOwner(false);
        data.setRefId("test");
        manifestoOwnerResponseList.add(data);
        data = new ManifestoOwnerResponse();
        data.setManifestoId(2);
        data.setManifestoType("Order");
        data.setOwner(true);
        data.setRefId("test");
        manifestoOwnerResponseList.add(data);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(new JobStatus());

        expect = new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_FAILED);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        AddManifestoRequest addManifestoRequest = new AddManifestoRequest();
        addManifestoRequest.setManifestoIds(manifestoIds);
        addManifestoRequest.setLoginId(1);

        SaveShippingResponse actual = manifestolistService.validateSaveShipping(addManifestoRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateSaveShippingUserNotFound() throws Exception {
        SaveShippingResponse expect;

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        ManifestoOwnerResponse data = new ManifestoOwnerResponse();
        data.setManifestoId(1);
        data.setManifestoType("Order");
        data.setOwner(false);
        data.setRefId("test");
        manifestoOwnerResponseList.add(data);
        data = new ManifestoOwnerResponse();
        data.setManifestoId(2);
        data.setManifestoType("Order");
        data.setOwner(true);
        data.setRefId("test");
        manifestoOwnerResponseList.add(data);

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(new JobStatus());

        expect = new SaveShippingResponse(ManifestolistConstant.VALIDATE_MANIFESTO, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        AddManifestoRequest addManifestoRequest = new AddManifestoRequest();
        addManifestoRequest.setManifestoIds(manifestoIds);
        addManifestoRequest.setLoginId(1);

        SaveShippingResponse actual = manifestolistService.validateSaveShipping(addManifestoRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getManifestoList() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_CONFIRM, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(PODNumberStatusConstant.JOB_DRIVER_CONFIRM, "JOB", "confirm");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPING);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // today list
        manifestolistRequest.setTab(ManifestolistConstant.TODAYLIST);
        masterStatus.setId(PODNumberStatusConstant.JOB_DRIVER_SO_WAIT);
        actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // delivered
        manifestolistRequest.setTab(ManifestolistConstant.DELIVERED);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);
        actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        manifestolistRequest.setTab(ManifestolistConstant.DELIVERED);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT);
        actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // shipped
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPED);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getManifestoListFailed() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);

        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_CONFIRM, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(PODNumberStatusConstant.JOB_DRIVER_CONFIRM, "JOB", "confirm");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.BAD_REQUEST);
        response.setMessage(HttpStatus.BAD_REQUEST.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPING);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // not found manifesto
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString()))
                .thenReturn(new ArrayList<>());

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);

        actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getDriverManifestoCheckerFailed() {
        GZResponse response = new GZResponse();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);

        // User not found
        // Tab shipping
        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 220, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(220, "JOB", "confirm");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.BAD_REQUEST);
        response.setMessage(HttpStatus.BAD_REQUEST.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        DriverManifestolistCheckerRequest request = new DriverManifestolistCheckerRequest();
        request.setDate("*");
        request.setLoginId(1);
        request.setTab("shipping");

        GZResponse actual = manifestolistService.getDriverManifestoListChecker(request);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // Manifesto not found
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(new ArrayList<>());
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.MANIFESTO_NOT_FOUND);

        actual = manifestolistService.getDriverManifestoListChecker(request);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // invalid user
        request.setDriverId(10);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster)
            .thenReturn(null);

        actual = manifestolistService.getDriverManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.BAD_REQUEST);
        response.setMessage(HttpStatus.BAD_REQUEST.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void getDriverManifestoCheckerSuccess() {
        GZResponse response = new GZResponse();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        // Tab wait
        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Manifestolist mani3 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, mani3);
        mani3.setDelayDate(null);
        list.add(mani3);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 220, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(220, "JOB", "confirm");
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        // mock driver status
        DriverListResponse driverListResponse = new DriverListResponse();
        driverListResponse.setLoginId(userTypeMaster.getLoginId());
        driverListResponse.setName(userTypeMaster.getName());
        Mockito.when(driverService.getDriverDetail(Mockito.anyString(), Mockito.anyInt())).thenReturn(driverListResponse);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        DriverManifestolistCheckerRequest request = new DriverManifestolistCheckerRequest();
        request.setDate("*");
        request.setLoginId(1);
        request.setTab("wait");

        GZResponse actual = manifestolistService.getDriverManifestoListChecker(request);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // Tab approved
        JobStatus jobStatus2 = new JobStatus(1, 2113, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus2 = new MasterStatus(2113, "JOB", "confirm");
        jobStatus2.setMasterStatus(masterStatus2);
        jobStatus2.setUserTypeMaster(userTypeMaster);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus2);

        DriverManifestolistCheckerRequest request2 = new DriverManifestolistCheckerRequest();
        request2.setLoginId(1);
        request2.setTab("approved");

        GZResponse actual2 = manifestolistService.getDriverManifestoListChecker(request2);

        GZResponse response2 = new GZResponse();
        response2.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response2.setCode(HttpStatus.OK);
        response2.setMessage(HttpStatus.OK.toString());
        response2.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        Assert.assertEquals(response2.getTitle(), actual2.getTitle());
        Assert.assertEquals(response2.getCode(), actual2.getCode());
        Assert.assertEquals(response2.getMessage(), actual2.getMessage());
        Assert.assertEquals(response2.getDeveloperMessage(), actual2.getDeveloperMessage());
    }

    @Test
    public void getDriverManifestoCheckerDelay() {
        GZResponse response = new GZResponse();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        List<Manifestolist> list = new ArrayList<>();

        Manifestolist manifestolist = ManifestolistConstant.MANIFESTO_RESPONSE;
        manifestolist.setDeliveryDate("2019-01-04");
        manifestolist.setDelayDate("2019-01-05");

        list.add(manifestolist);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 220, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(220, "JOB", "confirm");
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        // mock driver status
        DriverListResponse driverListResponse = new DriverListResponse();
        driverListResponse.setLoginId(userTypeMaster.getLoginId());
        driverListResponse.setName(userTypeMaster.getName());
        Mockito.when(driverService.getDriverDetail(Mockito.anyString(), Mockito.anyInt())).thenReturn(driverListResponse);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        DriverManifestolistCheckerRequest request = new DriverManifestolistCheckerRequest();
        request.setLoginId(1);
        request.setTab("wait");

        GZResponse actual = manifestolistService.getDriverManifestoListChecker(request);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getSearchManifestolistCheckerSuccess() {
        GZResponse response = new GZResponse();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.CHECKER);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.eq(1))).thenReturn(userTypeMaster);

        // Mock driver
        UserTypeMaster driver = new UserTypeMaster();
        driver.setId(10);
        driver.setName("driver");
        driver.setUserTypeId(PODNumberStatusConstant.DRIVER);

        // Mock manifestolist
        List<ManifestolistWithDriver> list = new ArrayList<>();

        ManifestolistWithDriver manifestolist1 = new ManifestolistWithDriver();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestolist1.setDeliveryDate("2019-01-04");
        manifestolist1.setDelayDate("2019-01-04");
        manifestolist1.setUserTypeMaster(driver);
        manifestolist1.setPodNumber("pod1");

        ManifestolistWithDriver manifestolist2 = new ManifestolistWithDriver();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist2);
        manifestolist2.setDeliveryDate("2019-01-04");
        manifestolist2.setDelayDate("2019-01-04");
        manifestolist2.setUserTypeMaster(driver);
        manifestolist2.setPodNumber("pod1");

        list.add(manifestolist1);
        list.add(manifestolist2);

        Mockito.when(manifestolistRepository.findByPodNumberIsNotNullAndDriverIdIsNotNull()).thenReturn(list);

        // do
        ManifestolistSearchRequest request = new ManifestolistSearchRequest();
        request.setLoginId(1);
        request.setPage(1);
        request.setPerPage(10);

        GZResponse actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // have date
        request.setDate("2019-01-04");

        actual = manifestolistService.getManifestoListChecker(request);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());


        // found no job
        Mockito.when(manifestolistRepository.findByPodNumberIsNotNullAndDriverIdIsNotNull()).thenReturn(new ArrayList<>());
        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }


    @Test
    public void getManifestoListCheckerUserError() {
        GZResponse response = new GZResponse();

        // no user
        ManifestolistCheckerRequest request = new ManifestolistCheckerRequest();
        request.setLoginId(123);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);

        GZResponse actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.BAD_REQUEST);
        response.setMessage(HttpStatus.BAD_REQUEST.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_FOUND);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());


        // user not allow
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.DRIVER);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.BAD_REQUEST);
        response.setMessage(HttpStatus.BAD_REQUEST.toString());
        response.setDeveloperMessage(PODNumberStatusConstant.USER_NOT_ALLOW);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // invalid page
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.CHECKER);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        // Mock driver
        UserTypeMaster driver = new UserTypeMaster();
        driver.setId(10);
        driver.setName("driver");
        driver.setUserTypeId(PODNumberStatusConstant.DRIVER);

        // Mock manifestolist
        List<ManifestolistWithDriver> list = new ArrayList<>();

        ManifestolistWithDriver manifestolist1 = new ManifestolistWithDriver();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestolist1.setDeliveryDate("2019-01-04");
        manifestolist1.setDelayDate("2019-01-04");
        manifestolist1.setUserTypeMaster(driver);
        manifestolist1.setPodNumber("pod1");

        ManifestolistWithDriver manifestolist2 = new ManifestolistWithDriver();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist2);
        manifestolist2.setDeliveryDate("2019-01-04");
        manifestolist2.setDelayDate("2019-01-04");
        manifestolist2.setUserTypeMaster(driver);
        manifestolist2.setPodNumber("pod1");

        list.add(manifestolist1);
        list.add(manifestolist2);

        Mockito.when(manifestolistRepository.findByPodNumberIsNotNullAndDriverIdIsNotNull()).thenReturn(list);

        // do
        request.setLoginId(1);
        request.setPage(100);
        request.setPerPage(10);

        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.BAD_REQUEST);
        response.setMessage(HttpStatus.BAD_REQUEST.toString());
        response.setDeveloperMessage(ManifestolistConstant.EXCEED_PAGE_NUMBER);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void responseShippedListConfirmCase() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_3);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 2113, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(2113, "JOB", "JOB.Driver.Confirm.And.Approve");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPED);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void responseShippedListRejectCase() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_3);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 2114, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(2114, "JOB", "JOB.Driver.Confirm.And.Approve");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPED);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void responseShippedStartCase() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);;
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 2115, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(2115, "JOB", "JOB.Driver.Confirm.And.Approve");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPED);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void responseShippedManifestNotFound() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        List<Manifestolist> list = new ArrayList<>();
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);;
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);

        JobStatus jobStatus = new JobStatus(1, 220, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(220, "JOB", "JOB.Driver.Confirm.And.Approve");
        jobStatus.setMasterStatus(masterStatus);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPED);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void responseShippedAllCase() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        // set Manifestolist
        List<Manifestolist> list = new ArrayList<>();
        Manifestolist manifestolist1 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        Manifestolist manifestolist2 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_LOG, manifestolist2);
        manifestolist1.setDelayDate(null);
        manifestolist1.setJobId("1");
        manifestolist2.setJobId("2");
        list.add(manifestolist1);
        list.add(manifestolist2);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_3);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);
        // set job
        JobStatus jobStatus = new JobStatus(1, 2113, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(2113, "JOB", "JOB.Driver.Confirm.And.Approve");
        jobStatus.setMasterStatus(masterStatus);

        JobStatus jobStatus2 = new JobStatus(2, 2115, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus2 = new MasterStatus(2115, "JOB", "JOB.Driver.Start");
        jobStatus2.setMasterStatus(masterStatus2);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString()))
                .thenReturn(jobStatus).thenReturn(jobStatus).thenReturn(jobStatus).thenReturn(jobStatus2);
        // set expect response
        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPED);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void responseManifestNotFoundCaseNotEqualShippedTab() throws Exception {
        GZManifestoListResponsePage response = new GZManifestoListResponsePage();
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        // set Manifestolist
        List<Manifestolist> list = new ArrayList<>();
        Manifestolist manifestolist1 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        Manifestolist manifestolist2 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_LOG, manifestolist2);
        Manifestolist manifestolist3 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_2, manifestolist3);
        Manifestolist manifestolist4 = new Manifestolist();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_3, manifestolist4);
        manifestolist1.setDelayDate(null);
        manifestolist1.setJobId("1");
        manifestolist2.setJobId("2");
        manifestolist2.setDelayDate(null);
        manifestolist3.setDelayDate(null);
        manifestolist4.setDelayDate(null);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_2);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_3);
        list.add(ManifestolistConstant.MANIFESTO_RESPONSE_LOG);
        Mockito.when(manifestolistRepository.findBydriverIdAndDeliveryDateAndPodNumberIsNotNull(Mockito.anyInt(), Mockito.anyString())).thenReturn(list);
        // set job
        JobStatus jobStatus = new JobStatus(1, 2113, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus = new MasterStatus(2113, "JOB", "JOB.Driver.Confirm.And.Approve");
        jobStatus.setMasterStatus(masterStatus);

        JobStatus jobStatus2 = new JobStatus(2, 2115, null, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        MasterStatus masterStatus2 = new MasterStatus(2115, "JOB", "JOB.Driver.Start");
        jobStatus2.setMasterStatus(masterStatus2);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString()))
                .thenReturn(jobStatus).thenReturn(jobStatus).thenReturn(jobStatus).thenReturn(jobStatus2);
        // set expect response
        response.setTitle(ManifestolistConstant.SHIPPING_LIST);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);

        ManifestolistRequest manifestolistRequest = new ManifestolistRequest();
        manifestolistRequest.setLoginId(1);
        manifestolistRequest.setTab(ManifestolistConstant.SHIPPING);
        GZManifestoListResponsePage actual = manifestolistService.getManifestoList(manifestolistRequest);
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getSearchManifestoListSuccess() {
        GZResponse response = new GZResponse();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.CHECKER);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.eq(1))).thenReturn(userTypeMaster);

        // Mock driver
        UserTypeMaster driver = new UserTypeMaster();
        driver.setId(10);
        driver.setName("driver");
        driver.setUserTypeId(PODNumberStatusConstant.DRIVER);

        // Mock manifestolist
        List<ManifestolistWithDriver> list = new ArrayList<>();

        ManifestolistWithDriver manifestolist1 = new ManifestolistWithDriver();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestolist1.setDeliveryDate("2019-01-04");
        manifestolist1.setDelayDate("2019-01-04");
        manifestolist1.setUserTypeMaster(driver);
        manifestolist1.setPodNumber("pod1");

        ManifestolistWithDriver manifestolist2 = new ManifestolistWithDriver();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist2);
        manifestolist2.setDeliveryDate("2019-01-04");
        manifestolist2.setDelayDate("2019-01-04");
        manifestolist2.setUserTypeMaster(driver);
        manifestolist2.setPodNumber("pod1");

        list.add(manifestolist1);
        list.add(manifestolist2);

        Mockito.when(manifestolistRepository.findByPodNumberIsNotNullAndDriverIdIsNotNull()).thenReturn(list);

        // search by driver name
        List<UserTypeMaster> driverList = new ArrayList<>();
        driverList.add(driver);
        Mockito.when(userTypeMasterRepository.findBynameContainingAndUserTypeId(Mockito.anyString(), Mockito.anyInt())).thenReturn(driverList);

        Mockito.when(manifestolistRepository.findJobWithDriverByListDriverId(Mockito.anyList())).thenReturn(list);

        ManifestolistSearchRequest request = new ManifestolistSearchRequest();
        request.setLoginId(1);
        request.setPage(1);
        request.setPerPage(10);
        request.setFieldType("driver");
        request.setValue("driver");

        GZResponse actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // search by driver not found
        Mockito.when(userTypeMasterRepository.findBynameContainingAndUserTypeId(Mockito.anyString(), Mockito.anyInt())).thenReturn(new ArrayList<>());
        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // search by job
        request.setFieldType("job");
        request.setValue("SM01");

        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // search by job not found
        request.setFieldType("job");
        request.setValue("xxxx");

        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.MANIFESTO_NOT_FOUND);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // search by route
        request.setFieldType("route");
        request.setValue("1");

        Mockito.when(manifestolistRepository.findJobWithDriverByrouteNameContaining(Mockito.anyString())).thenReturn(list);

        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

        // search by sapCode
        request.setFieldType("sapCode");
        request.setValue("1");

        Mockito.when(manifestolistRepository.findJobWithDriverBySapCustCodeContaining(Mockito.anyString())).thenReturn(list);

        actual = manifestolistService.getManifestoListChecker(request);

        response.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        response.setCode(HttpStatus.OK);
        response.setMessage(HttpStatus.OK.toString());
        response.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void groupByPODNumberDifferentDataTest() {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("driver");
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.DRIVER);

        List<ManifestoResponse > list = new ArrayList<>();

        ManifestoResponse manifestolist1 = new ManifestoResponse();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);

        MasterStatus masterStatus = new MasterStatus(222, "JOB", "confirm");
        JobStatus jobStatus1 = new JobStatus();
        jobStatus1.setId(1);
        jobStatus1.setJobId(1);
        jobStatus1.setStatusId(1);
        jobStatus1.setRemark("Job Remark");
        jobStatus1.setCreatedBy(1);
        jobStatus1.setMasterStatus(masterStatus);
        jobStatus1.setUserTypeMaster(userTypeMaster);

        JobStatus jobStatus2 = new JobStatus();
        jobStatus2.setId(1);
        jobStatus2.setJobId(1);
        jobStatus2.setStatusId(1);
        jobStatus2.setRemark("Job Remark");
        jobStatus2.setCreatedBy(1);
        jobStatus2.setUserTypeMaster(userTypeMaster);

        manifestolist1.setDeliveryDate("2019-01-04");
        manifestolist1.setDelayDate("2019-01-04");
        manifestolist1.setPodNumber("pod1");
        manifestolist1.setManifestoType("So");
        manifestolist1.setJobStatus(jobStatus1);

        ManifestoResponse  manifestolist2 = new ManifestoResponse ();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist2);
        manifestolist2.setDeliveryDate("2019-01-04");
        manifestolist2.setDelayDate("2019-01-04");
        manifestolist2.setPodNumber("pod1");
        manifestolist2.setManifestoType("Log");

        ManifestoResponse  manifestolist3 = new ManifestoResponse ();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist3);
        manifestolist3.setDeliveryDate("2019-01-04");
        manifestolist3.setDelayDate("2019-01-04");
        manifestolist3.setPodNumber("pod1");
        manifestolist3.setManifestoType("Log");
        manifestolist3.setJobStatus(jobStatus2);

        list.add(manifestolist1);
        list.add(manifestolist2);
        list.add(manifestolist3);

        // mock generic info
        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(genericInformation);

        MasterReasonRejectManifesto rejectManifesto = new MasterReasonRejectManifesto();
        rejectManifesto.setDescription("test");

        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(rejectManifesto)
                .thenReturn(null);

        // do
        List<ShippingResponse> actual = manifestolistService.groupByPODNumber(list, "", userTypeMaster);

        Assert.assertEquals(actual.size(), 1);
        Assert.assertEquals(actual.get(0).getPodNumber(), "pod1");
    }

    @Test
    public void getJobRemarkDetailForCheckerTest() {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("driver");
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.FINOPS);

        ManifestoResponse manifestolist1 = new ManifestoResponse();
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        MasterStatus masterStatus = new MasterStatus(222, "JOB", "confirm");
        JobStatus jobStatus1 = new JobStatus();
        jobStatus1.setId(1);
        jobStatus1.setJobId(1);
        jobStatus1.setStatusId(1);
        jobStatus1.setRemark("Job Remark");
        jobStatus1.setCreatedBy(1);
        jobStatus1.setMasterStatus(masterStatus);
        jobStatus1.setUserTypeMaster(userTypeMaster);

        JobStatus jobStatus2 = new JobStatus();
        jobStatus2.setId(1);
        jobStatus2.setJobId(1);
        jobStatus2.setStatusId(1);
        jobStatus2.setRemark("Job Remark");
        jobStatus2.setCreatedBy(1);
        jobStatus2.setUserTypeMaster(userTypeMaster);

        manifestolist1.setDeliveryDate("2019-01-04");
        manifestolist1.setDelayDate("2019-01-04");
        manifestolist1.setPodNumber("pod1");
        manifestolist1.setManifestoType("So");
        manifestolist1.setJobStatus(jobStatus1);

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(genericInformation);

        MasterReasonRejectManifesto rejectManifesto = new MasterReasonRejectManifesto();
        rejectManifesto.setDescription("test");
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(rejectManifesto);
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(genericInformation);

        ReceiveDocument receiveDocument = new ReceiveDocument();
        receiveDocument.setUserTypeMaster(userTypeMaster);
        Mockito.when(receiveDocumentRepository.findByManifestoIdAndStatusId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(receiveDocument);

        ManifestoCheckerResponse actual = manifestolistService.getJobRemarkDetailForChecker(manifestolist1, userTypeMaster);

        Assert.assertEquals(actual.getRejectRemark(), "test");

        // no reject remark
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(null);

        actual = manifestolistService.getJobRemarkDetailForChecker(manifestolist1, userTypeMaster);
        Assert.assertEquals(actual.getRejectRemark(), null);
    }

    @Test
    public void getCountApprovalItemByGroupTest() {

        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
        jobStatus.setMasterStatus(masterStatus);

        ManifestoResponse manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);

        ManifestoResponse manifestolist2 = new ManifestoResponse();
        manifestolist2.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_LOG, manifestolist2);

        List<ManifestoResponse> list = new ArrayList<>();
        list.add(manifestolist1);
        list.add(manifestolist2);

        List<Integer> actual = manifestolistService.getCountApprovalItemByGroup(list);

        Assert.assertEquals((int) actual.get(2), 1);
        Assert.assertEquals((int) actual.get(3), 1);
    }

    @Test
    public void checkShowStatusPODNumberCaseNull() {
        List<ManifestoResponse> manifestoListInApprove = new ArrayList<>();
        Integer actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 1, ManifestolistConstant.SHIPPED);

        Assert.assertNull(actual);

        //-------------------------- case JOB_CHECKER_NOT_CONFIRM ------------------------

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);

        ManifestoResponse manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 1, ManifestolistConstant.SHIPPED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM, (int) actual);

        //-------------------------- case JOB_DRIVER_CONFIRM ------------------------

        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_CONFIRM);

        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 1, ManifestolistConstant.SHIPPED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_DRIVER_CONFIRM, (int) actual);

        //-------------------------- case JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER ------------------------

        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);

        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 1, ManifestolistConstant.CHECKER_APPROVED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER, (int) actual);

        //-------------------------- case JOB_CHECKER_APPROVE ------------------------

        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_APPROVE);

        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.SHIPPED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_CHECKER_APPROVE, (int) actual);

        //-------------------------- case JOB_CHECKER_APPROVE ------------------------

        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);

        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.SHIPPED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE, (int) actual);

        //-------------------------- case JOB_DRIVER_START ------------------------

        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_START);

        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.SHIPPED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_DRIVER_START, (int) actual);

        //-------------------------- case JOB_DRIVER_END ------------------------

        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_END);

        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.SHIPPED);

        Assert.assertEquals(PODNumberStatusConstant.JOB_DRIVER_END, (int) actual);

        //-------------------------- case JOB_DRIVER_SO_CONFIRMALLDELIVERY ------------------------

        manifestoListInApprove = new ArrayList<>();
        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_LOG, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.CHECKER_APPROVED);

        Assert.assertEquals(PODNumberStatusConstant.PODNUMBERSTATUS_DRIVER_CONFIRMALLDELIVERY, (int) actual);

        //-------------------------- case JOB_DRIVER_SO_CONFIRMALLDELIVERY ------------------------

        manifestoListInApprove = new ArrayList<>();
        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_LOG_REJECT);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE_LOG, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.CHECKER_APPROVED);

        Assert.assertEquals(PODNumberStatusConstant.PODNUMBERSTATUS_DRIVER_REJECTALLDELIVERY, (int) actual);

        //-------------------------- case JOB_DRIVER_SO_CONFIRMALLDELIVERY ------------------------

        manifestoListInApprove = new ArrayList<>();
        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_PARTIALDELIVERY);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REJECT);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        jobStatus = new JobStatus();
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE);
        manifestolist1 = new ManifestoResponse();
        manifestolist1.setJobStatus(jobStatus);
        BeanUtils.copyProperties(ManifestolistConstant.MANIFESTO_RESPONSE, manifestolist1);
        manifestoListInApprove.add(manifestolist1);

        actual = manifestolistService.checkShowStatusPODNumber(manifestoListInApprove, 2, ManifestolistConstant.CHECKER_APPROVED);

        Assert.assertEquals(PODNumberStatusConstant.PODNUMBERSTATUS_DRIVER_PARTIALDELIVERY, (int) actual);
    }
}
