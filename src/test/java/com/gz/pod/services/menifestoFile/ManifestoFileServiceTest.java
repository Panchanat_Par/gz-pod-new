package com.gz.pod.services.menifestoFile;

import ch.qos.logback.core.encoder.ByteArrayUtil;
import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.manifestoFile.ManifestoFileConstant;
import com.gz.pod.categories.manifestoFile.ManifestoFileRepository;
import com.gz.pod.categories.manifestoFile.ManifestoFileService;
import com.gz.pod.categories.manifestoFile.requests.DeleteFileRequest;
import com.gz.pod.categories.manifestoFile.requests.GetUploadFileRequest;
import com.gz.pod.categories.manifestoFile.requests.UploadFileRequest;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.ManifestoFile;
import com.gz.pod.entities.Manifestolist;
import com.gz.pod.entities.UserTypeMaster;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class ManifestoFileServiceTest {

    @InjectMocks
    private ManifestoFileService manifestoFileService;

    @Mock
    private ManifestoFileRepository manifestoFileRepository;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private CommonService commonService;

    @Mock
    private UserTypeMasterRepository userTypeMasterRepository;

    @Test
    public void deleteFileByJobReturnSuccess() throws Exception {
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setDeveloperMessage(ManifestoFileConstant.DEL_MANIFESTOFILE_SUCCESS);

        DeleteFileRequest request = new DeleteFileRequest();
        request.setManifestoFileId(1);
        request.setLoginId(11);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setName("test_driverName");
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setUserTypeId(1);

        Integer driverId = 1;

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(manifestoFileRepository.findCreateBy(Mockito.anyInt())).thenReturn(driverId);

        GZResponse actual = manifestoFileService.deleteFileByJob(request);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        GZResponse gzResponse2 = new GZResponse();
        gzResponse2.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
        gzResponse2.setCode(HttpStatus.BAD_REQUEST);
        gzResponse2.setMessage(HttpStatus.BAD_REQUEST.toString());
        gzResponse2.setDeveloperMessage(ManifestoFileConstant.NOT_OWNER_MANIFESTOFILE);

        DeleteFileRequest request2 = new DeleteFileRequest();
        request2.setManifestoFileId(1);
        request2.setLoginId(12);

        UserTypeMaster userTypeMaster2 = new UserTypeMaster();
        userTypeMaster2.setName("test_driverName2");
        userTypeMaster2.setId(2);
        userTypeMaster2.setLoginId(22);
        userTypeMaster2.setUserTypeId(1);

        Integer driverIdNotOwner = 3;

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMaster2);
        Mockito.when(manifestoFileRepository.findCreateBy(Mockito.anyInt())).thenReturn(driverIdNotOwner);

        GZResponse actual2 = manifestoFileService.deleteFileByJob(request2);

        Assert.assertEquals(gzResponse2.getTitle(), actual2.getTitle());
        Assert.assertEquals(gzResponse2.getMessage(), actual2.getMessage());
        Assert.assertEquals(gzResponse2.getCode(), actual2.getCode());
        Assert.assertEquals(gzResponse2.getDeveloperMessage(), actual2.getDeveloperMessage());

        UserTypeMaster userTypeChecker= new UserTypeMaster();
        userTypeChecker.setName("test_checker");
        userTypeChecker.setId(2);
        userTypeChecker.setLoginId(33);
        userTypeChecker.setUserTypeId(2);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeChecker);
        Mockito.when(manifestoFileRepository.findCreateBy(Mockito.anyInt())).thenReturn(driverIdNotOwner);

        Assert.assertEquals(gzResponse2.getTitle(), actual2.getTitle());
        Assert.assertEquals(gzResponse2.getMessage(), actual2.getMessage());
        Assert.assertEquals(gzResponse2.getCode(), actual2.getCode());
        Assert.assertEquals(gzResponse2.getDeveloperMessage(), actual2.getDeveloperMessage());

        GZResponse gzResponse3 = new GZResponse();
        gzResponse3.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
        gzResponse3.setCode(HttpStatus.BAD_REQUEST);
        gzResponse3.setMessage(HttpStatus.BAD_REQUEST.toString());
        gzResponse3.setDeveloperMessage(ManifestoFileConstant.NOT_FOUND_MANIFESTOFILE);

        DeleteFileRequest request3 = new DeleteFileRequest();
        request3.setManifestoFileId(4);
        request3.setLoginId(44);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(null);
        Mockito.when(manifestoFileRepository.findCreateBy(Mockito.anyInt())).thenReturn(null);
        GZResponse actual3 = manifestoFileService.deleteFileByJob(request3);

        Assert.assertEquals(gzResponse3.getTitle(), actual3.getTitle());
        Assert.assertEquals(gzResponse3.getMessage(), actual3.getMessage());
        Assert.assertEquals(gzResponse3.getCode(), actual3.getCode());
        Assert.assertEquals(gzResponse3.getDeveloperMessage(), actual3.getDeveloperMessage());

    }


    @Test
    public void getManifestoFileReturnSuccess() throws Exception {
        //List<PODNumberStatus> podNumberStatus = new ArrayList<>();
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setDeveloperMessage(ManifestoFileConstant.GET_MANIFESTOFILE_SUCCESS);

        GetUploadFileRequest getUploadFileRequest = new GetUploadFileRequest();
        getUploadFileRequest.setManifestoFileId(1);

        byte[] byteArr = new byte[] {};

        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

        ManifestoFile manifestoFile = new ManifestoFile(1, byteArr, 1, currentDate);

        Mockito.when(manifestoFileRepository.findOne(Mockito.anyInt())).thenReturn(manifestoFile);

        GZResponse actual = manifestoFileService.getFileByJob(getUploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void getManifestoFileReturnNotFound() throws Exception {
        //List<PODNumberStatus> podNumberStatus = new ArrayList<>();
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
        gzResponse.setCode(HttpStatus.BAD_REQUEST);
        gzResponse.setMessage(HttpStatus.BAD_REQUEST.toString());
        gzResponse.setDeveloperMessage(ManifestoFileConstant.GET_MANIFESTOFILE_NOT_FOUND);

        GetUploadFileRequest getUploadFileRequest = new GetUploadFileRequest();
        getUploadFileRequest.setManifestoFileId(1);

        Mockito.when(manifestoFileRepository.findOne(Mockito.anyInt())).thenReturn(null);

        GZResponse actual = manifestoFileService.getFileByJob(getUploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void postFileByJobCaseManifestoNotFound() throws Exception {
        GZResponse gzResponse;
        GZResponse actual;

        MultipartFile multipartFileList = new MockMultipartFile("documents", "test.png", "image/png", "test.png".getBytes());
        UploadFileRequest uploadFileRequest = new UploadFileRequest();
        uploadFileRequest.setFile(multipartFileList);
        uploadFileRequest.setUpdateBy(1);
        uploadFileRequest.setManifestoId(1);

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(null);

        gzResponse = new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_FOUND);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void postFileByJobCaseUSER_NOT_FOUND() throws Exception {
        GZResponse gzResponse;
        GZResponse actual;

        MultipartFile multipartFileList = new MockMultipartFile("documents", "test.png", "image/png", "test.png".getBytes());
        UploadFileRequest uploadFileRequest = new UploadFileRequest();
        uploadFileRequest.setFile(multipartFileList);
        uploadFileRequest.setUpdateBy(1);
        uploadFileRequest.setManifestoId(1);

        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
                1, "1", "SOMS Order",
                "SM01", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson","2019-02-03");

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(null);

        gzResponse = new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void postFileByJobCaseNOT_USER_DRIVER() throws Exception {
        GZResponse gzResponse;
        GZResponse actual;

        MultipartFile multipartFileList = new MockMultipartFile("documents", "test.png", "image/png", "test.png".getBytes());
        UploadFileRequest uploadFileRequest = new UploadFileRequest();
        uploadFileRequest.setFile(multipartFileList);
        uploadFileRequest.setUpdateBy(1);
        uploadFileRequest.setManifestoId(1);

        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
                1, "1", "SOMS Order",
                "SM01", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson","2019-02-03");

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(PODNumberStatusConstant.CHECKER);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        gzResponse = new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), ManifestoFileConstant.NOT_USER_DRIVER);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void postFileByJobCaseFILE_PNG_JPG_PDF_ONLY() throws Exception {
        GZResponse gzResponse;
        GZResponse actual;

        MultipartFile multipartFileList = new MockMultipartFile("documents", "test.png", "image/pang", "test.png".getBytes());
        UploadFileRequest uploadFileRequest = new UploadFileRequest();
        uploadFileRequest.setFile(multipartFileList);
        uploadFileRequest.setUpdateBy(1);
        uploadFileRequest.setManifestoId(1);

        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
                1, "1", "SOMS Order",
                "SM01", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson","");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        gzResponse = new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), ManifestoFileConstant.FILE_PNG_JPG_PDF_ONLY);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void postFileByJobCaseFILE_OVER_MAXIMUM_2_MB() throws Exception {
        GZResponse gzResponse;
        GZResponse actual;

        byte[] bytes = new byte[1024 * 1024 * 1000];
        MultipartFile multipartFileList = new MockMultipartFile("documents", "test.png", "image/png", bytes);
        UploadFileRequest uploadFileRequest = new UploadFileRequest();
        uploadFileRequest.setFile(multipartFileList);
        uploadFileRequest.setUpdateBy(1);
        uploadFileRequest.setManifestoId(1);

        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
                1, "1", "SOMS Order",
                "SM01", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson","");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        gzResponse = new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), ManifestoFileConstant.FILE_OVER_MAXIMUM_2_MB);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }



    @Test
    public void postFileByJobCaseSuccessOrientationValue() throws Exception {
        GZResponse gzResponse;
        GZResponse actual;

        //get image from relative path
        BufferedImage bImage = ImageIO.read(new File("src/main/resources/static/goodchoiz.png"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpg", bos );
        byte [] bytes = bos.toByteArray();
        MultipartFile multipartFileList = new MockMultipartFile("documents", "test.png", "image/png", bytes);
        UploadFileRequest uploadFileRequest = new UploadFileRequest();
        uploadFileRequest.setFile(multipartFileList);
        uploadFileRequest.setUpdateBy(1);
        uploadFileRequest.setManifestoId(1);
        uploadFileRequest.setOrientation(1);

        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
                1, "1", "SOMS Order",
                "SM01", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson","");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

        ManifestoFile manifestoFile = new ManifestoFile(uploadFileRequest.getManifestoId(), bytes, userTypeMaster.getId(), currentDate);


        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(manifestoFileRepository.save(Mockito.any(ManifestoFile.class))).thenReturn(manifestoFile);

        gzResponse = new GZResponse(ManifestoFileConstant.UPDATE_MANIFESTOFILE, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestoFileConstant.UPDATE_MANIFESTOFILE_SUCCESS);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(2);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(3);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(4);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(5);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(6);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(7);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        uploadFileRequest.setOrientation(8);

        actual = manifestoFileService.postFileByJob(uploadFileRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }

}
