package com.gz.pod.services.common;

import com.gz.pod.categories.common.*;
import com.gz.pod.categories.common.responses.ManifestoOwnerResponse;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentConstant;
import com.gz.pod.entities.ManifestoOwner;
import com.gz.pod.entities.MasterStatus;
import com.gz.pod.entities.UserType;
import com.gz.pod.entities.UserTypeMaster;
import com.gz.pod.exceptions.GZException;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class CommonServiceTest {
    @InjectMocks
    @Spy
    private CommonService commonService = new CommonService();

    @Mock
    private UserTypeMasterRepository userTypeMasterRepository;

    @Mock
    private MasterStatusRepository masterStatusRepository;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private UserTypeRepository userTypeRepository;

    @Mock
    private CommonConstant commonConstant;

    @Test
    public void getUserTypeMasterForReturnCaseNotNull() throws Exception {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(33);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMaster);

        UserTypeMaster actual = commonService.getUserTypeMasterForReturn(88);
        Assert.assertEquals("Test Name", actual.getName());
    }

    @Test
    public void getUserTypeMasterForReturnCaseNull() throws Exception {
        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(null);

        UserTypeMaster actual = commonService.getUserTypeMasterForReturn(88);
        Assert.assertEquals(null, actual);
    }

    @Test(expected = GZException.class)
    public void isCheckMasterStatusReturnTrue() throws Exception {
        MasterStatus masterStatus = new MasterStatus();
        masterStatus.setId(1);
        masterStatus.setModuleType("POD");
        masterStatus.setName("pod.driver.confirm");
        masterStatus.setUserTypeId(1);
        Mockito.when(masterStatusRepository.findByIdAndUserTypeId(Mockito.anyInt(), Mockito.anyInt())).thenReturn(masterStatus);

        boolean actual = commonService.isCheckMasterStatus("title", 1,111);
        Assert.assertTrue(actual);

        // false
        Mockito.when(masterStatusRepository.findByIdAndUserTypeId(Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
        actual = commonService.isCheckMasterStatus("title", 1,111);
        Assert.assertFalse(actual);
    }

    @Test
    public void getUserTypeMasterReturnSuccessDate() throws Exception {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(88);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMaster);

        UserTypeMaster actual = commonService.getUserTypeMaster("title",88);
        Assert.assertEquals(userTypeMaster, actual);
    }

    @Test
    public void getUserTypeMasterByIdReturnSuccessDate() throws Exception {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(3);
        userTypeMaster.setName("Test Name");
        userTypeMaster.setLoginId(88);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMaster);

        UserTypeMaster actual = commonService.getUserTypeMaster("title",88);
        Assert.assertEquals(userTypeMaster, actual);
    }

    @Test
    public void validateOwnerByManifestoIdSuccess() throws Exception {
        ManifestoOwner manifestoOwner = new ManifestoOwner();
        manifestoOwner.setJobId("1001");
        manifestoOwner.setDriverId(7);
        manifestoOwner.setManifestoType("SO");
        manifestoOwner.setFulfillId(null);
        manifestoOwner.setId(301);

        List<ManifestoOwner> manifestoOwnerList = new ArrayList<>();
        manifestoOwnerList.add(manifestoOwner);

        Mockito.when(manifestolistRepository.findDriveIdByListId(Mockito.anyList(), Mockito.anyString())).thenReturn(manifestoOwnerList);

        ManifestoOwnerResponse manifestoOwnerResponse = new ManifestoOwnerResponse();
        manifestoOwnerResponse.setOwner(true);
        manifestoOwnerResponse.setManifestoId(301);
        manifestoOwnerResponse.setRefId("1001");
        manifestoOwnerResponse.setManifestoType("SO");
        manifestoOwnerResponse.setJobId("1001");
        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        manifestoOwnerResponseList.add(manifestoOwnerResponse);

        List<Integer> manifestoIdList = new ArrayList<>();
        manifestoIdList.add(301);

        List<ManifestoOwnerResponse> actual = commonService.validateOwnerByManifestoId(manifestoIdList,7);
        Assert.assertEquals(manifestoOwnerResponseList, actual);
    }

    @Test
    public void getRefDocByQRCodeFromSOMSSuccess() throws Exception
    {
        //////// Mock QRCode
        String qrResponse = "    {\n" +
                "        \"DocRef\": \"1000239\", " +
                "        \"DocName\": \"Log\" " +
                "    }\n";

        String jsonItem = qrResponse;
        final String secretKey = "secreKeyToTest";
        Mockito.doReturn(secretKey).when(commonService).generateSignature(Mockito.any());

        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        GZResponse actual = commonService.getRefDocByQRCodeFromSOMS("GZQ-1-221");
        Assert.assertEquals(ReceiveDocumentConstant.QRNUMBER_FOUND, actual.getDeveloperMessage());
    }

    @Test
    public void getRefDocByQRCodeFromSOMSErrorQRNotFoundCase() throws Exception
    {
        //////// Mock QRCode
        String qrResponse = "    {\n" +
                "        \"Message\": \""+ReceiveDocumentConstant.QRNUMBER_NOTFOUND+"\"" +
                "    }\n";

        String jsonItem = qrResponse;
        final String secretKey = "secreKeyToTest";
        Mockito.doReturn(secretKey).when(commonService).generateSignature(Mockito.any());

        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        GZResponse actual = commonService.getRefDocByQRCodeFromSOMS("GZQ-1-221");
        Assert.assertEquals(ReceiveDocumentConstant.QRNUMBER_NOTFOUND, actual.getDeveloperMessage());
    }

    @Test
    public void getRefDocByQRCodeFromSOMSErrorOtherCase() throws Exception
    {
        //////// Mock QRCode
        String qrResponse = "    {\n" +
                "        \"Message\": \"Test\"" +
                "    }\n";

        String jsonItem = qrResponse;
        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem, HttpStatus.OK);
        final String secretKey = "secreKeyToTest";
        Mockito.doReturn(secretKey).when(commonService).generateSignature(Mockito.any());
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET), Mockito.anyObject(), eq(String.class))).thenReturn(responseEntityGet);
        /////////////////////////////

        GZResponse actual = commonService.getRefDocByQRCodeFromSOMS("GZQ-1-221");
        Assert.assertEquals("Test", actual.getDeveloperMessage());
    }

    @Test
    public void getUserTypeTest() {
        UserType userType = new UserType();
        userType.setId(1);
        userType.setName("driver");

        Mockito.when(userTypeRepository.findById(Mockito.anyInt())).thenReturn(userType);

        UserType expect = new UserType();
        expect.setId(1);
        expect.setName("driver");

        UserType actual = commonService.getUserType(1);

        Assert.assertEquals(actual, expect);

        // not found
        Mockito.when(userTypeRepository.findById(Mockito.anyInt())).thenReturn(null);
        actual = commonService.getUserType(1);
        Assert.assertEquals(actual, null);
    }

    @Test(expected = GZException.class)
    public void getUserTypeMasterFailed() throws Exception {
        Mockito.when(userTypeMasterRepository.findByloginId(anyInt())).thenReturn(null);
        UserTypeMaster actual = commonService.getUserTypeMaster("", 10);
    }

    @Test
    public void getUserTypeMasterByIdTest() throws Exception {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("driver");
        userTypeMaster.setLoginId(10);

        Mockito.when(userTypeMasterRepository.findById(anyInt())).thenReturn(userTypeMaster);

        UserTypeMaster expect = new UserTypeMaster();
        expect.setId(1);
        expect.setName("driver");
        expect.setLoginId(10);

        UserTypeMaster actual = commonService.getUserTypeMasterById("", 10);

        Assert.assertEquals(expect, actual);

        // not found
        Mockito.when(userTypeMasterRepository.findById(anyInt())).thenReturn(null);
        actual = commonService.getUserTypeMasterById("", 10);

        Assert.assertEquals(actual, null);
    }

    @Test(expected = GZException.class)
    public void getUserTypeMasterByIdTestException() throws Exception {
        Mockito.when(userTypeMasterRepository.findById(anyInt())).thenThrow(NullPointerException.class);
        UserTypeMaster actual = commonService.getUserTypeMasterById("", 10);
    }
}

