package com.gz.pod.services.podNumberStatus;

import com.gz.pod.categories.MasterReasonRejectManifesto.MasterReasonRejectManifestoRepository;
import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.GenericInformationRepository;
import com.gz.pod.categories.common.MasterStatusRepository;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.common.responses.ManifestoOwnerResponse;
import com.gz.pod.categories.datafromsoms.DataFromSOMSService;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.JobStatusToSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.PicIdRequest;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestoFile.ManifestoFileRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.manifestolist.ManifestolistService;
import com.gz.pod.categories.manifestolist.responses.ManifestoResponse;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberDetailRequest;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusService;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberStatusRequest;
import com.gz.pod.categories.podNumberStatus.requests.ValidateUpdateShippingRequest;
import com.gz.pod.categories.podNumberStatus.responses.ManifestoDetailByPODNumberResponse;
import com.gz.pod.categories.podNumberStatus.responses.PODNumberStatusDetailResponse;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentRepository;
import com.gz.pod.entities.*;
import com.gz.pod.response.GZResponse;
import io.swagger.models.auth.In;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)
public class PODNumberStatusServiceTest {

    @InjectMocks
    private PODNumberStatusService podNumberStatusService;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private CommonService commonService;

    @Mock
    private JobStatusRepository jobStatusRepository;

    @Mock
    private UserTypeMasterRepository userTypeMasterRepository;

    @Mock
    private GenericInformationRepository genericInformationRepository;

    @Mock
    private ManifestoItemRepository manifestoItemRepository;

    @Mock
    private MasterReasonRejectManifestoRepository masterReasonRejectManifestoRepository;

    @Mock
    private ManifestolistService manifestolistService;

    @Mock
    private ReceiveDocumentRepository receiveDocumentRepository;

    @Mock
    private DataFromSOMSService dataFromSOMSService;

    @Mock
    private ManifestoFileRepository manifestoFileRepository;

    @Mock
    private MasterStatusRepository masterStatusRepository;

    @Before
    public void runBeforeTestMethod() {
        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();
        podNumberStatusDetailResponse.setPodNumber("092018-0005");
        podNumberStatusDetailResponse.setShipToAddress("ship_Address_test");
        podNumberStatusDetailResponse.setRouteName("RO01");
        podNumberStatusDetailResponse.setCustomerName("customer");

        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        manifestolist.setDeliveryDate(java.time.LocalDate.now().toString());
        listManifestoList.add(manifestolist);

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setDescription("test");
        masterReasonRejectManifesto.setId(1);

        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);
        Mockito.when(manifestolistRepository.findAllBypodNumberOrderByIdAsc(Mockito.anyString())).thenReturn(listManifestoList);

    }

    Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

    @Test
    public void checkStatusUpdateByCheckerConfirmAllCaseFieldTypeNotFound() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setStatusId(21);
        podNumberStatusRequest.setFieldType("podNumber");

        GZResponse actual = podNumberStatusService.checkStatusUpdateByCheckerConfirmAll(podNumberStatusRequest, "2019-01-15", currentDate, 1);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);

        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void checkStatusUpdateByCheckerConfirmAllCaseSuccess() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setStatusId(21);
        podNumberStatusRequest.setFieldType("driverId");
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setUserTypeId(1);
        podNumberStatusRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberStatusRequest.setManifestoIds(manifestoIds);

        validateUpdateStatusWithMatch();
        checkStatusPostponeAndReject();
        stampStatusToSOMS();
        MasterStatus masterStatus = new MasterStatus(221, "job", "JOB.Checker.Approve");

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_CHECKER_APPROVE, "", 1, currentDate);
        jobStatus.setMasterStatus(masterStatus);
        JobStatus jobStatus2 = new JobStatus(1, PODNumberStatusConstant.JOB_CHECKER_NOT_APPROVE, "", 1, currentDate);


        Mockito.when(manifestolistService.getLastJobStatus(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus).thenReturn(jobStatus2);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
//        Mockito.when(podNumberStatusServiceMock.checkStatusPostponeAndReject(Mockito.any(JobStatus.class), Mockito.anyString(), Mockito.any())).thenReturn(false).thenReturn(true);
//        doNothing().when(podNumberStatusServiceMock).checkOldPodNumberInStatus(Mockito.any(JobStatus.class), Mockito.anyInt(), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.checkStatusUpdateByCheckerConfirmAll(podNumberStatusRequest, "2019-01-15", currentDate, 1);

        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    public void validateUpdateStatusWithMatch() {
        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("name_remark");
        userTypeMaster.setUserTypeId(1);
        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(1);
        listManifestoToday.add(2);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday);
    }

    @Test
    public void validateUpdateStatusWithMatchCaseREFID_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setRefId("test");

        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_DRIVER_START);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REFID_NOT_FOUND);

        GZResponse actual = podNumberStatusService.validateUpdateStatusWithMatch(podNumberDetailRequest, listStatus, "2019-01-15");
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateUpdateStatusWithMatchCaseUSER_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setRefId("1");

        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_DRIVER_START);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = podNumberStatusService.validateUpdateStatusWithMatch(podNumberDetailRequest, listStatus, "2019-01-15");
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    public void checkStatusPostponeAndReject() {
        List<Manifestolist> listManifestoByPodNumber = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "SOMS Order", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",0,"", "contactPerson", "2019-01-15");
        listManifestoByPodNumber.add(manifestolist);
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);
        Mockito.when(manifestolistRepository.findAllBypodNumberAndDeliveryDateOrderByIdAsc(Mockito.anyString(), Mockito.anyString())).thenReturn(listManifestoByPodNumber);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_CHECKER_CONFIRM, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);
        Mockito.when(manifestolistRepository.save(Mockito.any(Manifestolist.class))).thenReturn(manifestolist);
    }

    @Test
    public void cancelRequestCaseSuccess() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setStatusId(PODNumberStatusConstant.CANCEL_REQUEST);
        podNumberStatusRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setUserTypeId(1);
        podNumberStatusRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberStatusRequest.setManifestoIds(manifestoIds);

        validateUpdateStatusWithMatch();

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_CHECKER_NOT_CONFIRM, "", 1, currentDate);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.cancelRequest(podNumberStatusRequest, "2019-01-15", currentDate, 1 );
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void cancelRequestCaseFIELDTYPE_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setStatusId(PODNumberStatusConstant.CANCEL_REQUEST);
        podNumberStatusRequest.setFieldType(PODNumberStatusConstant.PODNUMBER_STRING);
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setUserTypeId(1);
        podNumberStatusRequest.setUpdateBy(1);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);

        GZResponse actual = podNumberStatusService.cancelRequest(podNumberStatusRequest, "2019-01-15", currentDate, 1 );
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void stampStatusToSOMS() throws Exception {
        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "SOMS Order", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",0,"", "contactPerson", "2019-01-15");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_CHECKER_CONFIRM, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus);

        MasterStatus masterStatus = new MasterStatus(1, "JOB", "JOB.Driver.Confirm.And.Approve", PODNumberStatusConstant.DRIVER, true, false);
        Mockito.when(masterStatusRepository.findById(Mockito.anyInt())).thenReturn(masterStatus);

        GenericInformation genericInformation = new GenericInformation(PODNumberStatusConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN, 1, "1", 1, currentDate);
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setDescription("test_reject");
        masterReasonRejectManifesto.setId(1);
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);

        doNothing().when(dataFromSOMSService).callAPIPostStampStatusOrder(Mockito.any(JobStatusToSOMSRequest.class));

    }

    @Test
    public void stampStatusToSOMSCaseLog() throws Exception {
        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "log", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",1,"", "contactPerson", "2019-01-15");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_CHECKER_CONFIRM, "resr", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus);

        MasterStatus masterStatus = new MasterStatus(1, "JOB", "JOB.Driver.Confirm.And.Approve", PODNumberStatusConstant.DRIVER, true, true);
        Mockito.when(masterStatusRepository.findById(Mockito.anyInt())).thenReturn(masterStatus);

        GenericInformation genericInformation = new GenericInformation(PODNumberStatusConstant.JOBSTATUS_DB, ManifestolistConstant.REF_ACTION_UPDATEJOBSTATUS_DROPDOWN, 1, "1", 1, currentDate);
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setDescription("test_reject");
        masterReasonRejectManifesto.setId(1);
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);

        List<ManifestoFile> listPicId = new ArrayList<>();
        listPicId.add(new ManifestoFile(1, 1, 1, currentDate));
        Mockito.when(manifestoFileRepository.findBymanifestoIdOrderByIdDesc(Mockito.anyInt())).thenReturn(listPicId);

        doNothing().when(dataFromSOMSService).callAPIPostStampStatusOrder(Mockito.any(JobStatusToSOMSRequest.class));

        podNumberStatusService.stampStatusToSOMS(1, PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
    }

    @Test
    public void validateOwnerCaseSuccess() throws Exception {

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("name_remark");
        userTypeMaster.setUserTypeId(1);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        ManifestoOwnerResponse data = new ManifestoOwnerResponse(1, true, true, "2015135", "SO");
        manifestoOwnerResponseList.add(data);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);

        ValidateUpdateShippingRequest validateUpdateShippingRequest = new ValidateUpdateShippingRequest();
        validateUpdateShippingRequest.setLoginId(1);
        validateUpdateShippingRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        validateUpdateShippingRequest.setManifestoIds(manifestoIds);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_SUCCESS);

        GZResponse actual = podNumberStatusService.validateOwner(validateUpdateShippingRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateOwnerCaseSuccessButNoAllData() throws Exception {

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("name_remark");
        userTypeMaster.setUserTypeId(1);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        ManifestoOwnerResponse data = new ManifestoOwnerResponse(1, true, true, "2015135", "SO");
        manifestoOwnerResponseList.add(data);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);

        ValidateUpdateShippingRequest validateUpdateShippingRequest = new ValidateUpdateShippingRequest();
        validateUpdateShippingRequest.setLoginId(1);
        validateUpdateShippingRequest.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        validateUpdateShippingRequest.setManifestoIds(manifestoIds);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_SUCCESS);

        GZResponse actual = podNumberStatusService.validateOwner(validateUpdateShippingRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void validateOwnerCaseFalse() throws Exception {

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("name_remark");
        userTypeMaster.setUserTypeId(1);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        List<ManifestoOwnerResponse> manifestoOwnerResponseList = new ArrayList<>();
        ManifestoOwnerResponse data = new ManifestoOwnerResponse(1, false, false, "2015135", "SO");
        manifestoOwnerResponseList.add(data);
        Mockito.when(commonService.validateOwnerByManifestoId(Mockito.anyListOf(Integer.class), Mockito.anyInt())).thenReturn(manifestoOwnerResponseList);

        ValidateUpdateShippingRequest validateUpdateShippingRequest = new ValidateUpdateShippingRequest();
        validateUpdateShippingRequest.setLoginId(1);
        validateUpdateShippingRequest.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        validateUpdateShippingRequest.setManifestoIds(manifestoIds);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_FAILED);

        GZResponse actual = podNumberStatusService.validateOwner(validateUpdateShippingRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void PrepareJobStatus() throws Exception {

        MasterStatus masterStatus = new MasterStatus(1, "pod", "JOB.Driver.Status.Confirm");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("name_remark");
        userTypeMaster.setUserTypeId(1);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_status_test");
        jobStatus.setStatusId(2113);
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);

        List<ManifestoResponse> listManifestoList = new ArrayList<>();
        ManifestoResponse manifestolist = new ManifestoResponse();
        manifestolist.setDriverId(1);
        manifestolist.setManifestoId(1);
        manifestolist.setManifestoType("log");
        manifestolist.setJobId("1");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setShipToCode("10510");
        manifestolist.setShipToName("Test ship name");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setAddressType("Test addresss type");
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSkuQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setRouteName("RO01");
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setFulfillId(0);
        manifestolist.setDelayDate("test_date");
        manifestolist.setContactPerson("contactPerson");
        manifestolist.setInvoiceNo("");
        manifestolist.setNote("note");
        manifestolist.setJobStatus(jobStatus);
        manifestolist.setDelayDate("");
        listManifestoList.add(manifestolist);

        ManifestoDetailByPODNumberResponse expect = new ManifestoDetailByPODNumberResponse(1 , "log", "name_remark","remark_test", "remark_status_test","","1", "JOB.Driver.Status.Confirm","contactPerson" , "", "" , 0, true,"test_reject" , "name_remark" );

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);
        genericInformation.setRefId(1);

        genericInformation.setUserTypeMaster(userTypeMaster);
        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus);

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setDescription("test_reject");
        masterReasonRejectManifesto.setId(1);


        ReceiveDocument receiveDocument = new ReceiveDocument(1, 1,1, 1, new java.sql.Timestamp(new java.util.Date().getTime()));
        receiveDocument.setUserTypeMaster(userTypeMaster);
        Mockito.when(receiveDocumentRepository.findByManifestoIdAndStatusId(Mockito.anyInt(), Mockito.anyInt())).thenReturn(receiveDocument);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);
        Mockito.when(manifestolistService.PrepareGeneric(Mockito.anyInt(), Mockito.anyString(), Mockito.anyString())).thenReturn("test_reject");

        List<ManifestoDetailByPODNumberResponse> actual = podNumberStatusService.PrepareJobStatus(listManifestoList, "shipped");

        Assert.assertTrue(!actual.isEmpty());
        Assert.assertEquals(expect, actual.get(0));
    }

    @Test
    public void checkOldPodNumberInStatusCaseJobStatusEnd() throws Exception {

        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "SOMS Order", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",0,"", "contactPerson", "2019-01-15");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        List<Integer> listManifestoByPodNumber = new ArrayList<>();
        listManifestoByPodNumber.add(1);
        Mockito.when(manifestolistRepository.findByDeliveryDateAndPodNumberWithManifestoId(Mockito.anyString(), Mockito.anyString())).thenReturn(listManifestoByPodNumber);

        List<Integer> jobStatusEnd = new ArrayList<>();
        jobStatusEnd.add(10);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(jobStatusEnd);

        List<Integer> jobStatusStart = new ArrayList<>();
        jobStatusStart.add(1);
        Mockito.when(jobStatusRepository.findLastSecondJobStatusByJobIdReturnId(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatusStart);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_START, "", 1, currentDate);
        JobStatus jobStatus2 = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_END, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findOne(Mockito.anyInt())).thenReturn(jobStatus2);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);
        genericInformation.setRefId(1);

        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        stampStatusToSOMS();

        Mockito.when(jobStatusRepository.findLastByjobId(Mockito.anyInt())).thenReturn(jobStatus);

        podNumberStatusService.checkOldPodNumberInStatus(jobStatus, "2019-01-15", currentDate);
    }

    @Test
    public void checkOldPodNumberInStatusCaseJobStatusStart() throws Exception {
        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "SOMS Order", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",0,"", "contactPerson", "2019-01-15");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        List<Integer> listManifestoByPodNumber = new ArrayList<>();
        listManifestoByPodNumber.add(1);
        Mockito.when(manifestolistRepository.findByDeliveryDateAndPodNumberWithManifestoId(Mockito.anyString(), Mockito.anyString())).thenReturn(listManifestoByPodNumber);

        List<Integer> jobStatusStart = new ArrayList<>();
        jobStatusStart.add(1);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(new ArrayList<>()).thenReturn(jobStatusStart);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_START, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobId(Mockito.anyInt())).thenReturn(jobStatus);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);
        genericInformation.setRefId(1);

        Mockito.when(genericInformationRepository.findByRefModuleAndRefActionAndRefId(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(genericInformation);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        stampStatusToSOMS();
        podNumberStatusService.checkOldPodNumberInStatus(jobStatus, "2019-01-15", currentDate);
    }

    @Test
    public void updateJobStatusNewCaseFIELDTYPE_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("job");
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateUpdateStatusWithMatch();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);

        GZResponse actual = podNumberStatusService.updateJobStatusNew(podNumberDetailRequest, "2019-01-15", currentDate, 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    public void updateJobStatusNew() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("driverId");
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateUpdateStatusWithMatch();
        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_START, "", 1, currentDate);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        stampStatusToSOMS();
    }

    @Test
    public void updateJobStatusNewAllCase() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("driverId");
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateUpdateStatusWithMatch();
        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_START, "", 1, currentDate);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        stampStatusToSOMS();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.updateJobStatusNew(podNumberDetailRequest, "2019-01-15", currentDate, 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_CHECKER_APPROVE);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("driverId");
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.CHECKER);

        actual = podNumberStatusService.updateJobStatusNew(podNumberDetailRequest, "2019-01-15", currentDate, 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REJECT);
        podNumberDetailRequest.setRefId("092018-0001");
        podNumberDetailRequest.setFieldType("podNumber");
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);

        actual = podNumberStatusService.updateJobStatusNew(podNumberDetailRequest, "2019-01-15", currentDate, 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REJECT_START);
        podNumberDetailRequest.setRefId("092018-0001");
        podNumberDetailRequest.setFieldType("podNumber");
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);

        actual = podNumberStatusService.updateJobStatusNew(podNumberDetailRequest, "2019-01-15", currentDate, 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseJOB_STATUS_VALIDATE_FAILED() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_REQUEST_TO_APPROVE_FOR_CHECKER);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.JOB_STATUS_VALIDATE_FAILED);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseUSER_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.CHECKSTAMPSTATUS);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("name_remark");
        userTypeMaster.setUserTypeId(1);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster).thenReturn(null);


        GZResponse expect = new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void updatePODNumberStatusCaseCHECKSTAMPSTATUS() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.CHECKSTAMPSTATUS);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();
        checkStatusUpdateByCheckerConfirmAllCaseSuccess();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseCANCEL_REQUEST() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.CANCEL_REQUEST);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();
        cancelRequestCaseSuccess();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseDriverId() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.DRIVER_ID_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();
        updateJobStatusNew();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCasePodNumber() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CHECKCONFIRM);
        podNumberDetailRequest.setRefId("102019-0001");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.PODNUMBER_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        manifestoIds.add(2);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();
        updateJobStatusNew();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseDelay() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        podNumberDetailRequest.setRefId("102019-0001");
        podNumberDetailRequest.setFieldType(PODNumberStatusConstant.PODNUMBER_STRING);
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        podNumberDetailRequest.setDelay(true);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();
        updateDelayDate();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_DELAY_SUCCESS);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseJob() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_MOVE_SO);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("job");
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(213);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        Manifestolist manifestoSetDelay = new Manifestolist();
        manifestoSetDelay.setId(1);
        manifestoSetDelay.setJobId("1");
        manifestoSetDelay.setDelayDate("2019-01-15");
        manifestoSetDelay.setDeliveryDate("2019-01-15");


        GZResponse expect = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.STATUS_NOT_MAP);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CHECKCONFIRM);

        updateJobStatusCaseJobSuccess();

        expect = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestolistConstant.UPDATE_MANIFESTO_STAUTS_SUCCESS);


        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(manifestoSetDelay);
        Mockito.when(manifestolistRepository.save(Mockito.any(Manifestolist.class))).thenReturn(manifestoSetDelay);

        actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaselog() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CHECKCONFIRM);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("log");
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(213);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        Manifestolist manifestoSetDelay = new Manifestolist();
        manifestoSetDelay.setId(1);
        manifestoSetDelay.setJobId("1");
        manifestoSetDelay.setDelayDate("2019-01-15");
        manifestoSetDelay.setDeliveryDate("2019-01-15");


        GZResponse expect = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.STATUS_NOT_MAP);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_LOG_CONFIRM);

        updateJobStatusCaseJobSuccess();

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(manifestoSetDelay);
        Mockito.when(manifestolistRepository.save(Mockito.any(Manifestolist.class))).thenReturn(manifestoSetDelay);

        expect = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestolistConstant.UPDATE_MANIFESTO_STAUTS_SUCCESS);

        actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updatePODNumberStatusCaseFIELDTYPE_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CHECKCONFIRM);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("test");
        podNumberDetailRequest.setUserTypeId(PODNumberStatusConstant.DRIVER);
        podNumberDetailRequest.setUpdateBy(1);
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        podNumberDetailRequest.setManifestoIds(manifestoIds);

        validateOwnerCaseSuccess();

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.FIELDTYPE_NOT_FOUND);

        GZResponse actual = podNumberStatusService.updatePODNumberStatus(podNumberDetailRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void getPODNumberDetailByDriver() throws Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.OK, HttpStatus.OK.toString(),PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL_SUCCESS );

        MasterStatus masterStatus = new MasterStatus(1, "pod", "JOB.Driver.Status.Confirm");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("test_userTypeMaster_name");
        userTypeMaster.setUserTypeId(1);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(2111);
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);

        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();
        podNumberStatusDetailResponse.setPodNumber("092018-0005");
        podNumberStatusDetailResponse.setShipToAddress("ship_Address_test");
        podNumberStatusDetailResponse.setRouteName("RO01");
        podNumberStatusDetailResponse.setCustomerName("customer");

        gzResponse.setData(podNumberStatusDetailResponse);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus);


        Mockito.when(userTypeMasterRepository.findById(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("shipping");

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        JobStatus jobStatus2 = new JobStatus();
        jobStatus2.setId(1);
        jobStatus2.setJobId(1);
        jobStatus2.setRemark("remark_test");
        jobStatus2.setStatusId(2113);
        jobStatus2.setMasterStatus(masterStatus);
        jobStatus2.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest2 = new PODNumberDetailRequest();
        podNumberDetailRequest2.setLoginId(1);
        podNumberDetailRequest2.setPodNumber("102018-0140");
        podNumberDetailRequest2.setTab("shipped");

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus2);

        GZResponse actual2 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest2);
        Assert.assertEquals(gzResponse.getTitle(), actual2.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual2.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual2.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual2.getDeveloperMessage());

        JobStatus jobStatus3 = new JobStatus();
        jobStatus3.setId(1);
        jobStatus3.setJobId(1);
        jobStatus3.setRemark("remark_test");
        jobStatus3.setStatusId(212);
        jobStatus3.setMasterStatus(masterStatus);
        jobStatus3.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest3 = new PODNumberDetailRequest();
        podNumberDetailRequest3.setLoginId(1);
        podNumberDetailRequest3.setPodNumber("102018-0140");
        podNumberDetailRequest3.setTab("delivered");

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus3);

        GZResponse actual3 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest3);
        Assert.assertEquals(gzResponse.getTitle(), actual3.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual3.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual3.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual3.getDeveloperMessage());

        GZResponse gzResponse2 = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.WRONG_TAB );

        JobStatus jobStatus4 = new JobStatus();
        jobStatus4.setId(1);
        jobStatus4.setJobId(1);
        jobStatus4.setRemark("remark_test");
        jobStatus4.setStatusId(212);
        jobStatus4.setMasterStatus(masterStatus);
        jobStatus4.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest4 = new PODNumberDetailRequest();
        podNumberDetailRequest4.setLoginId(1);
        podNumberDetailRequest4.setPodNumber("102018-0140");
        podNumberDetailRequest4.setTab("wait");

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus4);

        GZResponse actual4 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest4);
        Assert.assertEquals(gzResponse2.getTitle(), actual4.getTitle());
        Assert.assertEquals(gzResponse2.getMessage(), actual4.getMessage());
        Assert.assertEquals(gzResponse2.getCode(), actual4.getCode());
        Assert.assertEquals(gzResponse2.getDeveloperMessage(), actual4.getDeveloperMessage());
    }

    @Test
    public void getPODNumberDetailByChecker() throws Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.OK, HttpStatus.OK.toString(),PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL_SUCCESS );

        MasterStatus masterStatus = new MasterStatus(1, "pod", "JOB.Driver.Status.Confirm");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("test_userTypeMaster_name");
        userTypeMaster.setUserTypeId(2);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(2113);
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);

        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();
        podNumberStatusDetailResponse.setPodNumber("092018-0005");
        podNumberStatusDetailResponse.setShipToAddress("ship_Address_test");
        podNumberStatusDetailResponse.setRouteName("RO01");
        podNumberStatusDetailResponse.setCustomerName("customer");
        podNumberStatusDetailResponse.setDriverName("test_driver");

        gzResponse.setData(podNumberStatusDetailResponse);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus);
        Mockito.when(userTypeMasterRepository.findById(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("checker");

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);
        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        JobStatus jobStatus2 = new JobStatus();
        jobStatus2.setId(1);
        jobStatus2.setJobId(1);
        jobStatus2.setRemark("remark_test");
        jobStatus2.setStatusId(2112);
        jobStatus2.setMasterStatus(masterStatus);
        jobStatus2.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest2 = new PODNumberDetailRequest();
        podNumberDetailRequest2.setLoginId(1);
        podNumberDetailRequest2.setPodNumber("102018-0140");
        podNumberDetailRequest2.setTab("approved");
        ReceiveDocument receiveDocument = new ReceiveDocument();
        receiveDocument.setId(1);
        receiveDocument.setManifestoId(1);
        receiveDocument.setStatusId(2112);
        receiveDocument.setCreatedBy(1);
        receiveDocument.setUserTypeMaster(userTypeMaster);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus2);
        Mockito.when(receiveDocumentRepository.findByManifestoIdAndStatusId(Mockito.anyInt(),Mockito.anyInt())).thenReturn(receiveDocument);

        GZResponse actual2 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest2);
        Assert.assertEquals(gzResponse.getTitle(), actual2.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual2.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual2.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual2.getDeveloperMessage());

        JobStatus jobStatus3 = new JobStatus();
        jobStatus3.setId(1);
        jobStatus3.setJobId(1);
        jobStatus3.setRemark("remark_test");
        jobStatus3.setStatusId(222);
        jobStatus3.setMasterStatus(masterStatus);
        jobStatus3.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest3 = new PODNumberDetailRequest();
        podNumberDetailRequest3.setLoginId(1);
        podNumberDetailRequest3.setPodNumber("102018-0140");
        podNumberDetailRequest3.setTab("wait");

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus3);

        GZResponse actual3 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest3);
        Assert.assertEquals(gzResponse.getTitle(), actual3.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual3.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual3.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual3.getDeveloperMessage());

        GZResponse gzResponse2 = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.WRONG_TAB );

        JobStatus jobStatus4 = new JobStatus();
        jobStatus4.setId(1);
        jobStatus4.setJobId(1);
        jobStatus4.setRemark("remark_test");
        jobStatus4.setStatusId(212);
        jobStatus4.setMasterStatus(masterStatus);
        jobStatus4.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest4 = new PODNumberDetailRequest();
        podNumberDetailRequest4.setLoginId(1);
        podNumberDetailRequest4.setPodNumber("102018-0140");
        podNumberDetailRequest4.setTab("shipping");

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus4);

        GZResponse actual4 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest4);
        Assert.assertEquals(gzResponse2.getTitle(), actual4.getTitle());
        Assert.assertEquals(gzResponse2.getMessage(), actual4.getMessage());
        Assert.assertEquals(gzResponse2.getCode(), actual4.getCode());
        Assert.assertEquals(gzResponse2.getDeveloperMessage(), actual4.getDeveloperMessage());

    }

    @Test
    public void getPODNumberDetailByFinops() throws Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.OK, HttpStatus.OK.toString(), PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL_SUCCESS);

        MasterStatus masterStatus = new MasterStatus(1, "pod", "JOB.Driver.Status.Confirm");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("test_userTypeMaster_name");
        userTypeMaster.setUserTypeId(3);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(2113);
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);

        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();
        podNumberStatusDetailResponse.setPodNumber("092018-0005");
        podNumberStatusDetailResponse.setShipToAddress("ship_Address_test");
        podNumberStatusDetailResponse.setRouteName("RO01");
        podNumberStatusDetailResponse.setCustomerName("customer");
        podNumberStatusDetailResponse.setDriverName("test_driver");

        gzResponse.setData(podNumberStatusDetailResponse);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);
        Mockito.when(userTypeMasterRepository.findById(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("finops");

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);
        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

        GZResponse gzResponse2 = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.toString(),PODNumberStatusConstant.WRONG_TAB );

        JobStatus jobStatus4 = new JobStatus();
        jobStatus4.setId(1);
        jobStatus4.setJobId(1);
        jobStatus4.setRemark("remark_test");
        jobStatus4.setStatusId(212);
        jobStatus4.setMasterStatus(masterStatus);
        jobStatus4.setUserTypeMaster(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest4 = new PODNumberDetailRequest();
        podNumberDetailRequest4.setLoginId(1);
        podNumberDetailRequest4.setPodNumber("102018-0140");
        podNumberDetailRequest4.setTab("wait");

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(),Mockito.anyString())).thenReturn(jobStatus4);

        GZResponse actual4 = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest4);
        Assert.assertEquals(gzResponse2.getTitle(), actual4.getTitle());
        Assert.assertEquals(gzResponse2.getMessage(), actual4.getMessage());
        Assert.assertEquals(gzResponse2.getCode(), actual4.getCode());
        Assert.assertEquals(gzResponse2.getDeveloperMessage(), actual4.getDeveloperMessage());
    }

    @Test
    public void getPODNumberDetailNull() throws Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.OK, HttpStatus.OK.toString(), PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL_SUCCESS);

        MasterStatus masterStatus = new MasterStatus(1, "pod", "JOB.Driver.Status.Confirm");

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        userTypeMaster.setName("test_userTypeMaster_name");
        userTypeMaster.setUserTypeId(1);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(2113);
        jobStatus.setMasterStatus(masterStatus);
        jobStatus.setUserTypeMaster(userTypeMaster);

        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();
        podNumberStatusDetailResponse.setPodNumber("092018-0005");
        podNumberStatusDetailResponse.setShipToAddress("ship_Address_test");
        podNumberStatusDetailResponse.setRouteName("RO01");
        podNumberStatusDetailResponse.setCustomerName("customer");
        podNumberStatusDetailResponse.setDriverName("test_driver");

        gzResponse.setData(podNumberStatusDetailResponse);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);
        Mockito.when(userTypeMasterRepository.findById(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("shipped");

        Mockito.when(manifestolistService.checkShowStatusPODNumber(Mockito.anyListOf(ManifestoResponse.class), Mockito.anyInt(), Mockito.anyString())).thenReturn(null);

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);

        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void getPODdetailManiListNull() throws Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_FOUND);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("shipped");

        List<Manifestolist> listManifestoList = new ArrayList<>();
        Mockito.when(manifestolistRepository.findAllBypodNumberOrderByIdAsc(Mockito.anyString())).thenReturn(listManifestoList);

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);
        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void getPODdetailUserNull() throws  Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("shipped");

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);
        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void getPODdetailUserNotOwner() throws  Exception {
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.GET_PODNUMBERSTATUSDETAIL, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_MAP);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(9);
        userTypeMaster.setLoginId(99);
        userTypeMaster.setName("test_driver_name");
        userTypeMaster.setUserTypeId(1);

        PODNumberDetailRequest podNumberDetailRequest = new PODNumberDetailRequest();
        podNumberDetailRequest.setLoginId(1);
        podNumberDetailRequest.setPodNumber("102018-0140");
        podNumberDetailRequest.setTab("shipped");

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(userTypeMaster);

        GZResponse actual = podNumberStatusService.getPODNumberDetail(podNumberDetailRequest);
        Assert.assertEquals(gzResponse.getTitle(), actual.getTitle());
        Assert.assertEquals(gzResponse.getMessage(), actual.getMessage());
        Assert.assertEquals(gzResponse.getCode(), actual.getCode());
        Assert.assertEquals(gzResponse.getDeveloperMessage(), actual.getDeveloperMessage());

    }
    @Test
    public void updateJobStatusWhenREFID_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("012019-0001");
        podNumberStatusRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);

        GZResponse response =  new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REFID_NOT_FOUND);

        GZResponse actual = podNumberStatusService.updateJobStatus(podNumberStatusRequest, "2019-01-15");

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateJobStatusWhenMANIFESTO_NOT_FOUND() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(null);

        GZResponse response =  new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_FOUND);

        GZResponse actual = podNumberStatusService.updateJobStatus(podNumberStatusRequest, "2019-01-15");

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateJobStatusWhenSTATUS_NOT_END() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_CONFIRMALLDELIVERY);

        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "SOMS Order", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",0,"", "contactPerson", "2019-01-15");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(null);

        GZResponse response =  new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.STATUS_NOT_END);

        GZResponse actual = podNumberStatusService.updateJobStatus(podNumberStatusRequest, "2019-01-15");

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateJobStatusWhenUPDATE_UNDELETE_JOB_REMARK_NULL() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setStatusId(PODNumberStatusConstant.JOB_DRIVER_SO_REJECTALLDELIVERY);

        Manifestolist manifestolist = new Manifestolist("1", "2019-01-15", "123456",
                1, "SOMS Order", "SM01",
                "1", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,
                "","","","",0,"", "contactPerson", "2019-01-15");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_END, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        GZResponse response =  new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.UPDATE_UNDELETE_JOB_REMARK_NULL);

        GZResponse actual = podNumberStatusService.updateJobStatus(podNumberStatusRequest, "2019-01-15");

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateJobStatusManifestoItemNotFound() throws Exception {
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setStatusId(ManifestolistConstant.JOB_DRIVER_CONFIRMALLDELIVERY );
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setRefId("1");
        podNumberStatusRequest.setFieldType("job");

        Manifestolist manifestolist = new Manifestolist("1", "MN001", "2018-07-26",
                1, "1", "SOMS Order",
                "SM01", 1, "1",
                "Test Test", "502100", "Test ja Test ja", "Test moo 1",
                1, 1, 1, "remark", 1,
                "addressType", BigDecimal.valueOf(1), "092018-0003","",1,0,"","","","",0,"", "contactPerson", null);
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);


        Mockito.when(manifestoItemRepository.findBymanifestoId(Mockito.anyInt())).thenReturn(null);
        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_END, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        GZResponse response = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), ManifestolistConstant.MANIFESTO_ITEM_NOT_FOUND);

        GZResponse actual = podNumberStatusService.updateJobStatus(podNumberStatusRequest, "2019-01-15");

        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateJobStatusRejectRemarkDropdownAllCase() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(212);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setRemark("test|");
        podNumberDetailRequest.setFieldType("job");

        GZResponse response;
        GZResponse actual;

        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_END, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        List<ManifestoItem> manifestoItemList = new ArrayList<>();
        ManifestoItem manifestoItem = new ManifestoItem();
        manifestoItem.setNewQuantity(10);
        manifestoItem.setQuantity(10);
        manifestoItem.setId(1);
        manifestoItemList.add(manifestoItem);

        Mockito.when(manifestoItemRepository.findBymanifestoId(Mockito.anyInt())).thenReturn(manifestoItemList);
        Mockito.when(manifestoItemRepository.save(Mockito.any(ManifestoItem.class))).thenReturn(manifestoItem);

        //case dropdown no int
        response = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REMARK_IS_REQUEST);
        actual = podNumberStatusService.updateJobStatus(podNumberDetailRequest, "2019-01-15");
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
        //end

        podNumberDetailRequest.setRemark("test|test");

        //case REMARK_NOT_FOUND
        response = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REMARK_NOT_FOUND);
        actual = podNumberStatusService.updateJobStatus(podNumberDetailRequest, "2019-01-15");
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
        //end

        podNumberDetailRequest.setRemark("test|1");
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(null);

        //case REMARK_NOT_FOUND
        response = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.REMARK_NOT_FOUND);
        actual = podNumberStatusService.updateJobStatus(podNumberDetailRequest, "2019-01-15");
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());
        //end

        podNumberDetailRequest.setRemark("test|1");
        podNumberDetailRequest.setUpdateBy(1);

        MasterReasonRejectManifesto masterReasonRejectManifesto = new MasterReasonRejectManifesto();
        masterReasonRejectManifesto.setDescription("tsst");
        masterReasonRejectManifesto.setTypeReject(1);
        masterReasonRejectManifesto.setId(1);
        Mockito.when(masterReasonRejectManifestoRepository.findOne(Mockito.anyInt())).thenReturn(masterReasonRejectManifesto);

        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);

        jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(1);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        MasterStatus masterStatus = new MasterStatus(1, "JOB", "JOB.Driver.Confirm.And.Approve", PODNumberStatusConstant.DRIVER, true, false);
        Mockito.when(masterStatusRepository.findById(Mockito.anyInt())).thenReturn(masterStatus);

        response = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestolistConstant.UPDATE_MANIFESTO_STAUTS_SUCCESS);
        actual = podNumberStatusService.updateJobStatus(podNumberDetailRequest, "2019-01-15");
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void updateJobStatusCaseJobSuccess() throws Exception {
        PODNumberStatusRequest podNumberDetailRequest = new PODNumberStatusRequest();
        podNumberDetailRequest.setStatusId(211);
        podNumberDetailRequest.setRefId("1");
        podNumberDetailRequest.setFieldType("job");
        podNumberDetailRequest.setUpdateBy(1);

        GZResponse response;
        GZResponse actual;

        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);

        JobStatus jobStatus = new JobStatus(1, PODNumberStatusConstant.JOB_DRIVER_END, "", 1, currentDate);
        Mockito.when(jobStatusRepository.findLastByjobIdAndDelivery(Mockito.anyInt(), Mockito.anyString())).thenReturn(jobStatus);

        List<ManifestoItem> manifestoItemList = new ArrayList<>();
        ManifestoItem manifestoItem = new ManifestoItem();
        manifestoItem.setNewQuantity(10);
        manifestoItem.setQuantity(10);
        manifestoItem.setId(1);
        manifestoItemList.add(manifestoItem);
        manifestoItem = new ManifestoItem();
        manifestoItem.setNewQuantity(5);
        manifestoItem.setQuantity(10);
        manifestoItem.setId(2);
        manifestoItemList.add(manifestoItem);

        Mockito.when(manifestoItemRepository.findBymanifestoId(Mockito.anyInt())).thenReturn(manifestoItemList);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setLoginId(11);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);

        jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(213);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);

        Manifestolist manifestoSetDelay = new Manifestolist();
        manifestoSetDelay.setId(1);
        manifestoSetDelay.setJobId("1");
        manifestoSetDelay.setDelayDate("2019-01-15");
        manifestoSetDelay.setDeliveryDate("2019-01-15");

        Mockito.when(manifestolistRepository.findAllManifestolistById(Mockito.anyInt())).thenReturn(manifestoSetDelay);
        Mockito.when(manifestolistRepository.save(Mockito.any(Manifestolist.class))).thenReturn(manifestoSetDelay);

        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        MasterStatus masterStatus = new MasterStatus(1, "JOB", "JOB.Driver.Confirm.And.Approve", PODNumberStatusConstant.DRIVER, true, false);
        Mockito.when(masterStatusRepository.findById(Mockito.anyInt())).thenReturn(masterStatus);

        response = new GZResponse(ManifestolistConstant.UPDATE_MANIFESTO_STAUTS, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestolistConstant.UPDATE_MANIFESTO_STAUTS_SUCCESS);
        actual = podNumberStatusService.updateJobStatus(podNumberDetailRequest, "2019-01-15");
        Assert.assertEquals(response.getTitle(), actual.getTitle());
        Assert.assertEquals(response.getCode(), actual.getCode());
        Assert.assertEquals(response.getMessage(), actual.getMessage());
        Assert.assertEquals(response.getDeveloperMessage(), actual.getDeveloperMessage());

    }



    @Test
    public void validateUpdateStatusReturnFalseCaseUserNotFound() throws Exception {
        UserTypeMaster driver = null;

        Mockito.when(commonService.getUserTypeMasterForReturn(Mockito.anyInt())).thenReturn(driver);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.VALIDATE_UPDATE_STATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        ValidateUpdateShippingRequest validateUpdateShippingRequest = new ValidateUpdateShippingRequest();
        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        validateUpdateShippingRequest.setStatusId(211);
        validateUpdateShippingRequest.setLoginId(1);
        validateUpdateShippingRequest.setManifestoIds(manifestoIds);
        GZResponse actual = podNumberStatusService.validateOwner(validateUpdateShippingRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateDelayDate() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(1);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");

        Mockito.when(manifestolistRepository.findBypodNumberAndDeliveryDate(Mockito.anyString(), Mockito.anyString())).thenReturn(listManifestoList);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday).thenReturn(listManifestoToday);
        Mockito.when(manifestolistRepository.findOne(Mockito.anyInt())).thenReturn(manifestolist);
        Mockito.when(manifestolistRepository.save(Mockito.any(Manifestolist.class))).thenReturn(manifestolist);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        Timestamp currentDate = new java.sql.Timestamp(new java.util.Date().getTime());

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_DELAY_SUCCESS);

        GZResponse actual = podNumberStatusService.updateDelayDate(podNumberStatusRequest, "2018-10-10", currentDate, 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void changeDeliveryDateForDelay() throws Exception
    {
        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        dataFromSOMSRequest.setDeliveryDate("");

        List<Integer> manifestoIdList = new ArrayList<>();
        manifestoIdList.add(2001);
        manifestoIdList.add(2002);
        manifestoIdList.add(2003);

        List<JobStatus> jobStatusList = new ArrayList<>();

        JobStatus jobStatus = new JobStatus();
        jobStatus.setJobId(2001);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        jobStatusList.add(jobStatus);

        Mockito.when(manifestolistRepository.findIdByDeliveryDateAndDelayDateIsNotNull(Mockito.anyString())).thenReturn(manifestoIdList);
        Mockito.when(jobStatusRepository.findJobIdByStatusIdListAndDelivery(Mockito.anyListOf(Integer.class), Mockito.anyListOf(Integer.class), Mockito.anyString())).thenReturn(jobStatusList);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED
                , HttpStatus.OK
                , HttpStatus.OK.toString()
                , PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        GZResponse actual = podNumberStatusService.changeDeliveryDateForDelay(dataFromSOMSRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void stampDelay() throws Exception
    {
        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        dataFromSOMSRequest.setDeliveryDate("");

        List<Integer> manifestoIdList = new ArrayList<>();
        manifestoIdList.add(2001);
        manifestoIdList.add(2002);
        manifestoIdList.add(2003);

        List<JobStatus> jobStatusList = new ArrayList<>();

        JobStatus jobStatus = new JobStatus();
        jobStatus.setJobId(2001);
        jobStatus.setStatusId(PODNumberStatusConstant.JOB_CHECKER_CONFIRM);
        jobStatusList.add(jobStatus);

        Mockito.when(manifestolistRepository.findIdByDeliveryDateAndDelayDateIsNull(Mockito.anyString())).thenReturn(manifestoIdList);
        Mockito.when(jobStatusRepository.findJobIdByStatusIdListAndDelivery(Mockito.anyList(), Mockito.anyList(), Mockito.anyString())).thenReturn(jobStatusList);

        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED
                , HttpStatus.OK
                , HttpStatus.OK.toString()
                , PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        GZResponse actual = podNumberStatusService.stampDelay(dataFromSOMSRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateDriverJobStatusStartCaseSuccess() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        List<Integer> listManifestoPrepareStart = new ArrayList<>();
        listManifestoPrepareStart.add(2113);

        List<Integer> listManifestoProcessStart = new ArrayList<>();

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(1);


        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listManifestoPrepareStart);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday).thenReturn(listManifestoToday).thenReturn(listManifestoProcessStart);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2115);
        podNumberStatusRequest.setStartDate("2018-10-10 15:52:00");
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void updateDriverJobStatusStartCaseDriverCannotStart() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(1);


        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listManifestoToday);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2115);
        podNumberStatusRequest.setStartDate("2018-10-10 15:52:00");
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.DRIVER_CAN_NOT_START);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateDriverJobStatusStartCaseStartIsRquest() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2115);
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.START_IS_REQUEST);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }
    @Test
    public void updateDriverJobStatusStartCaseManifestNotMappingStart() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        List<Integer> listManifestoPrepareStart = new ArrayList<>();
        listManifestoPrepareStart.add(2113);

        List<Integer> listManifestoMaching1 = new ArrayList<>();

        List<Integer> listManifestoMaching2 = new ArrayList<>();
        listManifestoMaching2.add(2113);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listManifestoPrepareStart);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoMaching1).thenReturn(listManifestoMaching2);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2115);
        podNumberStatusRequest.setStartDate("2018-10-10 15:52:00");
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_MAP);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }


    @Test
    public void updateDriverJobStatusEnd() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2115);

        List<Integer> listManifestoPrepareStart = new ArrayList<>();
        listManifestoPrepareStart.add(2115);


        JobStatus jobStatus = new JobStatus();

        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(1);


        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listManifestoPrepareStart);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday).thenReturn(listManifestoToday).thenReturn(listManifestoToday);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2116);
        podNumberStatusRequest.setEndDate("2018-10-10 15:52:00");
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateDriverJobStatusStartCaseEndIsRquest() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2116);
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.END_IS_REQUEST);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateDriverJobStatusStartCaseManifestNotMappingEnd() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        List<Integer> listManifestoPrepareStart = new ArrayList<>();
        listManifestoPrepareStart.add(2113);

        List<Integer> listManifestoMaching1 = new ArrayList<>();

        List<Integer> listManifestoMaching2 = new ArrayList<>();
        listManifestoMaching2.add(2113);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listManifestoPrepareStart);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoMaching1).thenReturn(listManifestoMaching2);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2116);
        podNumberStatusRequest.setEndDate("2018-10-10 15:52:00");
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.MANIFESTO_NOT_MAP);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void updateDriverJobStatusStartCaseDriverCannotEnd() throws Exception {
        List<Manifestolist> listManifestoList = new ArrayList<>();
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(1);
        manifestolist.setPodNumber("092018-5023");
        manifestolist.setCustomerName("customer_test");
        manifestolist.setRouteMasterId(1);
        manifestolist.setRouteName("RO01");
        manifestolist.setShipToAddress("ship_Address_test");
        manifestolist.setDriverId(1);
        manifestolist.setBoxQty(1);
        manifestolist.setNewBoxQty(1);
        manifestolist.setItemQty(1);
        manifestolist.setSKUQty(1);
        manifestolist.setRemark("remark_test");
        manifestolist.setManifestoType("soms_order");
        manifestolist.setJobId("jobId");
        manifestolist.setNote("note");
        listManifestoList.add(manifestolist);

        List<Integer> listManifestoToday = new ArrayList<>();
        listManifestoToday.add(2113);

        List<Integer> listPodStartIsEmpty= new ArrayList<>();

        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        jobStatus.setJobId(1);
        jobStatus.setRemark("remark_test");
        jobStatus.setStatusId(1);


        GenericInformation genericInformation = new GenericInformation();
        genericInformation.setValue("1");
        genericInformation.setId(1);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setId(1);
        userTypeMaster.setName("test");
        userTypeMaster.setUserTypeId(1);
        userTypeMaster.setLoginId(1);

        Mockito.when(manifestolistRepository.findByDeliveryDateAndDriverIdAndPodNumberIsNotNullWithManifestoId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listManifestoToday);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobId(Mockito.anyListOf(Integer.class), Mockito.anyString(), Mockito.anyListOf(Integer.class))).thenReturn(listManifestoToday).thenReturn(listManifestoToday).thenReturn(listPodStartIsEmpty);
        Mockito.when(commonService.getUserTypeMaster(Mockito.anyString(), Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(jobStatusRepository.save(Mockito.any(JobStatus.class))).thenReturn(jobStatus);
        Mockito.when(genericInformationRepository.save(Mockito.any(GenericInformation.class))).thenReturn(genericInformation);

        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        podNumberStatusRequest.setRefId("092018-5023");
        podNumberStatusRequest.setRemark("test unit test");
        podNumberStatusRequest.setUpdateBy(1);
        podNumberStatusRequest.setFieldType("podNumber");
        podNumberStatusRequest.setStatusId(2116);
        podNumberStatusRequest.setEndDate("2018-10-10 15:52:00");
        podNumberStatusRequest.setDelay(false);
        podNumberStatusRequest.setManifestoIds(listManifestoToday);
        GZResponse expect = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.DRIVER_NOT_START_DELIVER);

        GZResponse actual = podNumberStatusService.driverStartAndEndShipped(podNumberStatusRequest, "2018-10-10", 1);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }
}
