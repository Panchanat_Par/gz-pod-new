package com.gz.pod.services.driver;

import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.common.MasterStatusRepository;
import com.gz.pod.categories.common.UserTypeMasterRepository;
import com.gz.pod.categories.driver.DriverConstant;
import com.gz.pod.categories.driver.DriverService;
import com.gz.pod.categories.driver.requests.DriverListRequest;
import com.gz.pod.categories.driver.responses.DriverListResponse;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.entities.MasterStatus;
import com.gz.pod.entities.UserTypeMaster;
import com.gz.pod.response.GZResponse;
import com.gz.pod.response.GZResponsePage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DriverServiceTest {

    @InjectMocks
    private DriverService driverService;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private UserTypeMasterRepository userTypeMasterRepository;

    @Mock
    private MasterStatusRepository masterStatusRepository;

    @Mock
    private JobStatusRepository jobStatusRepository;

    @Mock
    private CommonService commonService;


    @Before
    public void runBeforeTestMethod() {
        List<MasterStatus> masterStatuses = new ArrayList<>();
        masterStatuses.add(new MasterStatus(2113, "POD", "JOB.Driver.Confirm.And.Approve"));
        masterStatuses.add(new MasterStatus(220, "POD", "JOB.Driver.Request.For.Checker"));

        List<Integer> driverList = new ArrayList<>();
        driverList.add(1);

        UserTypeMaster userTypeMasterLogin = new UserTypeMaster();
        userTypeMasterLogin.setName("checkerName");
        userTypeMasterLogin.setId(1);
        userTypeMasterLogin.setLoginId(11);
        userTypeMasterLogin.setUserTypeId(2);

        UserTypeMaster userTypeMaster = new UserTypeMaster();
        userTypeMaster.setName("driverName");
        userTypeMaster.setId(2);
        userTypeMaster.setLoginId(12);
        userTypeMaster.setUserTypeId(1);

        Mockito.when(masterStatusRepository.findAll()).thenReturn(masterStatuses);
        Mockito.when(manifestolistRepository.findByDeliveryDateGroupByDriver(Mockito.anyString())).thenReturn(driverList);
        Mockito.when(userTypeMasterRepository.findById(Mockito.anyInt())).thenReturn(userTypeMaster);
        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMasterLogin);

    }

    @Test
    public void getDriverListApproval()  throws Exception {
        GZResponsePage gzResponsePage = new GZResponsePage();
        DriverListRequest driverListRequest = new DriverListRequest();
        driverListRequest.setDeliveryDate("2019-01-15");
        driverListRequest.setLoginId(11);

        List<DriverListResponse> driverListResponse = new ArrayList<>();

        DriverListResponse driverListResponse1 = new DriverListResponse();
        driverListResponse1.setLoginId(12);
        driverListResponse1.setName("driverName");
        driverListResponse1.setStatus("JOB.Driver.Confirm.And.Approve");
        driverListResponse.add(driverListResponse1);

        List<Integer> listJobId = new ArrayList<>();
        listJobId.add(1);
        listJobId.add(2);
        listJobId.add(3);
        listJobId.add(4);

        List<Integer> allLastJobStatus = new ArrayList<>();
        allLastJobStatus.add(2113);
        allLastJobStatus.add(2114);
        allLastJobStatus.add(212);
        allLastJobStatus.add(216);

        Mockito.when(manifestolistRepository.findAllJobIdbyDriverId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listJobId);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobIdAndDeliveryDate(Mockito.anyListOf(Integer.class),Mockito.anyString(),Mockito.anyListOf(Integer.class))).thenReturn(allLastJobStatus);

        int countApproval = 1;
        int countAwaitingApproval = 0;

        gzResponsePage.setTotalItems(driverListResponse.size());
        gzResponsePage.setTotalApproval(countApproval);
        gzResponsePage.setTotalWaitingApproval(countAwaitingApproval);
        gzResponsePage.setData(driverListResponse);

        GZResponsePage actual = driverService.getDriverList(driverListRequest);

        Assert.assertEquals("1", actual.getTotalItems().toString());
        Assert.assertEquals(driverListResponse, actual.getData());
    }

    @Test
    public void getDriverListAwaiting()  throws Exception {
        GZResponsePage gzResponsePage = new GZResponsePage();
        DriverListRequest driverListRequest = new DriverListRequest();
        driverListRequest.setDeliveryDate("2019-01-16");
        driverListRequest.setLoginId(11);


        List<DriverListResponse> driverListResponse = new ArrayList<>();
        DriverListResponse driverListResponse1 = new DriverListResponse();
        driverListResponse1.setLoginId(12);
        driverListResponse1.setName("driverName");
        driverListResponse1.setStatus("JOB.Driver.Request.For.Checker");
        driverListResponse.add(driverListResponse1);

        List<Integer> listJobId = new ArrayList<>();
        listJobId.add(1);
        listJobId.add(2);

        List<Integer> allLastJobStatus = new ArrayList<>();
        allLastJobStatus.add(220);
        allLastJobStatus.add(212);

        Mockito.when(manifestolistRepository.findAllJobIdbyDriverId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listJobId);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobIdAndDeliveryDate(Mockito.anyListOf(Integer.class),Mockito.anyString(),Mockito.anyListOf(Integer.class))).thenReturn(allLastJobStatus);

        int countApproval = 0;
        int countAwaitingApproval = 1;

        gzResponsePage.setTotalItems(driverListResponse.size());
        gzResponsePage.setTotalApproval(countApproval);
        gzResponsePage.setTotalWaitingApproval(countAwaitingApproval);
        gzResponsePage.setData(driverListResponse);

        GZResponsePage actual = driverService.getDriverList(driverListRequest);

        Assert.assertEquals("1", actual.getTotalItems().toString());
        Assert.assertEquals(driverListResponse, actual.getData());
    }

    @Test
    public void getDriverListStatusEmpty()  throws Exception {
        GZResponsePage gzResponsePage = new GZResponsePage();
        DriverListRequest driverListRequest = new DriverListRequest();
        driverListRequest.setDeliveryDate("2019-01-16");
        driverListRequest.setLoginId(11);


        List<DriverListResponse> driverListResponse = new ArrayList<>();
        DriverListResponse driverListResponse1 = new DriverListResponse();
        driverListResponse1.setLoginId(12);
        driverListResponse1.setName("driverName");
        driverListResponse1.setStatus("");
        driverListResponse.add(driverListResponse1);

        List<Integer> listJobId = new ArrayList<>();
        listJobId.add(1);
        listJobId.add(2);

        List<Integer> allLastJobStatus = new ArrayList<>();
        allLastJobStatus.add(214);
        allLastJobStatus.add(219);

        Mockito.when(manifestolistRepository.findAllJobIdbyDriverId(Mockito.anyString(), Mockito.anyInt())).thenReturn(listJobId);
        Mockito.when(jobStatusRepository.findLastJobStatusByListJobIdAndDeliveryDate(Mockito.anyListOf(Integer.class),Mockito.anyString(),Mockito.anyListOf(Integer.class))).thenReturn(allLastJobStatus);

        int countApproval = 0;
        int countAwaitingApproval = 0;

        gzResponsePage.setTotalItems(driverListResponse.size());
        gzResponsePage.setTotalApproval(countApproval);
        gzResponsePage.setTotalWaitingApproval(countAwaitingApproval);
        gzResponsePage.setData(driverListResponse);

        GZResponsePage actual = driverService.getDriverList(driverListRequest);

        Assert.assertEquals("1", actual.getTotalItems().toString());
        Assert.assertEquals(driverListResponse, actual.getData());
    }

    @Test
    public void getDriverListWhendriverListNotFound() throws Exception {
        GZResponsePage expect = new  GZResponsePage(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), DriverConstant.GET_DRIVER_LIST_NOT_FOUND);

        List<MasterStatus> masterStatuses = new ArrayList<>();
        masterStatuses.add(new MasterStatus(120, "POD", "POD.Checker.Status.Wait"));
        Mockito.when(masterStatusRepository.findAll()).thenReturn(masterStatuses);
        Mockito.when(manifestolistRepository.findByDeliveryDateGroupByDriver(Mockito.anyString())).thenReturn(null);

        DriverListRequest driverListRequest = new DriverListRequest();
        driverListRequest.setDeliveryDate("2018-09-28");
        driverListRequest.setLoginId(2);

        GZResponsePage actual = driverService.getDriverList(driverListRequest);
        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());
    }

    @Test
    public void getdriverListByNotchecker() throws Exception {
        DriverListRequest driverListRequest = new DriverListRequest();
        driverListRequest.setDeliveryDate("2019-01-16");
        driverListRequest.setLoginId(12);

        UserTypeMaster userTypeMasterLogin = new UserTypeMaster();
        userTypeMasterLogin.setName("checkerName");
        userTypeMasterLogin.setId(1);
        userTypeMasterLogin.setLoginId(12);
        userTypeMasterLogin.setUserTypeId(1);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(userTypeMasterLogin);

        GZResponsePage expect = new GZResponsePage(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), DriverConstant.NOT_CHECKER);

        GZResponsePage actual = driverService.getDriverList(driverListRequest);

        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

    }

    @Test
    public void getdriverListByNotFoundUser() throws Exception {
        DriverListRequest driverListRequest = new DriverListRequest();
        driverListRequest.setDeliveryDate("2019-01-16");
        driverListRequest.setLoginId(999);

        Mockito.when(userTypeMasterRepository.findByloginId(Mockito.anyInt())).thenReturn(null);

        GZResponsePage expect = new GZResponsePage(DriverConstant.GET_DRIVER_LIST, HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.toString(), PODNumberStatusConstant.USER_NOT_FOUND);

        GZResponsePage actual = driverService.getDriverList(driverListRequest);

        Assert.assertEquals(expect.getTitle(), actual.getTitle());
        Assert.assertEquals(expect.getMessage(), actual.getMessage());
        Assert.assertEquals(expect.getCode(), actual.getCode());
        Assert.assertEquals(expect.getDeveloperMessage(), actual.getDeveloperMessage());

    }


}
