package com.gz.pod.services.logs;

import com.gz.pod.categories.logs.LogConstant;
import com.gz.pod.categories.logs.LogRepository;
import com.gz.pod.categories.logs.LogService;
import com.gz.pod.categories.logs.requests.LogRequest;
import com.gz.pod.entities.SOMSManifestoTracking;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LogServiceTest {

    private LogService logService;

    @Mock
    private LogRepository logRepository;

    @Before
    public void setUp() throws Exception {
        logService = new LogService(logRepository);
    }

    @Test
    public void setLog() throws Exception {

        //Arrange
        LogRequest logRequest = new LogRequest();
        logRequest.setDocType("Log");
        logRequest.setDocRef("12345678");
        logRequest.setCreateBy(1);
        logRequest.setStatusId(2);
        logRequest.setSaleOrderItemID(0);


        //Action
        GZResponse gzResponse = logService.setLog(logRequest);

        //Assert
        Assert.assertEquals(LogConstant.ADD_NEW_LOG, gzResponse.getTitle());
    }

    @Test
    public void setLogs() throws Exception {
        //Arrange
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        SOMSManifestoTracking request = new SOMSManifestoTracking();
        request.setDocType("Log");
        request.setDocRef("12345678");
        request.setCreateBy(1);
        request.setStatusId(2);
        request.setCreateDate(date.toString());
        request.setSaleOrderItem(0);
        String title = LogConstant.ADD_NEW_LOG;
        String devMessage = LogConstant.ADD_LOG_SUCCESS;

        when(logRepository.save(request)).thenReturn(request);


        //Action
        GZResponse gzResponse = logService.setLogs(request, title, devMessage);

        //Assert
        Assert.assertEquals(LogConstant.ADD_NEW_LOG, gzResponse.getTitle());
    }

    @Test
    public void updateLogs() throws Exception {

        //Arrange
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        int updateBy = 1;
        String updateDate = date.toString();
        SOMSManifestoTracking request = new SOMSManifestoTracking();
        request.setDocType("Log");
        request.setDocRef("12345678");
        request.setCreateBy(1);
        request.setStatusId(2);
        request.setCreateDate(date.toString());
        request.setSaleOrderItem(0);
        String title = LogConstant.UPDATE_LOG;
        String devMessage = LogConstant.UPDATE_LOG_SUCCESS;


        //Action
        GZResponse gzResponse = logService.updateLogs(updateBy, updateDate, request, title, devMessage);

        //Assert
        Assert.assertEquals(LogConstant.UPDATE_LOG, gzResponse.getTitle());
    }

    @Test
    public void setDataUpdateLog() throws Exception {

        //Arrange
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        SOMSManifestoTracking request = new SOMSManifestoTracking();
        request.setDocType("Log");
        request.setDocRef("12345678");
        request.setCreateBy(1);
        request.setStatusId(2);
        request.setCreateDate(date.toString());
        request.setSaleOrderItem(0);

        LogRequest logRequest = new LogRequest();
        logRequest.setDocType("Log");
        logRequest.setDocRef("12345678");
        logRequest.setCreateBy(1);
        logRequest.setStatusId(2);
        logRequest.setSaleOrderItemID(0);

        //Action
        GZResponse gzResponse = logService.setData(request, logRequest);

        //Assert
        Assert.assertEquals(LogConstant.UPDATE_LOG, gzResponse.getTitle());
    }

    @Test
    public void setDataAddNewLog() throws Exception {

        //Arrange
        SOMSManifestoTracking request = null;

        LogRequest logRequest = new LogRequest();
        logRequest.setDocType("Log");
        logRequest.setDocRef("12345678");
        logRequest.setCreateBy(1);
        logRequest.setStatusId(2);
        logRequest.setSaleOrderItemID(0);

        //Action
        GZResponse gzResponse = logService.setData(request, logRequest);

        //Assert
        Assert.assertEquals(LogConstant.ADD_NEW_LOG, gzResponse.getTitle());
    }
}
