package com.gz.pod.services.datafromsoms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.common.CommonService;
import com.gz.pod.categories.datafromsoms.DataFromSOMSConstant;
import com.gz.pod.categories.datafromsoms.DataFromSOMSService;
import com.gz.pod.categories.datafromsoms.model.DataFromSOMSModel;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.JobStatusToSOMSRequest;
import com.gz.pod.categories.datafromsoms.requests.PicIdRequest;
import com.gz.pod.categories.jobStatus.JobStatusRepository;
import com.gz.pod.categories.manifestoItem.ManifestoItemRepository;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistRepository;
import com.gz.pod.entities.JobStatus;
import com.gz.pod.entities.ManifestoItem;
import com.gz.pod.entities.Manifestolist;
import com.gz.pod.response.GZResponsePage;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.eq;

@RunWith(MockitoJUnitRunner.class)
public class DataFormSOMSServiceTest {

    private ModelMapper modelMapper = new ModelMapper();


    @Mock
    private ManifestoItemRepository manifestoItemRepository;

    @Mock
    private ManifestolistRepository manifestolistRepository;

    @Mock
    private JobStatusRepository jobStatusRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    private DataFromSOMSService dataFromSOMSService = new DataFromSOMSService(manifestolistRepository,restTemplate,manifestoItemRepository,jobStatusRepository);

    @Mock
    CommonService commonService;

    @Ignore
    @Test
    public void ImportManifestoListFromSOMSSuccess() throws Exception {

        DataFromSOMSRequest request = new DataFromSOMSRequest();
        List<Manifestolist> manifestoList = ManifestolistConstant.GET_MANIFESTO_LIST();
        ManifestoItem manifestoItem = new ManifestoItem();
        manifestoItem.setId(0);
        manifestoItem.setSoItem("20");
        manifestoItem.setMatDoc("11111");
        manifestoItem.setDocDate("2018-09-14T00:00:00");
        manifestoItem.setPostDate("2018-09-14T00:00:00");
        manifestoItem.setProductCode("11111");
        manifestoItem.setMaterialCode("11111");
        manifestoItem.setProductName("Test mock data");
        manifestoItem.setUom("EA");
        manifestoItem.setQuantity(100);
        manifestoItem.setNewQuantity(100);
        manifestoItem.setAmount(new BigDecimal(1)  );
        manifestoItem.setRemark("remark");
        manifestoItem.setRoundGet(1);
        List<DataFromSOMSModel> listData = new ArrayList<>();
        DataFromSOMSModel dataFromSOMSModel = new DataFromSOMSModel();
        dataFromSOMSModel.setRemark("remark");
        dataFromSOMSModel.setDriverId(1);
        dataFromSOMSModel.setShipToCode("11");
        dataFromSOMSModel.setCustomerName("customer_test");
        dataFromSOMSModel.setDeliveryDate("2018-10-10");
        dataFromSOMSModel.setPODFlag("N");
        listData.add(dataFromSOMSModel);
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(listData);
        String jsonItem = DataFromSOMSConstant.GET_ITEM;

        final String secretKey = "secreKeyToTest";
        Mockito.when(commonService.generateSignature(Mockito.any())).thenReturn(secretKey);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(jsonInString,HttpStatus.OK );
        ResponseEntity<String> responseEntityGet = new ResponseEntity<String>(jsonItem,HttpStatus.OK );
        JobStatus jobStatus = new JobStatus();
        jobStatus.setId(1);
        Mockito.when(manifestolistRepository.getManifestolistPODNumberNull(Mockito.anyString())).thenReturn(manifestoList);
        Mockito.when(manifestolistRepository.getExitingPODNumber(Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(manifestoList);
        Mockito.when(jobStatusRepository.save((JobStatus) Mockito.anyObject())).thenReturn(jobStatus);
        Mockito.when(manifestolistRepository.save((Manifestolist) Mockito.anyObject())).thenReturn(ManifestolistConstant.MANIFESTO_RESPONSE);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.POST),Mockito.anyObject(),eq(String.class))).thenReturn(responseEntity);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.PUT),Mockito.anyObject(),eq(String.class))).thenReturn(responseEntity);
        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.GET),Mockito.anyObject(),eq(String.class))).thenReturn(responseEntityGet);
        Mockito.when(manifestolistRepository.findManifestolistNotGetItem()).thenReturn(manifestoList);
        Mockito.when(manifestoItemRepository.save((ManifestoItem) Mockito.anyObject())).thenReturn(manifestoItem);
        Mockito.when(manifestolistRepository.findManifestolistPODFlagNotOld()).thenReturn(manifestoList);

        GZResponsePage expect = new GZResponsePage();
        expect.setCode(HttpStatus.OK);
        GZResponsePage actual = dataFromSOMSService.importManifestoList(request);
        Assert.assertEquals(expect.getCode(), actual.getCode());

        List<Manifestolist>  notList = new ArrayList<>();
        Mockito.when(manifestolistRepository.getExitingPODNumber(Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(notList);
        GZResponsePage expect2 = new GZResponsePage();
        expect2.setCode(HttpStatus.OK);
        GZResponsePage actual2 = dataFromSOMSService.importManifestoList(request);
        Assert.assertEquals(expect2.getCode(), actual2.getCode());
    }

    @Test
    public void callAPIPostStampStatusOrder() throws Exception {
        List<PicIdRequest> picIdRequests = new ArrayList<>();
        PicIdRequest picIdRequest1 = new PicIdRequest(1);
        picIdRequests.add(picIdRequest1);
        JobStatusToSOMSRequest jobStatusToSOMSRequest = new JobStatusToSOMSRequest("210", "order", "", 211, "2018-11-26", 1, picIdRequests);

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(jobStatusToSOMSRequest);
        ResponseEntity<String> responseEntity = new ResponseEntity<String>(jsonInString, HttpStatus.OK );

        final String secretKey = "secreKeyToTest";
        Mockito.when(commonService.generateSignature(Mockito.any())).thenReturn(secretKey);

        Mockito.when(restTemplate.exchange(Mockito.anyString(), eq(HttpMethod.POST),Mockito.anyObject(),eq(String.class))).thenReturn(responseEntity);

        dataFromSOMSService.callAPIPostStampStatusOrder(jobStatusToSOMSRequest);

    }

    @Test
    public void updatePODNumber_1_Success()
    {
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(201);

        List<Manifestolist> manifestoList = new ArrayList<>();
        manifestoList.add(manifestolist);

        Mockito.when(manifestolistRepository.getManifestolistPODNumberNull(Mockito.anyString())).thenReturn(manifestoList);

        dataFromSOMSService.UpdatePODNumber("2019-01-05");
    }

    @Test
    public void updatePODNumber_2_Success()
    {
        Manifestolist manifestolist = new Manifestolist();
        manifestolist.setId(201);

        List<Manifestolist> manifestoList = new ArrayList<>();
        manifestoList.add(manifestolist);

        Mockito.when(manifestolistRepository.getManifestolistByIdListPODNumberNull(Mockito.anyString(), Mockito.anyList())).thenReturn(manifestoList);

        List<Integer> listInt = new ArrayList<>();
        listInt.add(201);
        dataFromSOMSService.UpdatePODNumber("2019-01-05", listInt);
    }
}
