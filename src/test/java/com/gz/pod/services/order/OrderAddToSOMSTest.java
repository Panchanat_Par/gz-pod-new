package com.gz.pod.services.order;

import com.gz.pod.categories.order.OrderConstant;
import com.gz.pod.categories.order.OrderRepository;
import com.gz.pod.categories.order.OrderService;
import com.gz.pod.categories.order.requests.OrderRequest;
import com.gz.pod.categories.order.responses.OrderResponse;
import com.gz.pod.entities.SOMSOrder;
import com.gz.pod.response.GZResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderAddToSOMSTest {

    private OrderService orderService;
    private ModelMapper modelMapper = new ModelMapper();

    @Mock
    private OrderRepository orderRepository;

    @Before
    public void setUp() throws Exception {
        orderService = new OrderService(orderRepository);
    }

    @Test
    public void shouldReturnSOMSOrderWhenAddOrderSuccess() throws Exception {
        //Arrange
        OrderRequest orderRequest = OrderConstant.getOrderRequest();
        OrderResponse orderResponse = OrderConstant.getOrderResponse();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        when(orderRepository.save(modelMapper.map(orderRequest, SOMSOrder.class)))
                .thenReturn(modelMapper.map(orderResponse, SOMSOrder.class));

        //Action
        GZResponse gzResponse = orderService.addOrderInSOMS(orderRequest);

        //Assert
        verify(orderRepository, times(1)).save(modelMapper.map(orderRequest, SOMSOrder.class));
        Assert.assertEquals(OrderConstant.ADD_ORDER, gzResponse.getTitle());
    }
}
