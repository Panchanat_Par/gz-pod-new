package com.gz.pod.controllers.reject;

import com.gz.pod.categories.reject.RejectConstant;
import com.gz.pod.categories.reject.RejectController;
import com.gz.pod.categories.reject.RejectService;
import com.gz.pod.categories.reject.requests.RejectRequest;
import com.gz.pod.response.GZResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class RejectControllerTest {
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private RejectController rejectController;

    @Mock
    private RejectService rejectService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(rejectController).build();
    }

    @Test
    public void getRejectList() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(RejectConstant.REJECT_LIST);
        gzResponse.setDeveloperMessage(RejectConstant.GET_REJECT_SUCCESS);

        Mockito.when(rejectService.getRejectList()).thenReturn(gzResponse);

        String url = "/getRejectReason/rejectList";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url)
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(RejectConstant.REJECT_LIST))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(RejectConstant.GET_REJECT_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void getRejectListByType() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(RejectConstant.REJECT_LIST);
        gzResponse.setDeveloperMessage(RejectConstant.GET_REJECT_SUCCESS);

        Mockito.when(rejectService.getRejectListByType(Mockito.any(RejectRequest.class))).thenReturn(gzResponse);

        String url = "/getRejectReason/rejectListByType";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?typeReject=1")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(RejectConstant.REJECT_LIST))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(RejectConstant.GET_REJECT_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }
}
