package com.gz.pod.controllers.datafromsoms;

import com.gz.pod.categories.datafromsoms.DataFromSOMSConstant;
import com.gz.pod.categories.datafromsoms.DataFromSOMSController;
import com.gz.pod.categories.datafromsoms.DataFromSOMSService;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.response.GZResponsePage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class DataFromSOMSControllerTest {
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private DataFromSOMSController dataFromSOMSController;

    @Mock
    private DataFromSOMSService dataFromSOMSService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(dataFromSOMSController).build();
    }

    @Test
    public void importManifestoListTest() throws Exception{
        GZResponsePage gzResponsePage = new GZResponsePage();
        gzResponsePage.setCode(HttpStatus.OK);
        gzResponsePage.setMessage(HttpStatus.OK.toString());
        gzResponsePage.setTitle(DataFromSOMSConstant.TRANSFER_DATA);
        gzResponsePage.setDeveloperMessage(DataFromSOMSConstant.TRANSFER_SUCCESS);

        Mockito.when(dataFromSOMSService.importManifestoList(Mockito.any(DataFromSOMSRequest.class))).thenReturn(gzResponsePage);

        String url = "/somsmanifesto/";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?DeliveryDate=2018-07-26")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(DataFromSOMSConstant.TRANSFER_DATA))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(DataFromSOMSConstant.TRANSFER_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void ImportManifestoListJobCaseCronJob() throws Exception {
        GZResponsePage gzResponsePage = new GZResponsePage();
        gzResponsePage.setCode(HttpStatus.OK);
        gzResponsePage.setMessage(HttpStatus.OK.toString());
        gzResponsePage.setTitle(DataFromSOMSConstant.TRANSFER_DATA);
        gzResponsePage.setDeveloperMessage(DataFromSOMSConstant.TRANSFER_SUCCESS);

        Mockito.when(dataFromSOMSService.importManifestoList(Mockito.any(DataFromSOMSRequest.class))).thenReturn(gzResponsePage);

        dataFromSOMSController.ImportManifestoListJob();
    }
}
