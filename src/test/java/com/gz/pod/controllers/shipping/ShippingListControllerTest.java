//package com.gz.pod.controllers.shipping;
//
//import com.gz.pod.categories.shippinglist.ShippingConstant;
//import com.gz.pod.categories.shippinglist.ShippinglistController;
//import com.gz.pod.categories.shippinglist.requests.ShippinglistRequest;
//import com.gz.pod.categories.shippinglist.responses.ShipToCodeResponse;
//import com.gz.pod.controllers.PathRequestConstant;
//import com.gz.pod.response.GZResponsePage;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.support.PagedListHolder;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.util.List;
//
//import static org.hamcrest.Matchers.is;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(value = ShippinglistController.class, secure = false)
//public class ShippingListControllerTest {
//    @Autowired
//    private MockMvc mvc;
//
//    @MockBean
//    private ShippinglistController shippinglistController;
//
//    @Test
//    public void shouldReturnResponseEntityOKWhenGetShippingListSuccess() throws Exception {
//        ShippinglistRequest request = new ShippinglistRequest();
//        List<ShipToCodeResponse> response = ShippingConstant.GET_SHIPTOCODE_LIST();
//        request.setDriverId(ShippingConstant.DRIVER_ID);
//        request.setDate(ShippingConstant.DATE);
//        request.setStatusSO(ShippingConstant.STATUS_SO);
//        request.setPage(ShippingConstant.PAGE);
//        request.setPerPage(ShippingConstant.PER_PAGE);
//        PagedListHolder<ShipToCodeResponse> setPage = new PagedListHolder<>(response);
//        GZResponsePage gzResponsePage = new GZResponsePage();
//        setPage.setPage(request.getPage() - 1);
//        setPage.setPageSize(request.getPerPage());
//        gzResponsePage.setTitle(ShippingConstant.SHIPPING_LIST);
//        gzResponsePage.setCode(HttpStatus.OK);
//        gzResponsePage.setMessage(HttpStatus.OK.toString());
//        gzResponsePage.setDeveloperMessage(ShippingConstant.GET_LIST_SUCCESS);
//        gzResponsePage.setCurrentPage(request.getPage());
//        gzResponsePage.setTotalPage(setPage.getPageCount());
//        gzResponsePage.setPageSize(setPage.getPageSize());
//        gzResponsePage.setTotalItems(setPage.getNrOfElements());
//        gzResponsePage.setData(response);
//
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
//                .get(PathRequestConstant.SHIPPING_LIST_URL + "?driverId=1&date=2018-07-26&statusSO=3")
//                .contentType(MediaType.APPLICATION_JSON_UTF8);
//
//        given(shippinglistController.getShippinglist(request)).willReturn(ResponseEntity.ok(gzResponsePage));
//
//        mvc.perform(builder)
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.title", is(ShippingConstant.SHIPPING_LIST)))
//                .andExpect(jsonPath("$.developerMessage", is(ShippingConstant.GET_LIST_SUCCESS)))
//                .andExpect(jsonPath("$.totalItems", is(2)));
//    }
//
//}
