package com.gz.pod.controllers;

public class PathRequestConstant {
    public static final String SHIPPING_LIST_URL = "/api/v1/shippinglist";
    public static final String ORDER_URL = "/api/v1/order";
    public static final String CONFIRM_SHIPPED_MANAGER_URL = "/api/v1/shippedlist/manager/confirmShippedByManager";
}
