//package com.gz.pod.controllers.shipped.manager;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.gz.pod.categories.shippedlist.managershipped.ManagerShippedContant;
//import com.gz.pod.categories.shippedlist.managershipped.ManagerShippedController;
//import com.gz.pod.categories.shippedlist.managershipped.requests.ManagerConfirmRequest;
//import com.gz.pod.categories.shippedlist.managershipped.responses.ManagerConfirmResponse;
//import com.gz.pod.controllers.PathRequestConstant;
//import com.gz.pod.response.GZResponse;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import static org.hamcrest.Matchers.is;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(value = ManagerShippedController.class, secure = false)
//public class ManagerShippedControllerTest {
//    @Autowired
//    private MockMvc mvc;
//
//    @MockBean
//    private ManagerShippedController managerShippedController;
//
//    @Test
//    public void shouldReturnResponseEntityOKWhenConfirmSuccess() throws Exception {
//        ObjectMapper mapper = new ObjectMapper();
//        ManagerConfirmRequest request = new ManagerConfirmRequest();
//        ManagerConfirmResponse response = new ManagerConfirmResponse();
//        request.setManifestoId(ManagerShippedContant.MANIFESTO_ID);
//        request.setIsConfirmByManager(ManagerShippedContant.IS_CONFIRM_BY_MANAGER);
//        String jsonInString = mapper.writeValueAsString(request);
//        response.setManifestoId(ManagerShippedContant.MANIFESTO_ID);
//        response.setIsConfirmByManager(ManagerShippedContant.IS_CONFIRM_BY_MANAGER);
//        response.setRemarkByManager(ManagerShippedContant.REMARK_CONFIRM);
//        response.setReasonIdByManager(ManagerShippedContant.REASON_CONFIRM);
//        GZResponse gzResponse = new GZResponse();
//        gzResponse.setData(response);
//        gzResponse.setTitle(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER);
//        gzResponse.setMessage(HttpStatus.OK.toString());
//        gzResponse.setCode(HttpStatus.OK);
//        gzResponse.setDeveloperMessage(ManagerShippedContant.CONFIRM_SHIPPED_SUCCESS);
//
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
//                .put(PathRequestConstant.CONFIRM_SHIPPED_MANAGER_URL)
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(jsonInString);
//
//        given(managerShippedController.confirmShippedByManager(request)).willReturn(ResponseEntity.ok(gzResponse));
//
//        mvc.perform(builder)
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.title", is(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER)))
//                .andExpect(jsonPath("$.developerMessage", is(ManagerShippedContant.CONFIRM_SHIPPED_SUCCESS)))
//                .andExpect(jsonPath("$.data.isConfirmByManager", is(ManagerShippedContant.IS_CONFIRM_BY_MANAGER)));
//    }
//
//    @Test
//    public void shouldReturnResponseEntityOKWhenRejectSuccess() throws Exception {
//        ObjectMapper mapper = new ObjectMapper();
//        ManagerConfirmRequest request = new ManagerConfirmRequest();
//        ManagerConfirmResponse response = new ManagerConfirmResponse();
//        request.setManifestoId(ManagerShippedContant.MANIFESTO_ID);
//        request.setIsConfirmByManager(ManagerShippedContant.IS_REJECT_BY_MANAGER);
//        request.setReasonIdByManager(ManagerShippedContant.REASON_REJECT);
//        request.setRemarkByManager(ManagerShippedContant.REMARK_REJECT);
//        String jsonInString = mapper.writeValueAsString(request);
//        response.setManifestoId(ManagerShippedContant.MANIFESTO_ID);
//        response.setIsConfirmByManager(ManagerShippedContant.IS_REJECT_BY_MANAGER);
//        response.setRemarkByManager(ManagerShippedContant.REMARK_REJECT);
//        response.setReasonIdByManager(ManagerShippedContant.REASON_REJECT);
//        GZResponse gzResponse = new GZResponse();
//        gzResponse.setData(response);
//        gzResponse.setTitle(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER);
//        gzResponse.setMessage(HttpStatus.OK.toString());
//        gzResponse.setCode(HttpStatus.OK);
//        gzResponse.setDeveloperMessage(ManagerShippedContant.CONFIRM_SHIPPED_SUCCESS);
//
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
//                .put(PathRequestConstant.CONFIRM_SHIPPED_MANAGER_URL)
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(jsonInString);
//
//        given(managerShippedController.confirmShippedByManager(request)).willReturn(ResponseEntity.ok(gzResponse));
//
//        mvc.perform(builder)
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.title", is(ManagerShippedContant.CONFIRM_SHIPPED_MANAGER)))
//                .andExpect(jsonPath("$.developerMessage", is(ManagerShippedContant.CONFIRM_SHIPPED_SUCCESS)))
//                .andExpect(jsonPath("$.data.isConfirmByManager", is(ManagerShippedContant.IS_REJECT_BY_MANAGER)))
//                .andExpect(jsonPath("$.data.remarkByManager", is(ManagerShippedContant.REMARK_REJECT)))
//                .andExpect(jsonPath("$.data.reasonIdByManager", is(ManagerShippedContant.REASON_REJECT)));
//    }
//}
