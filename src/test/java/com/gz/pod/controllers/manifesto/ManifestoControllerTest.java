package com.gz.pod.controllers.manifesto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.ManifestolistController;
import com.gz.pod.categories.manifestolist.ManifestolistService;
import com.gz.pod.categories.manifestolist.requests.*;
import com.gz.pod.categories.podNumberStatus.responses.SaveShippingResponse;
import com.gz.pod.response.GZManifestoListResponsePage;
import com.gz.pod.response.GZResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ManifestoControllerTest {
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private ManifestolistController manifestolistController;

    @Mock
    private ManifestolistService manifestolistService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(manifestolistController).build();
    }

    @Test
    public void shouldReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/manifestolist/updateBox";
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_PODNUMBER);
        gzResponse.setDeveloperMessage(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_SUCCESS);

        ManifestoPutBoxQtyRequest manifestoPutBoxQtyRequest = new ManifestoPutBoxQtyRequest();
        manifestoPutBoxQtyRequest.setUpdateBy(1);
        manifestoPutBoxQtyRequest.setBoxQty(1);
        manifestoPutBoxQtyRequest.setJobId(1);

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(manifestoPutBoxQtyRequest);

        Mockito.when(manifestolistService.updateBoxManifestoByJob(Mockito.any(ManifestoPutBoxQtyRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_PODNUMBER))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.UPDATE_MANIFESTO_BOXQTY_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }



    @Test
    public void validateAddJob() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        gzResponse.setDeveloperMessage(ManifestolistConstant.VALIDATE_ADD_JOB_SUCCESS);

        Mockito.when(manifestolistService.validateAddJob(Mockito.any(ValidateAddJobRequest.class))).thenReturn(gzResponse);

        String url = "/manifestolist/validateAddJob/";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?refModuleType=so&refId=320&driverId=77")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.VALIDATE_ADD_JOB_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void updateAddJob() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB);
        gzResponse.setDeveloperMessage(ManifestolistConstant.UPDATE_ADD_JOB_SUCCESS);

        String url = "/manifestolist/updateAddJob";

        UpdateAddJobRequest updateAddJobRequest = new UpdateAddJobRequest();
        updateAddJobRequest.setRefModuleType("so");
        updateAddJobRequest.setDriverId(88);
        updateAddJobRequest.setRefId("1");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(updateAddJobRequest);

        Mockito.when(manifestolistService.updateAddJob(Mockito.any(UpdateAddJobRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.VALIDATE_AND_UPDATE_ADD_JOB))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.UPDATE_ADD_JOB_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void validateDeleteJob() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        gzResponse.setDeveloperMessage(ManifestolistConstant.UPDATE_DELETE_JOB_SUCCESS);

        Mockito.when(manifestolistService.validateDeleteJob(Mockito.any(ValidateDeleteJobRequest.class))).thenReturn(gzResponse);

        String url = "/manifestolist/validateDeleteJob/";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?loginId=70&manifestoId=320, 310")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.VALIDATE_AND_DELETE_JOB))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.UPDATE_DELETE_JOB_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void updateDeleteJob() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.VALIDATE_AND_DELETE_JOB);
        gzResponse.setDeveloperMessage(ManifestolistConstant.UPDATE_DELETE_JOB_SUCCESS);

        String url = "/manifestolist/updateDeleteJob";

        List<Integer> manifestoList = new ArrayList<>();
        manifestoList.add(320);
        manifestoList.add(321);

        UpdateDeleteJobRequest updateDeleteJobRequest = new UpdateDeleteJobRequest();
        updateDeleteJobRequest.setLoginId(70);
        updateDeleteJobRequest.setManifestoId(manifestoList);
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(updateDeleteJobRequest);

        Mockito.when(manifestolistService.updateDeleteJob(Mockito.any(UpdateDeleteJobRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.VALIDATE_AND_DELETE_JOB))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.UPDATE_DELETE_JOB_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void rollBackManifestoReturnSuccess() throws Exception {
        String url = "/manifestolist/rollback";
        GZResponse expect = new GZResponse();
        expect.setTitle(ManifestolistConstant.ROLLBACK);
        expect.setCode(HttpStatus.OK);
        expect.setMessage(HttpStatus.OK.toString());
        expect.setDeveloperMessage(ManifestolistConstant.ROLLBACK_SUCCESS);

        ManifestoRollbackRequest manifestoRollbackRequest = new ManifestoRollbackRequest();
        manifestoRollbackRequest.setManifestoId(1);

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(manifestoRollbackRequest);

        Mockito.when(manifestolistService.rollbackManifesto(Mockito.any(ManifestoRollbackRequest.class))).thenReturn(expect);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.ROLLBACK))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.ROLLBACK_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }
    @Test
    public void getManifestoDetail() throws Exception{
        GZResponse gzResponsePage = new GZResponse();
        gzResponsePage.setCode(HttpStatus.OK);
        gzResponsePage.setMessage(HttpStatus.OK.toString());
        gzResponsePage.setTitle(ManifestolistConstant.GET_DETAIL);
        gzResponsePage.setDeveloperMessage(ManifestolistConstant.GET_DETAIL_SUCCESS);

        Mockito.when(manifestolistService.getManifestoDetailByManifestoId(Mockito.any(ManifestoDetailRequest.class))).thenReturn(gzResponsePage);

        String url = "/manifestolist/detail/";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?manifestoId=1&loginId=1")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.GET_DETAIL))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.GET_DETAIL_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void saveShipping() throws Exception {
        String url = "/manifestolist/saveShipping";
        SaveShippingResponse gzResponse = new SaveShippingResponse(ManifestolistConstant.SAVE_MANIFESTO, HttpStatus.OK,
                HttpStatus.OK.toString(), ManifestolistConstant.SAVE_MANIFESTO_SUCCESS);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);
        AddManifestoRequest addManifestoRequest = new AddManifestoRequest();
        addManifestoRequest.setLoginId(1);
        addManifestoRequest.setManifestoIds(manifestoIds);

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(addManifestoRequest);

        Mockito.when(manifestolistService.saveShipping(Mockito.any(AddManifestoRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.SAVE_MANIFESTO))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.SAVE_MANIFESTO_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void getShipping() throws Exception {
        GZManifestoListResponsePage gzResponsePage = new GZManifestoListResponsePage();
        gzResponsePage.setCode(HttpStatus.OK);
        gzResponsePage.setMessage(HttpStatus.OK.toString());
        gzResponsePage.setTitle(ManifestolistConstant.SHIPPING_LIST);
        gzResponsePage.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        Mockito.when(manifestolistService.getManifestoList(Mockito.any(ManifestolistRequest.class))).thenReturn(gzResponsePage);

        String url = "/manifestolist";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?loginId=1&tab=shipping")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.SHIPPING_LIST))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.GET_LIST_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void getManifestoListChecker() throws Exception {
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        gzResponse.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Mockito.when(manifestolistService.getManifestoListChecker(Mockito.any(ManifestolistCheckerRequest.class)))
                .thenReturn(gzResponse);

        String url = "/manifestolist/checker";
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?loginId=1")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.SHIPPING_STATUS_CHECKER))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));

    }

    @Test
    public void getDriverShipping() throws Exception {
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.SHIPPING_LIST);
        gzResponse.setDeveloperMessage(ManifestolistConstant.GET_LIST_SUCCESS);

        Mockito.when(manifestolistService.getDriverManifestoListChecker(Mockito.any(DriverManifestolistCheckerRequest.class)))
            .thenReturn(gzResponse);

        String url = "/manifestolist/driverShipping";
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?driverId=1&loginId=1&tab=wait")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.SHIPPING_LIST))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.GET_LIST_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void getSearchManifestoList() throws Exception {
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ManifestolistConstant.SHIPPING_STATUS_CHECKER);
        gzResponse.setDeveloperMessage(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS);

        Mockito.when(manifestolistService.getManifestoListChecker(Mockito.any(ManifestolistSearchRequest.class)))
                .thenReturn(gzResponse);

        String url = "/manifestolist/search";
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?loginId=1")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestolistConstant.SHIPPING_STATUS_CHECKER))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestolistConstant.SHIPPING_STATUS_CHECKER_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));

    }
}

