package com.gz.pod.controllers.podNumberStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.datafromsoms.requests.DataFromSOMSRequest;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusController;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusService;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberDetailRequest;
import com.gz.pod.categories.podNumberStatus.requests.PODNumberStatusRequest;
import com.gz.pod.categories.podNumberStatus.requests.ValidateUpdateShippingRequest;
import com.gz.pod.categories.podNumberStatus.responses.PODNumberStatusDetailResponse;
import com.gz.pod.response.GZResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PODNumberStatusControllerTest {
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private PODNumberStatusController podNumberStatusController;

    @Mock
    private PODNumberStatusService podNumberStatusService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(podNumberStatusController).build();
    }

    @Test
    public void shouldPodNumberStatusReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/podNumberStatus";
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS);
        gzResponse.setDeveloperMessage(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS);

        List<Integer> manifestoIds = new ArrayList<>();
        manifestoIds.add(1);

        PODNumberStatusRequest podNumberStatusCheckerRequest = new PODNumberStatusRequest();
        podNumberStatusCheckerRequest.setFieldType("driverId");
        podNumberStatusCheckerRequest.setUserTypeId(1);
        podNumberStatusCheckerRequest.setStatusId(201);
        podNumberStatusCheckerRequest.setRefId("1");
        podNumberStatusCheckerRequest.setUpdateBy(1);
        podNumberStatusCheckerRequest.setManifestoIds(manifestoIds);
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(podNumberStatusCheckerRequest);

        Mockito.when(podNumberStatusService.updatePODNumberStatus(Mockito.any(PODNumberStatusRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInString);
        mvc.perform(builder)
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("OK"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void shouldGetPODNumberDetailReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/podNumberStatus/detail";
        GZResponse response = new GZResponse();
        List<PODNumberStatusDetailResponse> podNumberStatusDetailResponseList = new ArrayList<>();
        PODNumberStatusDetailResponse podNumberStatusDetailResponse = new PODNumberStatusDetailResponse();
        podNumberStatusDetailResponse.setPodNumber("092018-0005");
        podNumberStatusDetailResponse.setShipToAddress("shipToAddress");
        podNumberStatusDetailResponse.setRouteName("RO01");
        podNumberStatusDetailResponse.setCustomerName("customer");
        podNumberStatusDetailResponseList.add(podNumberStatusDetailResponse);
        response.setData(podNumberStatusDetailResponseList);


        Mockito.when(podNumberStatusService.getPODNumberDetail(Mockito.any(PODNumberDetailRequest.class))).thenReturn(response);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?podNumber=1&loginId=1&tab=shipping")
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].podNumber").value("092018-0005"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].shipToAddress").value("shipToAddress"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].routeName").value("RO01"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].customerName").value("customer"));

    }

    @Test
    public void shouldChangeDeliveryDateForDelayReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/podNumberStatus/changeDeliveryDateForDelay";
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        dataFromSOMSRequest.setDeliveryDate("2018-11-11");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(dataFromSOMSRequest);

        Mockito.when(podNumberStatusService.changeDeliveryDateForDelay(Mockito.any(DataFromSOMSRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInString);
        mvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("OK"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void shouldStampDelayReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/podNumberStatus/stampDelay";
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        DataFromSOMSRequest dataFromSOMSRequest = new DataFromSOMSRequest();
        dataFromSOMSRequest.setDeliveryDate("2018-11-11");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(dataFromSOMSRequest);

        Mockito.when(podNumberStatusService.stampDelay(Mockito.any(DataFromSOMSRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInString);
        mvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("OK"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void checkChangeDeliveryDateForDelayReturnResponseEntityOKWhenCallSuccess() throws Exception {

        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        Mockito.when(podNumberStatusService.changeDeliveryDateForDelay(Mockito.any(DataFromSOMSRequest.class))).thenReturn(gzResponse);

        podNumberStatusController.changeDeliveryDateForDelay();

    }

    @Test
    public void checkStampDelayReturnResponseEntityOKWhenCallSuccess() throws Exception {

        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        Mockito.when(podNumberStatusService.stampDelay(Mockito.any(DataFromSOMSRequest.class))).thenReturn(gzResponse);

        podNumberStatusController.stampDelay();

    }

    @Test
    public void shouldReturnResponseWhenCallFailed() throws Exception {
        String url = "/api/v1/podNumberStatus";
        PODNumberStatusRequest podNumberStatusRequest = new PODNumberStatusRequest();
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(podNumberStatusRequest);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInString);
        mvc.perform(builder).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void shouldValidateUpdateShippingReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/podNumberStatus/validateUpdate";
        GZResponse gzResponse = new GZResponse(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED, HttpStatus.OK,
                HttpStatus.OK.toString(), PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS);

        ValidateUpdateShippingRequest validateUpdateShippingRequest = new ValidateUpdateShippingRequest();

        List<Integer> manifestoIdList = new ArrayList<>();
        manifestoIdList.add(21031);
        manifestoIdList.add(21032);

        validateUpdateShippingRequest.setManifestoIds(manifestoIdList);
        validateUpdateShippingRequest.setLoginId(82);
        validateUpdateShippingRequest.setStatusId(2113);
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(validateUpdateShippingRequest);

        Mockito.when(podNumberStatusService.validateOwner(Mockito.any(ValidateUpdateShippingRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInString);
        mvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(PODNumberStatusConstant.UPDATE_PODNUMBERSTATUS_BY_SHEDULED_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("OK"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }
}
