package com.gz.pod.controllers.manifestoFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.manifestoFile.ManifestoFileConstant;
import com.gz.pod.categories.manifestoFile.ManifestoFileController;
import com.gz.pod.categories.manifestoFile.ManifestoFileService;
import com.gz.pod.categories.manifestoFile.requests.GetUploadFileRequest;
import com.gz.pod.categories.manifestoFile.requests.UploadFileRequest;
import com.gz.pod.response.GZResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

@RunWith(MockitoJUnitRunner.class)
public class ManifestoFileControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private ManifestoFileController manifestoFileController;

    @Mock
    private ManifestoFileService manifestoFileService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(manifestoFileController).build();
    }

    @Test
    public void getManifestoFileReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/manifestoFile";
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(ManifestoFileConstant.GET_MANIFESTOFILE);
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setDeveloperMessage(ManifestoFileConstant.GET_MANIFESTOFILE_SUCCESS);

        GetUploadFileRequest getUploadFileRequest = new GetUploadFileRequest();
        getUploadFileRequest.setManifestoFileId(1);

        Mockito.when(manifestoFileService.getFileByJob(Mockito.any(GetUploadFileRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?manifestoFileId=1")
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestoFileConstant.GET_MANIFESTOFILE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestoFileConstant.GET_MANIFESTOFILE_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void postFileByJobReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/manifestoFile/uploadFile";
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(ManifestoFileConstant.UPDATE_MANIFESTOFILE);
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setDeveloperMessage(ManifestoFileConstant.UPDATE_MANIFESTOFILE_SUCCESS);

        Mockito.when(manifestoFileService.postFileByJob(Mockito.any(UploadFileRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.fileUpload(url)
            .file(new MockMultipartFile("file", "test.png", "image/png", "test.png".getBytes()))
            .param("updateBy", "1")
            .param("manifestoId", "1")
            .param("orientation", "1")
            .contentType(MediaType.MULTIPART_FORM_DATA);

        mvc.perform(builder)
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestoFileConstant.UPDATE_MANIFESTOFILE))
            .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestoFileConstant.UPDATE_MANIFESTOFILE_SUCCESS))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }


}
