package com.gz.pod.controllers.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.order.OrderConstant;
import com.gz.pod.categories.order.OrderController;
import com.gz.pod.categories.order.requests.OrderRequest;
import com.gz.pod.categories.order.responses.OrderResponse;
import com.gz.pod.controllers.PathRequestConstant;
import com.gz.pod.response.GZResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = OrderController.class, secure = false)
public class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderController orderController;

    @Test
    public void shouldReturnResponseEntityOKWhenCallSuccess() throws Exception {
        //Arrange
        ObjectMapper mapper = new ObjectMapper();
        OrderRequest orderRequest = OrderConstant.getOrderRequest();
        String jsonInString = mapper.writeValueAsString(orderRequest);
        OrderResponse orderResponse = OrderConstant.getOrderResponse();
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(OrderConstant.ADD_ORDER);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setDeveloperMessage(OrderConstant.ADD_ORDER_SUCCESS);
        gzResponse.setData(orderResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .post(PathRequestConstant.ORDER_URL)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonInString);

        given(orderController.addOrder(orderRequest)).willReturn(ResponseEntity.ok(gzResponse));

        mvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.title", is(OrderConstant.ADD_ORDER)))
                .andExpect(jsonPath("$.developerMessage", is(OrderConstant.ADD_ORDER_SUCCESS)))
                .andExpect(jsonPath("$.data.customerNumber", is(OrderConstant.CUSTOMER_NUMBER)));
    }

}
