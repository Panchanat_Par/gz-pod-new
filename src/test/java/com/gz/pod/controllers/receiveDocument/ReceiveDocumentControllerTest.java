package com.gz.pod.controllers.receiveDocument;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.manifestolist.ManifestolistConstant;
import com.gz.pod.categories.manifestolist.requests.UpdateDeleteJobRequest;
import com.gz.pod.categories.manifestolist.requests.ValidateDeleteJobRequest;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentConstant;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentController;
import com.gz.pod.categories.receiveDocument.ReceiveDocumentService;
import com.gz.pod.categories.receiveDocument.requests.UpdateReceiveDocument;
import com.gz.pod.categories.receiveDocument.requests.ValidateReceiveDocument;
import com.gz.pod.response.GZResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class ReceiveDocumentControllerTest {
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private ReceiveDocumentController receiveDocumentController;

    @Mock
    private ReceiveDocumentService receiveDocumentService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(receiveDocumentController).build();
    }

    @Test
    public void validateReceiveDocument() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        gzResponse.setDeveloperMessage(ReceiveDocumentConstant.CAN_RECEIVE);

        Mockito.when(receiveDocumentService.validateReceiveDocumentMain(Mockito.any(ValidateReceiveDocument.class))).thenReturn(gzResponse);

        String url = "/receiveDocument/validateReceiveDocument/";

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url + "?refId=1916&refModuleType=manifestoId&loginId=60&statusId=421")
                .contentType(MediaType.APPLICATION_JSON_UTF8);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ReceiveDocumentConstant.RECEIVE_DOCUMENT))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ReceiveDocumentConstant.CAN_RECEIVE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }

    @Test
    public void updateReceiveDocument() throws Exception{
        GZResponse gzResponse = new GZResponse();
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setTitle(ReceiveDocumentConstant.RECEIVE_DOCUMENT);
        gzResponse.setDeveloperMessage(ReceiveDocumentConstant.RECEIVE_SUCCESS);

        String url = "/receiveDocument/updateReceiveDocument";

        UpdateReceiveDocument request = new UpdateReceiveDocument();
        request.setRefId("GZQ-1-1");
        request.setRefModuleType("qrCode");
        request.setLoginId(60);
        request.setStatusId(421);
        request.setRemark("test remark");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(request);

        Mockito.when(receiveDocumentService.updateReceiveDocumentMain(Mockito.any(UpdateReceiveDocument.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ReceiveDocumentConstant.RECEIVE_DOCUMENT))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ReceiveDocumentConstant.RECEIVE_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }
}
