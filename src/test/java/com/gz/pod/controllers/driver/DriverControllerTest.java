package com.gz.pod.controllers.driver;

import com.gz.pod.categories.driver.DriverController;
import com.gz.pod.categories.driver.DriverService;
import com.gz.pod.categories.driver.requests.DriverListRequest;
import com.gz.pod.categories.driver.responses.DriverListResponse;
import com.gz.pod.categories.podNumberStatus.PODNumberStatusConstant;
import com.gz.pod.response.GZResponsePage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DriverControllerTest {
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private DriverController driverController;

    @Mock
    private DriverService driverService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(driverController).build();
    }

    @Test
    public void shouldgetDriverlistReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/driver/list";
        GZResponsePage gzResponsePage = new GZResponsePage();
        List<DriverListResponse> driverListResponse = new ArrayList<>();
        DriverListResponse driverListResponse1 = new DriverListResponse();
        driverListResponse1.setName("test_driverName");
        driverListResponse1.setStatus(PODNumberStatusConstant.CHECKER_WAIT_STRING);
        driverListResponse1.setLoginId(1);
        driverListResponse.add(driverListResponse1);
        gzResponsePage.setTotalItems(driverListResponse.size());
        gzResponsePage.setData(driverListResponse);

        Mockito.when(driverService.getDriverList(Mockito.any(DriverListRequest.class))).thenReturn(gzResponsePage);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .get(url  + "?deliveryDate=2018-09-28&loginId=1")
                .contentType(MediaType.APPLICATION_JSON);
        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk());
//                .andExpect(MockMvcResultMatchers.jsonPath("$.totalItems").value(1))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].loginId").value(1))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].name").value("test_driverName"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].podNumberStatus").value(PODNumberStatusConstant.CHECKER_WAIT_STRING));
    }

//    @Test
//    public void shouldgetDriverlistByCheckerReturnResponseEntityOKWhenCallSuccess() throws Exception {
//        String url = "/driver/list/checker";
//        GZResponse gzResponse = new GZResponse();
//        List<DriverListByCheckerResponse> driverListByCheckerResponses = new ArrayList<>();
//        DriverListByCheckerResponse driverListResponse1 = new DriverListByCheckerResponse();
//        driverListResponse1.setDriverName("bay");
//        driverListResponse1.setLoginId(1);
//        driverListByCheckerResponses.add(driverListResponse1);
//        gzResponse.setData(driverListByCheckerResponses);
//
////        Mockito.when(driverService.getDriverlistByChecker(Mockito.any(DriverListByCheckerRequest.class))).thenReturn(gzResponse);
//
//        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
//                .get(url  + "?loginId=1")
//                .contentType(MediaType.APPLICATION_JSON);
//        mvc.perform(builder)
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].loginId").value(1))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].driverName").value("bay"));
//    }
}
