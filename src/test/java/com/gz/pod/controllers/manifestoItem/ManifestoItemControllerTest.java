package com.gz.pod.controllers.manifestoItem;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gz.pod.categories.manifestoItem.ManifestoItemConstant;
import com.gz.pod.categories.manifestoItem.ManifestoItemController;
import com.gz.pod.categories.manifestoItem.ManifestoItemService;
import com.gz.pod.categories.manifestoItem.requests.ManifestoItemPutQtyRequest;
import com.gz.pod.categories.manifestoItem.requests.QuantityRequest;
import com.gz.pod.response.GZResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ManifestoItemControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private ManifestoItemController manifestoItemController;

    @Mock
    private ManifestoItemService manifestoItemService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(manifestoItemController).build();
    }

    @Test
    public void shouldReturnResponseEntityOKWhenCallSuccess() throws Exception {
        String url = "/manifestoItem/updateQuantity";
        GZResponse gzResponse = new GZResponse();
        gzResponse.setTitle(ManifestoItemConstant.UPDATE_MANIFESTOITEM);
        gzResponse.setCode(HttpStatus.OK);
        gzResponse.setMessage(HttpStatus.OK.toString());
        gzResponse.setDeveloperMessage(ManifestoItemConstant.UPDATE_MANIFESTOITEM_SUCCESS);

        ManifestoItemPutQtyRequest manifestoItemPutQtyRequest = new ManifestoItemPutQtyRequest();
        List<QuantityRequest> quantityRequests = new ArrayList<>();
        QuantityRequest quantityRequest = new QuantityRequest();
        quantityRequest.setQuantity(1);
        quantityRequest.setRefId("1");
        quantityRequest.setRemark("remark");
        quantityRequests.add(quantityRequest);
        manifestoItemPutQtyRequest.setQuantities(quantityRequests);
        manifestoItemPutQtyRequest.setUpdateBy(1);
        manifestoItemPutQtyRequest.setFieldType("item");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(manifestoItemPutQtyRequest);

        Mockito.when(manifestoItemService.updateQuantityItemByJob(Mockito.any(ManifestoItemPutQtyRequest.class))).thenReturn(gzResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put(url)
                .content(jsonInString)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(ManifestoItemConstant.UPDATE_MANIFESTOITEM))
                .andExpect(MockMvcResultMatchers.jsonPath("$.developerMessage").value(ManifestoItemConstant.UPDATE_MANIFESTOITEM_SUCCESS))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(HttpStatus.OK.toString()));
    }
}
