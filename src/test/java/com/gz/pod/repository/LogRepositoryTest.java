//package com.gz.pod.repository;
//
//
//import com.gz.pod.categories.logs.LogRepository;
//import com.gz.pod.entities.SOMSManifestoTracking;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//public class LogRepositoryTest {
//
//    @Autowired
//    private TestEntityManager entityManager;
//
//    @Autowired
//    private LogRepository logRepository;
//
// //   @Test
// //   public void checkLog() throws Exception {
//        //Arrange
////        SOMSManifestoTracking data = new SOMSManifestoTracking();
////        data.setDocType("Log");
////        data.setDocRef("210063682");
////        data.setStatusId(9);
////        data.setCreateBy(9);
////        data.setUpdateDate("2018-08-29 10:31:54.647");
////        data.setSaleOrderItem(0);
////        entityManager.persist(data);
////
////        //Act
////        SOMSManifestoTracking log = logRepository.checkLog(
////                data.getDocType(), data.getDocRef(), data.getCreateBy(), data.getStatusId());
////
////        //Assert
////        Assert.assertEquals(log.getDocRef(), data.getDocRef());
////        Assert.assertEquals(log.getDocType(), data.getDocType());
////        Assert.assertEquals(log.getStatusId(), data.getStatusId());
////        Assert.assertEquals(log.getCreateBy(), data.getCreateBy());
////        Assert.assertEquals(log.getCreateDate(), data.getCreateDate());
//    }
//
////    @Test
////    public void updateLog() throws Exception {
////        //Arrange
////        SOMSManifestoTracking data = new SOMSManifestoTracking();
////        data.setDocType("Log");
////        data.setDocRef("210063682");
////        data.setStatusId(9);
////        data.setCreateBy(9);
////        data.setUpdateDate("2018-08-29 10:31:54.647");
////        data.setSaleOrderItem(0);
////        entityManager.persist(data);
////
////        //Act
////        logRepository.updateLog(data.getCreateBy(), data.getUpdateDate(), 1);
////
////        //Assert
//////        Assert.assertTrue();
////    }
//
////    @Test
////    public void checkLogPerItem() throws Exception {
////        //Arrange
////        SOMSManifestoTracking data = new SOMSManifestoTracking();
////        data.setDocType("Log");
////        data.setDocRef("210063682");
////        data.setStatusId(9);
////        data.setCreateBy(3);
////        data.setUpdateDate("2018-08-29 10:31:54.647");
////        data.setSaleOrderItem(1);
////        entityManager.persist(data);
////
////        //Action
////        SOMSManifestoTracking log = logRepository.checkLogPerItem(
////                data.getDocType(), data.getDocRef(), data.getCreateBy(), data.getStatusId(), data.getSaleOrderItem());
////
////        //Assert
////        Assert.assertEquals(log.getDocRef(), data.getDocRef());
////        Assert.assertEquals(log.getDocType(), data.getDocType());
////        Assert.assertEquals(log.getStatusId(), data.getStatusId());
////        Assert.assertEquals(log.getCreateBy(), data.getCreateBy());
////        Assert.assertEquals(log.getCreateDate(), data.getCreateDate());
////        Assert.assertEquals(log.getSaleOrderItem(), data.getSaleOrderItem());
////    }
//}
